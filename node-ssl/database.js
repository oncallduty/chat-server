/* global exports,require */

/**
 * User: denistrofimov
 * Date: 04.10.12
 * Time: 11:37
 */


exports.init = function( args ) {
    this.selectUserBySKey = args.selectUserBySKey;
	this.selectDepositionByID = args.selectDepositionByID;

	function getClient() {
		var client = require('mysql').createClient( {
			user:args.user,
			password:args.password,
			host:args.host,
			port:args.port,
			database:args.database
		} );
		return client;
	}

    this.getUserBySessionKey = function( sKey, callback ) {
//		console.log( 'mysql.getUserBySessionKey: ' + sKey );
		var client = getClient();
		client.escape( sKey );
		client.query( this.selectUserBySKey, [sKey], function( err, results ) {
//			console.log( results );
			callback( err, results );
			client.end();
		} );
	};

	this.getDepositionByID = function( depoID, callback ) {
//		console.log( 'mysql.getDepositionByID: ' + depoID );
//		console.log( this.selectDepositionByID );
		var client = getClient();
		client.escape( depoID );
		client.query( this.selectDepositionByID, [depoID], function( err, results ) {
//			console.log( results );
			callback( err, results );
			client.end();
		} );
	};

	return this;
};
