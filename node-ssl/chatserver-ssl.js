/* global GLOBAL,require,process,module */

var fs = require('fs');
var dateFormat = require('dateformat');
var url = require('url');
var os = require('os');
var https = require('https');
var net = require('net');
var localIP = os.networkInterfaces().eth0[0].address;

var config = (fs.existsSync( "./local.js" )) ? require( "./local.js" ) : require( "./config.js" ) ;
module.exports = config;
GLOBAL.config = config;
GLOBAL.https = https;
GLOBAL.net = net;

var listenerHostPort = localIP + ':' + config.https.port;
ConsoleLog( 'listening on:', listenerHostPort );

if( config.handleErrors ) {
	process.on( 'uncaughtException', function( err ) {
		var now = new Date();
		console.error( '========================\n' + now.toISOString() + ' Caught exception: ', err, '\n' );
		console.error( err.stack, '\n========================\n' );
		lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'uncaughtException', args:{nodeId:nodeId, err:err.toString()}}} ) );
		process.exit( 1 );
	} );
}

process.on( 'SIGTERM', function() {
	var now = new Date();
	console.error( '========================\n' + now.toISOString() + ' SIGTERM\n========================' );
	logToFile( null, 'Chat Server: Exiting.' );
	process.exit( 0 );
} );

process.on( 'exit', function( code ) {
	var now = new Date();
	console.error( '========================\n' + now.toISOString() + ' Exit: ', code, '\n========================\n' );
	logToFile( null, 'Chat Server: Exiting.' );
} );

var RedisStore = require( 'socket.io/lib/stores/redis' );
var redis = require( 'socket.io/node_modules/redis' );

//redis client
var rClient = redis.createClient( config.redis.port, config.redis.host, config.redis.opts );
rClient.on( 'error', function( err ) { ConsoleLog( 'Redis Client; Error', err ); ConsoleLog( err.stack ); } );
rClient.on( 'connect', function() { ConsoleLog( 'Redis Client; Connect' ); } );
rClient.on( 'reconnect', function() { ConsoleLog( 'Redis Client; Reconnect' ); } );

//redis store publish channel
var rPub = redis.createClient( config.redis.port, config.redis.host, config.redis.opts );
rPub.on( 'error', function( err ) { ConsoleLog( 'Redis Pub; Error', err ); ConsoleLog( err.stack ); } );
rPub.on( 'connect', function() { ConsoleLog( 'Redis Pub; Connect' ); } );
rPub.on( 'reconnect', function() { ConsoleLog( 'Redis Pub; Reconnect' ); } );

//redis store subscribe channel
var rSub = redis.createClient( config.redis.port, config.redis.host, config.redis.opts );
rSub.on( 'error', function( err ) { ConsoleLog( 'Redis Sub; Error', err ); ConsoleLog( err.stack ); } );
rSub.on( 'connect', function() { ConsoleLog( 'Redis Sub; Connect' ); } );
rSub.on( 'reconnect', function() { ConsoleLog( 'Redis Sub; Reconnect' ); } );

var redisStore = new RedisStore( {redisPub:rPub, redisSub:rSub, redisClient:rClient} );
var nodeId = redisStore.nodeId;
GLOBAL.nodeId = nodeId;

//redis publish channel
var lPub = redis.createClient( config.redis.port, config.redis.host, config.redis.opts );
lPub.on( 'error', function( err ) { ConsoleLog( 'Local Pub Error', err ); ConsoleLog( err.stack ); } );
lPub.on( 'connect', function() { ConsoleLog( 'Local Pub; Connect' ); } );
lPub.on( 'reconnect', function() { ConsoleLog( 'Local Pub; Reconnect' ); } );

//redis subscribe channel
var lSub = redis.createClient( config.redis.port, config.redis.host, config.redis.opts );
lSub.on( 'error', function( err ) { ConsoleLog( 'Local Sub Error', err ); ConsoleLog( err.stack ); } );
lSub.on( 'connect', function() { ConsoleLog( 'Local Sub; Connect' ); loadRedisData(); } );
lSub.on( 'reconnect', function() { ConsoleLog( 'Local Sub; Reconnect' ); loadRedisData(); } );

GLOBAL.rClient = rClient;
GLOBAL.lPub = lPub;

var Deposition = require( './entity/Deposition' ).Deposition;
var User = require( './entity/User' ).User;
var Room = require( './entity/Room' ).Room;
var Message = require( './entity/Message' ).Message;
var Presentation = require( './entity/Presentation' ).Presentation;
var Transcript = require( './entity/Transcript' ).Transcript;

eval( require( 'fs' ).readFileSync( './utils.js', 'utf8' ) );

// Entities
var depositions = {};
GLOBAL.depositions = depositions;

var users = {};
GLOBAL.users = users;

var rooms = {};
GLOBAL.rooms = rooms;

var messages = {};
GLOBAL.messages = messages;

var presentations = {};
GLOBAL.presentations = presentations;

var transcripts = {};
GLOBAL.transcripts = transcripts;
// --

var transcriptIntervals = {};

var mysql = require('./database.js').init( {
	user: config.database.user,
	password: config.database.password,
	database: config.database.name,
	host: config.database.host,
	port:  config.database.port,
	selectUserBySKey: config.database.selectUserBySKey,
	selectDepositionByID: config.database.selectDepositionByID
} );

var httpsOptions = {
	key: fs.readFileSync( '../SSL/com.lexcity.key' ),
	cert: fs.readFileSync( '../SSL/com.lexcity.crt' ),
	ca: fs.readFileSync( '../SSL/gd_bundle-g2-g1.crt' ),
	requestCert: false,
	rejectUnauthorized: false,
	honorCipherOrder: true,
	ciphers: 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:AES256-GCM-SHA384:AES256-SHA256:AES256-SHA'
};

var httpsServer = https.createServer( httpsOptions, function( req, res ) {
	var urlInfo = url.parse( req.url, true );
	switch( urlInfo.pathname ) {
		case '/ping':
			sendOk( res );
			break;
	}
	var remoteAddress = req.headers['x-forwarded-for'] || req.headers['x-real-ip'] || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress;
	if( !verifyIP( remoteAddress ) ) {
		ConsoleLog( 'Remote Address:', remoteAddress );
		res.writeHead( 501, "Authorization failed", {"Content-Type":"application/json", "Strict-Transport-Security": "max-age=15768000"} );
		res.end( JSON.stringify( {success:false, error:"Authorization failed", code:501} ) );
		return;
	}
	if( req.method.toLowerCase() === 'post' ) {
		switch( req.url ) {
			case '/':
				notImplemented( req, res );
				break;
			case '/sharefiles':
				filesShared( req, res );
				break;
			case '/introduced_exhibit':
				sharedObject( req, res, 'introduced_exhibit' );
				break;
			case '/shared_file':
				sharedObject( req, res, 'shared_file' );
				break;
			case '/shared_folder':
				sharedObject( req, res, 'shared_folder' );
				break;
			case '/deposition_start':
				depositionStarted( req, res );
				break;
			case '/deposition_end':
				depositionFinished( req, res );
				break;
			case '/reset_deposition':
				resetDeposition( req, res );
				break;
			case '/set_demo_deposition_owner':
				setDemoDepositionOwner( req, res );
				break;
			case '/abortshare':
				sharingAborted( req, res );
				break;
			case '/select_complete':
				selectComplete( req, res );
				break;
			case '/deposition_link':
				depositionLink( req, res );
				break;
			case '/notify_attendee_limit':
				notifyAttendeeLimit( req, res );
				break;
			case '/attendee_limit_extended':
				attendeeLimitExtended( req, res );
				break;
			case '/attendee_limit_extended':
				attendeeLimitExtended( req, res );
				break;
			case '/attendee_kick':
				attendeeKick( req, res );
				break;
			case '/deposition_setspeaker':
				depositionSetSpeaker( req, res );
				break;
			case '/deposition_uploaded_files':
				depositionUploadedFiles( req, res );
				break;
			case '/deposition_deleted_file':
				depositionDeletedFile( req, res );
				break;
			case '/deposition_deleted_folder':
				depositionDeletedFolder( req, res );
				break;
			case '/witness_uploaded':
				witnessUploadFiles( req, res );
				break;
			case '/file_modified':
				fileModified( req, res );
				break;
			case '/cases_updated':
				casesUpdated( req, res );
				break;
			case '/getPresentationInfo':
				getPresentationInfo( req, res );
				break;
			case '/presentation.publish':
				presentationPublish( req, res );
				break;
			case '/authorize_witness':
				authorizeWitness( req, res );
				break;
			case '/reject_witness':
				rejectWitness( req, res );
				break;
			case '/didChangeCustomSort':
				didChangeCustomSort( req, res );
				break;
			default:
				notFound( req, res );
				break;
		}
	} else {
		switch( urlInfo.pathname ) {
			case '/status':
				status( req, res );
				break;
			case '/requestConsoleLog':
				requestConsoleLog( req, res );
				break;
			case '/enableConsoleLog':
				enableConsoleLog( req, res );
				break;
			case '/clearConsoleLog':
				clearConsoleLog( req, res );
				break;
			default:
				notImplemented( req, res );
				break;
		}
	}
} ).listen( config.https.port );
var httpsSocketIO = require( 'socket.io' ).listen( httpsServer );

httpsSocketIO.configure( function() {
	httpsSocketIO.set( 'heartbeat timeout', 21 );
	httpsSocketIO.set( 'heartbeat interval', 10 );
	httpsSocketIO.set( 'authorization', function( handshakeData, callback ) {
		var remoteAddress = handshakeData.headers['x-forwarded-for'] || handshakeData.headers['x-real-ip'] || handshakeData.address.address;
		ConsoleLog( '====== authorization ======', 'remote IP:', remoteAddress, 'userID:', handshakeData.query['id'] );

		if( handshakeData.query ) {
			var sKey = handshakeData.query['sKey'];
			if( typeof sKey === 'string' && sKey.length > 0 ) {
				//var startConnectToDB = Date.now();
				mysql.getUserBySessionKey( sKey, function( error, results ) {
//					ConsoleLog( '--- error: ' + JSON.stringify( error ) );
//					console.log( '--- results:', results );

					if( config.socket.authorization ) {
						if( error || results.length === 0 ) {
							ConsoleLog( error );
							if( typeof callback === 'function' ) {
								callback( 'Database error: ' + error, false );
							}
						} else {
							var guest = false;
							var userID = parseInt( results[0].userID );
							if( results[0].guestID ) {
								guest = true;
								userID = parseInt( results[0].guestID );
							}

							var user = new User( userID );
							user.name = results[0].name;
							user.email = results[0].email;
							user.clientid = results[0].clientID;
							user.clientTypeID = results[0].clientTypeID;
							user.active = true;
							user.guest = guest;
							user.userType = (guest) ? results[0].role : 'M';
							user.blockExhibits = false;
							user.connectionCount = 0;
							user.sKey = handshakeData.query['sKey'].substr( 0, 6 ) + '~' + handshakeData.query['sKey'].substr( -6, 6 );
							if( results[0].role === 'TW' ) {
								user.userType = 'W';
								user.isTempWitness = true;
							}
							handshakeData.user = user;
							if( typeof callback === 'function' ) {
								callback( null, true );
							}
						}

						if( false && typeof config.debug !== 'undefined' && config.debug ) {
							for( var item in results ) {
								if( results.hasOwnProperty( item ) && typeof results[item] !== 'function' ) {
									ConsoleLog( '\n=============================\nDEBUG\n=============================' );
									for( var key in results[item] ) {
										if( typeof results[item][key] !== 'function' ) {
											ConsoleLog( key, '=', results[item][key] );
										}
									}
									ConsoleLog( '=============================\n' );
								}
							}
						}
					} else {
						//no authorization
						if( typeof callback === 'function' ) {
							callback( null, false );
						}
					}
				} );
			} else {
				//no sKey
				ConsoleLog( '--- authorization; not authorized' );
				ConsoleLog( 'query:', handshakeData.query );
				if( typeof callback === 'function' ) {
					callback( null, false );
				}
				return;
            }
        }
    });

    httpsSocketIO.set( 'log level', 2 );
	httpsSocketIO.set( 'store', redisStore );
} );

GLOBAL.httpsSocketIO = httpsSocketIO;

httpsSocketIO.sockets.on( 'connection', function( socket ) {
	ConsoleLog( 'socket.on(connection)' );
	if( typeof socket.handshake.user === 'undefined' ) {
		ConsoleLog( 'Missing handshake user:', socket.handshake );
		return;
	}
	ConsoleLog( 'connection for:', socket.handshake.user.id, socket.handshake.user.name );
    var user = socket.handshake.user;
	if( !(user instanceof User) ) {
		ConsoleLog( 'handshake user not instance of User, constructing user from:', user );
		var cUser = new User( user.id );
		cUser.assign( user );
		user = cUser;
	}
//	user.socket = socket;
	var oldUser = null;
	if( typeof users[user.id] !== 'undefined' ) {
		oldUser = JSON.parse( JSON.stringify( users[user.id] ) );	//clone
//		console.log( 'OLD USER:', oldUser, user );
	}

	user.uid = socket.store.id;
	user.active = true;
	user.blockExhibits = (oldUser && typeof oldUser.blockExhibits !== 'undefined' ) ? oldUser.blockExhibits : false;
	user.connectionCount = (typeof oldUser !== 'undefined' && oldUser !== null && oldUser.sKey === user.sKey) ? ++oldUser.connectionCount : 1;
	user.save();
	users[user.id] = user;
	var currentUserID = user.id;

	//ED-2207 -- end presentations if leader connects with new skey
	if( typeof oldUser !== 'undefined' && oldUser !== null && oldUser.sKey !== user.sKey ) {
//		console.log( 'ED-2207 -- Looking for presentations ...' );
		var presentationIDs = Object.keys( presentations );
		for( var i=0; i<presentationIDs.length; ++i ) {
			var pID = presentationIDs[i];
			var pDepo = depositions[pID];
			if( typeof pDepo !== 'undefined' && pDepo !== null ) {
				if( pDepo.speakerID !== user.id ) {
					continue;
				}
				var presentation = presentations[pID];
				if( typeof presentation !== 'undefined' && presentation !== null ) {
					ConsoleLog( 'endPresentation', pID, presentation.roomID );
					presentation.deleteAllCallouts( function() {
						presentation.deleteAllAnnotations( function() {
							rClient.del( presentation.rKey, function( err ) {
								if( err ) ConsoleLog( err );
							} );
							httpsSocketIO.sockets.in( presentation.roomID ).emit( 'endPresentation' );
							delete presentations[pID];
							lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Presentation.end', args:pID}} ) );
						} );
					} );
				}
			}
		}
	}

	if( oldUser && typeof oldUser.uid !== 'undefined' ) {
		oldUser.active = false;
		if ( oldUser.depositionID && oldUser.depositionID > 0 ) {
			ConsoleLog( 'old User uid:', oldUser.uid, oldUser.depositionID );
			ConsoleLog( 'new User uid:', user );
//			user.assign( {depositionID: oldUser.depositionID} );

//			var oldUserDepo = depositions[oldUser.depositionID];
//			if( oldUserDepo && oldUserDepo instanceof Deposition ) {
//				for( var roomID in oldUserDepo.rooms ) {
//					if( oldUserDepo.rooms.hasOwnProperty( roomID ) && typeof rooms[roomID] !== 'undefined' && rooms[roomID] instanceof Room ) {
//						var oldUserRoom = rooms[roomID];
//						ConsoleLog( '  Room', roomID );
//						if( typeof oldUserRoom.users[currentUserID] !== 'undefined' ) {
//							oldUserRoom.addUser( user );
//							oldUserRoom.save();
//						}
//					}
//				}
//			}

			if ( oldUser && oldUser.uid ) {
				ConsoleLog( 'emit kick event to user' );
				//oldUser.socket.emit( 'attendee_kick', {"depositionID":oldUser.deposition.id, "userID":user.id, "permanently":false, "anotherUserLoggedIn":true} );
				httpsSocketIO.sockets.socket( oldUser.uid ).emit( 'attendee_kick', {"depositionID":oldUser.depositionID, "userID":user.id, "permanently":false, "anotherUserLoggedIn":true} );
				setTimeout( function() { httpsSocketIO.sockets.socket( oldUser.uid ).disconnect(); }, 200 );
			}

			user.save();

		} else if ( oldUser.uid ) {
			ConsoleLog( 'Kicking user -- already logged in:', oldUser.id, '"' + oldUser.name + '"', oldUser.uid );
			//oldUser.socket.emit( 'attendee_kick', {"anotherUserLoggedIn":true} );
			httpsSocketIO.sockets.socket( oldUser.uid ).emit( 'attendee_kick', {"anotherUserLoggedIn":true} );
			httpsSocketIO.sockets.socket( oldUser.uid ).disconnect();
		}
	}

	var remoteAddress = socket.handshake.headers['x-forwarded-for'] || socket.handshake.headers['x-real-ip'] || socket.handshake.address.address;
 	ConsoleLog( 'socket connection from:', remoteAddress, 'user:', currentUserID, '"' + user.name + '"', user.sKey, user.connectionCount );

	resetUserIsActive();

    // depositions
	//

	/**
	 *
	 * @param {Object} args
	 * @param {function} callback
	 * @returns
	 */
	function selectDeposition( args, callback )
	{
		ConsoleLog( "socket.on(selectDeposition)", currentUserID, '"' + user.name + '"', args );

		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}

		if ( typeof args === 'undefined' || !args.hasOwnProperty( 'deposition' ) || !args.deposition ) {
			console.error( 'error: deposition not specified' );
			if( typeof callback === 'function' ) callback( {error:'Deposition not specified'} );
			return;
		}

		var depositionID = parseInt( args.deposition );
		var depoRoomID = depositionID + '.deposition';

		if( cUser.depositionID && cUser.depositionID !== depositionID ) {
			var prevDepo = depositions[depositionID];
			if( prevDepo && prevDepo instanceof Deposition ) {
				prevDepo.users[currentUserID] = false;	//inactive
				prevDepo.save();
			}
		}

		var deposition;

		var userSelectDeposition = function() {
			if( sessionIsDemo( deposition.class ) && currentUserID === deposition.ownerID ) {
				rClient.setex( 'demo:' + depositionID, config.redis.expire, 'i:' + currentUserID + ';', function( err ) {
					if( err ) ConsoleLog( err );
				} );
			}

			deposition.users[currentUserID] = true;	//active
			deposition.save();

			//create 'All Attendees' if not exists
			if( typeof rooms[depoRoomID] !== 'undefined' && rooms[depoRoomID] instanceof Room ) {
				ConsoleLog( 'joining room:', depoRoomID );
				rooms[depoRoomID].addUser( cUser );
			} else {
				ConsoleLog( 'creating room:', depoRoomID );
				createRoom( depositionID, depoRoomID, currentUserID, {title:'All Attendees', private:false, readOnly:true, isChatEnabled:false} );
				rooms[depoRoomID].addUser( cUser );
			}
			rooms[depoRoomID].save();
			httpsSocketIO.sockets.in( depoRoomID ).emit( 'user_did_connect_to_deposition', {deposition:{id:depositionID}, user:cUser.info()} );

			cUser.depositionID = depositionID;
			cUser.save();

			rejoinRooms( cUser );

			if( typeof callback === 'function' ) {
				var _depo = deposition.info();
				var _users = [];
				var _rooms = [];
				for( var userID in _depo.users ) {
					if( _depo.users.hasOwnProperty( userID ) && typeof users[userID] !== 'undefined' ) {
						var _user = users[userID].info();
						_user.active = !!_depo.users[userID];
						_users.push( _user );
					}
				}
				for( var roomID in _depo.rooms ) {
					if( _depo.rooms.hasOwnProperty( roomID ) && typeof rooms[roomID] !== 'undefined' ) {
						if( typeof rooms[roomID].users[currentUserID] === 'undefined' ) {
							continue;
						}
						var _room = rooms[roomID].info( cUser );
						_rooms.push( _room );
					}
				}
				_depo.users = _users;
				_depo.rooms = _rooms;
				_depo.conductorID = deposition.ownerID;
				callback( _depo );
			}
		};

		//create deposition if not exists
		if( typeof depositions[depositionID] === 'undefined' || !(depositions[depositionID] instanceof Deposition) || !depositions[depositionID].statusID || depositions[depositionID].statusID === "N" ) {
			ConsoleLog( '--- selectDepostion; new Deposition' );
			if( typeof depositions[depositionID] === 'undefined' || !(depositions[depositionID] instanceof Deposition) ) {
				deposition = new Deposition( depositionID, cUser );
			} else {
				deposition = depositions[depositionID];
			}
			depositions[depositionID] = deposition;
			mysql.getDepositionByID( depositionID, function( error, results ) {
				ConsoleLog( '--- error: ' + JSON.stringify( error ) );
				ConsoleLog( '--- results: ' + JSON.stringify( results ) );
				if( results && typeof results[0] === 'object' ) {
					deposition.title = results[0].depositionOf;
					deposition.ownerID = parseInt( results[0].ownerID );
					deposition.speakerID = parseInt( (results[0].speakerID) ? results[0].speakerID : results[0].ownerID );
					deposition.class = results[0].class;
					deposition.statusID = results[0].statusID;
					deposition.save();

					userSelectDeposition();
				}

			} );
		} else {
			deposition = depositions[depositionID];
			userSelectDeposition();
		}

	};
	socket.on( 'selectDeposition', selectDeposition );

	/**
	 *
	 * @param {Object} args
	 * @param {function} callback
	 * @returns
	 */
	function deselectDeposition( args, callback )
	{
		ConsoleLog( "socket.on(deselectDeposition)", args );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}

		// disconnect user
		ConsoleLog( '-- deselectDeposition -- uid: ' + cUser.uid );
		if( cUser.depositionID ) {
			var userDepo = depositions[cUser.depositionID];
			if( userDepo && userDepo instanceof Deposition ) {
				ConsoleLog( '-- deselectDeposition -- user_did_disconnect_from_deposition:', userDepo.depoRoomID );

				//clean up demo
				if( sessionIsDemo( userDepo.class ) && currentUserID === userDepo.ownerID ) {
					ConsoleLog( 'deselectDeposition -- removing demo owner key' );
					rClient.del( 'demo:' + cUser.depositionID, function( err ) {
						if( err ) ConsoleLog( err );
					} );
				}

				var dUser = cUser.info();
				dUser.active = false;
				userDepo.users[currentUserID] = false;
				userDepo.save();

				var userRooms = httpsSocketIO.sockets.manager.roomClients[cUser.uid];
				for( var key in userRooms ) {
					if( userRooms.hasOwnProperty( key ) ) {
						ConsoleLog( '-- deselectDeposition -- userRooms key:', key );
						var roomID = key.substring( 1, key.length );
						ConsoleLog( '-- deselectDeposition -- roomID:', roomID );
						if( roomID !== '' ) {
							if( typeof rooms[roomID] !== 'undefined' && rooms[roomID] instanceof Room ) {
								rooms[roomID].users[currentUserID] = false;
								rooms[roomID].save();
//								httpsSocketIO.sockets.socket( cUser.uid ).broadcast.to( roomID ).emit( 'user_did_disconnect', {room:{id:roomID}, user:dUser} );	//deprecated at some point, probably v1.5
							}
							httpsSocketIO.sockets.socket( cUser.uid ).leave( roomID );
						}
					}
				}

				socket.broadcast.to( userDepo.depoRoomID ).emit( 'user_did_disconnect_from_deposition', {deposition:{id:cUser.depositionID}, user:dUser} );
				httpsSocketIO.sockets.socket( cUser.uid ).leave( userDepo.depoRoomID );	//leave All Attendees
				cUser.depositionID = null;
				cUser.save();
				callback( {success:true} );
			} else {
				callback( {success:true} );	//lies
			}
		} else {
			errMsg = 'Deposition not selected';
			ConsoleLog( errMsg );
			callback( {success:false, error:errMsg} );
		}
	}
	socket.on( 'deselectDeposition', deselectDeposition );

    // rooms

	socket.on( 'join_to_room', function( args, callback ) {
		ConsoleLog( "socket.on(join_to_room)", currentUserID, '"' + user.name + '"', args );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var _room = null;
		var didCreateRoom = false;
		if( cUser.depositionID && typeof depositions[cUser.depositionID] !== 'undefined' ) {
			var userDepo = depositions[cUser.depositionID];
			var roomID = args.id;
			if( typeof rooms[roomID] !== 'undefined' ) {
				_room = rooms[roomID];
			} else {
				ConsoleLog( 'creating room:', roomID, 'creatorID:', currentUserID, args );
				didCreateRoom = true;
				createRoom( cUser.depositionID, roomID, currentUserID, {title:args.title, private:args.private, readOnly:false, isChatEnabled:true} );
				_room = rooms[roomID];
			}
		} else {
			errMsg = 'Deposition not selected';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
				return;
			}
		}

		if( !_room ) {
			errMsg = 'Room not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}

		if( typeof _room.users[currentUserID] !== 'undefined' && !didCreateRoom ) {
			httpsSocketIO.sockets.socket( cUser.uid ).join( _room.id );	// join room
			errMsg = 'Already in room';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}

		ConsoleLog( 'User join to room:', args.id, currentUserID, '"' + cUser.name + '"', cUser.uid );

		_room.addUser( cUser );
		_room.save();
		
		if( typeof callback === 'function' ) {
			var roomInfo = _room.info( cUser );
			callback( roomInfo );
		}
    } );

    socket.on( 'leave_room', function( args, callback ) {
		ConsoleLog( "socket.on(leave_room)" );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
        // leave room if user in deposition and room exist
        if( cUser.depositionID && typeof depositions[cUser.depositionID] !== 'undefined' && typeof rooms[args.id] !== 'undefined' ) {
            rooms[args.id].removeUser( cUser );
			rooms[args.id].save();
			if( typeof callback === 'function' ) {
				callback( {success:true} );
			}
        } else {
			var err = 'Room does not exist in selected deposition';
			ConsoleLog( err );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:err} );
			}
        }
    });

    // administration

    socket.on( 'rename_room', function( args, callback ) {
		ConsoleLog( "socket.on(rename_room)", args );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}

        //if user in deposition
        if(cUser.depositionID && typeof depositions[cUser.depositionID] !== 'undefined' ) {
			var userDepo = depositions[cUser.depositionID];
			var roomID = args.id;
            //if room exists
            if( typeof rooms[roomID] !== 'undefined' ) {
				var _room = rooms[roomID];
                //if all args is setted
                if( args.hasOwnProperty( 'title' ) && typeof args.title === 'string' ) {
                    //if user is creator of this room
                    if( currentUserID === _room.creatorID ) {
                        _room.title = args.title;
                        httpsSocketIO.sockets.in( roomID ).emit( 'room_renamed', {room:{id:roomID, title:_room.title, users:_room.info().users}} );
						if( typeof callback === 'function' ) {
							callback( {success:true} );
						}
						return;
                    } else {
						errMsg = 'User not creator';
						ConsoleLog( errMsg );
						if( typeof callback === 'function' ) {
							callback( {success:false, error:errMsg} );
						}
						return;
                    }
                } else {
                    errMsg = "Missing 'title' parameter";
					ConsoleLog( errMsg );
					if( typeof callback === 'function' ) {
						callback( {success:false, error:errMsg} );
					}
					return;
                }
            } else {
                errMsg = 'Room does not exist';
				ConsoleLog( errMsg );
				if( typeof callback === 'function' ) {
					callback( {success:false, error:errMsg} );
				}
				return;
            }
        } else {
            errMsg = 'Deposition not selected';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
        }
    } );

	function closeRoom( args, callback )
	{
		ConsoleLog( "socket.on(closeRoom)" );
		ConsoleLog( args );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( !cUser.depositionID ) {
			errMsg = 'Deposition not selected';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
				return;
			}
		}
		ConsoleLog( 'cUser.depositionID:', cUser.depositionID );
		//get room
		var _room = rooms[args.id];
		if( !_room || !(_room instanceof Room) ) {
			errMsg = 'Room does not exist';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
				return;
			}
		}
		var roomID = _room.id;
		ConsoleLog( 'Room:', roomID );
		if( cUser.id !== _room.creatorID || _room.readOnly ) {
			errMsg = 'User not room creator';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
				return;
			}
		}
		ConsoleLog( 'Closing room:', roomID );
		httpsSocketIO.sockets.in( roomID ).emit( 'room_closed', {room:{id:roomID}} );
		for( var userID in _room.users ) {
			if( _room.users.hasOwnProperty( userID ) && typeof users[userID] !== 'undefined' ) {
				ConsoleLog( 'UserID:', userID, 'leaving room:', roomID, '"' + _room.title + '"' );
				httpsSocketIO.sockets.socket( cUser.uid ).leave( roomID );
				_room.removeUser( users[userID] );
			}
		}
		ConsoleLog( 'Deleting room:', roomID );
		//delete cUser.deposition.rooms[args.id];
		rClient.del( _room.rKey, function( err ) {
			if( err ) ConsoleLog( err );
		} );
		delete rooms[roomID];
		delete depositions[cUser.depositionID].rooms[roomID];
		depositions[cUser.depositionID].save();
		lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Room.close', args:roomID}} ) );
	};
	socket.on( 'close_room', closeRoom );

	function inviteUsersToRoom( args, callback ) {
		ConsoleLog( "socket.on(inviteUsersToRoom)", args );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( !cUser.depositionID || typeof depositions[cUser.depositionID] === 'undefined' ) {
			errMsg = 'Deposition not selected';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var userDepo = depositions[cUser.depositionID];
		if( typeof rooms[args.roomID] === 'undefined' ) {
			errMsg = 'Room does not exist';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var _room = rooms[args.roomID];
		if( typeof _room.users[currentUserID] === 'undefined' ) {
			errMsg =  'User not in room';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		// prepare a soft invite for every member of the group
		for( var i=0; i<args.users.length; ++i ) {
			var userID = parseInt( args.users[i] );
			var joinTS = Math.round( (new Date()).getTime() / 1000 );
			if( typeof userDepo.users[userID] !== 'undefined' ) {
				_room.users[userID] = userDepo.users[userID];
				_room.joinTimestamp[userID] = (typeof _room.joinTimestamp[userID] === "number" ? _room.joinTimestamp[userID] : joinTS );
			}
		}
		_room.save();
		var room = _room.info();
		var roomInvite = {room:room, user:cUser.info()};
		// send invitations to all connected group members
		for( var i=0; i<args.users.length; ++i ) {
			var userID = parseInt( args.users[i] );
			if( typeof users[userID] !== 'undefined' ) {
				var _user = users[userID];
				if( typeof _user !== 'undefined' ) {
					httpsSocketIO.sockets.socket( _user.uid ).emit( 'invite', roomInvite );
				}
			}
		}
		if( typeof callback === 'function' ) {
			callback( {success:true} );
		}
	};
	socket.on( 'inviteUsersToRoom', inviteUsersToRoom );
	socket.on( 'invite_users_to_room', inviteUsersToRoom );

    socket.on( 'remove_users_from_room', function( args, callback ) {
		ConsoleLog( "socket.on(remove_users_from_room)" );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
        //if user in deposition
        if( cUser.depositionID ) {
			var roomID = args.id;
            if( typeof rooms[roomID] !== 'undefined' ) {
				var _room = rooms[roomID];
                if( args.users ) {
                    if( currentUserID === _room.creatorID ) {
                        for( i=0; i<args.users.length; i++ ) {
							var userID = parseInt( args.users[i] );
                            if( typeof users[userID] !== 'undefined' ) {
                                if( typeof _room.users[userID] !== 'undefined' ) {
                                    _room.removeUser( users[userID] );
                                }
                            }
							_room.save();
                        }
					} else {
						errMsg = 'User not creator';
						ConsoleLog( errMsg );
						if( typeof callback === 'function' ) {
							callback( {success:false, error:errMsg} );
						}
						return;
					}
                } else {
					errMsg = "Missing 'users' parameter";
                    ConsoleLog( errMsg );
					if( typeof callback === 'function' ) {
						callback( {success:false, error:errMsg} );
					}
					return;
                }
            } else {
				errMsg = 'Room does not exist';
                ConsoleLog( errMsg );
				if( typeof callback === 'function' ) {
					callback( {success:false, error:errMsg} );
				}
				return;
            }
        } else {
            errMsg = 'Deposition not selected';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
        }
    });

    // messages
    socket.on('message_to_room',  function( args, callback )
	{
		ConsoleLog( "socket.on(message_to_room)", args.roomID );
		var copyArgs = JSON.parse( JSON.stringify( args ) );
		delete copyArgs.message;
		ConsoleLog( 'args: ' + JSON.stringify( copyArgs ) );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}

        if( cUser.depositionID && typeof depositions[cUser.depositionID] !== 'undefined' ) {
			var userDepo = depositions[cUser.depositionID];
            if( typeof rooms[args.roomID] !== 'undefined' ) {
				var _room = rooms[args.roomID];
				//console.log( 'room: ' + JSON.stringify( cUser.deposition.getRoom( args.roomID ) ) );
                if( typeof _room.users[cUser.id] !== 'undefined' ) {
					var message = new Message( args.roomID, args.message, currentUserID, args.system );
					if( typeof messages[args.roomID] === 'undefined' ) {
						ConsoleLog( 'creating room message history' );
						messages[args.roomID] = [];
					}
					messages[args.roomID].push( message );
					message.save();
                    httpsSocketIO.sockets.in( args.roomID ).emit( 'message_to_room', message.info() );
					if( typeof callback === 'function' ) {
						callback( {success:true} );
					}
                } else {
                	ConsoleLog( "User Not In ROOM '", args.roomID,"'\n", cUser.info() );
					if( typeof callback === 'function' ) {
						callback( {error:'User not in room'} );
					}
                }
            } else {
				if( typeof callback === 'function' ) {
					callback({error:'Room does not exist'});
				}
            }
        } else {
			if( typeof callback === 'function' ) {
				callback({error:'Deposition not selected'});
			}
        }
    } );

    socket.on( 'message_to_user', function( args, callback ) {
		ConsoleLog( "socket.on(message_to_user) -----" );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
        if( typeof users[args.userID] !== 'undefined' ) {
            var message = {
                message:args.message,
                timestamp:Math.round((new Date()).getTime() / 1000),
                user:cUser.info()
            };
			httpsSocketIO.sockets.socket( users[args.userID].uid ).emit( 'message_from_user', message );
			if( typeof callback === 'function' ) {
				callback( {success:true} );
			}
        } else {
			if( typeof callback === 'function' ) {
				callback( {success:false, error:'User does not exist/online'} );
			}
        }
    } );

    // files

    socket.on('confirm_sharing', function (args, callback) {
		ConsoleLog( "socket.on(confirm_sharing) -----" );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
        if( typeof users[args.userID] !== 'undefined' ) {
			httpsSocketIO.sockets.socket( users[args.userID].uid ).emit( 'sharing_confirmed', {shareID:args.shareID, user:cUser.info()} );
			if( typeof callback === 'function' ) {
				callback( {success:true} );
			}
        } else {
            callback( {error:'User does not exist/online'} );
        }
    } );

    socket.on( 'cancel_sharing', function( args, callback )
	{
		ConsoleLog( "socket.on(cancel_sharing) -----" );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
        if( typeof users[args.userID] !== 'undefined' ) {
			httpsSocketIO.sockets.socket( users[args.userID].uid ).emit( 'sharing_canceled', {shareID:args.shareID, user:cUser.info()} );
			if( typeof callback === 'function' ) {
				callback( {success:true} );
			}
        } else {
			if( typeof callback === 'function' ) {
				callback( {success:false, error:'User does not exist/online'} );
			}
        }
    } );

    socket.on( 'complete_sharing', function( args, callback ) {
		ConsoleLog( "socket.on(complete_sharing) -----" );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
        if( typeof users[args.userID] !== 'undefined' ) {
			httpsSocketIO.sockets.socket( users[args.userID].uid ).emit( 'sharing_completed', {shareID:args.shareID, user:cUser.info()} );
			if( typeof callback === 'function' ) {
				callback( {success:true} );
			}
        } else {
			if( typeof callback === 'function' ) {
				callback( {success:false, error:'User does not exist/online'} );
			}
        }
    } );

    socket.on('progress_sharing', function (args, callback) {
		ConsoleLog( "socket.on(progress_sharing) -----" );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
        if( typeof users[args.userID] !== 'undefined' ) {
            httpsSocketIO.sockets.socket( users[args.userID].uid ).emit( 'sharing_updated', {shareID:args.shareID, progress:args.progress, user:cUser.info()} );
			if( typeof callback === 'function' ) {
				callback( {success:true} );
			}
        } else {
            callback( {success:false, error:'User does not exist/online'} );
        }
    } );

	function cleanupUserSocket()
	{
		ConsoleLog( 'cleanupUserSocket', currentUserID );
		if( typeof users[currentUserID] === 'undefined' ) {
			return;
		}
		var cUser = users[currentUserID];

		if( config.debug ) {
			ConsoleLog( 'User did disconnect:', currentUserID, '"' + user.name + '"', 'uid:', cUser.uid, 'depo:', cUser.depositionID );
		}

		// disconnect user -- make sure its the same socket
		if( user.uid === cUser.uid ) {
			ConsoleLog( '-- disconnect -- uid:', user.uid );
			cUser.active = false;
			var userRooms = httpsSocketIO.sockets.manager.roomClients[cUser.uid];
			for( var key in userRooms) {
				if( userRooms.hasOwnProperty( key ) ) {
					var roomID = key.substring( 1, key.length );
					if( roomID !== '' ) {
						ConsoleLog( '-- disconnect -- roomID:', '"'+ roomID + '"' );
						if( typeof rooms[roomID] !== 'undefined' ) {
							rooms[roomID].users[cUser.id] = false;
							rooms[roomID].save();
							httpsSocketIO.sockets.socket( cUser.uid ).leave( roomID );
						}
					} else {
						if( roomID === '' ) {
							httpsSocketIO.sockets.socket( cUser.uid ).leave( roomID );
						}
					}
				}
			}

			if( cUser.depositionID ) {
				var userDepo = depositions[cUser.depositionID];
				if( userDepo && userDepo instanceof Deposition ) {
					if( sessionIsDemo( userDepo.class ) && cUser.id === userDepo.ownerID ) {
						ConsoleLog( 'cleanupUserSocket -- removing demo owner key' );
						rClient.del( 'demo:' + cUser.depositionID, function( err ) {
							if( err ) ConsoleLog( err );
						} );
					}
					var dUser = cUser.info();
					ConsoleLog( '-- disconnect -- user_did_disconnect_from_deposition:', userDepo.depoRoomID, JSON.stringify( dUser ) );
					userDepo.users[currentUserID] = false;
					userDepo.save();
					socket.broadcast.to( userDepo.depoRoomID ).emit( 'user_did_disconnect_from_deposition', {deposition:{id:cUser.depositionID}, user:dUser} );
				}
				cUser.depositionID = null;
			}

			cUser.uid = null;
			cUser.save();
		}
	};

	function socketDidDisconnect()
	{
		ConsoleLog( "socketDidDisconnect -----" );
		setTimeout( cleanupUserSocket, 500 );	//allow redis propigation of possible new user
	};
    socket.on( 'disconnect', socketDidDisconnect );

//	TODO -- needs more security around witness
	function exhibitsBlock( args, callback )
	{
		ConsoleLog( "socket.on(exhibitsBlock) -----" );
		ConsoleLog( "exhibitsBlock -- depoID:", args.depositionID, "userID:", args.userID, "blocked:", !!args.block );
		var deposition = depositions[args.depositionID];
		var witness = users[args.userID];
		if( typeof deposition !== 'undefined' && typeof witness !== 'undefined' ) {
			witness.blockExhibits = !!args.block;	//cast to boolean
			//witness.socket.emit( 'exhibits_block', {"depositionID":args.depositionID, "userID":args.userID, "block":args.block} );
			var params = {"depositionID":deposition.id, "userID":witness.id, "block":witness.blockExhibits};
			ConsoleLog( 'exhibits_block', JSON.stringify( params ) );
			httpsSocketIO.sockets.socket( witness.uid ).emit( 'exhibits_block', params );
			witness.save();
		}
		if( callback ) callback( {"success":true} );
	};
	socket.on( 'exhibitsBlock', exhibitsBlock );
	socket.on( 'exhibits_block', exhibitsBlock );

	/**
	 * Start Presentation
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function startPresentation( args, callback ) {
		ConsoleLog( 'startPresentation', args );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( !args || typeof args !== 'object' || !args.hasOwnProperty( 'depositionID' ) ) {
			errMsg = 'Invalid deposition ID';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {error:errMsg} );
			}
			return;
		}
		var depositionID = args.depositionID;
		if( typeof depositions[depositionID] !== 'undefined' ) {
			var _depo = depositions[depositionID];
			if( typeof presentations[depositionID] !== 'undefined' ) {
				errMsg = 'Presentation already started for this deposition';
				ConsoleLog( errMsg );
				if( typeof callback === 'function' ) {
					callback( {success:false, error:errMsg} );
				}
				return;
			}
			if( parseInt( currentUserID ) !== parseInt( _depo.speakerID ) ) {
				errMsg = 'Only the leader role may start a presentation';
				ConsoleLog( errMsg );
				if( typeof callback === 'function' ) {
					callback( {success:false, error:errMsg} );
				}
				return;
			}
			if( _depo.users[currentUserID] === 'undefined' || !cUser.depositionID || parseInt( cUser.depositionID ) !== parseInt( _depo.id ) ) {
				errMsg = 'Cannot start a presentation for a deposition you are not attending';
				ConsoleLog( errMsg );
				if( typeof callback === 'function' ) {
					callback( {success:false, error:errMsg} );
				}
				return;
			}
			if( typeof args.roomID === 'undefined' || !args.roomID || typeof _depo.rooms[args.roomID] === 'undefined' || typeof rooms[args.roomID] === 'undefined' || !(rooms[args.roomID] instanceof Room) ) {
				errMsg = 'Invalid roomID';
				ConsoleLog( errMsg );
				if( typeof callback === 'function' ) {
					callback( {success:false, error:errMsg} );
				}
				return;
			}
			var roomID = args.roomID;
			if( typeof rooms[roomID].users[currentUserID] === 'undefined' ) {
				errMsg = 'Invalid roomID';
				ConsoleLog( errMsg );
				if( typeof callback === 'function' ) {
					callback( {success:false, error:errMsg} );
				}
				return;
			}
			if( !args.hasOwnProperty( 'fileID' ) || !(args.fileID > 0) ) {
				errMsg = 'Invalid fileID';
				ConsoleLog( errMsg );
				if( typeof callback === 'function' ) {
					callback( {success:false, error:errMsg} );
				}
				return;
			}
			if( typeof args.pageIndex === 'undefined' || !(args.pageIndex >= 0) ) {
				errMsg = 'Invalid pageIndex';
				ConsoleLog( errMsg );
				if( typeof callback === 'function' ) {
					callback( {success:false, error:errMsg} );
				}
				return;
			}
			if( typeof args.orientation === 'undefined' || typeof args.orientation !== 'string' || (args.orientation !== 'landscape' && args.orientation !== 'portrait') ) {
				errMsg = 'Invalid orientation';
				ConsoleLog( errMsg );
				if( typeof callback === 'function' ) {
					callback( {success:false, error:errMsg} );
				}
				return;
			}
			var presentation = new Presentation( depositionID );
			presentation.annotatorID = _depo.speakerID;
			presentation.roomID = roomID;
			presentation.subscribers.push( currentUserID );
			presentation.orientation = args.orientation;
			presentation.fileID = args.fileID;
			presentation.pageIndex = args.pageIndex;
			var eventData = presentation.sanitizeProperties( args );
			for( var _prop in eventData ) {
				if( eventData.hasOwnProperty( _prop ) && presentation.hasOwnProperty( _prop ) ) {
					ConsoleLog( _prop, '=>', eventData[_prop] );
					presentation[_prop] = eventData[_prop];
				}
			}
			presentations[depositionID] = presentation;	//preload
			presentation.save( function() {
				ConsoleLog( 'startPresentation', presentation.info() );
				socket.broadcast.to( roomID ).emit( 'startPresentation', presentation.info() );
				ConsoleLog( 'Presentation started!' );
				if( typeof callback === 'function' ) {
					callback( {success:true} );
				}
			} );
			return;
		} else {
			errMsg = 'Invalid deposition ID';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
	}
	socket.on( 'startPresentation', startPresentation );

	/**
	 * End Presentation
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function endPresentation( args, callback )
	{
		ConsoleLog( 'endPresentation' );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( !args || typeof args !== 'object' || !args.hasOwnProperty( 'depositionID' ) ) {
			errMsg = 'Invalid deposition ID';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var depositionID = args.depositionID;
		if( typeof depositions[depositionID] !== 'undefined' ) {
			var _depo = depositions[depositionID];
			if( typeof presentations[depositionID] !== 'undefined' ) {
				if( parseInt( currentUserID ) !== parseInt( _depo.speakerID ) ) {
					errMsg = 'Only the leader role may end the presentation';
					ConsoleLog( errMsg );
					if( typeof callback === 'function' ) {
						callback( {success:false, error:errMsg} );
					}
					return;
				}
				if( _depo.users[currentUserID] === 'undefined' || typeof cUser.depositionID === 'undefined' || parseInt( cUser.depositionID ) !== parseInt( _depo.id ) ) {
					errMsg = 'Cannot end a presentation for a deposition you are not attending';
					ConsoleLog( errMsg );
					if( typeof callback === 'function' ) {
						callback( {success:false, error:errMsg} );
					}
					return;
				}
				var presentation = presentations[depositionID];
				ConsoleLog( 'endPresentation', _depo.id, presentation.roomID );
				presentation.deleteAllCallouts( function() {
					presentation.deleteAllAnnotations( function() {
						rClient.del( presentation.rKey, function( err ) {
							if( err ) ConsoleLog( err );
						} );
						httpsSocketIO.sockets.in( presentation.roomID ).emit( 'endPresentation' );
						delete presentations[depositionID];
						lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Presentation.end', args:depositionID}} ) );
						if( typeof callback === 'function' ) {
							callback( {success:true} );
						}
					} );
				} );
				return;
			} else {
				errMsg = 'Cannot end an unknown presentation';
				ConsoleLog( errMsg );
				if( typeof callback === 'function' ) {
					callback( {success:false, error:errMsg} );
				}
				return;
			}
		} else {
			errMsg = 'Invalid deposition ID';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
	}
	socket.on( 'endPresentation', endPresentation );

	/**
	 * Join Presentation
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function joinPresentation( args, callback )	{
		ConsoleLog( 'joinPresentation', args );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( !args || typeof args !== 'object' || !args.hasOwnProperty( 'depositionID' ) || typeof depositions[args.depositionID] === 'undefined' || !(depositions[args.depositionID] instanceof Deposition) ) {
			errMsg = 'Invalid deposition ID';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var depositionID = parseInt( args.depositionID );
		var _depo = depositions[depositionID];
		if( typeof _depo.users[currentUserID] === 'undefined' || !cUser.depositionID || parseInt( cUser.depositionID ) !== parseInt( _depo.id ) ) {
			errMsg = 'Cannot join presentation for a deposition you are not attending';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( typeof presentations[depositionID] === 'undefined' ) {
			errMsg = 'No presentation to join for this deposition';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var presentation = presentations[depositionID];
		if( typeof rooms[presentation.roomID].users[currentUserID] === 'undefined' ) {
			errMsg = 'Access denied';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var userID = parseInt( currentUserID );
		if( presentation.subscribers.indexOf( userID ) === -1 ) {
			presentation.subscribers.push( userID );
			presentation.save( function() {
				ConsoleLog( 'emit: userDidJoinPresentation' );
				httpsSocketIO.sockets.socket( cUser.uid ).broadcast.to( presentation.roomID ).emit( 'userDidJoinPresentation', {user:cUser.info()} );
			} );
		}
		if( typeof callback === 'function' ) {
			callback( {success:true, presentationInfo:presentation.info()} );
		}
	}
	socket.on( 'joinPresentation', joinPresentation );

	/**
	 * Leave Presentation
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function leavePresentation( args, callback ) {
		ConsoleLog( 'leavePresentation' );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( !args || typeof args !== 'object' || !args.hasOwnProperty( 'depositionID' ) || typeof depositions[args.depositionID] === 'undefined' || !(depositions[args.depositionID] instanceof Deposition) ) {
			errMsg = 'Invalid deposition ID';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var depositionID = parseInt( args.depositionID );
		var _depo = depositions[depositionID];
		if( typeof _depo.users[currentUserID] === 'undefined' || !cUser.depositionID || parseInt( cUser.depositionID ) !== parseInt( _depo.id ) ) {
			errMsg = 'Cannot leave presentation for a deposition you are not attending';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( typeof presentations[depositionID] === 'undefined' ) {
			errMsg = 'No presentation to leave for this deposition';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var presentation = presentations[depositionID];
		if( typeof rooms[presentation.roomID].users[currentUserID] === 'undefined' ) {
			errMsg = 'Access denied';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var userID = parseInt( currentUserID );
		if( userID === parseInt( _depo.speakerID ) ) {
			errMsg = 'Cannot leave your own presentation';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var userIndex = presentation.subscribers.indexOf( userID );
		if( userIndex !== -1 ) {
			presentation.subscribers.splice( userIndex, 1 );
			presentation.save( function() {
				httpsSocketIO.sockets.socket( cUser.uid ).broadcast.to( presentation.roomID ).emit( 'userDidLeavePresentation', {user:cUser.info()} );
			} );
		}
		if( typeof callback === 'function' ) {
			callback( {success:true} );
		}
	}
	socket.on( 'leavePresentation', leavePresentation );

	/**
	 * Presentation Status
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function presentationStatus( args, callback ) {
		ConsoleLog( 'presentationStatus' );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( !args || typeof args !== 'object' || !args.hasOwnProperty( 'depositionID' ) || typeof depositions[args.depositionID] === 'undefined' || !(depositions[args.depositionID] instanceof Deposition) ) {
			errMsg = 'Invalid deposition ID';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var depositionID = parseInt( args.depositionID );
		var _depo = depositions[depositionID];
		if( typeof _depo.users[currentUserID] === 'undefined' || !cUser.depositionID || parseInt( cUser.depositionID ) !== parseInt( _depo.id ) ) {
			errMsg = 'Cannot get presentation status for a deposition you are not attending';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( typeof presentations[depositionID] === 'undefined' ) {
			errMsg = 'Unknown presentation';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var presentation = presentations[depositionID];
		if( typeof rooms[presentation.roomID].users[currentUserID] === 'undefined' ) {
			errMsg = 'Access denied';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( typeof callback === 'function' ) {
			callback( {success:true, presentationInfo:presentation.info()} );
		}
	}
	socket.on( 'presentationStatus', presentationStatus );

	/**
	 * Set Page Properties
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function setPageProperties( args, callback )
	{
		ConsoleLog( 'setPageProperties:', args );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( !args || typeof args !== 'object' || !args.hasOwnProperty( 'depositionID' ) ) {
			errMsg = 'Invalid deposition ID';
			ConsoleLog( 'setPageProperties error:', errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( !args || typeof args !== 'object' || !args.hasOwnProperty( 'eventData' ) || typeof args.eventData !== 'object' ) {
			errMsg = 'Missing required event data';
			ConsoleLog( 'setPageProperties error:', errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var depositionID = parseInt( args.depositionID );
		var isValidResult = validatePresentationAnnotator( currentUserID, depositionID );
		if( isValidResult === true ) {
			if( typeof presentations[depositionID] !== 'undefined' ) {
				var presentation = presentations[depositionID];
				var eventData = presentation.sanitizeProperties( args.eventData );
				presentation.assign( eventData );
				presentation.save( function() {
					var subscribers = JSON.parse( JSON.stringify( presentation.subscribers ) ); //clone
					var indexOfSelf = subscribers.indexOf( currentUserID );
					if( indexOfSelf >= 0 ) {
						subscribers.splice( indexOfSelf, 1 );
					}
					notifySubscribers( subscribers, 'setPageProperties', eventData );
					if( typeof callback === 'function' ) {
						callback( {success:true} );
					}
				} );
				return;
			} else {
				errMsg = 'Unknown presentation';
				ConsoleLog( 'setPageProperties error:', errMsg );
				if( typeof callback === 'function' ) {
					callback( {success:false, error:errMsg} );
				}
				return;
			}
		} else {
			errMsg = isValidResult;
			ConsoleLog( 'setPageProperties error:', errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
	}
	socket.on( 'setPageProperties', setPageProperties );

	function setPresentationFile( args, callback ) {
		ConsoleLog( 'setPresentationFile:', args );
		var errMsg;
		var _user = users[currentUserID];
		if( typeof _user === 'undefined' || !_user ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var depositionID = parseInt( args.depositionID );
		var fileID = parseInt( args.fileID );
		var isValidResult = validatePresentationLeader( currentUserID, depositionID );
		if( isValidResult === true && !isNaN( fileID ) && fileID > 0 ) {
			var presentation = presentations[depositionID];
			var eventData = presentation.sanitizeProperties( args );
			presentation.assign( eventData );
			presentation.fileID = fileID;
			presentation.deleteAllCallouts( function() {
				presentation.deleteAllAnnotations( function() {
					presentation.save( function() {
						var subscribers = JSON.parse( JSON.stringify( presentation.subscribers ) ); //clone
						var indexOfSelf = subscribers.indexOf( currentUserID );
						if( indexOfSelf >= 0 ) {
							subscribers.splice( indexOfSelf, 1 );
						}
						ConsoleLog( 'setPresentationFile; notify subscribers: reloadPresentationSource', subscribers );
						notifySubscribers( subscribers, 'reloadPresentationSource', presentation );
						if( typeof callback === 'function' ) {
							callback( {success:true} );
						}
					} );
				} );
			} );
		} else {
			errMsg = isValidResult;
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
		}
	}
	socket.on( 'setPresentationFile', setPresentationFile );

	/**
	 * Set Annotator
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function setAnnotator( args, callback )
	{
		ConsoleLog( 'setAnnotator:', args );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( typeof args !== 'object' || !args.hasOwnProperty( 'eventData' ) || typeof args.eventData !== 'object' || !args.eventData.hasOwnProperty( 'userID' ) ) {
			errMsg = 'Inhvalid arguments';
			ConsoleLog( 'setAnnotator error:', errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var depositionID = parseInt( args.depositionID );
		var annotatorID = parseInt( args.eventData.userID );
		var isValidResult = validatePresentationLeader( currentUserID, depositionID );
		if( isValidResult === true ) {
			var _depo = depositions[depositionID];
			if( !isNaN( annotatorID ) && typeof _depo.users[annotatorID] !== 'undefined' && typeof users[annotatorID] !== 'undefined' ) {
				var _user = users[annotatorID];
				ConsoleLog( 'Annotator:', _user );
				if( (_user.userType === 'M' && _user.id === _depo.speakerID) || (_user.userType === 'W' || _user.userType === 'WM') ) {
					if( typeof presentations[depositionID] !== 'undefined' ) {
						var presentation = presentations[depositionID];
						presentation.annotatorID = annotatorID;
						presentation.save( function() {
							httpsSocketIO.sockets.in( presentation.roomID ).emit( 'setAnnotator', {annotatorID:annotatorID} );
							if( typeof callback === 'function' ) {
								callback( {success:true} );
							}
						} );
					}
				} else {
					errMsg = 'Invalid user';
					ConsoleLog( 'setAnnotator error:', errMsg );
					if( typeof callback === 'function' ) {
						callback( {success:false, error:errMsg} );
					}
					return;
				}
			} else {
				errMsg = 'User not found';
				ConsoleLog( 'setAnnotator error:', errMsg );
				if( typeof callback === 'function' ) {
					callback( {success:false, error:errMsg} );
				}
				return;
			}
		}
	}
	socket.on( 'setAnnotator', setAnnotator );

	/**
	 * Add Annotation
	 * @param {Object} args Argument parameters
	 * @param {Function} callback
	 */
	function addAnnotation( args, callback ) {
		ConsoleLog( 'Socket.addAnnotation:', args );
		var _user = users[currentUserID];
		if( typeof _user === 'undefined' || !_user ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var sessionID = parseInt( args.depositionID );
		var presentation = presentations[sessionID];
		if( typeof presentation !== "undefined" && presentation !== null ) {
			presentation.eventQueue.push( {"sessionID":sessionID,"userID":currentUserID,"op":"addAnnotation","args":args,"callback":callback} );
			presentationProcessEventQueue( presentation );
		} else {
			errMsg = 'Presentation not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
	}
	socket.on( 'addAnnotation', addAnnotation );

	/**
	 * Modify Annotation
	 * @param {Object} args Argument parameters
	 * @param {Function} callback
	 */
	function modifyAnnotation( args, callback ) {
		ConsoleLog( 'Socket.modifyAnnotation:', args );
		var _user = users[currentUserID];
		if( typeof _user === 'undefined' || !_user ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var sessionID = parseInt( args.depositionID );
		var presentation = presentations[sessionID];
		if( typeof presentation !== "undefined" && presentation !== null ) {
			presentation.eventQueue.push( {"sessionID":sessionID,"userID":currentUserID,"op":"modifyAnnotation","args":args,"callback":callback} );
			presentationProcessEventQueue( presentation );
		} else {
			errMsg = 'Presentation not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
	}
	socket.on( 'modifyAnnotation', modifyAnnotation );

	/**
	 * Delete Annotation
	 * @param {Object} args Argument parameters
	 * @param {Function} callback
	 */
	function deleteAnnotation( args, callback ) {
		ConsoleLog( 'Socket.deleteAnnotation:', args );
		var _user = users[currentUserID];
		if( typeof _user === 'undefined' || !_user ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var sessionID = parseInt( args.depositionID );
		var presentation = presentations[sessionID];
		if( typeof presentation !== "undefined" && presentation !== null ) {
			presentation.eventQueue.push( {"sessionID":sessionID,"userID":currentUserID,"op":"deleteAnnotation","args":args,"callback":callback} );
			presentationProcessEventQueue( presentation );
		} else {
			errMsg = 'Presentation not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
	}
	socket.on( 'deleteAnnotation', deleteAnnotation );

	/**
	 * Initial Annotation
	 * @param {Object} args Argument parameters
	 * @param {Function} callback
	 */
	function initialAnnotation( args, callback ) {
		ConsoleLog( 'Socket.initialAnnotation:', args );
		var _user = users[currentUserID];
		if( typeof _user === 'undefined' || !_user ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var sessionID = parseInt( args.depositionID );
		var presentation = presentations[sessionID];
		if( typeof presentation !== "undefined" && presentation !== null ) {
			presentation.eventQueue.push( {"sessionID":sessionID,"userID":currentUserID,"op":"initialAnnotation","args":args,"callback":callback} );
			presentationProcessEventQueue( presentation );
		} else {
			errMsg = 'Presentation not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
	}
	socket.on( 'initialAnnotation', initialAnnotation );

	/**
	 * Clear Annotations
	 * @param {Object} args Argument parameters
	 * @param {Function} callback
	 */
	function clearAnnotations( args, callback ) {
		ConsoleLog( 'Socket.clearAnnotations:', args );
		var _user = users[currentUserID];
		if( typeof _user === 'undefined' || !_user ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var sessionID = parseInt( args.depositionID );
		var presentation = presentations[sessionID];
		if( typeof presentation !== "undefined" && presentation !== null ) {
			presentation.eventQueue.push( {"sessionID":sessionID,"userID":currentUserID,"op":"clearAnnotations","args":args,"callback":callback} );
			presentationProcessEventQueue( presentation );
		} else {
			errMsg = 'Presentation not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
	}
	socket.on( 'clearAnnotations', clearAnnotations );

	function annotationUndo( args, callback ) {
		ConsoleLog( 'Socket.annotationUndo:', args );
		var _user = users[currentUserID];
		if( typeof _user === 'undefined' || !_user ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var sessionID = parseInt( args.depositionID );
		var presentation = presentations[sessionID];
		if( typeof presentation !== "undefined" && presentation !== null ) {
			presentation.eventQueue.push( {"sessionID":sessionID,"userID":currentUserID,"op":"annotationUndo","args":args,"callback":callback} );
			presentationProcessEventQueue( presentation );
		} else {
			errMsg = 'Presentation not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
	}
	socket.on( 'annotationUndo', annotationUndo );

	function getAnnotations( args, callback ) {
		ConsoleLog( 'Socket.getAnnotations', args );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( !args || typeof args !== 'object' || !args.hasOwnProperty( 'depositionID' ) || typeof depositions[args.depositionID] === 'undefined' || !(depositions[args.depositionID] instanceof Deposition) ) {
			errMsg = 'Invalid deposition ID';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var depositionID = parseInt( args.depositionID );
		var _depo = depositions[depositionID];
		if( typeof _depo.users[currentUserID] === 'undefined' || !cUser.depositionID || parseInt( cUser.depositionID ) !== parseInt( _depo.id ) ) {
			errMsg = 'Cannot get annotations for a presentation in a deposition you are not attending';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( typeof presentations[depositionID] === 'undefined' ) {
			errMsg = 'Unknown presentation';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var presentation = presentations[depositionID];
		if( typeof rooms[presentation.roomID].users[currentUserID] === 'undefined' ) {
			errMsg = 'Access denied';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		//ConsoleLog( "Socket.getAnnotations;", presentation.pageAnnotations );
		if( typeof callback === 'function' ) {
			callback( {success:true, annotations:presentation.pageAnnotations} );
		}
	}
	socket.on( 'getAnnotations', getAnnotations );


	/**
	 * Add Callout
	 * @param {Object} args Argument parameters
	 * @param {Function} callback
	 */
	function addCallout( args, callback ) {
		ConsoleLog( "Socket.addCallout", args );
		var errMsg;
		var sessionID = parseInt( args.depositionID );
		var presentation = presentations[sessionID];
		if( typeof presentation !== "undefined" && presentation !== null ) {
			presentation.eventQueue.push( {"sessionID":sessionID,"userID":currentUserID,"op":"addCallout","args":args,"callback":callback} );
			presentationProcessEventQueue( presentation );
		} else {
			errMsg = "Presentation not found";
			ConsoleLog( "Socket.addCallout; error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	}
	socket.on( "addCallout", addCallout );

	/**
	 * Modify Callout
	 * @param {Object} args Argument parameters
	 * @param {Function} callback
	 */
	function modifyCallout( args, callback ) {
		ConsoleLog( "Socket.modifyCallout:", args );
		var errMsg;
		var sessionID = parseInt( args.depositionID );
		var presentation = presentations[sessionID];
		if( typeof presentation !== "undefined" && presentation !== null ) {
			presentation.eventQueue.push( {"sessionID":sessionID,"userID":currentUserID,"op":"modifyCallout","args":args,"callback":callback} );
			presentationProcessEventQueue( presentation );
		} else {
			errMsg = "Presentation not found";
			ConsoleLog( "Socket.modifyCallout; error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	}
	socket.on( "modifyCallout", modifyCallout );

	/**
	 * Delete Callout
	 * @param {Object} args Argument parameters
	 * @param {Function} callback
	 */
	function deleteCallout( args, callback ) {
		ConsoleLog( "Socket.deleteCallout:", args );
		var errMsg;
		var sessionID = parseInt( args.depositionID );
		var presentation = presentations[sessionID];
		if( typeof presentation !== "undefined" && presentation !== null ) {
			presentation.eventQueue.push( {"sessionID":sessionID,"userID":currentUserID,"op":"deleteCallout","args":args,"callback":callback} );
			presentationProcessEventQueue( presentation );
		} else {
			errMsg = "Presentation not found";
			ConsoleLog( "Socket.deleteCallout; error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	}
	socket.on( "deleteCallout", deleteCallout );

	/**
	 * Set Callout(s) Z-Index
	 * @param {Object} args Argument parameters
	 * @param {Function} callback
	 */
	function setCalloutZIndex( args, callback ) {
		ConsoleLog( "Socket.setCalloutZIndex:", args );
		var errMsg;
		var sessionID = parseInt( args.depositionID );
		var presentation = presentations[sessionID];
		if( typeof presentation !== "undefined" && presentation !== null ) {
			presentation.eventQueue.push( {"sessionID":sessionID,"userID":currentUserID,"op":"setCalloutZIndex","args":args,"callback":callback} );
			presentationProcessEventQueue( presentation );
		} else {
			errMsg = "Presentation not found";
			ConsoleLog( "Socket.setCalloutZIndex; error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	}
	socket.on( "setCalloutZIndex", setCalloutZIndex );

	/**
	 * Get Callouts
	 * @param {Object} args Argument parameters
	 * @param {Function} callback
	 */
	function getCallouts( args, callback ) {
		ConsoleLog( "Socket.getCallouts", args );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === "undefined" || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( "Socket.getCallouts; error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( !args || typeof args !== "object" || !args.hasOwnProperty( "depositionID" ) || typeof depositions[args.depositionID] === "undefined" || !(depositions[args.depositionID] instanceof Deposition) ) {
			errMsg = "Invalid session ID";
			ConsoleLog( "Socket.getCallouts; error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var depositionID = parseInt( args.depositionID );
		var _depo = depositions[depositionID];
		if( typeof _depo.users[currentUserID] === "undefined" || !cUser.depositionID || parseInt( cUser.depositionID ) !== parseInt( _depo.id ) ) {
			errMsg = "Cannot get callouts for a presentation in a deposition you are not attending";
			ConsoleLog( "Socket.getCallouts; error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( typeof presentations[depositionID] === "undefined" ) {
			errMsg = "Unknown presentation";
			ConsoleLog( "Socket.getCallouts; error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var presentation = presentations[depositionID];
		if( typeof rooms[presentation.roomID].users[currentUserID] === "undefined" ) {
			errMsg = "Access denied";
			ConsoleLog( "Socket.getCallouts; error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		//ConsoleLog( "Socket.getCallouts;", presentation.pageCallouts );
		if( typeof callback === "function" ) {
			callback( {"success":true, "callouts":presentation.pageCallouts} );
		}
	}
	socket.on( "getCallouts", getCallouts );

	/**
	 * Log to file, device Console Log entry
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function logToConsole( args, callback ) {
		ConsoleLog( "socket.on(consoleLog) -----" );
		logToFile( users[currentUserID], args.message );
		if( typeof callback === 'function' ) {
			callback( {success:true} );
		}
	};
	socket.on( 'consoleLog', logToConsole );

	/**
	 * get Deposition Info
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function depositionInfo( args, callback )
	{
		ConsoleLog( 'depositionInfo', args );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( !args || typeof args !== 'object' || !args.hasOwnProperty( 'depositionID' ) || typeof depositions[args.depositionID] === 'undefined' || !(depositions[args.depositionID] instanceof Deposition) ) {
			errMsg = 'Invalid deposition ID';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var depositionID = parseInt( args.depositionID );
		var _depo = depositions[depositionID];
		if( typeof _depo.users[currentUserID] === 'undefined' || !cUser.depositionID || parseInt( cUser.depositionID ) !== parseInt( _depo.id ) ) {
			errMsg = 'Cannot get info for a deposition you are not attending';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( typeof callback === 'function' ) {
			var depoInfo = {
				ID: _depo.id,
				witness: _depo.title,
				ownerID: _depo.ownerID,
				speakerID: _depo.speakerID,
				class: _depo.class
			};
			callback( {success:true, deposition:depoInfo} );
		}
	};
	socket.on( 'depositionInfo', depositionInfo );


	/**
	 * Witness Login
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function witnessLogin( args, callback )
	{
		ConsoleLog( 'witnessLogin', args );
		var errMsg;
		if( !args || typeof args !== 'object' || !args.hasOwnProperty( 'depositionID' ) || typeof depositions[args.depositionID] === 'undefined' || !(depositions[args.depositionID] instanceof Deposition) ) {
			errMsg = 'Invalid deposition ID';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			socket.disconnect();
			return;
		}
		if( !args.hasOwnProperty( 'witnessID' ) ) {
			errMsg = 'Missing argument: witnessID';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			socket.disconnect();
			return;
		}
		if( !args.hasOwnProperty( 'name' ) ) {
			errMsg = 'Missing argument: name';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			socket.disconnect();
			return;
		}
		var depositionID = parseInt( args.depositionID );
		var _depo = depositions[depositionID];
		var witnessID = parseInt( args.witnessID );
		var witnessName = args.name;
		var witness = users[witnessID];
		if( typeof witness === 'undefined' || witness === null || typeof witness.isTempWitness === 'undefined' || !witness.isTempWitness ) {
			errMsg = 'Invalid witness';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			socket.disconnect();
			return;
		}
		var conductor;
		if( typeof _depo.users[_depo.ownerID] !== 'undefined' && typeof users[_depo.ownerID] !== 'undefined' ) {
			conductor = users[_depo.ownerID];
			if( !conductor.depositionID || parseInt( conductor.depositionID ) !== parseInt( _depo.id ) ) {
				conductor = false;
			}
		}
		if( !conductor ) {
			errMsg = 'Request is unable to be authorized';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			socket.disconnect();
			return;
		}
		witness.depositionID = depositionID;
		witness.save();
		ConsoleLog( 'witness asking for authorization', witness );
		httpsSocketIO.sockets.socket( conductor.uid ).emit( 'witness_login', {"depositionID":depositionID, "witnessID":witnessID, "name":witnessName} );
		if( typeof callback === 'function' ) {
			callback( {success:true} );
		}
	}
	socket.on( 'witnessLogin', witnessLogin );

	/**
	 * Deny Witness Login
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function rejectWitnesses( args, callback )
	{
		ConsoleLog( 'rejectWitnesses', args );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var depositionID = cUser.depositionID;
		if( typeof depositions[depositionID] === 'undefined' || depositions[depositionID] === null || depositions[depositionID].ownerID !== cUser.id ) {
			errMsg = 'Access denied';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var allUsers = Object.keys( users );
		for( var u=0; u<allUsers.length; ++u ) {
			var _userID = allUsers[u];
			var _user = users[_userID];
			if( typeof _user === 'undefined' || _user === null ) {
				continue;
			}
			if( typeof _user.depositionID === 'undefined' || _user.depositionID === null || _user.depositionID !== depositionID ) {
				continue;
			}
			if( typeof _user.isTempWitness === 'undefined' || !_user.isTempWitness ) {
				continue;
			}
			ConsoleLog( 'rejectWitnesses;', depositionID, ' witness:', _user.id );
			httpsSocketIO.sockets.socket( _user.uid ).emit( 'rejected_witness', {depositionID:depositionID, witnessID:_user.id} );
			httpsSocketIO.sockets.socket( _user.uid ).disconnect();
			delete users[_user.id];
		}
	}
	socket.on( 'rejectWitnesses', rejectWitnesses );

	/**
	 * Join Transcript
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function joinTranscript( args, callback )
	{
		ConsoleLog( 'joinTranscript', args );
		var errMsg;
		var eventID = parseInt( args.eventID );
		if( isNaN( eventID ) || eventID <= 0 ) {
			errMsg = 'Invalid Event ID';
			ConsoleLog( errMsg, args.eventID );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var accessKey = args.accessKey;
		if( typeof accessKey !== "string" || accessKey === null || accessKey.trim().length < 4 ) {
			errMsg = 'Invalid Access Key';
			ConsoleLog( errMsg, args.accessKey );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		accessKey = accessKey.trim();
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var depositionID = cUser.depositionID;
		if( typeof depositionID === 'undefined' || depositionID === null || typeof depositions[depositionID] === 'undefined' || depositions[depositionID] === null ) {
			errMsg = 'Deposition not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var transcript;
		if( typeof transcripts[depositionID] === 'undefined' || transcripts[depositionID] === null || typeof transcripts[depositionID].guid === 'undefined' || transcripts[depositionID].guid === null ) {
			transcript = new Transcript( depositionID );
			transcripts[depositionID] = transcript;
		} else {
			transcript = transcripts[depositionID];
		}
		transcript.authenticate( cUser, eventID, accessKey, callback );
	}
	socket.on( 'joinTranscript', joinTranscript );

	/**
	 * Reset Transcript
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function resetTranscript( args, callback )
	{
		ConsoleLog( 'resetTranscript', args );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === 'undefined' || !cUser ) {
			errMsg = 'User not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var depositionID = cUser.depositionID;
		if( typeof depositionID === 'undefined' || depositionID === null || typeof depositions[depositionID] === 'undefined' || depositions[depositionID] === null ) {
			errMsg = 'Deposition not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var deposition = depositions[depositionID];
		if( deposition.ownerID !== cUser.id ) {
			errMsg = 'Access denied';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( typeof transcripts[depositionID] === 'undefined' || transcripts[depositionID] === null || typeof transcripts[depositionID].guid === 'undefined' || transcripts[depositionID].guid === null ) {
			errMsg = 'Transcript not found';
			ConsoleLog( errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		} else {
			var transcript = transcripts[depositionID];
			transcript.abort();
			if( typeof callback === 'function' ) {
				callback( {success:true} );
			}
		}
	}
	socket.on( 'resetTranscript', resetTranscript );

	function resetUserIsActive() {
		var cUser = users[currentUserID];
		if( typeof cUser === "undefined" || !cUser ) {
			ConsoleLog( "resetUserIsActive; User not found" );
		}
		var depoIDs = Object.keys( depositions );
		for( var d in depoIDs ) {
			var depoID = depoIDs[d];
			if( typeof depoID === "function" ) {
				continue;
			}
			var depo = depositions[depoID];
			if( typeof depo.users[currentUserID] !== "undefined" ) {
				if( depo.users[currentUserID] ) {
					var dUser = cUser.info();
					dUser.active = false;
					depo.users[currentUserID] = false;
					depo.save();
					ConsoleLog( "-- resetUserIsActive -- user_did_disconnect_from_deposition:", depo.depoRoomID );
					socket.broadcast.to( depo.depoRoomID ).emit( "user_did_disconnect_from_deposition", {"deposition":{"id":depo.id}, "user":dUser} );
				}
			}
		}
		var roomKeys = Object.keys( rooms );
		for( var r in roomKeys ) {
			var roomID = roomKeys[r];
			if( typeof roomID === "function" ) {
				continue;
			}
			var room = rooms[roomID];
			if( typeof room.users[currentUserID] !== "undefined" ) {
				if( room.users[currentUserID] ) {
					ConsoleLog( "-- resetUserIsActive -- flagging user inactive for room:", room.id );
					room.users[currentUserID] = false;
					room.save();
				}
			}
		}
	};

	/**
	 * Debug Event
	 * @param {Object} args
	 * @param {Function} callback
	 */
//	function debugEvent( args, callback )
//	{
//		ConsoleLog( 'debugEvent', args );
//	}
//	socket.on( 'debugEvent', debugEvent );

} );


function rejoinRooms( user ) {
	ConsoleLog( 'rejoinRooms; userID:', user.id, 'depositionID:', user.depositionID, 'uid:', user.uid );
	if( typeof depositions[user.depositionID] === 'undefined' ) return;
	var deposition = depositions[user.depositionID];
//	ConsoleLog( 'Rooms:', deposition.rooms );
	for( var roomID in deposition.rooms ) {
		if( deposition.rooms.hasOwnProperty( roomID ) && typeof rooms[roomID] !== 'undefined' ) {
			var _room = rooms[roomID];
			if( _room.hasOwnProperty( 'readOnly' ) && _room.readOnly ) {
				ConsoleLog( 'Room is read only' );
				continue;
			}
			if( _room.hasOwnProperty( 'isChatEnabled' ) && !_room.isChatEnabled ) {
				ConsoleLog( 'Room is not chat enabled' );
				continue;
			}
//			console.log( '--- roomID:', roomID, 'title:', _room.title );
//			console.log( roomID, '--- users:', _room.users );
			for( var userID in _room.users ) {
				if( _room.users.hasOwnProperty( userID ) ) {
					if( user.id === parseInt( userID ) ) {
						if( _room.users.hasOwnProperty( userID ) && typeof users[userID] !== 'undefined' ) {
							var roomMember = users[userID];
//							console.log( roomID, '--- user:', roomMember );
							ConsoleLog( '--- user in room:', roomMember.id );
							if( !!_room.users[user.id] ) {
								ConsoleLog( '--- userID:', user.id, 'is already an active user in room' );
//								ConsoleLog( roomMember );
								httpsSocketIO.sockets.socket( user.uid ).join( roomID );
							} else {
								ConsoleLog( '--- JOINING roomID:', roomID, 'title:', _room.title );
								_room.addUser( user );
								_room.save();
							}
						}
					} else {
//						console.log( '--- rejoinRooms; skipping user:', userID, user.id );
					}
				}
			}
		}
	}
}


// API

function depositionStarted( request, response ) {
	console.log( "\n[200]", request.method, "to", request.url );
	request.on( 'data', function( chunk ) {
		console.log( "Received body data:", chunk.toString() );
		var object = JSON.parse( chunk.toString() );
		if( depositions[object.args.depositionID] ) {
			var depoRoomID = depositions[object.args.depositionID].depoRoomID;
			httpsSocketIO.sockets.in( depoRoomID ).emit( 'deposition_start', {deposition:depositions[object.args.depositionID].info()} );
		} else {
//			mysql.selectDepositionByID();
			console.log( 'Deposition does not exist' );
		}
	} );
	request.on( 'end', function(){ sendOk( response ); } );
}

function depositionFinished( request, response ) {
	console.log("\n[200]", request.method, "to", request.url );
	request.on( 'data', function( chunk ) {
		console.log( "Received body data:", chunk.toString() );
		var object = JSON.parse( chunk.toString() );
		if( depositions[object.args.depositionID] ) {
			var depoRoomID = depositions[object.args.depositionID].depoRoomID;
			httpsSocketIO.sockets.in( depoRoomID ).emit( 'deposition_end', {deposition:depositions[object.args.depositionID].info()} );

			if( sessionIsDemo( depositions[object.args.depositionID].class ) ) {
				ConsoleLog( 'depositionFinished -- removing demo owner key' );
				rClient.del( 'demo:' + object.args.depositionID, function( err ) {
					if( err ) ConsoleLog( err );
				} );
			}

			closeDeposition( object.args.depositionID );
			lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'closeDeposition', args:object.args.depositionID}} ) );
		}
	} );
	request.on('end', function(){ sendOk( response ); } );
}

/**
 * Reset Demo Depositions
 * @param {Object} request
 * @param {Object} response
 */
function resetDeposition( request, response ) {
	ConsoleLog( "[200]", request.method, "to", request.url );
	var allChunks = '';
	request.on( 'data', function( chunk ) {
		allChunks += chunk.toString();
	} );
	request.on( 'end', function() {
		console.log( "resetDeposition data:", allChunks.toString() );
		var object = JSON.parse( allChunks );
		var depositionID = parseInt( object.args.depositionID );
		var depoRoomID = depositionID + '.deposition';
		if( object.args.hasOwnProperty( 'notifyUsers' ) ) {
			var notifyUsers = object.args.notifyUsers;
			ConsoleLog( 'Reset Deposition - Notify Users:', notifyUsers );
			if( typeof depositions[depositionID] !== 'undefined' && sessionIsDemo( depositions[depositionID].class ) ) {
				var _depo = depositions[depositionID];
				for( var idx in notifyUsers ) {
					if( notifyUsers.hasOwnProperty( idx ) ) {
						var userID = parseInt( notifyUsers[idx] );
						if( typeof users[userID] !== 'undefined' && users[userID].depositionID === depositionID ) {
							var _user = users[userID];
							ConsoleLog( 'Notify User:', _user.id, '"' + _user.name + '", to vacate deposition' );
							_user.depositionID = null;
							_user.save();
							httpsSocketIO.sockets.socket( _user.uid ).emit( 'vacate_deposition', {"depositionID":depositionID} );
							httpsSocketIO.sockets.socket( _user.uid ).leave( depoRoomID );
						} else {
							ConsoleLog( 'UserID:', userID, 'not found or not attending deposition' );
						}
					}
				}
			} else {
				console.log( 'DepositionID:', depositionID, 'not found or not a demo' );
			}
		} else {
			if( typeof depositions[depositionID] !== 'undefined' && sessionIsDemo( depositions[depositionID].class ) ) {
				//reset redis key
				ConsoleLog( 'resetDeposition -- removing demo owner key' );
				rClient.del( 'demo:' + depositionID, function( err ) {
					if( err ) ConsoleLog( err );
				} );

				var _depo = depositions[depositionID];

				//force per user deselectDeposition
				for( var u in _depo.users ) {
					if( _depo.users.hasOwnProperty( u ) && typeof users[u] !== 'undefined' ) {
						var _user = users[u];
						_user.depositionID = null;
						_user.save();
					}
				}
				ConsoleLog( 'Notify Room:', depoRoomID, 'to vacate deposition' );
				httpsSocketIO.sockets.in( depoRoomID ).emit( 'vacate_deposition', {"depositionID":depositionID} );
				closeDeposition( depositionID );
				lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'closeDeposition', args:depositionID}} ) );
			}
		}
		sendOk( response );
	} );
}

/**
 * Set Demo Deposition Owner
 * @param {Object} request
 * @param {Object} response
 */
function setDemoDepositionOwner( request, response ) {
	console.log("\n[200]", request.method, "to", request.url );
	request.on( 'data', function( chunk ) {
		console.log( "Received body data:", chunk.toString() );
		var object = JSON.parse( chunk.toString() );
		if( typeof depositions[object.args.depositionID] !== 'undefined' && sessionIsDemo( depositions[object.args.depositionID].class ) ) {
			var depositionID = parseInt( object.args.depositionID );
			if( typeof object === 'object' && object.hasOwnProperty( 'args' ) && typeof object.args === 'object' && object.args.hasOwnProperty( 'ownerID' ) ) {
				var deposition = depositions[depositionID];
				if( typeof users[deposition.ownerID] !== 'undefined' ) {
					var owner = users[deposition.ownerID];
					if( owner.depositionID && owner.depositionID === depositionID ) {
						ConsoleLog( 'Demo deposition owner changed, previous owner needs to vacate.\nDepositionID:', depositionID, 'previous Owner:', owner.id, 'new Owner:', object.args.ownerID );
						httpsSocketIO.sockets.socket( owner.uid ).emit( 'vacate_deposition', {"depositionID":depositionID} );
					}
				}
				ConsoleLog( 'setDemoDepositionOwner:', deposition.class, deposition.ownerID, 'to', object.args.ownerID );
				deposition.ownerID = parseInt( object.args.ownerID );
				deposition.speakerID = deposition.ownerID;
				deposition.save();
			}
		}
	} );
	request.on('end', function(){ sendOk( response ); } );
}

function filesShared( request, response ) {
	ConsoleLog( "[200]", request.method, "to", request.url );
	var allChunks = '';

	request.on( 'data', function( chunk ) {
		allChunks += chunk.toString();
	} );

	request.on( 'end', function() {
		var object = JSON.parse( allChunks );
		var params = object.args;
//		console.log( '--- receivers: ', params.receivers );
//		console.log( '--- deposition: ', params.depositionID );
		if ( typeof params.receivers !== 'undefined' && typeof params.depositionID !== 'undefined' ) {
			var depositionID = parseInt( params.depositionID );
			if( typeof depositions[depositionID] !== 'undefined' ) {
				var _depo = depositions[depositionID];
				for( var i=0; i<params.receivers.length; ++i ) {
					var receiver = params.receivers[i];
					var receiverUserID = parseInt( receiver.userID ? receiver.userID : receiver.guestID );
//					console.log( '--- receivers[' + i + ']: ' + receiverUserID );
					if( typeof users[receiverUserID] !== 'undefined' && typeof _depo.users[receiverUserID] !== 'undefined' && _depo.users[receiverUserID] ) {
						ConsoleLog( 'receiver[' + receiverUserID + '].socket.emit: files_shared' );
						//_depo.users[receiverUserID].socket.emit( 'files_shared', {deposition: {id: _depo.id}, files: receiver.files, folder: receiver.folder, sharedBy: _depo.users[params.sharedBy].info()} );
						var _sharedBy = users[params.sharedBy].info();
						httpsSocketIO.sockets.socket( users[receiverUserID].uid ).emit( 'files_shared', {deposition: {id: depositionID}, files: receiver.files, folder: receiver.folder, sharedBy: _sharedBy} );
					}
				}
			}
		}
		sendOk( response );
	} );
}

function sharedObject( request, response, eventName ) {
	ConsoleLog( "[200]", request.method, "to", request.url );
	var allChunks = '';

	request.on( 'data', function( chunk ) {
		allChunks += chunk.toString();
	} );

	request.on( 'end', function() {
		var object = JSON.parse( allChunks );
		var params = object.args;
//		console.log( '--- receivers: ', params.receivers );
//		console.log( '--- deposition: ', params.depositionID );
		if ( typeof params.receivers !== 'undefined' && typeof params.depositionID !== 'undefined' ) {
			var depositionID = parseInt( params.depositionID );
			if( typeof depositions[depositionID] !== 'undefined' ) {
				var _depo = depositions[depositionID];
				for( var i=0; i<params.receivers.length; ++i ) {
					var receiver = params.receivers[i];
					var receiverUserID = parseInt( receiver.userID ? receiver.userID : receiver.guestID );
//					console.log( '--- receivers[' + i + ']: ' + receiverUserID );
					if( typeof users[receiverUserID] !== 'undefined' && typeof _depo.users[receiverUserID] !== 'undefined' && _depo.users[receiverUserID] ) {
						ConsoleLog( 'receiver[' + receiverUserID + '].socket.emit: ' + eventName );
						//_depo.users[receiverUserID].socket.emit( 'files_shared', {deposition: {id: _depo.id}, files: receiver.files, folder: receiver.folder, sharedBy: _depo.users[params.sharedBy].info()} );
						var _sharedBy = users[params.sharedBy].info();
						httpsSocketIO.sockets.socket( users[receiverUserID].uid ).emit( eventName, {deposition: {id: depositionID}, files: receiver.files, folder: receiver.folder, sharedBy: _sharedBy} );
					}
				}
			}
		}
		sendOk( response );
	} );
}

function sharingAborted( request, response ) {
	console.log( "\n[200]", request.method, "to", request.url );
	request.on( 'data', function( chunk ) {
		var object = JSON.parse( chunk.toString() );
		console.log( "******************************************");
		console.log( "Received body data:", chunk.toString() );
		console.log( 'EVENT:', object.name );

		var params = object.args;

		console.log( 'recievers:', params.recievers );
		console.log( 'deposition:', params.depositionID );

		for( var i=0; i<params.recievers.length; i++ ) {
			var reciever = params.recievers[i];
			var receiverUserId = reciever.userID;
			if ( !receiverUserId ) receiverUserId = reciever.guestID;
			console.log( 'recieverID:', receiverUserId );
			if( typeof users[receiverUserId] !== 'undefined' ) {
				//users[receiverUserId].socket.emit('sharing_canceled', {deposition:{id:params.depositionID}, files:reciever.files, sharedBy:users[params.abortedBy].info()} );
				httpsSocketIO.sockets.socket( users[receiverUserId].uid ).emit( 'sharing_canceled', {deposition:{id:params.depositionID}, files:reciever.files, sharedBy:users[params.abortedBy].info()} );
			}
		}
		console.log( "******************************************" );
	} );
	request.on( 'end', function(){ sendOk( response ); } );
}

function selectComplete( request, response ) {
	console.log( "\n[200]", request.method, "to", request.url );
	request.on( 'data', function(chunk) {
		console.log( "Received body data:", chunk.toString() );
		var object = JSON.parse( chunk.toString() );
		if( depositions[object.args.depositionID] ) {
			var depoRoomID = depositions[object.args.depositionID].depoRoomID;
			httpsSocketIO.sockets.in( depoRoomID ).emit( 'select_complete', {deposition:depositions[object.args.depositionID].info()} );
		}
	});
	request.on( 'end', function(){ sendOk( response ); } );
}

// error handlers

function notImplemented( request, response ) {
	console.log( "\n[501]", request.method, "to", request.url );
	if( config.https.responseMode === 'HTML' ) {
		response.writeHead( 501, "Not implemented", {"Content-Type": "text/html", "Strict-Transport-Security": "max-age=15768000"} );
		response.end( "<html><head><title>501 - Not implemented</title></head><body><h1>Not implemented!</h1></body></html>" );
	} else {
		response.writeHead( 501, "Not implemented", {"Content-Type": "application/json", "Strict-Transport-Security": "max-age=15768000"} );
		response.end( JSON.stringify( {success:false, error:'Not implemented', code:501} ) );
	}
}

function notFound( request, response ) {
	console.log( "\n[404]", request.method, "to", request.url );
	if( config.https.responseMode === 'HTML' ) {
		response.writeHead( 404, "Not found", {"Content-Type": "text/html", "Strict-Transport-Security": "max-age=15768000"} );
		response.end( '<html><head><title>404 - Not found</title></head><body><h1>Not found.</h1></body></html>' );
	} else {
		response.writeHead( 404, 'Not found', {"Content-Type": "application/json", "Strict-Transport-Security": "max-age=15768000"} );
		response.end( JSON.stringify( {success:false, error:'Not found', code:404} ) );
	}
}

// ok
function sendOk( response ) {
    if( config.https.responseMode === 'HTML' ) {
        response.writeHead( 200, "OK", {"Content-Type": "text/html", "Strict-Transport-Security": "max-age=15768000"} );
        response.end();
    } else {
        response.writeHead( 200, "OK", {"Content-Type": "application/json", "Strict-Transport-Security": "max-age=15768000"} );
        response.end( JSON.stringify( {success:true} ) );
    }
}


function depositionLink( request, response ) {
    console.log( "\n[200]", request.method, "to", request.url );
    request.on( 'data', function( chunk ) {
        var object = JSON.parse( chunk.toString() );
		for( var u in users ) {
 			if ( users[u] && object.args.clientID === users[u].clientid ) {
  				console.log( "client:", users[u].id, "|", users[u].name );
                //users[u].socket.emit('deposition_link', {"targetDepositionID":object.args.targetDepositionID} );
                httpsSocketIO.sockets.socket( users[u].uid ).emit( 'deposition_link', {"targetDepositionID":object.args.targetDepositionID} );
 			}
 		}
    } );
	request.on( 'end', function(){ sendOk( response ); } );
}

// TODO
function attendeeKick( request, response ) {
    ConsoleLog( "[200]", request.method, "to", request.url );
	var allChunks = '';
	request.on( 'data', function( chunk ) {
		allChunks += chunk.toString();
	} );
    request.on( 'end', function() {
	    var object = JSON.parse( allChunks.toString() );
  	    console.log( "-----", allChunks.toString() );
		if ( object.args.depositionID ) {
			var deposition = depositions[object.args.depositionID];
			var user = (object.args.guestID) ? users[object.args.guestID] : users[object.args.userID];
 			if ( typeof user !== 'undefined' && typeof deposition !== 'undefined' ) {
				user.active = false;
				user.save();
				delete deposition.users[user.id];
				deposition.save();
				console.log( "emit kick to user\n=================" );
				console.log( user.info() );
				console.log( "=================" );
				//user.socket.emit( 'attendee_kick', {"depositionID":object.args.depositionID, "userID":user.id, "permanently":object.args.ban} );
				httpsSocketIO.sockets.socket( user.uid ).emit( 'attendee_kick', {"depositionID":object.args.depositionID, "userID":user.id, "permanently":object.args.ban} );
				httpsSocketIO.sockets.socket( user.uid ).disconnect();

				//remove shadow users
				for( var roomID in deposition.rooms ) {
					if( deposition.rooms.hasOwnProperty( roomID ) && typeof rooms[roomID] !== 'undefined' ) {
						ConsoleLog( 'kicking', user.id, 'from', roomID );
						var room = rooms[roomID];
						var _user = users[user.id];
						if( _user instanceof User ) {
							room.removeUser( user );
							room.save();
						}
					}
				}
				var userInfo = user.info();
				console.log( "sockets.in( ", deposition.depoRoomID, " ).emit( 'user_kicked',", JSON.stringify( userInfo ), ")" );
				httpsSocketIO.sockets.in( deposition.depoRoomID ).emit( 'user_kicked', {"user":userInfo} );
			}
		}
		console.log( "=================" );
		sendOk( response );
    } );
}


function depositionSetSpeaker( request, response ) {
	ConsoleLog( "[200]", request.method, "to", request.url );

	var allChunks = '';

	request.on( 'data', function( chunk ) {
		allChunks += chunk.toString();
	} );

	request.on( 'end', function(){
		var object = JSON.parse( allChunks.toString() );
		console.log( "-----", allChunks.toString() );

		if ( object.args.depositionID ) {
			var mDeposition = depositions[object.args.depositionID];
			if( mDeposition instanceof Deposition ) {
				ConsoleLog( '----- Deposition:', mDeposition.id );
				mDeposition.speakerID = parseInt( object.args.newSpeakerID );
				mDeposition.save();

				//check for presentation
				if( typeof presentations[mDeposition.id] !== 'undefined' ) {
					var presentation = presentations[mDeposition.id];
					ConsoleLog( 'speaker changed -- endPresentation', mDeposition.id, presentation.roomID );
					presentation.deleteAllAnnotations( function() {
						rClient.del( presentation.rKey, function( err ) {
							if( err ) ConsoleLog( err );
							httpsSocketIO.sockets.in( presentation.roomID ).emit( 'endPresentation' );
							delete presentations[mDeposition.id];
							lPub.publish( 'metadata', JSON.stringify( {"nodeId":nodeId, "args":{"event":"Presentation.end", "args":mDeposition.id}} ) );
						} );
					} );
				}

				var witnesses = [];
				for( var userID in mDeposition.users ) {
					if( mDeposition.users.hasOwnProperty( userID ) && typeof users[userID] !== 'undefined' ) {
						if( typeof users[userID].userType !== 'undefined' && (users[userID].userType === 'W' || users[userID].userType === 'WM') ) {
							witnesses.push( users[userID].info() );
						}
					}
				}

				var info = {
					"depositionID":object.args.depositionID,
					"newSpeakerID":object.args.newSpeakerID,
					"oldSpeakerID":object.args.oldSpeakerID,
					"exhibitTitle":object.args.exhibitTitle,
					"exhibitSubTitle":object.args.exhibitSubTitle,
					"exhibitDate":object.args.exhibitDate,
					"exhibitXOrigin":object.args.exhibitXOrigin,
					"exhibitYOrigin":object.args.exhibitYOrigin,
					"witnesses":witnesses
				};
				httpsSocketIO.sockets.in( mDeposition.depoRoomID ).emit( 'deposition_setspeaker', info );
			}
		}
		sendOk( response );
	} );
}

function depositionUploadedFiles( request, response ) {
	console.log("\n[200]", request.method, "to", request.url );
	var allChunks = '';

	request.on( 'data', function( chunk ) { allChunks += chunk.toString(); } );

	request.on( 'end', function(){
		var object = JSON.parse( allChunks );
		console.log("-----", allChunks.toString() );
		var folderID = object.args.folderID;
		var folderName = object.args.folderName;
		var notifyUsers = object.args.notifyUsers;
		var depositionID = object.args.depositionID;
		var byUserID = object.args.byUserID;
		if (object.args.depositionParentID) depositionID = object.args.depositionParentID;
		var mDeposition = depositions[depositionID];
		var attendee;

		if( mDeposition instanceof Deposition ) {
			console.log('----- Deposition:', mDeposition.id, 'users:', Object.keys( mDeposition.users ).length, 'folder: (', folderID, ') ', folderName );

			for( n=0; n<notifyUsers.length; ++n ) {
				var notifyUserID = notifyUsers[n];
				console.log( '----- looking for:', notifyUserID, 'as attendee' );

				attendee = users[notifyUserID];
				if( typeof attendee !== 'undefined' ) {
					console.log( '----- found attendee (m): (', attendee.id, ')', attendee.name );
					//attendee.socket.emit( 'deposition_uploaded_files', {"depositionID": object.args.depositionID, "folderID": folderID, "folderName": folderName} );
					var uploadInfo = {"depositionID":object.args.depositionID, "folderID":folderID, "folderName":folderName, "byUserID":byUserID};
					ConsoleLog( 'deposition_uploaded_files', uploadInfo );
					httpsSocketIO.sockets.socket( attendee.uid ).emit( 'deposition_uploaded_files', uploadInfo );
				}
			}
		} else {
			console.log( '----- Deposition not found by ID:', depositionID );
		}

		sendOk( response );
	} );
}

function depositionDeletedFile( request, response ) {
	ConsoleLog( "[200]", request.method, "to", request.url );
	var allChunks = '';

	request.on( 'data', function( chunk ) {
		allChunks += chunk.toString();
	} );

	request.on( 'end', function() {
		var object = JSON.parse( allChunks );
//		console.log( "-----", allChunks );
		if( typeof object === 'object' && object.hasOwnProperty( 'args' ) && typeof object.args === 'object' && object.args.hasOwnProperty( 'users' ) ) {
			var notifyUsers = object.args.users;
			for( var userID in notifyUsers ) {
				if( notifyUsers.hasOwnProperty( userID ) ) {
					ConsoleLog( 'depositionDeletedFile; Looking for UserID:', userID );
					if( typeof users[userID] !== 'undefined' && users[userID] instanceof User ) {
						var _user = users[userID];
						var userArgs = notifyUsers[userID];
						ConsoleLog( 'depositionDeletedFile; emit to', _user.id, '"' + _user.name + '"', userArgs );
						httpsSocketIO.sockets.socket( _user.uid ).emit( 'deposition_deleted_file', userArgs );
					} else {
						ConsoleLog( 'depositionDeletedFile; Invalid or Unknown UserID:', userID );
					}
				}
			}
		} else {
			ConsoleLog( 'depositionDeletedFile; Invalid arguments:', object );
		}
	} );

	sendOk( response );
}

function depositionDeletedFolder( request, response ) {
	ConsoleLog( "[200]", request.method, "to", request.url );
	var allChunks = '';

	request.on( 'data', function( chunk ) {
		allChunks += chunk.toString();
	} );

	request.on( 'end', function() {
		var object = JSON.parse( allChunks );
//		console.log( "-----", allChunks );
		if( typeof object === 'object' && object.hasOwnProperty( 'args' ) && typeof object.args === 'object' && object.args.hasOwnProperty( 'users' ) ) {
			var notifyUsers = object.args.users;
			for( var userID in notifyUsers ) {
				if( notifyUsers.hasOwnProperty( userID ) ) {
					ConsoleLog( 'depositionDeletedFolder; Looking for UserID:', userID );
					if( typeof users[userID] !== 'undefined' && users[userID] instanceof User ) {
						var _user = users[userID];
						var userArgs = notifyUsers[userID];
						ConsoleLog( 'depositionDeletedFolder; emit to', _user.id, '"' + _user.name + '"', userArgs );
						httpsSocketIO.sockets.socket( _user.uid ).emit( 'deposition_deleted_folder', userArgs );
					} else {
						ConsoleLog( 'depositionDeletedFolder; Invalid or Unknown UserID:', userID );
					}
				}
			}
		} else {
			ConsoleLog( 'depositionDeletedFolder; Invalid arguments:', object );
		}
	} );

	sendOk( response );
}

function didChangeCustomSort( request, response ) {
	console.log("\n[200]", request.method, "to", request.url );
	var allChunks = '';

	request.on( 'data', function( chunk ) { allChunks += chunk.toString(); } );

	request.on( 'end', function() {
		var object = JSON.parse( allChunks );
		console.log("-----", allChunks.toString() );
		var sessionID = (typeof object.args.sessionID !== "undefined") ? object.args.sessionID : false;
		var sessionParentID = (typeof object.args.sessionParentID !== "undefined") ? object.args.sessionParentID : sessionID;
		var sortObject = (typeof object.args.sortObject === "string") ? object.args.sortObject : false;
		var folderID = (typeof object.args.folderID !== "undefined") ? object.args.folderID : false;
		var notifyUsers = (typeof object.args.notifyUsers !== "undefined") ? object.args.notifyUsers : [];
		if( !sessionID || (sortObject !== "Folders" && sortObject !== "Files") || notifyUsers.length === 0 || (sortObject === "Files" && !folderID) ) {
			ConsoleLog( "didChangeCustomSort; invalid or missing parameters:", object.args );
		}
		var mSession = depositions[sessionParentID];
		var attendee;
		if( mSession && mSession instanceof Deposition ) {
			console.log( '----- Session:', mSession.id, 'users:', Object.keys( mSession.users ).length );
			for( n=0; n<notifyUsers.length; ++n ) {
				var notifyUserID = notifyUsers[n];
				console.log( '----- looking for:', notifyUserID, 'as session attendee' );
				attendee = users[notifyUserID];
				if( typeof attendee !== 'undefined' ) {
					//attendee.socket.emit( 'deposition_uploaded_files', {"depositionID": object.args.depositionID, "folderID": folderID, "folderName": folderName} );
					var userInfo = {
						"sessionID": sessionID,
						"sortObject": sortObject,
						"folderID": folderID
					};
					ConsoleLog( 'socket.emit: session_changed_custom_sort', attendee.id, attendee.name, userInfo );
					httpsSocketIO.sockets.socket( attendee.uid ).emit( 'session_changed_custom_sort', userInfo );
				}
			}
		} else {
			console.log( '----- Session not found by ID:', sessionParentID );
		}
		sendOk( response );
	} );
}

function witnessUploadFiles( request, response ) {
	console.log( "\n[200]", request.method, "to", request.url );
	var allChunks = '';

	request.on( 'data', function( chunk ) { allChunks += chunk.toString(); } );

    request.on( 'end', function(){
		var object = JSON.parse( allChunks );
  	    console.log("-----", allChunks.toString() );
		var folderID = object.args.folderID;
		var folderName = object.args.folderName;
		var notifyUsers = object.args.notifyUsers;
		var mDeposition = depositions[object.args.depositionID];

		if( mDeposition instanceof Deposition ) {
			console.log( '----- Deposition:', mDeposition.id, 'folder: (', folderID, ')', folderName );

			for( n=0; n<notifyUsers.length; ++n ) {
				var notifyUserID = notifyUsers[n];
				console.log( '----- looking for:', notifyUserID, 'as attendee' );

				var attendee = users[notifyUserID];
				if ( typeof attendee !== 'undefined' ) {
					console.log( '----- found attendee: (', attendee.id, ')', attendee.name );
					//attendee.socket.emit( 'witness_uploaded', {"depositionID": mDeposition.id, "folderID": folderID, "folderName": folderName} );
					httpsSocketIO.sockets.socket( attendee.uid ).emit( 'witness_uploaded', {"depositionID":object.args.targetDepositionID , "folderID": folderID, "folderName": folderName} );
				}
			}
		} else {
			console.log( '----- Deposition not found by ID:', object.args.depositionID );
		}

		sendOk( response );
	} );
}


function casesUpdated( request, response ) {
	ConsoleLog( "[200]", request.method, "to", request.url );
	var allChunks = '';
	request.on( 'data', function( chunk ) {
		allChunks += chunk.toString();
	} );
    request.on( 'end', function() {
		var object = JSON.parse( allChunks );
  	    console.log( '----', allChunks.toString() );
		var notifyUsers = object.args.notifyUsers;
		for( var n in notifyUsers ) {
			if( notifyUsers.hasOwnProperty( n ) ) {
				var notifyUserID = parseInt( notifyUsers[n] );
				var _user = users[notifyUserID];
				if( typeof _user !== 'undefined' ) {
					httpsSocketIO.sockets.socket( _user.uid ).emit( 'cases_updated' );
					ConsoleLog( 'user:', _user.id, '"' + _user.name + '", has been notified. (cases_updated)' );
				} else {
					console.log( '----- userID:', notifyUserID, 'was not found.' );
				}
			}
		}
		sendOk( response );
		ConsoleLog( "===== completed (casesUpdated) =====\n" );
	} );
}

function fileModified( request, response ) {
	ConsoleLog( "[200]", request.method, "to", request.url );
	var allChunks = '';

	request.on( 'data', function( chunk ) {
		allChunks += chunk.toString();
	} );

	request.on( 'end', function() {
		var object = JSON.parse( allChunks );
//		console.log( "-----", allChunks );
		if( typeof object === 'object' && object.hasOwnProperty( 'args' ) && typeof object.args === 'object' && object.args.hasOwnProperty( 'users' ) ) {
			var notifyUsers = object.args.users;
			var byUser = (typeof object.args.byUser === "string" ? object.args.byUser : "");
			var byUserID = (typeof object.args.byUserID === "number" ? object.args.byUserID : 0);
			for( var userID in notifyUsers ) {
				if( notifyUsers.hasOwnProperty( userID ) ) {
					ConsoleLog( 'fileModified; Looking for UserID:', userID );
					if( typeof users[userID] !== 'undefined' && users[userID] instanceof User ) {
						var _user = users[userID];
						var eventData = {"fileID": notifyUsers[userID], "byUserID": byUserID, "byUser": byUser};
						ConsoleLog( 'fileModified; emit to', _user.id, '"' + _user.name + '"', eventData );
						httpsSocketIO.sockets.socket( _user.uid ).emit( 'file_modified', eventData );
					} else {
						ConsoleLog( 'fileModified; Invalid or Unknown UserID:', userID );
					}
				}
			}
		} else {
			ConsoleLog( 'fileModified; Invalid arguments:', object );
		}
	} );

	sendOk( response );
}

/**
 * Close Deposition -- purge chat services
 * @param {Integer} depoID
 */
function closeDeposition( depoID ) {
	depoID = parseInt( depoID );
	if( typeof depositions[depoID] !== 'undefined' && depositions[depoID] instanceof Deposition ) {
		ConsoleLog( 'Closing Deposition:', depoID );
		var _depo = depositions[depoID];
		for( var roomID in _depo.rooms ) {
			if( _depo.rooms.hasOwnProperty( roomID ) ) {
				if( typeof rooms[roomID] !== 'undefined' && rooms[roomID] instanceof Room ) {
					ConsoleLog( 'Closing Room:', roomID );
					for( var userID in rooms[roomID].users ) {
						if( rooms[roomID].users.hasOwnProperty( userID ) ) {
							var _user = users[userID];
							if( typeof _user === 'undefined' || _user === null ) {
								ConsoleLog( 'Closing Room:', roomID, 'for user:', userID, '-- not found!' );
								continue;
							}
							ConsoleLog( 'Closing Room:', roomID, 'for user:', userID );
							httpsSocketIO.sockets.socket( _user.uid ).leave( roomID );
						}
					}
					rClient.del( rooms[roomID].rKey, function( err ) {
						if( err ) ConsoleLog( err );
					} );
					delete rooms[roomID];
					delete _depo.rooms[roomID];
				}
			}
		}
		var transcript = transcripts[depoID];
		if( typeof transcript !== 'undefined' && transcript !== null ) {
			transcript.abort( true );
			setTimeout( function() {
				rClient.del( transcript.rKey, function( err ) {
					if( err ) ConsoleLog( err );
				} );
				delete transcripts[depoID];
			}, 300 );
		}

		rClient.del( depositions[depoID].rKey, function( err ) {
			if( err ) ConsoleLog( err );
		} );
		delete depositions[depoID];
	}
}


function getPresentationInfo( request, response ) {
	ConsoleLog( "[200]", request.method, "to", request.url );
	var allChunks = '';

	request.on( 'data', function( chunk ) {
		allChunks += chunk.toString();
	} );

    request.on( 'end', function() {
		ConsoleLog( allChunks );
		var args = JSON.parse( allChunks );
		var _depo = depositions[args.depositionID];
		var userID = args.userID;
		var _user = users[args.userID];
		var presentation = presentations[args.depositionID];
		if( _depo && _depo instanceof Deposition ) {
			if( _user && _user instanceof User ) {
				if( presentation && presentation instanceof Presentation ) {
					var _room = rooms[presentation.roomID];
					if( _room && _room instanceof Room ) {
						if( typeof _room.users[userID] !== 'undefined' ) {
							response.end( JSON.stringify( {success:true, presentationInfo:presentation.info()} ) );
						} else {
							response.end( JSON.stringify( {success:false, error:"Access denied"} ) );
						}
					} else {
						response.end( JSON.stringify( {success:false, error:"Unknown room ID:" + presentation.roomID} ) );
					}
				}
			} else {
				response.end( JSON.stringify( {success:false, error:"Unknown user ID:" + args.userID} ) );
			}
		} else {
			response.end( JSON.stringify( {success:false, error:"Unknown deposition ID:" + args.depositionID} ) );
		}
	} );
}

function presentationPublish( request, response ) {
	ConsoleLog( "[200]", request.method, "to", request.url );
	var allChunks = '';

	request.on( 'data', function( chunk ) {
		allChunks += chunk.toString();
	} );

	request.on( 'end', function() {
		ConsoleLog( allChunks );
		var data = JSON.parse( allChunks );
		if( typeof data === 'undefined' || data === null || typeof data.args === 'undefined' || data.args === null ) {
			response.end( JSON.stringify( {success:false, error:"Bad arguments"} ) );
		}
		var args = data.args;
		var _depo = depositions[args.sessionID];
		var presentation = presentations[args.sessionID];
		if( _depo && _depo instanceof Deposition ) {
			if( presentation && presentation instanceof Presentation ) {
				lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Presentation.publish', args:_depo.id}} ) );
				response.end( JSON.stringify( {success:true} ) );
			} else {
				ConsoleLog( 'missing presentation in:', data );
				response.end( JSON.stringify( {success:false, error:"Unknown presentation: " + args.sessionID} ) );
			}
		} else {
			ConsoleLog( 'missing sessionID in:', data );
			response.end( JSON.stringify( {success:false, error:"Unknown session ID: " + args.sessionID} ) );
		}
	} );
}

function authorizeWitness( request, response ) {
	ConsoleLog( "[200]", request.method, "to", request.url );
	var allChunks = '';

	request.on( 'data', function( chunk ) {
		allChunks += chunk.toString();
	} );

    request.on( 'end', function() {
		ConsoleLog( allChunks );
		var data = JSON.parse( allChunks );
		var depositionID = parseInt( data.args.depositionID );
		var witnessID = parseInt( data.args.witnessID );
		var errMsg;
		if( typeof users[witnessID] === 'undefined' ) {
			errMsg = 'Witness not found';
			ConsoleLog( errMsg, witnessID );
			response.end( JSON.stringify( {success:false, error:errMsg} ) );
			return;
		}
		var witness = users[witnessID];
		witness.userType = 'W';
		witness.isTempWitness = false;
		witness.save();

		var allUsers = Object.keys( users );
		for( var u=0; u<allUsers.length; ++u ) {
			var _userID = allUsers[u];
			var _user = users[_userID];
			if( typeof _user === 'undefined' || _user === null ) {
				continue;
			}
			if( typeof _user.depositionID === 'undefined' || _user.depositionID === null || _user.depositionID !== depositionID ) {
				continue;
			}
			if( typeof _user.isTempWitness === 'undefined' || !_user.isTempWitness ) {
				continue;
			}
			ConsoleLog( 'rejectWitnesses;', depositionID, ' witness:', _user.id, _user.uid );
			httpsSocketIO.sockets.socket( _user.uid ).emit( 'rejected_witness', {"depositionID":depositionID, "witnessID":_user.id} );
			httpsSocketIO.sockets.socket( _user.uid ).disconnect();
			delete users[_user.id];
		}

		httpsSocketIO.sockets.socket( witness.uid ).emit( 'authorized_witness', {"depositionID":depositionID, "witnessID":witnessID} );

		response.end( JSON.stringify( {success:true} ) );
	} );
}

function status( request, response ) {
	ConsoleLog( "[200]", request.method, "to", request.url );
	if( config.https.responseMode === 'HTML' ) {
        response.writeHead( 200, "OK", {"Content-Type":'text/html', "Strict-Transport-Security": "max-age=15768000"} );
        response.end();
    } else {
        response.writeHead( 200, "OK", {"Content-Type":'application/json', "Strict-Transport-Security": "max-age=15768000"} );
		var _depos = [];
		var depoKeys = Object.keys( depositions );
		for( var depoIdx in depoKeys ) {
			if( depoKeys.hasOwnProperty( depoIdx ) ) {
				var depoID = parseInt( depoKeys[depoIdx] );
				if( typeof depositions[depoID] !== 'undefined' && depositions[depoID] instanceof Deposition ) {
					_depos.push( depositions[depoID].bundle() );
				}
			}
		}
//		var _rooms = [];
//		for( var roomID in rooms ) {
//			if( rooms.hasOwnProperty( roomID ) ) {
//				var _room = rooms[roomID].info();
//				_room.numMessages = messages[roomID].length;
//				_rooms.push( _room );
//			}
//		}
		var status = {
			"Total Depositions": Object.keys( depositions ).length,
			"Depositions": _depos,
			"Total Rooms": Object.keys( rooms ).length,
			"Rooms": rooms,
//			"_Rooms": _rooms,
			"SocketIO Rooms": httpsSocketIO.sockets.manager.rooms,
			"Total Users": Object.keys( users ).length,
			"Users": users,
			"Transcripts":transcripts,
			"Presentations":presentations
		};
		var detail = {
			"remoteAddress": request.headers['x-forwarded-for'] || request.headers['x-real-ip'] || request.connection.remoteAddress || request.socket.remoteAddress || request.connection.socket.remoteAddress,
			"Total Depositions": Object.keys( depositions ).length,
			"Total Users": Object.keys( users ).length,
			"Total Rooms": Object.keys( rooms ).length
		};
		logToFile( null, 'Chat Server status: ' + JSON.stringify( detail ) );
        response.end( JSON.stringify( status ) );
    }
}

function requestConsoleLog( request, response ) {
	ConsoleLog( "[200]", request.method, "to", request.url );
	var urlInfo = url.parse( request.url, true );
	var userID = parseInt( urlInfo.query.userID );
	if( typeof users[userID] !== 'undefined' ) {
		var user = users[userID];
		logToFile( null, 'Chat Server requestConsoleLog; userID: ' + user.id );
		//user.socket.emit( 'request_console_log' );
		httpsSocketIO.sockets.socket( user.uid ).emit( 'request_console_log' );
		response.end( JSON.stringify( {"success":true} ) );
		return;
	} else {
		console.log( 'requestConsoleLog; User not found!' );
	}
	response.end( JSON.stringify( {"success":false} ) );
}

function enableConsoleLog( request, response ) {
	ConsoleLog( "[200]", request.method, "to", request.url );
	var urlInfo = url.parse( request.url, true );
	var userID = parseInt( urlInfo.query.userID );
	if( typeof users[userID] !== 'undefined' ) {
		var user = users[userID];
		logToFile( null, 'Chat Server enableConsoleLog; userID: ' + user.id + ' enabled: ' + urlInfo.query.enabled );
		//user.socket.emit( 'enable_console_log', {"enabled":urlInfo.query.enabled} );
		httpsSocketIO.sockets.socket( user.uid ).emit( 'enable_console_log', {"enabled":urlInfo.query.enabled} );
		response.end( JSON.stringify( {"success":true} ) );
		return;
	} else {
		console.log( 'enableConsoleLog; User not found!' );
	}
	response.end( JSON.stringify( {"success":false} ) );
}

function clearConsoleLog( request, response ) {
	ConsoleLog( "[200]", request.method, "to", request.url );
	var urlInfo = url.parse( request.url, true );
	var userID = parseInt( urlInfo.query.userID );
	if( typeof users[userID] !== 'undefined' ) {
		var user = users[userID];
		logToFile( null, 'Chat Server clearConsoleLog; userID: ' + user.id );
		//user.socket.emit( 'clear_console_log' );
		httpsSocketIO.sockets.socket( user.uid ).emit( 'clear_console_log' );
		response.end( JSON.stringify( {"success":true} ) );
		return;
	} else {
		console.log( 'clearConsoleLog; User not found!' );
	}
	response.end( JSON.stringify( {"success":false} ) );
}

/**
 *
 * @param {String} v "1.4" -- current version
 * @param {String} r "1.5" -- version required
 * @returns {Boolean}
 */
function compareVersions( v, r ) {
	if( typeof v !== 'string' || v.indexOf( '.' ) === -1 || typeof r !== 'string' || r.indexOf( '.' ) === -1 ) return false;
	//current version
	var _v = v.split( '.' );
	_v[0] = parseInt( _v[0] );	//major
	_v[1] = parseInt( _v[1] );	//minor
	// required version
	var _r = r.split( '.' );
	_r[0] = parseInt( _r[0] );	//major
	_r[1] = parseInt( _r[1] );	//minor
	if( !isNaN( _v[0] ) && !isNaN( _r[0] ) && _v[0] >= _r[0] )
	{
		if( !isNaN( _v[1] ) && !isNaN( _r[1] ) && _v[1] >= _r[1] )
			return true;
	}
	return false;
}

/**
 *
 * @param {String} ra remote ip address
 * @returns {Boolean}
 */
function verifyIP( ra ) {
	ConsoleLog( "verifyIP:", ra );
	var wl = config.whitelistIPs;
	if( ra.indexOf( ":" ) >= 0 ) {
		var addrs = ra.split( ":" );
		var ipv4 = addrs[addrs.length - 1];
		if( ipv4 ) {
			ConsoleLog( "verifyIP (IPv4):", ipv4 );
			ra = ipv4;
		}
	}
	var r = ra.split( '.' );
	for( var i=0; i<wl.length; ++i ) {
		var a = wl[i].split( '.' );
		if( (parseInt( a[0] ) === parseInt( r[0] ) || a[0] === '*')
				&& (parseInt( a[1] ) === parseInt( r[1] ) || a[1] === '*')
				&& (parseInt( a[2] ) === parseInt( r[2] ) || a[2] === '*')
				&& (parseInt( a[3] ) === parseInt( r[3] ) || a[3] === '*') ) {
			return true;
		}
	}
	return false;
}

function logToFile( user, message ) {
	var now = new Date();
	var now_ISO2822 = dateFormat(now, 'ddd, dd mmm yyyy HH:MM:ss +0000');
	var msg = {
		"now":now_ISO2822,
		"userID":(user instanceof User && typeof user.id !== 'undefined') ? user.id : 0,
		"sKey":(user instanceof User && typeof user.sKey !== 'undefined') ? user.sKey : "-",
		"message":(typeof message !== 'string') ? JSON.stringify(message) : message
	};
	fs.appendFile( config.logFile, JSON.stringify( msg ) + '\n', function(err){ if( err ) console.log( err ); } );
}

function ConsoleLog() {
	var now = new Date();
	var logStr = now.toISOString();
	for( var n in arguments ) {
		if( !arguments.hasOwnProperty( n ) ) continue;
		//console.log( 'arguments', n, arguments[n] );
		if( arguments[n] && typeof arguments[n] === 'object' ) {
			logStr += ' ' + JSON.stringify( arguments[n] );
		} else if( arguments[n] && typeof arguments[n].toString === 'function' ) {
			logStr += ' ' + arguments[n].toString();
		} else {
			logStr += ' ' + arguments[n];
		}
	}
	console.log( logStr );
}
GLOBAL.ConsoleLog = ConsoleLog;


lSub.subscribe( 'metadata' );
lSub.on( 'message', function( channel, jsonData ) {
//	ConsoleLog( 'lSub::onMessage:', channel, jsonData );
	var data = JSON.parse( jsonData );
	if( !data || typeof data.nodeId === 'undefined' || typeof data.args === 'undefined' ) {
		ConsoleLog( 'lSub -- unsupported message', channel, jsonData );
		return;
	}

	//ignore messages from ourselves
	var performOnSelf = ['User.publish','Room.publish','Deposition.publish','Message.publish','Transcript.publish','Transcript.chatter'];
	if( data.nodeId === nodeId && performOnSelf.indexOf( data.args.event ) === -1 ) {
//		ConsoleLog( 'skipping message from same node:', data.args.event );
		return;
	}

	var args = data.args;
	if( !args || typeof args.event === 'undefined' || typeof args.args === 'undefined' ) {
		ConsoleLog( 'lSub -- missing event or args!', channel, jsonData );
		return;
	}

	switch( args.event ) {
		case 'User.publish':
			updateUser( args.args );
			break;
		case 'Deposition.publish':
			updateDeposition( args.args, false );
			break;
		case 'closeDeposition':
			syncCloseDeposition( args.args );
			break;
		case 'Room.publish':
			updateRoom( args.args );
			break;
		case 'Room.close':
			syncRoomClose( args.args );
			break;
		case 'Message.publish':
			updateMessage( args.args );
			break;
		case 'Presentation.publish':
			updatePresentation( args.args );
			break;
		case 'Presentation.end':
			syncPresentationEnd( args.args );
			break;
		case 'Annotation.publish':
			syncPresentationAnnotations( args.args );
			break;
		case "Callout.publish":
			syncPresentationCallouts( args.args );
			break;
		case 'Transcript.publish':
			transcriptPublish( args.args );
			break;
		case 'Transcript.chatter':
			transcriptChatter( args.args );
			break;
		case 'Transcript.streaming':
			transcriptStreaming( args.args );
			break;
		case 'Transcript.reset':
			transcriptReset( args.args );
			break;
		case 'Transcript.abort':
			transcriptAbort( args.args );
			break;
		case 'uncaughtException':
			onSubscribeUncaughtException( args.args );
			break;
		default:
			ConsoleLog( channel, args.event, args.args );
			break;
	}
} );


function updateUser( userID ) {
	ConsoleLog( 'updateUser:', userID );
	var userID = parseInt( userID );
	var user = new User( userID );
	rClient.get( user.rKey, function( err, response ) {
		if( err ) ConsoleLog( err );
		if( response ) {
			var _user = JSON.parse( response );
			if( typeof _user.id !== 'undefined' ) {
				user.assign( _user );
				users[user.id] = user;
			}
		}
	} );
}


function updateRoom( roomID ) {
	ConsoleLog( 'updateRoom:', roomID );
	var room = new Room( roomID );
	rClient.get( room.rKey, function( err, response ) {
		if( err ) ConsoleLog( err );
		if( response ) {
			var _room = JSON.parse( response );
			if( typeof _room.id !== 'undefined' ) {
				room.assign( _room );
				rooms[roomID] = room;
			}
		}
	} );
}


function updateMessage( roomID ) {
	ConsoleLog( 'updateMessage:', roomID );
	var rKey = 'room:' + roomID + ':messages';
	if( typeof messages[roomID] === 'undefined' ) {
		messages[roomID] = [];
	}
	var messageIndex = messages[roomID].length;
	var _roomID = roomID;
	rClient.lrange( rKey, messageIndex, -1, function( msgsErr, msgsResponse ) {
		if( msgsErr ) ConsoleLog( 'Error getting messages for room', msgsErr );
		if( msgsResponse ) {
			ConsoleLog( 'New messages for:', _roomID, 'count:', msgsResponse.length );
			for( var msgIndex in msgsResponse ) {
				if( msgsResponse.hasOwnProperty( msgIndex ) ) {
					var _msg = JSON.parse( msgsResponse[msgIndex] );
					if( _msg.hasOwnProperty( 'roomID' ) ) {
						var message = new Message( _msg.roomID );
						message.assign( _msg );
						messages[_msg.roomID].push( message );
					} else {
						ConsoleLog( _msg );
					}
				}
			}
			ConsoleLog( 'Messages:', _roomID, 'count:', messages[_roomID].length );
		}
	} );
}


function updateDeposition( depositionID, recursive ) {
	ConsoleLog( 'updateDeposition:', depositionID );
	var deposition = new Deposition( depositionID );
	rClient.get( deposition.rKey, function( err, response ) {
		if( err ) ConsoleLog( err );
		if( response ) {
			var _deposition = JSON.parse( response );
			if( typeof _deposition.id !== 'undefined' ) {
				deposition.assign( _deposition );
				depositions[deposition.id] = deposition;
				if( !!recursive ) {
					for( var userID in deposition.users ) {
						if( deposition.users.hasOwnProperty( userID ) ) {
							updateUser( userID );
						}
					}
					for( var roomID in deposition.rooms ) {
						if( deposition.rooms.hasOwnProperty( roomID ) ) {
							updateRoom( roomID );
							updateMessage( roomID );
						}
					}
					updatePresentation( depositionID );
				}
				var hasActiveUsers = false;
				var depoUsers = Object.keys( deposition.users );
				for( var u=0; u<depoUsers.length; ++u ) {
					var _userID = depoUsers[u];
					var isActive = deposition.users[_userID];
					var _user = users[_userID];
					if( typeof _user === 'undefined' || _user === null ) {
						continue;
					}
					//witnesses do not qualify as active
					if( isActive && ['M','G'].indexOf( _user.userType ) !== -1 ) {
						hasActiveUsers = true;
						break;
					}
				}
				if( !hasActiveUsers ) {
					if( typeof transcripts[depositionID] !== 'undefined' && transcripts[depositionID] !== null ) {
						var transcript = transcripts[depositionID];
						if( transcript.isStreamHost ) {
							ConsoleLog( 'killing live transcript, no active attendees' );
							transcript.reset();
						}
					}
				}
			}
		}
	} );
}

function syncCloseDeposition( depositionID ) {
	ConsoleLog( 'syncCloseDeposition:', depositionID );
	if( typeof depositions[depositionID] !== 'undefined' && depositions[depositionID] instanceof Deposition ) {
		closeDeposition( depositionID );
	}
}


function updatePresentation( sessionID, onComplete ) {
	ConsoleLog( 'updatePresentation:', sessionID );
	var presentation =	(typeof presentations[sessionID] !== "undefined" ) ? presentations[sessionID] : new Presentation( sessionID );
	rClient.get( presentation.rKey, function( err, response ) {
		if( err ) ConsoleLog( err );
		if( response ) {
			var _presentation = JSON.parse( response );
			if( typeof _presentation.id !== "undefined" ) {
				presentation.assign( _presentation );
				presentations[sessionID] = presentation;
				presentation.reloadCallouts( null, function() {
					presentation.reloadAnnotations( null, onComplete );
				} );
			}
		}
	} );
}

function syncPresentationEnd( sessionID ) {
	ConsoleLog( 'syncPresentationEnd:', sessionID );
	if( typeof presentations[sessionID] !== 'undefined' && presentations[sessionID] instanceof Presentation ) {
		delete presentations[sessionID];
	} else {
		ConsoleLog( 'Presentation not instance of Presentation:', sessionID, presentations[sessionID] );
	}
}


function syncPresentationAnnotations( args ) {
	ConsoleLog( 'syncPresentationAnnotations:', args.sessionID, args.pageIndex );
	var sessionID = parseInt( args.sessionID );
	var pageIndex = parseInt( args.pageIndex );
	if( typeof presentations[sessionID] !== 'undefined' && presentations[sessionID] instanceof Presentation ) {
		presentations[sessionID].reloadAnnotations( pageIndex );
	}
}

function syncPresentationCallouts( args ) {
	ConsoleLog( "syncPresentationCallouts:", args.sessionID, args.pageIndex );
	var sessionID = parseInt( args.sessionID );
	var pageIndex = parseInt( args.pageIndex );
	if( typeof presentations[sessionID] !== "undefined" && presentations[sessionID] instanceof Presentation ) {
		presentations[sessionID].reloadCallouts( pageIndex );
	}
}

/**
 * Validate Presention Leader (Owner)
 * @param {Integer} currentUserID
 * @param {Integer} depositionID
 * @returns {String|Boolean}
 */
function validatePresentationLeader( currentUserID, depositionID ) {
	var cUser = users[currentUserID];
	if( typeof cUser === 'undefined' || !cUser ) {
		return 'User not found';
	}
	var userID = parseInt( cUser.id );
	depositionID = parseInt( depositionID );
	if( !(depositionID > 0) ) {
		return 'Invalid deposition ID';
	}
	if( typeof depositions[depositionID] !== 'undefined' ) {
		var _depo = depositions[depositionID];
		if( typeof presentations[depositionID] !== 'undefined' ) {
			if( userID !== parseInt( _depo.speakerID ) ) {
				return 'Unauthorized: user is not leader';
			}
			if( _depo.users[userID] === 'undefined' || !cUser.depositionID || parseInt( cUser.depositionID ) !== parseInt( _depo.id ) ) {
				return 'Cannot perform actions for a deposition you are not attending';
			}
			return true;
		} else {
			return 'Unknown presentation';
		}
	}
	return 'Invalid deposition ID';
}

/**
 * Validate Presention Annotator
 * @param {Integer} currentUserID
 * @param {Integer} depositionID
 * @returns {String|Boolean}
 */
function validatePresentationAnnotator( currentUserID, depositionID ) {
	var cUser = users[currentUserID];
	if( typeof cUser === 'undefined' ) {
		return 'User not found';
	}
	var userID = parseInt( cUser.id );
	depositionID = parseInt( depositionID );
	if( !(depositionID > 0) ) {
		return 'Invalid deposition ID';
	}
	if( typeof depositions[depositionID] !== 'undefined' ) {
		var _depo = depositions[depositionID];
		if( typeof presentations[depositionID] !== 'undefined' ) {
			if( userID !== presentations[depositionID].annotatorID ) {
				return 'Unauthorized: user is not annotator';
			}
			if( _depo.users[userID] === 'undefined' || !cUser.depositionID || parseInt( cUser.depositionID ) !== parseInt( _depo.id ) ) {
				return 'Cannot perform actions for a deposition you are not attending';
			}
			return true;
		} else {
			return 'Unknown presentation';
		}
	}
	return 'Invalid deposition ID';
}

function notifySubscribers( subscribers, event, eventData ) {
	if( typeof subscribers !== "object" || subscribers === null ) {
		return;
	}
	for( var u in subscribers ) {
		if( subscribers.hasOwnProperty( u ) ) {
			var userID = parseInt( subscribers[u] );
			if( typeof users[userID] !== 'undefined' && users[userID] instanceof User ) {
				var _user = users[userID];
				ConsoleLog( 'notify user:', userID, 'for event:', event );
				httpsSocketIO.sockets.socket( _user.uid ).emit( event, eventData );
			} else {
				ConsoleLog( 'notify user -- user not found:', userID );
			}
		}
	}
}

/**
 * Initial Annotation
 * @param {Integer} currentUserID
 * @param {Object} args Argument parameters
 * @param {Function} callback
 */
function PresentationInitialAnnotation( currentUserID, args, callback ) {
	ConsoleLog( 'PresentationInitialAnnotation:', args );
	var errMsg;
	var _user = users[currentUserID];
	if( typeof _user === 'undefined' || !_user ) {
		errMsg = 'User not found';
		ConsoleLog( errMsg );
		if( typeof callback === 'function' ) {
			callback( {success:false, error:errMsg} );
		}
		return;
	}
	var depositionID = parseInt( args.depositionID );
	var isValidResult = validatePresentationAnnotator( currentUserID, depositionID );
	if( isValidResult === true ) {
		if( args && args.hasOwnProperty( 'eventData' ) && args.eventData && args.eventData.hasOwnProperty( 'Index' ) && args.eventData.hasOwnProperty( 'Page' ) ) {
			if( typeof presentations[depositionID] !== 'undefined' ) {
				var presentation = presentations[depositionID];
				var pageIndex = parseInt( args.eventData.Page );
				presentation.initialAnnotation( pageIndex, args.eventData.Index, args.eventData, true, function() {
					console.log( "presentation.initialAnnotation; onComplete" );
					updatePresentation( depositionID, function() {
						if( typeof callback === 'function' ) {
							callback( {success:true} );
						}
					} );
				} );
				return;
			} else {
				errMsg = 'Unknown presentation';
				ConsoleLog( 'PresentationInitialAnnotation error:', errMsg );
				if( typeof callback === 'function' ) {
					callback( {success:false, error:errMsg} );
				}
				return;
			}
		} else {
			errMsg = 'Missing event data or index';
			ConsoleLog( 'PresentationInitialAnnotation error:', errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
	} else {
		errMsg = isValidResult;
		ConsoleLog( 'PresentationInitialAnnotation error:', errMsg );
		if( typeof callback === 'function' ) {
			callback( {success:false, error:errMsg} );
		}
		return;
	}
}

/**
 * Add Annotation
 * @param {Integer} currentUserID
 * @param {Object} args Argument parameters
 * @param {Function} callback
 */
function PresentationAddAnnotation( currentUserID, args, callback ) {
	ConsoleLog( 'PresentationAddAnnotation;', args );
	var errMsg;
	var _user = users[currentUserID];
	if( typeof _user === 'undefined' || !_user ) {
		errMsg = 'User not found';
		ConsoleLog( errMsg );
		if( typeof callback === 'function' ) {
			callback( {success:false, error:errMsg} );
		}
		return;
	}
	var depositionID = parseInt( args.depositionID );
	var isValidResult = validatePresentationAnnotator( currentUserID, depositionID );
	if( isValidResult === true ) {
		if( args && args.hasOwnProperty( 'eventData' ) && args.eventData && args.eventData.hasOwnProperty( 'Index' ) && args.eventData.hasOwnProperty( 'Page' ) ) {
			if( typeof presentations[depositionID] !== 'undefined' ) {
				var presentation = presentations[depositionID];
				var pageIndex = parseInt( args.eventData.Page );
				presentation.addAnnotation( pageIndex, args.eventData.Index, args.eventData, true, function() {
					ConsoleLog( "presentation.addAnnotation; onComplete" );
					updatePresentation( depositionID, function() {
						var subscribers = JSON.parse( JSON.stringify( presentation.subscribers ) ); //clone
						var indexOfSelf = subscribers.indexOf( currentUserID );
						if( indexOfSelf >= 0 ) {
							subscribers.splice( indexOfSelf, 1 );
						}
						notifySubscribers( subscribers, 'addAnnotation', args.eventData );
						if( typeof callback === 'function' ) {
							callback( {success:true, undoStackDepth:presentation.undoStack.length} );
						}
					} );
				} );
				return;
			} else {
				errMsg = 'Unknown presentation';
				ConsoleLog( 'PresentationAddAnnotation error:', errMsg );
				if( typeof callback === 'function' ) {
					callback( {success:false, error:errMsg} );
				}
				return;
			}
		} else {
			errMsg = 'Missing event data or index';
			ConsoleLog( 'PresentationAddAnnotation error:', errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
	} else {
		errMsg = isValidResult;
		ConsoleLog( 'PresentationAddAnnotation error:', errMsg );
		if( typeof callback === 'function' ) {
			callback( {success:false, error:errMsg} );
		}
		return;
	}
}

/**
 * Modify Annotation
 * @param {Integer} currentUserID
 * @param {Object} args Argument parameters
 * @param {Function} callback
 */
function PresentationModifyAnnotation( currentUserID, args, callback ) {
	ConsoleLog( 'PresentationModifyAnnotation:', args );
	var errMsg;
	var _user = users[currentUserID];
	if( typeof _user === 'undefined' || !_user ) {
		errMsg = 'User not found';
		ConsoleLog( errMsg );
		if( typeof callback === 'function' ) {
			callback( {success:false, error:errMsg} );
		}
		return;
	}
	var depositionID = parseInt( args.depositionID );
	var isValidResult = validatePresentationAnnotator( currentUserID, depositionID );
	if( isValidResult === true ) {
		if( args && args.hasOwnProperty( 'eventData' ) && args.eventData && args.eventData.hasOwnProperty( 'Index' ) && args.eventData.hasOwnProperty( 'Page' ) ) {
			if( typeof presentations[depositionID] !== 'undefined' ) {
				var presentation = presentations[depositionID];
				var pageIndex = parseInt( args.eventData.Page );
				var annotationIndex = args.eventData.Index;
				if( typeof presentation.pageAnnotations[pageIndex] === 'object' && typeof presentation.pageAnnotations[pageIndex][annotationIndex] === 'object' ) {
					presentation.modifyAnnotation( pageIndex, annotationIndex, args.eventData, true, function() {
						ConsoleLog( "presentation.modifyAnnotation; onComplete" );
						updatePresentation( depositionID, function() {
							var subscribers = JSON.parse( JSON.stringify( presentation.subscribers ) ); //clone
							var indexOfSelf = subscribers.indexOf( currentUserID );
							if( indexOfSelf >= 0 ) {
								subscribers.splice( indexOfSelf, 1 );
							}
							notifySubscribers( subscribers, 'modifyAnnotation', args.eventData );
							if( typeof callback === 'function' ) {
								callback( {success:true, undoStackDepth:presentation.undoStack.length} );
							}
						} );
					} );
					return;
				} else {
					errMsg = 'Unknown annotation';
					ConsoleLog( 'PresentationModifyAnnotation error:', errMsg );
					if( typeof callback === 'function' ) {
						callback( {success:false, error:errMsg} );
					}
					return;
				}
			} else {
				errMsg = 'Unknown presentation';
				ConsoleLog( 'PresentationModifyAnnotation error:', errMsg );
				if( typeof callback === 'function' ) {
					callback( {success:false, error:errMsg} );
				}
				return;
			}
		} else {
			errMsg = 'Missing event data or index';
			ConsoleLog( 'PresentationModifyAnnotation error:', errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
	} else {
		errMsg = isValidResult;
		ConsoleLog( 'PresentationModifyAnnotation error:', errMsg );
		if( typeof callback === 'function' ) {
			callback( {success:false, error:errMsg} );
		}
		return;
	}
}

/**
 * Delete Annotation
 * @param {Integer} currentUserID
 * @param {Object} args Argument parameters
 * @param {Function} callback
 */
function PresentationDeleteAnnotation( currentUserID, args, callback ) {
	ConsoleLog( 'PresentationDeleteAnnotation:', args );
	var errMsg;
	var _user = users[currentUserID];
	if( typeof _user === 'undefined' || !_user ) {
		errMsg = 'User not found';
		ConsoleLog( errMsg );
		if( typeof callback === 'function' ) {
			callback( {success:false, error:errMsg} );
		}
		return;
	}
	var depositionID = parseInt( args.depositionID );
	var isValidResult = validatePresentationAnnotator( currentUserID, depositionID );
	if( isValidResult === true ) {
		if( args && args.hasOwnProperty( 'eventData' ) && args.eventData && args.eventData.hasOwnProperty( 'Index' ) && args.eventData.hasOwnProperty( 'Page' ) ) {
			var pageIndex = parseInt( args.eventData.Page );
			var annotationIndex = args.eventData.Index;
			if( typeof presentations[depositionID] !== 'undefined' ) {
				var presentation = presentations[depositionID];
				if( typeof presentation.pageAnnotations[pageIndex] !== 'undefined' && typeof presentation.pageAnnotations[pageIndex][annotationIndex] !== 'undefined' ) {
					presentation.deleteAnnotation( pageIndex, annotationIndex, args.eventData, true, function() {
						ConsoleLog( "presentation.deleteAnnotation; onComplete" );
						updatePresentation( depositionID, function() {
							var subscribers = JSON.parse( JSON.stringify( presentation.subscribers ) ); //clone
							var indexOfSelf = subscribers.indexOf( currentUserID );
							if( indexOfSelf >= 0 ) {
								subscribers.splice( indexOfSelf, 1 );
							}
							notifySubscribers( subscribers, 'deleteAnnotation', args.eventData );
							if( typeof callback === 'function' ) {
								callback( {success:true, undoStackDepth:presentation.undoStack.length} );
							}
						} );
					} );
					return;
				} else {
					errMsg = 'Unknown annotation';
					ConsoleLog( 'PresentationDeleteAnnotation error:', errMsg );
					if( typeof callback === 'function' ) {
						callback( {success:false, error:errMsg} );
					}
					return;
				}
			} else {
				errMsg = 'Unknown presentation';
				ConsoleLog( 'PresentationDeleteAnnotation error:', errMsg );
				if( typeof callback === 'function' ) {
					callback( {success:false, error:errMsg} );
				}
				return;
			}
		} else {
			errMsg = 'Missing event data, page or index';
			ConsoleLog( 'PresentationDeleteAnnotation error:', errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
	} else {
		errMsg = isValidResult;
		ConsoleLog( 'PresentationDeleteAnnotation error:', errMsg );
		if( typeof callback === 'function' ) {
			callback( {success:false, error:errMsg} );
		}
		return;
	}
}

/**
 * Undo Annotation
 * @param {Integer} currentUserID
 * @param {Object} args Argument parameters
 * @param {Function} callback
 */
function PresentationAnnotationUndo( currentUserID, args, callback ) {
	ConsoleLog( 'PresentationAnnotationUndo', args );
	var errMsg;
	var _user = users[currentUserID];
	if( typeof _user === 'undefined' || !_user ) {
		errMsg = 'User not found';
		ConsoleLog( errMsg );
		if( typeof callback === 'function' ) {
			callback( {success:false, error:errMsg} );
		}
		return;
	}
	var depositionID = parseInt( args.depositionID );
	var isValidResult = validatePresentationLeader( currentUserID, depositionID );
	if( isValidResult === true ) {
		if( typeof presentations[depositionID] !== 'undefined' ) {
			var presentation = presentations[depositionID];
			var lastAction = presentation.undoStack.pop();
			if( typeof lastAction !== 'undefined' && lastAction.hasOwnProperty( 'event' ) && lastAction.hasOwnProperty( 'Page' ) && lastAction.hasOwnProperty( 'Index' ) ) {
				var emitAction = null;
				function savePresentionAndEmit() {
					presentation.save( function() {
						updatePresentation( depositionID, function() {
							if( emitAction !== null ) {
								//send to everyone
								httpsSocketIO.sockets.in( presentation.roomID ).emit( emitAction, eventData );
							}
							if( typeof callback === "function" ) {
								callback( {success:true, undoStackDepth:presentation.undoStack.length} );
							}
						} );
					} );
				}
				function updateAnnotationHistory( onComplete ) {
					ConsoleLog( 'PresentationAnnotationUndo; updateHistory' );
					rClient.setex( historyRedisKey, config.redis.expire, JSON.stringify( annotationHistory ), function( err ) {
						if( err ) ConsoleLog( err );
						if( typeof onComplete === "function" ) {
							onComplete();
						}
					} );
				}
				var eventData = {Page:lastAction.Page, Index:lastAction.Index};
				ConsoleLog( 'Last Action:', lastAction );
				var historyRedisKey = presentation.rKey + ':annotationHistory:' + lastAction.Page + ':' + lastAction.Index;
				var annotationHistory = presentation.annotationHistory[lastAction.Page][lastAction.Index];
				if( lastAction.event === 'addAnnotation' ) {
					emitAction = 'deleteAnnotation';
					ConsoleLog( 'Undo add = delete' );
					presentation.deleteAnnotation( lastAction.Page, lastAction.Index, {}, false, function() {
						annotationHistory.pop();
						updateAnnotationHistory( savePresentionAndEmit );
					} );
				} else if( lastAction.event === 'modifyAnnotation' ) {
					emitAction = 'modifyAnnotation';
					presentation.annotationHistory[lastAction.Page][lastAction.Index].pop();
					var history = presentation.annotationHistory[lastAction.Page][lastAction.Index];
					eventData = history[(history.length - 1)];
					ConsoleLog( 'Undo modify = modify:', eventData );
					presentation.modifyAnnotation( lastAction.Page, lastAction.Index, eventData, false, function() {
						updateAnnotationHistory( savePresentionAndEmit );
					} );
				} else if( lastAction.event === 'deleteAnnotation' ) {
					emitAction = 'addAnnotation';
					var history = presentation.annotationHistory[lastAction.Page][lastAction.Index];
					eventData = history[(history.length - 1)];
					ConsoleLog( 'Undo delete = add:', eventData );
					presentation.addAnnotation( lastAction.Page, lastAction.Index, eventData, false, function() {
						savePresentionAndEmit();
					} );
				}
			} else {
				errMsg = 'Unable to undo';
				if( typeof callback === 'function' ) {
					callback( {success:false, error:errMsg, undoStackDepth:presentation.undoStack.length} );
				}
			}
		}
	}
}

/**
 * Clear Annotations
 * @param {Integer} currentUserID
 * @param {Object} args Argument parameters
 * @param {Function} callback
 */
function PresentationClearAnnotations( currentUserID, args, callback ) {
	ConsoleLog( 'PresentationClearAnnotations:', args );
	var errMsg;
	var _user = users[currentUserID];
	if( typeof _user === 'undefined' || !_user ) {
		errMsg = 'User not found';
		ConsoleLog( errMsg );
		if( typeof callback === 'function' ) {
			callback( {success:false, error:errMsg} );
		}
		return;
	}
	var depositionID = parseInt( args.depositionID );
	var isValidResult = validatePresentationLeader( currentUserID, depositionID );
	if( isValidResult === true ) {
		if( typeof presentations[depositionID] !== 'undefined' ) {
			var presentation = presentations[depositionID];
			var pageAnnotationIndexes = {};
			for( var page in presentation.pageAnnotations ) {
				if( presentation.pageAnnotations.hasOwnProperty( page ) ) {
					pageAnnotationIndexes[page] = Object.keys( presentation.pageAnnotations[page] );
				}
			}
			presentation.deleteAllAnnotations();
			var subscribers = JSON.parse( JSON.stringify( presentation.subscribers ) ); //clone
			var indexOfSelf = subscribers.indexOf( currentUserID );
			if( indexOfSelf >= 0 ) {
				subscribers.splice( indexOfSelf, 1 );
			}
			notifySubscribers( subscribers, 'clearAnnotations', pageAnnotationIndexes );
			if( typeof callback === 'function' ) {
				callback( {success:true, annotationIndexes:pageAnnotationIndexes, undoStackDepth:presentation.undoStack.length} );
			}
			return;
		} else {
			errMsg = 'Unknown presentation';
			ConsoleLog( 'clearAnnotations error:', errMsg );
			if (typeof callback === 'function') {
				callback( {success:false, error: errMsg} );
			}
			return;
		}
	} else {
		errMsg = isValidResult;
		ConsoleLog('clearAnnotations error:', errMsg);
		if (typeof callback === 'function') {
			callback( {success:false, error: errMsg} );
		}
		return;
	}
}

/**
 * Add Callout
 * @param {Integer} currentUserID
 * @param {Object} args Argument parameters
 * @param {Function} callback
 */
function PresentationAddCallout( currentUserID, args, callback ) {
	ConsoleLog( "PresentationAddCallout;", args );
	var errMsg;
	var _user = users[currentUserID];
	if( typeof _user === "undefined" || !_user ) {
		errMsg = "User not found";
		ConsoleLog( "PresentationAddCallout; error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
	var depositionID = parseInt( args.depositionID );
	var isValidResult = validatePresentationAnnotator( currentUserID, depositionID );
	if( isValidResult === true ) {
		if( args && args.hasOwnProperty( "eventData" ) && args.eventData && args.eventData.hasOwnProperty( "Index" ) && args.eventData.hasOwnProperty( "Page" ) ) {
			if( typeof presentations[depositionID] !== "undefined" ) {
				var presentation = presentations[depositionID];
				var pageIndex = parseInt( args.eventData.Page );
				errMsg = presentation.addCallout( pageIndex, args.eventData.Index, args.eventData, function() {
					ConsoleLog( "presentation.addCallout; onComplete" );
					updatePresentation( depositionID, function() {
						var subscribers = JSON.parse( JSON.stringify( presentation.subscribers ) ); //clone
						var indexOfSelf = subscribers.indexOf( currentUserID );
						if( indexOfSelf >= 0 ) {
							subscribers.splice( indexOfSelf, 1 );
						}
						notifySubscribers( subscribers, "addCallout", args.eventData );
						if( typeof callback === "function" ) {
							callback( {"success":true} );
						}
					} );
				} );
				if( errMsg ) {
					ConsoleLog( "PresentationAddCallout error:", errMsg );
					callback( {"success":false, "error":errMsg} );
				}
				return;
			} else {
				errMsg = "Unknown presentation";
				ConsoleLog( "PresentationAddCallout error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
		} else {
			errMsg = "Missing event data or index";
			ConsoleLog( "PresentationAddCallout error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	} else {
		errMsg = isValidResult;
		ConsoleLog( "PresentationAddCallout error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
}

/**
 * Modify Callout
 * @param {Integer} currentUserID
 * @param {Object} args Argument parameters
 * @param {Function} callback
 */
function PresentationModifyCallout( currentUserID, args, callback ) {
	ConsoleLog( "PresentationModifyCallout:", args );
	var errMsg;
	var _user = users[currentUserID];
	if( typeof _user === "undefined" || !_user ) {
		errMsg = "User not found";
		ConsoleLog( "PresentationModifyCallout; error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
	var depositionID = parseInt( args.depositionID );
	var isValidResult = validatePresentationAnnotator( currentUserID, depositionID );
	if( isValidResult === true ) {
		if( args && args.hasOwnProperty( "eventData" ) && args.eventData && args.eventData.hasOwnProperty( "Index" ) && args.eventData.hasOwnProperty( "Page" ) ) {
			if( typeof presentations[depositionID] !== "undefined" ) {
				var presentation = presentations[depositionID];
				var pageIndex = parseInt( args.eventData.Page );
				var calloutIndex = args.eventData.Index;
				if( typeof presentation.pageCallouts[pageIndex] === "object" && typeof presentation.pageCallouts[pageIndex][calloutIndex] === "object" ) {
					errMsg = presentation.modifyCallout( pageIndex, calloutIndex, args.eventData, function() {
						ConsoleLog( "presentation.modifyAnnotation; onComplete" );
						updatePresentation( depositionID, function() {
							var subscribers = JSON.parse( JSON.stringify( presentation.subscribers ) ); //clone
							var indexOfSelf = subscribers.indexOf( currentUserID );
							if( indexOfSelf >= 0 ) {
								subscribers.splice( indexOfSelf, 1 );
							}
							notifySubscribers( subscribers, "modifyCallout", args.eventData );
							if( typeof callback === "function" ) {
								callback( {"success":true} );
							}
						} );
					} );
					if( errMsg ) {
						ConsoleLog( "PresentationModifyCallout; error:", errMsg );
						callback( {"success":false, "error":errMsg} );
					}
					return;
				} else {
					errMsg = "Unknown callout";
					ConsoleLog( "PresentationModifyCallout; error:", errMsg );
					if( typeof callback === "function" ) {
						callback( {success:false, error:errMsg} );
					}
					return;
				}
			} else {
				errMsg = "Unknown presentation";
				ConsoleLog( "PresentationModifyCallout; error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {success:false, error:errMsg} );
				}
				return;
			}
		} else {
			errMsg = "Missing event data or index";
			ConsoleLog( "PresentationModifyCallout; error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
	} else {
		errMsg = isValidResult;
		ConsoleLog( "PresentationModifyCallout; error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {success:false, error:errMsg} );
		}
		return;
	}
}

/**
 * Delete Callout
 * @param {Integer} currentUserID
 * @param {Object} args Argument parameters
 * @param {Function} callback
 */
function PresentationDeleteCallout( currentUserID, args, callback ) {
	ConsoleLog( "PresentationDeleteCallout:", args );
	var errMsg;
	var _user = users[currentUserID];
	if( typeof _user === "undefined" || !_user ) {
		errMsg = "User not found";
		ConsoleLog( "PresentationDeleteCallout; error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
	var depositionID = parseInt( args.depositionID );
	var isValidResult = validatePresentationAnnotator( currentUserID, depositionID );
	if( isValidResult === true ) {
		if( args && args.hasOwnProperty( "eventData" ) && args.eventData && args.eventData.hasOwnProperty( "Index" ) && args.eventData.hasOwnProperty( "Page" ) ) {
			var pageIndex = parseInt( args.eventData.Page );
			var calloutIndex = args.eventData.Index;
			if( typeof presentations[depositionID] !== "undefined" ) {
				var presentation = presentations[depositionID];
				if( typeof presentation.pageCallouts[pageIndex] !== "undefined" && typeof presentation.pageCallouts[pageIndex][calloutIndex] !== "undefined" ) {
					errMsg = presentation.deleteCallout( pageIndex, calloutIndex, args.eventData, function() {
						ConsoleLog( "presentation.deleteCallout; onComplete" );
						updatePresentation( depositionID, function() {
							var subscribers = JSON.parse( JSON.stringify( presentation.subscribers ) ); //clone
							var indexOfSelf = subscribers.indexOf( currentUserID );
							if( indexOfSelf >= 0 ) {
								subscribers.splice( indexOfSelf, 1 );
							}
							notifySubscribers( subscribers, "deleteCallout", args.eventData );
							if( typeof callback === "function" ) {
								callback( {"success":true} );
							}
						} );
					} );
					if( errMsg ) {
						ConsoleLog( "PresentationDeleteCallout; error:", errMsg );
						callback( {"success":false, "error":errMsg} );
					}
					return;
				} else {
					errMsg = "Unknown callout";
					ConsoleLog( "PresentationDeleteCallout; error:", errMsg );
					if( typeof callback === "function" ) {
						callback( {"success":false, "error":errMsg} );
					}
					return;
				}
			} else {
				errMsg = "Unknown presentation";
				ConsoleLog( "PresentationDeleteCallout; error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
		} else {
			errMsg = "Missing event data, page or index";
			ConsoleLog( "PresentationDeleteCallout; error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	} else {
		errMsg = isValidResult;
		ConsoleLog( "PresentationDeleteCallout; error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
}

/**
 * Set Callout(s) Z-Index
 * @param {Integer} currentUserID
 * @param {Object} args Argument parameters
 * @param {Function} callback
 */
function PresentationSetCalloutZIndex( currentUserID, args, callback ) {
	ConsoleLog( "PresentationSetCalloutZIndex:", args );
	var errMsg;
	var _user = users[currentUserID];
	if( typeof _user === "undefined" || !_user ) {
		errMsg = "User not found";
		ConsoleLog( "PresentationSetCalloutZIndex; error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
	var depositionID = parseInt( args.depositionID );
	var isValidResult = validatePresentationAnnotator( currentUserID, depositionID );
	if( isValidResult === true ) {
		if( args && args.hasOwnProperty( "eventData" ) && args.eventData && args.eventData.hasOwnProperty( "Indices" ) && args.eventData.hasOwnProperty( "Page" ) ) {
			var pageIndex = parseInt( args.eventData.Page );
			if( typeof presentations[depositionID] !== "undefined" ) {
				var presentation = presentations[depositionID];
				errMsg = presentation.setCalloutZIndex( pageIndex, args.eventData.Indices, args.eventData, function() {
					ConsoleLog( "presentation.setCalloutZIndex; onComplete" );
					updatePresentation( depositionID, function() {
						var subscribers = JSON.parse( JSON.stringify( presentation.subscribers ) ); //clone
						var indexOfSelf = subscribers.indexOf( currentUserID );
						if( indexOfSelf >= 0 ) {
							subscribers.splice( indexOfSelf, 1 );
						}
						notifySubscribers( subscribers, "setCalloutZIndex", args.eventData );
						if( typeof callback === "function" ) {
							callback( {"success":true} );
						}
					} );
				} );
				if( errMsg ) {
					ConsoleLog( "PresentationSetCalloutZIndex; error:", errMsg );
					callback( {"success":false, "error":errMsg} );
				}
				return;
			} else {
				errMsg = "Unknown presentation";
				ConsoleLog( "PresentationSetCalloutZIndex; error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
		} else {
			errMsg = "Missing event data, page or indices";
			ConsoleLog( "PresentationSetCalloutZIndex; error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	} else {
		errMsg = isValidResult;
		ConsoleLog( "PresentationSetCalloutZIndex; error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
}

function presentationProcessEventQueue( presentation ) {
	ConsoleLog( "presentationProcessEventQueue; eventQueue.length:", presentation.eventQueue.length, " lock:", presentation.lock );
	if( presentation.lock ) {
		ConsoleLog( "presentationProcessEventQueue; locked!" );
		return;
	}
	if( presentation.eventQueue.length > 0 ) {
		presentation.lock = true;
		var qItem = presentation.eventQueue.shift();
		function callbackComplete( retval ) {
			if( typeof qItem.callback === "function" ) {
				qItem.callback( retval );
			}
			onComplete();
		};
		ConsoleLog( "presentationProcessEventQueue;", qItem.op );
		switch( qItem.op ) {
			case "addAnnotation":
				PresentationAddAnnotation( qItem.userID, qItem.args, callbackComplete );
				break;
			case "modifyAnnotation":
				PresentationModifyAnnotation( qItem.userID, qItem.args, callbackComplete );
				break;
			case "deleteAnnotation":
				PresentationDeleteAnnotation( qItem.userID, qItem.args, callbackComplete );
				break;
			case "initialAnnotation":
				PresentationInitialAnnotation( qItem.userID, qItem.args, callbackComplete );
				break;
			case "clearAnnotations":
				PresentationClearAnnotations( qItem.userID, qItem.args, callbackComplete );
				break;
			case "annotationUndo":
				PresentationAnnotationUndo( qItem.userID, qItem.args, callbackComplete );
				break;
			case "addCallout":
				PresentationAddCallout( qItem.userID, qItem.args, callbackComplete );
				break;
			case "modifyCallout":
				PresentationModifyCallout( qItem.userID, qItem.args, callbackComplete );
				break;
			case "deleteCallout":
				PresentationDeleteCallout( qItem.userID, qItem.args, callbackComplete );
				break;
			case "setCalloutZIndex":
				PresentationSetCalloutZIndex( qItem.userID, qItem.args, callbackComplete );
				break;
		}
	} else {
		ConsoleLog( "presentationProcessEventQueue; empty!" );
	}
	function onComplete() {
		presentation.lock = false;
		ConsoleLog( "presentationProcessEventQueue; done(", qItem.op, ") -- eventQueue.length:", presentation.eventQueue.length );
		if( presentation.eventQueue.length > 0 ) {
			presentationProcessEventQueue( presentation );
		}
	}
}

function syncRoomClose( roomID ) {
	ConsoleLog( 'syncRoomClose:', roomID );
	if( typeof rooms[roomID] !== 'undefined' && rooms[roomID] instanceof Room ) {
		delete rooms[roomID];
	} else {
		ConsoleLog( 'Room not instance of Room:', roomID, rooms[roomID] );
	}
}

function transcriptPublish( depositionID ) {
	ConsoleLog( 'transcriptPublish:', depositionID );
	var transcript;
	if( typeof transcripts[depositionID] !== 'undefined' && transcripts[depositionID] !== null ) {
		transcript = transcripts[depositionID];
	} else {
		ConsoleLog( 'Creating new instance of Transcript for depositionID:', depositionID );
		transcript = new Transcript( depositionID );
	}
	rClient.get( transcript.rKey, function( err, response ) {
		if( err ) {
			ConsoleLog( transcript.roomID, err );
		}
		if( response ) {
			var _transcript = JSON.parse( response );
			if( typeof _transcript.id !== 'undefined' ) {
				transcript.assign( _transcript );
				transcripts[depositionID] = transcript;
				transcript.processQueue();
			}
		}
	} );
}

function transcriptChatter( args ) {
//	ConsoleLog( 'transcriptChatter:', args );
	var depositionID = args.depositionID;
	var message = args.message;
	if( typeof depositionID === 'undefined' || depositionID === null || typeof message === 'undefined' || message === null ) {
		ConsoleLog( 'transcriptChatter -- missing depositionID or message:', depositionID, message );
		return;
	}
	if( typeof transcripts[depositionID] !== 'undefined' && transcripts[depositionID] !== null ) {
		var transcript = transcripts[depositionID];
		transcript.history += message;
		transcript.history = transcript.resolveHistory( transcript.history );
	}
}

function transcriptStreaming( args ) {
	ConsoleLog( 'transcriptStreaming' );
	var depositionID = args.depositionID;
	var streaming = !!args.streaming;
	if( typeof depositionID === 'undefined' || depositionID === null ) {
		ConsoleLog( 'transcriptStreaming -- missing depositionID:', depositionID );
		return;
	}
	if( typeof transcripts[depositionID] !== 'undefined' && transcripts[depositionID] !== null ) {
		var transcript = transcripts[depositionID];
		ConsoleLog( transcript.roomID, 'transcriptStreaming:', streaming );
		transcript.streaming = streaming;
	}
}

function transcriptReset( args ) {
	ConsoleLog( 'transcriptReset' );
	var depositionID = args.depositionID;
	if( typeof depositionID === 'undefined' || depositionID === null ) {
		ConsoleLog( 'transcriptReset -- missing depositionID:', depositionID );
		return;
	}
	if( typeof transcripts[depositionID] !== 'undefined' && transcripts[depositionID] !== null ) {
		var transcript = transcripts[depositionID];
		ConsoleLog( transcript.roomID, 'transcriptReset; streaming:', transcript.streaming, 'isStreamHost:', transcript.isStreamHost );
		transcript.reset( true, false );
	}
}

function transcriptAbort( args ) {
	ConsoleLog( 'transcriptAbort' );
	var depositionID = args.depositionID;
	if( typeof depositionID === 'undefined' || depositionID === null ) {
		ConsoleLog( 'transcriptAbort -- missing depositionID:', depositionID );
		return;
	}
	if( typeof transcripts[depositionID] !== 'undefined' && transcripts[depositionID] !== null ) {
		var transcript = transcripts[depositionID];
		ConsoleLog( transcript.roomID, 'transcriptAbort; streaming:', transcript.streaming, 'isStreamHost:', transcript.isStreamHost );
		transcript.abort();
	}
}

function onSubscribeUncaughtException( args ) {
	ConsoleLog( 'onSubscribeUncaughtException:', args );
}


function createRoom( depositionID, roomID, creatorID, args ) {
	ConsoleLog( 'createRoom', depositionID, roomID, creatorID, args );
	var _room = new Room( roomID, args.title, creatorID, args.private );
	_room.creatorID = creatorID;
	_room.readOnly = ((typeof args.readOnly !== 'undefined') ? args.readOnly : false);
	_room.isChatEnabled = ((typeof args.isChatEnabled !== 'undefined') ? args.isChatEnabled : true);
	_room.users[creatorID] = true;
	rooms[roomID] = _room;
	_room.save();
	httpsSocketIO.sockets.socket( users[creatorID].uid ).join( roomID );
	depositions[depositionID].rooms[roomID] = true;
	depositions[depositionID].save();
}

function sessionIsDemo( sClass ) {
	return (["Demo","WPDemo","Demo Trial"].indexOf( sClass ) >= 0);
}

function sessionIsWitnessPrep( sClass ) {
	return (["WitnessPrep","WPDemo"].indexOf( sClass ) >= 0);
}

function sessionIsTrial( sClass ) {
	return (["Trial","Mediation","Arbitration","Hearing","Demo Trial"].indexOf( sClass ) >= 0);
}

// load data from redis
function loadRedisData() {
	rClient.keys( 'deposition:*', function( err, depoKeys ) {
		if( err ) ConsoleLog( err );
		if( depoKeys ) {
			for( var keyIndex in depoKeys ) {
				if( depoKeys.hasOwnProperty( keyIndex ) ) {
					var rKey = depoKeys[keyIndex];
					var depositionID = rKey.split( ':' )[1];
					ConsoleLog( 'Syncing depositionID:', depositionID );
					updateDeposition( depositionID, true );
				}
			}
		}
	} );
}

logToFile( null, 'Chat Server: Okay to go.' );
