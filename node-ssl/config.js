/**
 * Configuration
 */

var config = module.exports = {};

config.https = {
	"port":8443,
	"responseMode":"JSON"
};

config.socket = {"authorization":true};

config.database = {
	name:"ed_sandboxdb",
	user:"root",
	password:"S4ndb0x!",
	host:"ed-sandboxdb.cd2jroqwwfdt.us-east-1.rds.amazonaws.com",
	port:13306,
	selectUserBySKey:"SELECT s.userID, u.clientID, s.attendeeID as guestID, IF(u.ID IS NULL, da.name, CONCAT(u.firstName,' ',u.lastName)) AS name, IF(u.ID IS NULL, da.email, u.email) AS email, da.role, u.typeID, IFNULL(c.typeID,'') as clientTypeID FROM APISessions s LEFT JOIN Users u ON (u.ID=s.userID) LEFT JOIN Clients c ON (c.ID=u.clientID) LEFT JOIN DepositionAttendees da ON (da.ID=s.attendeeID) WHERE sKey=?",
	selectDepositionByID:"SELECT d.ID, d.depositionOf, d.ownerID, d.speakerID, d.class, d.statusID FROM Depositions d WHERE d.ID=? AND d.parentID IS NULL"
};

config.whitelistIPs = [
	"127.0.0.1",		//localhost
	"192.168.*.*",		//private
	"10.0.*.*",			//private
	"172.31.0.*",		//us-east-1a
	"172.31.16.*",		//us-east-1d
	"172.31.17.*",		//staging
        "172.31.7.9",           //gystaging
	"69.169.138.228"	//utvpn
];

config.redis = {
	host:"127.0.0.1",
	port:6379,
	opts:{},
	expire:604800
};

config.courtroomConnect = {
	remoteCounselAPI:{
		//https://staging.remotecounsel.com/api/events/6836/accessible_event_schedule.json?token=8u5eyp8c51ePA5zd351U0Y7st325o4DI&email=rrussell@edepoze.com&key=9f7b2f2c06
		options:{
			host:"remotecounsel.com",
			path:""
		},
		basePath:"/api/events/",
		eventSchedule:"/accessible_event_schedule.json",
		token:"66699e8d-6b76-42c0-a15d-9c382f4f6c86"
	},
	liveStreamAPI:{
		options:{
			host:"text.speche.com",
			port:80,
			path:"/livestream.aspx",
			method:"GET"
		}
	},
	liveTranscriptsAPI:{
		//https://staging.speche.com/Livetranscripts/46eb520b-d682-46ca-889f-0d817486bbd0-1033.ptf
		options:{
			host:"speche.com",
			path:""
		},
		basePath:"/Livetranscripts/",
		ptfSuffix:"-1033.ptf"
	},
	client:"login.edepoze.com@54.84.21.190"
};

config.debug = true;
config.console = true;
config.handleErrors = true;
config.logFile = "../logs/mountrainier.log";

