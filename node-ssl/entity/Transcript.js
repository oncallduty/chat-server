/**
 * Transcript
 * @param {Integer} depositionID
 * @returns {Transcript}
 */
function Transcript( depositionID )
{
    this.id = parseInt( depositionID );
	this.rKey = 'transcript:' + this.id;
	this.roomID = this.id + '.transcript';
	this.firstPage = 1;
	this.pageLen = 25;
	this.history = '';
	this.eventID = null;
	this.guid = null;
	this.start_time = null;
	this.messageQueue = [];
	this.bufferInput = true;
	this.streaming = false;
	this.isStreamHost = false;
	this.status = 0;
	this.isDemo = false;

	var tx = this;
	var client = null;
	var statuses = {"INIT":0,"OK":1};
	var connectionAttempts = 0;
	var flushQueueInterval = null;
	var privateProperties = ['messageQueue','bufferInput','promise','isStreamHost','status'];

	this.assign = function( entity )
	{
		ConsoleLog( 'Transcript::assign()', this.id );
		for( var property in entity ) {
			if( privateProperties.indexOf( property ) !== -1 ) {
//				ConsoleLog( this.roomID, 'Transcript::assign(); warning: private property:', property );
				continue;
			}
			if( entity.hasOwnProperty( property ) ) {
				if( this.hasOwnProperty( property ) ) {
//					if( property !== "history" ) {
//						ConsoleLog( this.roomID, 'Transcript::assign(); info: property:', property, entity[property] );
//					}
					this[property] = entity[property];
				} else {
					ConsoleLog( this.roomID, 'Transcript::assign(); error: unknown property:', property );
				}
			}
		}
	};
	this.bundle = function()
	{
		ConsoleLog( 'Transcript::bundle()', this.id );
		var _this = {};
		for( var property in this ) {
			if( this.hasOwnProperty( property ) && typeof this[property] !== 'function' ) {
				if( privateProperties.indexOf( property ) !== -1 ) continue;
				_this[property] = JSON.parse( JSON.stringify( this[property] ) );	//clone
			}
		}
		return _this;
	};
	this.save = function()
	{
		ConsoleLog( 'Transcript::save()', this.id );
		var depoID = this.id;
		rClient.setex( this.rKey, config.redis.expire, JSON.stringify( this.bundle() ), function( err ) {
			if( err ) {
				ConsoleLog( tx.roomID, err );
			}
			lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Transcript.publish', args:depoID}} ) );
		} );
	};

	this.authenticate = function( cUser, eventID, accessKey, callback )
	{
		ConsoleLog( tx.roomID, "Transcript::authenticate --", cUser.id, cUser.name, "-- eventID:", eventID, "accessKey:", accessKey );
		var errMsg;
		var delayStream = 200;
		var depositionID = cUser.depositionID;
		if( typeof cUser.depositionID === 'undefined' || !(cUser.depositionID > 0) || typeof depositions[cUser.depositionID] === 'undefined' || depositions[cUser.depositionID] === null ) {
			errMsg = "Invalid Deposition, an error occurred while determining the deposition you are attending.";
			ConsoleLog( tx.roomID, "Transcript::authenticate -- ", errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var deposition = depositions[depositionID];
		if( this.eventID !== null && eventID !== this.eventID ) {
			errMsg = "Invalid Event ID, please check your Event ID and try again.";
			ConsoleLog( tx.roomID, "Transcript::authenticate -- ", errMsg, this.eventID );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( this.eventID === null && cUser.id !== deposition.ownerID ) {
			errMsg = "Unable to authenticate, the Taking Attorney must subscribe first.";
			ConsoleLog( tx.roomID, "Transcript::authenticate -- ", errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		if( eventID === 1234 && accessKey === 'edepoze' ) {
			ConsoleLog( tx.roomID, 'switching to demo mode' );
			this.isDemo = true;
			tx.eventID = eventID;
			tx.guid = '5ebeed36-cf78-424a-be18-a69f8e394999';
			ConsoleLog( tx.roomID, "demo -- streaming:", tx.streaming, "isStreamHost:", tx.isStreamHost );
			if( tx.streaming === false && tx.isStreamHost === false ) {
				connectionAttempts = 0;
				setTimeout( tx.liveStream, delayStream );
			}
			if( typeof callback === 'function' ) {
				callback( {success:true, transcriptHistory:tx.history} );
			}
			httpsSocketIO.sockets.socket( cUser.uid ).join( tx.roomID );
			return;
		}
		var response = '';
		var cfgAPI = config.courtroomConnect.remoteCounselAPI;
		var options = JSON.parse( JSON.stringify( cfgAPI.options ) );
		options.path = cfgAPI.basePath + eventID + cfgAPI.eventSchedule + "?token=" + cfgAPI.token + "&email=" + encodeURIComponent( cUser.email ) + "&key=" + accessKey;
		//https://staging.remotecounsel.com/api/events/6836/accessible_event_schedule.json?token=8u5eyp8c51ePA5zd351U0Y7st325o4DI&email=user@domain.com&key=9f7b2f2c06
		https.get( options, function( res ) {
			res.setEncoding( "utf8" );
			res.on( "data", function( chunk ) {
				response += chunk.toString();
			} );
			res.on( "end", function() {
				if( res.statusCode !== 200 ) {
					ConsoleLog( tx.roomID, "Transcript::authenticate -- invalid request:", res.statusCode );
					if( typeof callback === 'function' ) {
						callback( {success:false, error:"Invalid Event ID or Access Key (" + res.statusCode + ")"} );
					}
					return;
				}
				ConsoleLog( tx.roomID, "Transcript::authenticate -- end --", response );
				var _res;
				try {
					_res = JSON.parse( response );
				} catch( e ) {
					ConsoleLog( tx.roomID, "Transcript::authenticate -- exception:", e );
					if( typeof callback === 'function' ) {
						callback( {success:false, error:"Invalid Event ID or Access Key (" + res.statusCode + ")"} );
					}
					return;
				}
				if( typeof _res === 'undefined' || _res === null || typeof _res.event_schedule !== 'object' || _res.event_schedule === null ) {
//					ConsoleLog( tx.roomID, "Transcript::authenticate --", _res );
					if( typeof _res === 'object' && _res !== null && typeof _res.error !== 'undefined' ) {
						errMsg = _res.error.join( "" );
					} else {
						errMsg = response;
					}
					if( typeof callback === 'function' ) {
						callback( {success:false, error:errMsg} );
					}
					return;
				}
				tx.eventID = eventID;
				tx.guid = _res.event_schedule.guid;
				tx.start_time = _res.event_schedule.start_time;
				if( [3366].indexOf( eventID ) !== -1 ) {
					tx.guid = "5ebeed36-cf78-424a-be18-a69f8e394999";	//use demo stream
				}
				ConsoleLog( tx.roomID, "streaming:", tx.streaming, "isStreamHost:", tx.isStreamHost );
				if( tx.streaming === false && tx.isStreamHost === false ) {
					connectionAttempts = 0;
					setTimeout( tx.liveStream, delayStream );
				}
//				tx.save();
				if( typeof callback === 'function' ) {
					callback( {success:true, transcriptHistory:tx.history} );
				}
				httpsSocketIO.sockets.socket( cUser.uid ).join( tx.roomID );
			} );
		} ).on( "error", function( e ) {
			console.log( tx.roomID, "Transcript::authenticate -- Error:", e );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:e} );
			}
			return;
		} );
	};

	this.reset = function( skipSave, publish )
	{
//		ConsoleLog( tx.roomID, 'Transcript::reset' );
		skipSave = !!skipSave;	//cast to boolean
		publish = !!publish;
		clearInterval( flushQueueInterval );
		if( client !== null ) {
			client.destroy();
		}
		tx.streaming = tx.isStreamHost = false;
		tx.bufferInput = true;
		tx.status = statuses["INIT"];
		tx.messageQueue = [];
		tx.history = '';
		tx.isDemo = false;
		tx.eventID = null;
		if( !skipSave ) {
			tx.save();
		}
		if( publish ) {
			lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Transcript.reset', args:{"depositionID":tx.id}}} ) );
		}
	};

	this.abort = function( skipPublishSave )
	{
		ConsoleLog( tx.roomID, 'Transcript::abort' );
		httpsSocketIO.sockets.in( tx.roomID ).emit( 'transcriptReset', {"depositionID":depositionID} );
		var _rooms = httpsSocketIO.sockets.manager.rooms;
		var sioRoomID = '/' + tx.roomID;
		if( typeof _rooms[sioRoomID] !== 'undefined' && _rooms[sioRoomID] !== null ) {
			var txRoom = JSON.parse( JSON.stringify( _rooms[sioRoomID] ) );	//clone
			for( var i=0; i<txRoom.length; i++ ) {
				var uid = txRoom[i];
				ConsoleLog( tx.roomID, '(TranscriptReset) Removing', '(' + i + ')', 'uid:', uid );
				httpsSocketIO.sockets.socket( uid ).leave( tx.roomID );
			}
		}
		if( tx.streaming && tx.isStreamHost !== true ) {
			ConsoleLog( tx.roomID, "Node is not stream host, publishing 'Transcript.abort' event" );
			lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Transcript.abort', args:{depositionID:tx.id}}} ) );
		}
		tx.eventID = null;
		skipPublishSave = !!skipPublishSave;
		tx.reset( false, !skipPublishSave );
		connectionAttempts = 0;
	};

	this.liveStream = function()
	{
		ConsoleLog( tx.roomID, 'Transcript::liveStream' );
		if( connectionAttempts >= 3 ) {
			ConsoleLog( tx.roomID, 'Transcript::liveStream -- too many connection attempts, aborting' );
			return;
		}
		++connectionAttempts;
		ConsoleLog( tx.roomID, 'Transcript::liveStream -- connection attempt:', connectionAttempts );
		var response = null;
		tx.history = '';
		tx.streaming = tx.isStreamHost = tx.bufferInput = true;
		var headers = [
			"From: " + config.courtroomConnect.client,
			"Event: " + tx.guid,
			"Lang: 01033",
			"User-Agent: eDepoze",
			"Connection: Keep-Alive",
			"Content-Type: text/plain",
			"JavaVersion: 0.0_0",
			"Pragma: no-cache,rate=1.000000,stream-time=0",
			"Pragma: xPlayStrm=1"
		];
		var options = JSON.parse( JSON.stringify( config.courtroomConnect.liveStreamAPI.options ) );
		if( tx.isDemo ) {
			ConsoleLog( tx.roomID, "Transcript::liveStream -- using demo mode" );
			options.host = "staging.text.speche.com";
//			ConsoleLog( tx.roomID, "Transcript::liveStream -- options", options );
		}
		client = new net.Socket();
		client.setEncoding( 'utf8' );
		client.bufferSize = 4096;
		client.setTimeout( 15000, function() {
			ConsoleLog( tx.roomID, 'connection timeout!' );
			tx.reset( true, true );
		} );
		client.on( 'data', function( data ) {
//			ConsoleLog( tx.roomID, 'onDATA:', [data.toString()] );
			var msg = data.toString().replace( /\x0d/g, "" ).replace( /\u0000/g, "" );
			if( response === null ) {
				response = msg;
				ConsoleLog( tx.roomID, 'LiveStream RESPONSE:', [response] );
				var rHeaders = response.split( "\n" );
				for( var h=0; h<rHeaders.length; ++h ) {
					var header = rHeaders[h].replace( /\x0d/g, "" );	//strip \r
					if( header === "HTTP/1.1 200 OK" ) {
//						ConsoleLog( 'good response, reset connection attempts' );
						connectionAttempts = 0;
					}
				}
				return;
			}
			if( tx.bufferInput ) {
				tx.messageQueue.push( msg );
//				ConsoleLog( tx.roomID, '(buffering) DATA:', [msg] );
			} else {
//				ConsoleLog( tx.roomID, 'DATA:', [msg] );
				tx.messageQueue.push( msg );
				var msgQueue = tx.messageQueue.join( "" );
//				ConsoleLog( tx.roomID, 'MSG QUEUE:', [msgQueue] );
				if( msgQueue.length > 0 && msgQueue.indexOf( "\n" ) !== -1 ) {
//					ConsoleLog( tx.roomID, 'reset flushQueue interval' );
					clearInterval( flushQueueInterval );
					flushQueueInterval = setInterval( function() {
//						ConsoleLog( tx.roomID, 'flushQueue by interval' );
						tx.flushQueue();
					}, 3000 );
					tx.flushQueue();
				}
			}
		} );
		client.on( "end", function( data ) {
			ConsoleLog( tx.roomID, "Transcript::liveStream -- end", data );
			tx.flushQueue();
			tx.reset( false, true );
			tx.save();
		} );
		client.on( "close", function( hadError ) {
			ConsoleLog( tx.roomID, "Transcript::liveStream -- close; hadError:", hadError );
			tx.flushQueue();
			tx.reset( true, true );
			if( hadError ) {
				setTimeout( tx.liveStream, 500 );	//try harder, try again
			}
		} );
		client.connect( options.port, options.host, function() {
			ConsoleLog( tx.roomID, "Transcipt -- connected to:", options.host );
			var req = options.method + ' ' + options.path + " HTTP/1.1\r\nHost: " + options.host + "\r\n";
			for( var h in headers ) {
				var header = headers[h];
				if( typeof header !== 'string' ) {
					continue;
				}
				req = req + header + "\r\n";
			}
			req = req + "\r\n";
//			console.log( tx.roomID, 'REQUEST:', req );
			client.write( req );
//			tx.save();
			lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Transcript.streaming', args:{"depositionID":tx.id,"streaming":tx.streaming}}} ) );
			setTimeout( tx.ptfStream, 2000 );	//wait 2s for buffer to have data to merge with history
		} );
	};

	this.flushQueue = function()
	{
//		ConsoleLog( tx.roomID, 'Transcript::flushQueue' );
		if( tx.bufferInput ) {
//			ConsoleLog( tx.roomID, 'Transcript::flushQueue -- buffering input (abort)' );
			return;
		}
		var msgQueue = tx.messageQueue.join( "" );
		tx.messageQueue = [];
		if( msgQueue.length > 0 ) {
//			console.log( tx.roomID, 'MSG:', [msgQueue] );
			lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Transcript.chatter', args:{depositionID:tx.id,message:msgQueue}}} ) );
			httpsSocketIO.sockets.in( tx.roomID ).emit( 'transcriptMessage', {message:msgQueue} );
		}
	};

	this.ptfStream = function()
	{
		ConsoleLog( tx.roomID, "Transcript::ptfStream" );
		var api = config.courtroomConnect.liveTranscriptsAPI;
		var options = JSON.parse( JSON.stringify( api.options ) );
		if( tx.isDemo ) {
			ConsoleLog( tx.roomID, "Transcript::ptfStream -- using demo mode" );
			options.host = "staging.speche.com";
//			ConsoleLog( tx.roomID, "Transcript::ptfStream -- options", options );
		}
		options.path = api.basePath + tx.guid + api.ptfSuffix;
		ConsoleLog( tx.roomID, "Transcript::ptfStream -- connecting to:", options.host );
		var response = '';
		https.get( options, function( res ) {
			res.setEncoding( "utf8" );
			res.on( "data", function( chunk ) {
				response += chunk.toString();
			} );
			res.on( "end", function() {
				tx.history = '';
				tx.parsePTF( response );
				tx.syncStreams();
				ConsoleLog( tx.roomID, 'Transcript::ptfStream -- emit: liveTranscriptHistory', tx.history.length, 'length' );
				httpsSocketIO.sockets.in( tx.roomID ).emit( 'liveTranscriptHistory', {transcriptHistory:tx.history} );
				tx.status = statuses["OK"];
				tx.save();
			} );
		} ).on( "error", function( e ) {
			console.log( tx.roomID, "Transcript::ptfStream Error:", e );
			tx.reset( true, true );
			setTimeout( tx.liveStream, 500 );	//try harder, try again
		} );
	};

	this.parsePTF = function( ptf )
	{
		var lines = ptf.split( "\r\n" );
		if( lines[0].trim() !== "begin=Head" ) {
			return;
		}
		var section;
		for( var i in lines ) {
			var line = lines[i];
			if( typeof line !== "string" ) {
				continue;
			}
			if( line.indexOf( "=" ) !== -1 ) {
				if( line.indexOf( "fmt=pb" ) === 0 ) {
					continue;
				}
				var components = line.split( "=" );
				if( components[0] === "begin" ) {
					section = components[1].trim();
				} else if( components[0] === "end" ) {
					section = null;
				} else if( section === 'TranscriptInfo' && components[0] === "firstpage" ) {
					var val = parseInt( components[1].trim() );
					if( !isNaN( val ) ) {
//						ConsoleLog( tx.roomID, 'parsePTF -- setting firstPage:', val );
						tx.firstPage = val;
					}
				} else if( section === 'TranscriptInfo' && components[0] === "pagelen" ) {
					var val = parseInt( components[1].trim() );
					if( !isNaN( val ) ) {
//						ConsoleLog( tx.roomID, 'parsePTF -- setting pageLen:', val );
						tx.pageLen = val;
					}
				} else if( section === 'Text' ) {
//					console.log( tx.roomID, [line] );
					var idx = parseInt( components[0] );
					if( !isNaN( idx ) ) {
						var text = components[1].replace( /\x0d/g, "" );	//strip stray carriage returns
						if( text.length > 0 ) {
							tx.history += [text, "\n"].join( "" );
						}
					}
				}
			}
		}
//		console.log( tx.roomID, 'Transcript::parsePTF -- last line:', [tx.history.slice(-80)] );
	};

	this.syncStreams = function()
	{
//		ConsoleLog( tx.roomID, "Transcripts::syncStreams" );
		if( typeof tx.history !== "string" || tx.history === null ) {
//			ConsoleLog( tx.roomID, 'Transcripts::syncStreams -- no history to sync', tx.history );
			return;
		}
		var history = tx.history + '';	//clone
		if( history.endsWith( "\n" ) ) {
//			console.log( tx.roomID, 'trim history' );
			history = history.slice( 0, -2 );	//trim
//			console.log( tx.roomID, 'Transcript::syncStreams -- last line:', [history.slice(-78)] );
		}
		var msgQueue = tx.resolveHistory( tx.messageQueue.join( "" ) );
		tx.messageQueue = [];
		
//		ConsoleLog( tx.roomID, 'msgQueue:   ', [msgQueue], msgQueue.length );
		if( msgQueue.length === 0 ) {
			tx.bufferInput = false;
			return;	//nothing to sync
		}
		var minLength = Math.min( (tx.history.length - 1), 8 );
		for( var i=msgQueue.length; i>minLength; --i ) {
			var substr = msgQueue.slice( 0, i );
//			console.log( tx.roomID, 'looking for:', [substr] );
			if( history.endsWith( substr ) ) {
//				console.log( tx.roomID, 'FOUND!', [substr], [msgQueue.slice( i )] );
				history += msgQueue.slice( i );
//				break;
				tx.history = tx.resolveHistory( history );
				return;
			}
		}
		ConsoleLog( tx.roomID, 'Transcript::syncStreams -- Unable to merge' );
		tx.reset( true, true );
		setTimeout( tx.liveStream, 500 );	//try harder, try again
	};

	this.resolveHistory = function( history )
	{
		var i = 0;
//		history = history.replace( /\x0d/g, "" );	//strip \r
		while( history.indexOf( "\x08" ) !== -1 ) {
			history = history.replace( /.?\x08/, "" );
			++i;
			if( i >= 500 ) {
				history.replace( /\x08/g, "" );
				break;
			}
		}
//		console.log( tx.roomID, 'Transcript::resolveHistory -- last line:', [history.slice(-80)] );
		return history;
	};

	this.processQueue = function()
	{
//		ConsoleLog( tx.roomID, 'Transcript::processQueue' );
//		ConsoleLog( tx.roomID, 'Transcript::processQueue -- streaming:', tx.streaming );
//		ConsoleLog( tx.roomID, 'Transcript::processQueue -- isStreamHost:', tx.isStreamHost );
//		ConsoleLog( tx.roomID, 'Transcript::processQueue -- status:', tx.status );
		if( !tx.streaming || !tx.isStreamHost || tx.status !== statuses["OK"] ) {
//			ConsoleLog( tx.roomID, 'Transcript::processQueue -- abort' );
			return;
		}
		var msgQueue = tx.messageQueue.join( "" );
//		ConsoleLog( tx.roomID, 'Transcript::processQueue -- message queue:', [msgQueue] );
		tx.messageQueue = [];
		tx.bufferInput = false;
		if( msgQueue.length > 0 ) {
			lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Transcript.chatter', args:{depositionID:tx.id,message:msgQueue}}} ) );
			httpsSocketIO.sockets.in( tx.roomID ).emit( 'transcriptMessage', {message:msgQueue} );
		}
	};
}
exports.Transcript = Transcript;