/**
 * Message
 * @param {String} roomID
 * @param {String} message
 * @param {Integer} userID
 * @param {Boolean} system
 * @returns {Message}
 */
function Message( roomID, message, userID, system )
{
	this.roomID = roomID;
    this.rKey = 'room:' + roomID + ':messages';
	this.message = message;
	this.timestamp = Math.round( (new Date()).getTime() / 1000);
	this.userID = parseInt( userID );
	this.system = !!system;
}

Message.prototype.assign = function( entity )
{
//	ConsoleLog( 'Message::assign()', this.roomID );
	for( var property in entity ) {
		if( entity.hasOwnProperty( property ) ) {
			if( this.hasOwnProperty( property ) ) {
				this[property] = entity[property];
			} else {
				ConsoleLog( 'Message::assign(); error: unknown property:', property );
			}
		}
	}
};

Message.prototype.bundle = function()
{
//	ConsoleLog( 'Message::bundle()', this.roomID );
	var _this = {};
	for( var property in this ) {
		if( this.hasOwnProperty( property ) ) {
			if( ['rKey'].indexOf( property ) !== -1 ) continue;
			_this[property] = JSON.parse( JSON.stringify( this[property] ) );	//clone
		}
	}
	return _this;
};

Message.prototype.save = function()
{
	ConsoleLog( 'Message::save()', this.roomID );
	var roomID = this.roomID;
	// expire in 2 days
	rClient.rpush( this.rKey, JSON.stringify( this.bundle() ), function( err ) {
		if( err ) ConsoleLog( err );
		lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Message.publish', args:roomID}} ) );
	} );
	rClient.expire( this.rKey, config.redis.expire, function( err ) {
		if( err ) ConsoleLog( err );
	} );
};

Message.prototype.info = function()
{
	return( {roomID: this.roomID,
		message: this.message,
		timestamp: this.timestamp,
		userID: this.userID,
		system: this.system,
		user: (typeof users[this.userID] !== 'undefined') ? users[this.userID].info() : null,
		room: {id:this.roomID,title:''},
		deposition: {id:this.roomID.split( '.' )[0]}} );
};

exports.Message = Message;