/* global rClient,config,nodeId,lPub,require,httpsSocketIO,exports */

/**
 *
 * @param {String} id
 * @param {String} title
 * @param {Integer} creatorID
 * @param {Boolean} private
 * @returns {Room}
 */
function Room( id, title, creatorID, private ) {
	this.id = id;
	this.rKey = 'room:' + this.id;
	this.creatorID = parseInt( creatorID );
	this.title = title;
	this.users = {};
//	this.messages = [];
	this.joinTimestamp = {};
	this.private = private;
	this.readOnly = false;
	this.isChatEnabled = true;
}

Room.prototype.assign = function( entity ) {
//	ConsoleLog( 'Room::assign()', this.id );
//	ConsoleLog( 'Room::assign():', entity );
	for( var property in entity ) {
		if( entity.hasOwnProperty( property ) ) {
			if( this.hasOwnProperty( property ) ) {
//				ConsoleLog( 'Room::assign();', property, '=>', entity[property] );
				this[property] = entity[property];
			} else {
				ConsoleLog( 'Room::assign(); error: unknown property:', property );
			}
		}
	}
};

Room.prototype.save = function() {
	ConsoleLog( 'Room::save()', this.id );
	var roomID = this.id;
	// expire in 1 day
	rClient.setex( this.rKey, config.redis.expire, JSON.stringify( this.bundle() ), function( err ) {
		if( err ) ConsoleLog( err );
		lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Room.publish', args:roomID}} ) );
	} );
};

Room.prototype.set = function( entity ) {
//	console.log( 'Room::set():', entity.id );
	for( var property in entity ) {
		if( entity.hasOwnProperty( property ) ) {
//			console.log( 'Room::' + property + ' =', entity[property] );
			this[property] = entity[property];
		}
	}
//	this.id            = entity.id;
//	this.creator       = entity.creator;
//	this.title         = entity.title;
//	this.users         = entity.users;
//	this.messages      = entity.messages;
//	this.joinTimestamp = entity.joinTimestamp;
//	this.private       = entity.private;
//	this.readOnly      = entity.readOnly;
//	this.isChatEnabled = entity.isChatEnabled;
};

Room.prototype.bundle = function() {
//	ConsoleLog( 'Room::bundle()', this.id );
	var User = require( './User' ).User;
	var _this = {};
	for( var property in this ) {
		if( this.hasOwnProperty( property ) ) {
			if( ['rKey'].indexOf( property ) !== -1 ) continue;
			_this[property] = JSON.parse( JSON.stringify( this[property] ) );	//clone
		}
	}
//	_this.users = {};
//	for( var userID in this.users ) {
//		if( this.users.hasOwnProperty( userID ) ) {
//			_this.users[userID] = this.users[userID].bundle();
//		}
//	}
//	if( this.creator instanceof User ) {
//		_this.creator = this.creator.bundle();
//	} else {
//		console.log( 'Room::bundle(): Room Creator not instance of User:', this.creator );
//	}
	return _this;
};

Room.prototype.setProperty = function( property, value, isSubscribeEvent ) {
	this[property] = value;
	if( typeof isSubscribeEvent === 'undefined' || !isSubscribeEvent ) {
//JKL - sync
//		lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Room.setProperty', args:{room:this.id, property:property, value:value}}} ) );
	}
};

Room.prototype.addMessage = function( message, isSubscribeEvent ) {
    this.messages.push( message );
	if( typeof isSubscribeEvent === 'undefined' || !isSubscribeEvent ) {
//JKL - sync
//		lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Room.addMessage', args:{room:this.id, message:message}}} ) );
	}
};

Room.prototype.addUser = function( user, isSubscribeEvent ) {
	ConsoleLog( 'Room::addUser', this.id, user.id );
//	console.trace();
	var userID = parseInt( user.id );
    this.users[userID] = true;
    if( !this.joinTimestamp[userID] && ["M","G"].indexOf( user.userType ) >= 0 ) {
        this.joinTimestamp[userID] = Math.round( (new Date()).getTime() / 1000 );
    }
//	this.save();
	if( typeof isSubscribeEvent === 'undefined' || !isSubscribeEvent ) {
//JKL - sync
//		lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Room.addUser', args:{room:this.id, user:user.id}}} ) );
	}
	if( user && typeof user.uid !== 'undefined' ) {
		httpsSocketIO.sockets.socket( user.uid ).join( this.id );
		httpsSocketIO.sockets.socket( user.uid ).broadcast.to( this.id ).emit( 'user_did_connect', {room:{id:this.id}, user:user.info()} );
	}
};

Room.prototype.close = function( isSubscribeEvent ) {
    for( var user in this.users ) {
        if( this.users.hasOwnProperty( user ) ) {
			if( user.socket ) user.socket.leave( this.id );
        }
    }
	if( typeof isSubscribeEvent === 'undefined' || !isSubscribeEvent ) {
//JKL - sync
//		lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Room.close', args:{room:this.id}}} ) );
	}
};

Room.prototype.removeUser = function( user, isSubscribeEvent ) {
	ConsoleLog( 'Room.removeUser():', this.id, user.id, '"' + user.name + '"', user.uid );
    delete this.users[user.id];
	if( typeof isSubscribeEvent === 'undefined' || !isSubscribeEvent ) {
//JKL - sync
//		lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Room.removeUser', args:{room:this.id, user:user.id}}} ) );
	}
//	if( user.socket ) {
		ConsoleLog( "broadcast.to( '" + this.id + "' ).emit( 'user_did_disconnect' )" );
		httpsSocketIO.sockets.socket( user.uid ).broadcast.to( this.id ).emit( 'user_did_disconnect', {room:{id:this.id}, user:user.info()} );
		ConsoleLog( "emit( 'room_closed' )" );
		httpsSocketIO.sockets.socket( user.uid ).leave( this.id );
		httpsSocketIO.sockets.socket( user.uid ).emit( 'room_closed', {room:{id:this.id}} );
//	}
};

Room.prototype.indexOfUser = function(user) {
    return this.users.indexOfObject(
        function(obj) {
            console.log('asdfasdfasdfasdf');
        if( obj ) {
            return obj.id === user.id;
        }
        return false;
    });
};

Room.prototype.containsUser = function( user ) {
    return this.users[user.id] !== null;
};

Room.prototype.info = function( user ) {
	ConsoleLog( 'Room::info', this.id, 'for user:', (user?user.id:null) );
    var _messages = [];

    if( typeof user === 'object' && user.hasOwnProperty( 'id' ) && this.users[user.id] ) {
        var joined = this.joinTimestamp[user.id];
//		var _messages = this.messages;
//		console.log( messages[this.id] );
		if( typeof messages[this.id] !== 'undefined' && joined > 0 ) {
			for( var msgIndex in messages[this.id] ) {
//			for( var i=0; i<_messages.length; ++i ) {
				if( messages[this.id].hasOwnProperty( msgIndex ) ) {
//					ConsoleLog( 'Message index:', msgIndex );
					var message = messages[this.id][msgIndex];
					if( message.hasOwnProperty( 'timestamp' ) && message.timestamp >= joined ) {
						_messages.push( message.info() );
					}
				}
			}
		}
    }

	var depoID = parseInt( this.id.split('.')[0] );
    var roomUsers = [];
    for( var userID in this.users ) {
        if( this.users.hasOwnProperty( userID ) && typeof users[userID] !== 'undefined' ) {
            var _user = users[userID].info();
            _user.timestamp = this.joinTimestamp[userID];
			if( _user.depositionID !== depoID ) {
				_user.active = false;
				//_user.disconnected = true;
			}
            roomUsers.push( _user );
        }
    }

	ConsoleLog( 'Room::info; numMessages:', _messages.length, (typeof messages[this.id] !== 'undefined') ? messages[this.id].length : 0 );

    if( _messages.length > 0 ) {
        return ({
            id:this.id,
            title:this.title,
			userCount:Object.keys( this.users ).length,
            users:roomUsers,
            messages:_messages,
			numMessages:_messages.length,
			creatorID: this.creatorID,
            creator:(this.creatorID && typeof users[this.creatorID] !== 'undefined') ? users[this.creatorID].info() : null,
            private:this.private,
			readOnly:this.readOnly,
			isChatEnabled:this.isChatEnabled
        });
    } else {
        return ({
            id:this.id,
            title:this.title,
			userCount:Object.keys( this.users ).length,
            users:roomUsers,
			numMessages:_messages.length,
			creator:(this.creatorID && typeof users[this.creatorID] !== 'undefined') ? users[this.creatorID].info() : null,
			creatorID: this.creatorID,
            private:this.private,
			readOnly:this.readOnly,
			isChatEnabled:this.isChatEnabled
        });
    }
};

exports.Room = Room;