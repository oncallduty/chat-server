/* global rClient,lPub,config,nodeId,exports */

/**
 * Presentation
 * @param {Integer} sessionID
 * @returns {Presentation}
 */
function Presentation( sessionID ) {

	this.id = parseInt( sessionID );
	this.rKey = "presentation:" + this.id;
	this.annotatorID = null;
	this.pageAnnotations = {};
	this.pageCallouts = {};
	this.subscribers = [];
	this.roomID = "";
	this.orientation = 0;
	this.fileID = 0;
	this.pageIndex = 0;
	this.pageOffset = 0;
	this.pageOffsetX = 0.0;
	this.pageOffsetY = 0.0;
	this.pageScale = 1.0;
	this.pageOrientation = 0;
	this.rectOriginX = 0.0;
	this.rectOriginY = 0.0;
	this.rectSizeWidth = 0.0;
	this.rectSizeHeight = 0.0;
	this.undoStack = [];
	this.annotationHistory = {};
	this.title = "";
	this.eventQueue = [];
	this.lock = false;

	var self = this;
	var indexLength = 31;

	this.assign = function _assign( entity ) {
		//ConsoleLog( 'Presentation::assign()', this.id );
		for( var property in entity ) {
			if( entity.hasOwnProperty( property ) ) {
				if( this.hasOwnProperty( property ) ) {
					this[property] = entity[property];
				} else {
					ConsoleLog( 'Presentation::assign(); error: unknown property:', property );
				}
			}
		}
	};

	function _bundle() {
		ConsoleLog( 'Presentation::bundle()', self.id );
		var _this = {};
		for( var property in self ) {
			if( self.hasOwnProperty( property ) && typeof self[property] !== "function" ) {
				if( ['rKey','pageAnnotations','annotationHistory','eventQueue','lock'].indexOf( property ) !== -1 ) {
					continue;
				}
				_this[property] = JSON.parse( JSON.stringify( self[property] ) ); //clone
			}
		}
		return _this;
	}
	this.bundle = _bundle;

	function _save( onComplete ) {
		ConsoleLog( 'Presentation::save()', self.id );
		var sessionID = self.id;
		// expire in 1 days
		rClient.setex( self.rKey, config.redis.expire, JSON.stringify( self.bundle() ), function( err ) {
			if( err ) ConsoleLog( err );
			lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Presentation.publish', args:sessionID}} ) );
			if( typeof onComplete === "function" ) {
				ConsoleLog( "Presentation::save; onComplete()" );
				onComplete();
			}
		} );
	}
	this.save = _save;

	this.addAnnotation = function _addAnnotation( page, index, eventData, appendHistory, onComplete ) {
		appendHistory = !!appendHistory;
		ConsoleLog( "Presentation::addAnnotation; sessionID:", this.id, "page:", page, "index:", index );
		page = parseInt( page );
		if( isNaN( page ) || page < 0 || index.length !== indexLength ) {
			ConsoleLog( 'Invalid page or index:', page, index, index.length );
			if( typeof onComplete === "function" ) {
				onComplete();
			}
			return;
		}
		var rEventData = JSON.parse( JSON.stringify( eventData )  ); //clone
		if( typeof this.pageAnnotations[page] !== 'undefined' && typeof this.pageAnnotations[page][index] !== 'undefined' ) {
			ConsoleLog( 'Cannot add annotation when index already exists' );
			if( typeof onComplete === "function" ) {
				onComplete();
			}
			return;	//add is not replace
		}
		ConsoleLog( 'Presentation::addAnnotation:', this.id, '-- adding index:', index, 'for page:', page );
		if( typeof this.pageAnnotations[page] === 'undefined' ) {
			this.pageAnnotations[page] = {};
		}
		var sessionID = this.id;
		var redisKey = this.rKey + ':annotations';
		var presentation = this;
		if( typeof this.annotationHistory[page] === 'undefined' ) {
			this.annotationHistory[page] = {};
		}
		if( typeof this.annotationHistory[page][index] === 'undefined' ) {
			this.annotationHistory[page][index] = [];
		}
		var annotationRedisKey = redisKey + ':' + page + ':' + index;
		var annotationHistoryRedisKey = this.rKey + ':annotationHistory:' + page + ':' + index;
		var annotationHistory = this.annotationHistory[page][index];

		rClient.setex( annotationRedisKey, config.redis.expire, JSON.stringify( rEventData ), function( err ) {
			if( err ) {
				ConsoleLog( err );
			}
			if( appendHistory ) {
				annotationHistory.push( rEventData );
				presentation.save( function() {
					rClient.setex( annotationHistoryRedisKey, config.redis.expire, JSON.stringify( annotationHistory ), function( err ) {
					if( err ) ConsoleLog( err );
						presentation.undoStack.push( {event:"addAnnotation", Page:page, Index:index} );
						presentation.save( onComplete );
					} );
				} );
			} else {
				presentation.save( onComplete );
			}
			lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Annotation.publish', args:{sessionID:sessionID, pageIndex:page}}} ) );
		} );
	};

	this.modifyAnnotation = function _modifyAnnotation( page, index, eventData, appendHistory, onComplete ) {
		appendHistory = !!appendHistory;
		ConsoleLog( 'Presentation::modifyAnnotation; sessionID:', this.id, 'page:', page, 'index:', index );
		page = parseInt( page );
		if( isNaN( page ) || page < 0 || index.length !== indexLength ) {
			ConsoleLog( 'Invalid page or index:', page, index, index.length );
			if( typeof onComplete === "function" ) {
				onComplete();
			}
			return;
		}
		var rEventData = JSON.parse( JSON.stringify( eventData )  ); //clone
		if( typeof this.pageAnnotations[page] === 'undefined' || typeof this.pageAnnotations[page][index] === 'undefined' ) {
			if( typeof onComplete === "function" ) {
				onComplete();
			}
			return; //modify is not add
		}
		ConsoleLog( 'Presentation::modifyAnnotation:', this.id, '-- replacing index:', index, 'for page:', page );
		var annotation = JSON.parse( JSON.stringify( this.pageAnnotations[page][index] ) );
		for( var property in rEventData ) {
			if( rEventData.hasOwnProperty( property ) ) {
				annotation[property] = rEventData[property];
			}
		}
		var sessionID = this.id;
		var redisKey = this.rKey + ':annotations';
		var pageAnnotations = this.pageAnnotations[page];
		var annotationRedisKey = redisKey + ':' + page + ':' + index;
		var annotationHistoryRedisKey = this.rKey + ':annotationHistory:' + page + ':' + index;
		var annotationHistory = this.annotationHistory[page][index];
		var presentation = this;
		rClient.setex( annotationRedisKey, config.redis.expire, JSON.stringify( annotation ), function( err ) {
			if( err ) ConsoleLog( err );
			if( appendHistory ) {
				annotationHistory.push( annotation );
				presentation.save( function() {
					rClient.setex( annotationHistoryRedisKey, config.redis.expire, JSON.stringify( annotationHistory ), function( err ) {
						if( err ) ConsoleLog( err );
						presentation.undoStack.push( {event:"modifyAnnotation", Page:page, Index:index} );
						presentation.save( onComplete );
					} );
				} );
			} else {
				presentation.save( onComplete );
			}
			lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Annotation.publish', args:{sessionID:sessionID, pageIndex:page}}} ) );
		} );
	};

	this.deleteAnnotation = function _deleteAnnotation( page, index, eventData, appendHistory, onComplete ) {
		appendHistory = !!appendHistory;
		ConsoleLog( 'Presentation::deleteAnnotation: ', this.id, '-- deleteing index:', index, 'for page:', page );
		page = parseInt( page );
		if( isNaN( page ) || page < 0 || index.length !== indexLength ) {
			ConsoleLog( 'Invalid page or index:', page, index, index.length );
			if( typeof onComplete === "function" ) {
				onComplete();
			}
			return;
		}
		if( typeof this.pageAnnotations[page] === 'undefined' || typeof this.pageAnnotations[page][index] === 'undefined' ) {
			if( typeof onComplete === "function" ) {
				onComplete();
			}
			return;	//delete is not add
		}
		ConsoleLog( 'Presentation::deleteAnnotation:', this.id, '-- deleting index:', index, 'for page:', page );
		var sessionID = this.id;
		var redisKey = this.rKey + ':annotations:'+ page + ':' + index;
		var presentation = this;
		rClient.del( redisKey, function( err ) {
			if( err ) ConsoleLog( err );
			delete presentation.pageAnnotations[page][index];
			if( appendHistory ) {
				presentation.undoStack.push( {event:"deleteAnnotation", Page:page, Index:index} );
			}
			presentation.save( onComplete );
			lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Annotation.publish', args:{sessionID:sessionID,pageIndex:page}}} ) );
		} );
	};

	this.initialAnnotation = function _initialAnnotation( page, index, eventData, appendHistory, onComplete ) {
		appendHistory = !!appendHistory;
		ConsoleLog('Presentation::initialAnnotation; sessionID:', this.id, 'page:', page, 'index:', index);
		page = parseInt( page );
		if( isNaN( page ) || page < 0 || index.length !== indexLength ) {
			ConsoleLog( 'Invalid page or index:', page, index, index.length );
			if( typeof onComplete === "function" ) {
				onComplete();
			}
			return;
		}
		var rEventData = JSON.parse( JSON.stringify( eventData )  ); //clone
		if( typeof this.pageAnnotations[page] === 'undefined' ) {
			this.pageAnnotations[page] = {};
		}
		if( typeof this.pageAnnotations[page][index] !== 'undefined' ) {
			ConsoleLog( 'Cannot initialize annotation when index already exists' );
			if( typeof onComplete === "function" ) {
				onComplete();
			}
			return;	//replace is not initialize
		}
		//ConsoleLog( 'pageAnnotations:', this.pageAnnotations[page] );
		ConsoleLog( 'Presentation::initialAnnotation:', this.id, '-- adding index:', index, 'for page:', page );
		var sessionID = this.id;
		var redisKey = this.rKey + ':annotations';
		var presentation = this;
		if( typeof this.annotationHistory[page] === 'undefined' ) {
			this.annotationHistory[page] = {};
		}
		if( typeof this.annotationHistory[page][index] === 'undefined' ) {
			this.annotationHistory[page][index] = [];
		}
		var annotationRedisKey = redisKey + ':' + page + ':' + index;
		var annotationHistoryRedisKey = this.rKey + ':annotationHistory:' + page + ':' + index;
		var annotationHistory = this.annotationHistory[page][index];

		rClient.setex( annotationRedisKey, config.redis.expire, JSON.stringify( rEventData ), function( err ) {
			if( err ) {
				ConsoleLog( err );
			}
			if( appendHistory ) {
				annotationHistory.push( rEventData );
				presentation.save( function() {
					rClient.setex( annotationHistoryRedisKey, config.redis.expire, JSON.stringify( annotationHistory ), function( err ) {
						if( err ) ConsoleLog( err );
						if( typeof onComplete === "function" ) {
							onComplete();
						}
					} );
				} );
			} else {
				if( typeof onComplete === "function" ) {
					onComplete();
				}
			}
			lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Annotation.publish', args:{sessionID:sessionID, pageIndex:page}}} ) );
		} );
	};

	this.deleteAllAnnotations = function _deleteAllAnnotations( onComplete ) {
		ConsoleLog( 'Presentation::deleteAllAnnotations; sessionID:', this.id );
		var sessionID = this.id;
		this.undoStack = [];
		this.pageAnnotations = {};
		this.annotationHistory = {};
		var redisKeyMask = this.rKey + ':annotation*';	//annotations and annotationHistory
		rClient.keys( redisKeyMask, function( err, annotationKeys ) {
			if( err ) ConsoleLog( err );
			if( typeof annotationKeys === 'object' && annotationKeys.hasOwnProperty( 'length' ) && annotationKeys.length > 0 ) {
				var numKeys = annotationKeys.length;
				var delKeys = 0;
				while( annotationKeys.length > 0 ) {
					var annotationKey = annotationKeys.shift();
					rClient.del( annotationKey, function( err ) {
						if( err ) ConsoleLog( err );
						++delKeys;
						if( delKeys === numKeys ) {
							ConsoleLog( "Presentation::deleteAllAnnotations; onComplete; key mask:", redisKeyMask );
							_save( onComplete );
						}
					} );
				}
			} else {
				ConsoleLog( "Presentation::deleteAllAnnotations; onComplete" );
				_save( onComplete );
			}
			lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Annotation.publish', args:{sessionID:sessionID}}} ) );
		} );
	};

	this.addCallout = function _addCallout( page, index, eventData, onComplete ) {
		ConsoleLog( "Presentation.addCallout; sessionID:", this.id, "page:", page, "index:", index );
		var errMsg;
		page = parseInt( page );
		if( isNaN( page ) || page < 0 ) {
			errMsg = ["Invalid page or index:", page, index].join( " " );
			ConsoleLog( "Presentation.addCallout; error:", errMsg );
			return errMsg;
		}
		var rEventData = JSON.parse( JSON.stringify( eventData )  ); //clone
		if( typeof this.pageCallouts[page] !== "undefined" && typeof this.pageCallouts[page][index] !== "undefined" ) {
			errMsg = "Cannot add callout when index already exists";
			ConsoleLog( "Presentation.addCallout; error:", errMsg );
			return errMsg; //add is not replace
		}
		ConsoleLog( "Presentation.addCallout:", this.id, "-- adding index:", index, "for page:", page );
		if( typeof this.pageCallouts[page] === "undefined" ) {
			this.pageCallouts[page] = {};
		}
		var sessionID = this.id;
		var redisKey = this.rKey + ":callouts" + ":" + page + ":" + index;
		var presentation = this;
		rClient.setex( redisKey, config.redis.expire, JSON.stringify( rEventData ), function( err ) {
			if( err ) {
				ConsoleLog( "Presentation.addCallout; error:", err );
			}
			presentation.save( onComplete );
			lPub.publish( "metadata", JSON.stringify( {"nodeId":nodeId, "args":{"event":"Callout.publish", "args":{"sessionID":sessionID, "pageIndex":page}}} ) );
		} );
	};

	this.modifyCallout = function _modifyCallout( page, index, eventData, onComplete ) {
		ConsoleLog( "Presentation.modifyCallout; sessionID:", this.id, "page:", page, "index:", index );
		var errMsg;
		page = parseInt( page );
		if( isNaN( page ) || page < 0 ) {
			errMsg = ["Invalid page or index:", page, index].join( " " );
			ConsoleLog( "Presentation.modifyCallout; error:", errMsg );
			return errMsg;
		}
		var rEventData = JSON.parse( JSON.stringify( eventData )  ); //clone
		if( typeof this.pageCallouts[page] === "undefined" || typeof this.pageCallouts[page][index] === "undefined" ) {
			errMsg = "Cannot modify a callout that does not exist";
			ConsoleLog( "Presentation.modifyCallout; error:", errMsg );
			return errMsg; //modify is not add
		}
		ConsoleLog( "Presentation::modifyCallout:", this.id, "-- replacing index:", index, "for page:", page );
		var callout = JSON.parse( JSON.stringify( this.pageCallouts[page][index] ) );
		for( var property in rEventData ) {
			if( rEventData.hasOwnProperty( property ) ) {
				callout[property] = rEventData[property];
			}
		}
		var sessionID = this.id;
		var redisKey = this.rKey + ":callouts" + ":" + page + ":" + index;
		var pageCallouts = this.pageCallouts[page];
		var presentation = this;
		rClient.setex( redisKey, config.redis.expire, JSON.stringify( callout ), function( err ) {
			if( err ) ConsoleLog( err );
			presentation.save( onComplete );
			lPub.publish( "metadata", JSON.stringify( {nodeId:nodeId, args:{event:"Callout.publish", args:{sessionID:sessionID, pageIndex:page}}} ) );
		} );
	};

	this.deleteCallout = function _deleteCallout( page, index, eventData, onComplete ) {
		ConsoleLog( "Presentation.deleteCallout: ", this.id, "-- deleteing index:", index, "for page:", page );
		page = parseInt( page );
		if( isNaN( page ) || page < 0 ) {
			errMsg = ["Invalid page or index:", page, index].join( " " );
			ConsoleLog( "Presentation.deleteCallout; error:", errMsg );
			return errMsg;
		}
		if( typeof this.pageCallouts[page] === "undefined" || typeof this.pageCallouts[page][index] === "undefined" ) {
			errMsg = "Cannot delete a callout that does not exist";
			ConsoleLog( "Presentation.deleteCallout; error:", errMsg );
			return errMsg;
		}
		ConsoleLog( "Presentation.deleteCallout:", this.id, "-- deleting index:", index, "for page:", page );
		var sessionID = this.id;
		var redisKey = this.rKey + ":callouts:"+ page + ":" + index;
		var presentation = this;
		rClient.del( redisKey, function( err ) {
			if( err ) ConsoleLog( err );
			delete presentation.pageCallouts[page][index];
			presentation.save( onComplete );
			lPub.publish( "metadata", JSON.stringify( {"nodeId":nodeId, "args":{"event":"Callout.publish", "args":{"sessionID":sessionID, "pageIndex":page}}} ) );
		} );
	};

	this.setCalloutZIndex = function _setCalloutZIndex( page, indices, eventData, onComplete ) {
		ConsoleLog( "Presentation.setCalloutZIndex:", this.id, "for page:", page );
		page = parseInt( page );
		if( isNaN( page ) || page < 0 || typeof indices !== "object" ) {
			errMsg = ["Invalid page or indices:", page, indices].join( " " );
			ConsoleLog( "Presentation.setCalloutZIndex; error:", errMsg );
			return errMsg;
		}
		if( typeof this.pageCallouts[page] === "undefined" ) {
			errMsg = "Cannot set zIndex on callouts that do not exist";
			ConsoleLog( "Presentation.setCalloutZIndex; error:", errMsg );
			return errMsg;
		}
		ConsoleLog( "Presentation.setCalloutZIndex:", this.id, "for page:", page, "-- setting zIndex" );
		var sessionID = this.id;
		var presentation = this;
		var keys = Object.keys( indices );
		var keysCount = 0;
		for( var i in keys ) {
			var idx = keys[i];
			if( typeof this.pageCallouts[page][idx] === "undefined" ) {
				continue;
			}
			var zIndex = parseInt( indices[idx] );
			if( !isNaN( zIndex ) && zIndex >= 0 ) {
				this.pageCallouts[page][idx].zIndex = zIndex;
				var redisKey = this.rKey + ":callouts:" + page + ":" + idx;
				var callout = JSON.stringify( this.pageCallouts[page][idx] );
				rClient.setex( redisKey, config.redis.expire, callout, function( err ) {
					if( err ) ConsoleLog( err );
					++keysCount;
					if( keysCount === keys.length ) {
						presentation.save( onComplete );
						lPub.publish( "metadata", JSON.stringify( {nodeId:nodeId, args:{event:"Callout.publish", args:{sessionID:sessionID, pageIndex:page}}} ) );
					}
				} );
			}
		}
	};

	this.deleteAllCallouts = function _deleteAllCallouts( onComplete ) {
		ConsoleLog( "Presentation.deleteAllCallouts; sessionID:", this.id );
		var sessionID = this.id;
		this.undoStack = [];
		this.pageCallouts = {};
		var redisKeyMask = this.rKey + ":callouts:*";
		rClient.keys( redisKeyMask, function( err, calloutKeys ) {
			if( err ) ConsoleLog( err );
			if( typeof calloutKeys === "object" && calloutKeys.hasOwnProperty( "length" ) && calloutKeys.length > 0 ) {
				var numKeys = calloutKeys.length;
				var delKeys = 0;
				while( calloutKeys.length > 0 ) {
					var calloutKey = calloutKeys.shift();
					rClient.del( calloutKey, function( err ) {
						if( err ) ConsoleLog( err );
						++delKeys;
						if( delKeys === numKeys ) {
							ConsoleLog( "Presentation.deleteAllCallouts; onComplete; key mask:", redisKeyMask );
							_save( onComplete );
						}
					} );
				}
			} else {
				ConsoleLog( "Presentation::deleteAllCallouts; onComplete" );
				_save( onComplete );
			}
			lPub.publish( "metadata", JSON.stringify( {"nodeId":nodeId, "args":{"event":"Callout.publish", "args":{"sessionID":sessionID}}} ) );
		} );
	};

	this.reloadAnnotations = function _reloadAnnotations( page, onComplete ) {
		if( page ) {
			page = parseInt( page );
			if( isNaN( page ) ) {
				page = 0;
			}
		}
		ConsoleLog( 'Presentation::reloadAnnotations; ID:', this.id, 'page:', page );
		this.pageAnnotations = {};
		this.annotationHistory = {};
		var pageAnnotations = this.pageAnnotations;
		var redisKeyMask = this.rKey + ':annotations:' + (page ? page + ':' : '') + '*';
		var redisHistoryKey = this.rKey + ':annotationHistory:' + (page ? page + ':' : '') + '*';
		var processingAnnotations = true;
		var processingAnnotationHistory = true;
		rClient.keys( redisKeyMask, function( err, annotationKeys ) {
			if( err ) ConsoleLog( err );
			if( typeof annotationKeys === 'object' && annotationKeys.hasOwnProperty( 'length' ) && annotationKeys.length > 0 ) {
				rClient.mget( annotationKeys, function( err, annotations ) {
					if( err ) ConsoleLog( err );
					ConsoleLog( "Presentation::reloadAnnotations; annotationKeys:", annotationKeys );
					if( annotations ) {
						for( var a in annotations ) {
							if( annotations.hasOwnProperty( a ) ) {
								var annotation = JSON.parse( annotations[a] );
								if( annotation && typeof annotation === 'object' && annotation.hasOwnProperty( 'Page' ) && annotation.hasOwnProperty( 'Index' ) ) {
									var aPage = parseInt( annotation.Page );
									var aIndex = annotation.Index;
									if( typeof pageAnnotations[aPage] === 'undefined' ) {
										pageAnnotations[aPage] = {};
									}
									pageAnnotations[aPage][aIndex] = annotation;
								}
							}
						}
					}
					processingAnnotations = false;
					if( !processingAnnotationHistory && typeof onComplete === "function" ) {
						ConsoleLog( "Presentation::reloadAnnotations; processingAnnotations -- onComplete" );
						onComplete();
						onComplete = null;
					}
				} );
			} else {
				processingAnnotations = false;
				if( !processingAnnotationHistory && typeof onComplete === "function" ) {
					ConsoleLog( "Presentation::reloadAnnotations; processingAnnotations -- onComplete" );
					onComplete();
					onComplete = null;
				}
			}
		} );
		var annotationHistory = this.annotationHistory;
		rClient.keys( redisHistoryKey, function( err, historyKeys ) {
			if( err ) ConsoleLog( err );
			if( typeof historyKeys === 'object' && historyKeys.hasOwnProperty( 'length' ) && historyKeys.length > 0 ) {
				rClient.mget( historyKeys, function( err, history ) {
					if( err ) ConsoleLog( err );
					ConsoleLog( "Presentation::reloadAnnotations; historyKeys:", historyKeys );
					if( history ) {
						for( var i in history ) {
							if( history.hasOwnProperty( i ) ) {
								var aHistory = JSON.parse( history[i] );
								if( aHistory !== null && typeof aHistory[0] !== 'undefined' ) {
									var first = aHistory[0];
									if( first.hasOwnProperty( 'Page' ) && first.hasOwnProperty( 'Index' ) ) {
										if( typeof annotationHistory[first.Page] === 'undefined' ) {
											annotationHistory[first.Page] = {};
										}
										annotationHistory[first.Page][first.Index] = aHistory;
									}
								}
							}
						}
					}
					processingAnnotationHistory = false;
					if( !processingAnnotations && typeof onComplete === "function" ) {
						ConsoleLog( "Presentation::reloadAnnotations; processingAnnotationHistory -- onComplete" );
						onComplete();
						onComplete = null;
					}
				} );
			} else {
				processingAnnotationHistory = false;
				if( !processingAnnotations && typeof onComplete === "function" ) {
					ConsoleLog( "Presentation::reloadAnnotations; processingAnnotationHistory -- onComplete" );
					onComplete();
					onComplete = null;
				}
			}
		} );
	};

	this.reloadCallouts = function _reloadCallouts( page, onComplete ) {
		if( page ) {
			page = parseInt( page );
			if( isNaN( page ) ) {
				page = 0;
			}
		}
		ConsoleLog( "Presentation::reloadCallouts; ID:", this.id, "page:", page );
		this.pageCallouts = {};
		var pageCallouts = this.pageCallouts;
		var redisKeyMask = this.rKey + ':callouts:' + (page ? page + ':' : '') + '*';
		rClient.keys( redisKeyMask, function( err, calloutKeys ) {
			if( err ) {
				ConsoleLog( "Presentation.reloadCallouts; error:", err.name, err.message );
				console.log( err.stack );
			}
			if( typeof calloutKeys === "object" && calloutKeys.hasOwnProperty( "length" ) && calloutKeys.length > 0 ) {
				rClient.mget( calloutKeys, function( err, callouts ) {
					if( err ) ConsoleLog( err );
					ConsoleLog( "Presentation::reloadCallouts; calloutKeys:", calloutKeys );
					if( callouts ) {
						for( var c in callouts ) {
							if( callouts.hasOwnProperty( c ) ) {
								var callout = JSON.parse( callouts[c] );
								if( callout && typeof callout === "object" && callout.hasOwnProperty( "Page" ) && callout.hasOwnProperty( "Index" ) ) {
									var aPage = parseInt( callout.Page );
									var aIndex = callout.Index;
									if( typeof pageCallouts[aPage] === "undefined" ) {
										pageCallouts[aPage] = {};
									}
									pageCallouts[aPage][aIndex] = callout;
								}
							}
						}
					}
					if( typeof onComplete === "function" ) {
						ConsoleLog( "Presentation::reloadCallouts -- onComplete" );
						onComplete();
						onComplete = null;
					}
				} );
			} else {
				if( typeof onComplete === "function" ) {
					ConsoleLog( "Presentation::reloadCallouts -- onComplete" );
					onComplete();
					onComplete = null;
				}
			}
		} );
	};

	/**
	 * Sanitize Properties
	 * @param {Object} properties
	 * @returns {Object}
	 */
	this.sanitizeProperties = function _sanitizeProperties( properties ) {
		var sProps = {};
		//console.log( 'properties:', properties );
		for( var _prop in properties ) {
			if( properties.hasOwnProperty( _prop ) ) {
				var propValue = properties[_prop];
				switch( _prop ) {
					case 'fileID':
						propValue = parseInt( propValue );
						if( !isNaN( propValue ) && propValue > 0 ) {
							sProps[_prop] = propValue;
						}
						break;
					case 'pageIndex':
					case 'pageOffset':
					case 'pageOffsetTop':
						propValue = parseInt( propValue );
						if( !isNaN( propValue ) && propValue >= 0 ) {
							sProps[_prop] = propValue;
						}
						break;
					case 'pageOrientation':
						propValue = parseInt( propValue );
						if( !isNaN( propValue ) && propValue >= 0 && propValue <= 3 ) {
							sProps[_prop] = propValue;
						}
						break;
					case 'pageOffsetX':
					case 'pageOffsetY':
					case 'rectSizeWidth':
					case 'rectSizeHeight':
						propValue = parseFloat( propValue );
						if( !isNaN( propValue ) && propValue >= 0 ) {
							sProps[_prop] = propValue;
						}
						break;
					case 'pageScale':
						propValue = parseFloat( propValue );
						if( !isNaN( propValue ) && propValue >= 0.75 && propValue <= 5 ) {
							sProps[_prop] = propValue;
						}
						break;
					case 'rectOriginX':
					case 'rectOriginY':
						propValue = parseFloat( propValue );
						if( !isNaN( propValue ) ) {
							sProps[_prop] = propValue;
						}
						break;
					case 'orientation':
						if( typeof propValue === 'string' && (propValue === 'landscape' || propValue === 'portrait') ) {
							sProps[_prop] = propValue;
						}
						break;
					case 'title':
						if( typeof propValue === 'string' ) {
							sProps[_prop] = propValue;
						}
						break;
				}
			}
		}
		//console.log( 'sProps:', sProps );
		return sProps;
	};

	this.info = function _info() {
		var info = {};
		info.subscribers = this.subscribers;
		info.roomID = this.roomID;
		info.orientation = this.orientation;
		info.fileID = this.fileID;
		info.pageIndex = this.pageIndex;
		info.pageOffset = this.pageOffset;
		info.pageOffsetX = this.pageOffsetX;
		info.pageOffsetY = this.pageOffsetY;
		info.pageScale = this.pageScale;
		info.pageOrientation = this.pageOrientation;
		info.rectOriginX = this.rectOriginX;
		info.rectOriginY = this.rectOriginY;
		info.rectSizeWidth = this.rectSizeWidth;
		info.rectSizeHeight = this.rectSizeHeight;
		info.annotatorID = this.annotatorID;
		info.undoStackDepth = this.undoStack.length;
		info.title = this.title;
		return info;
	};
}

exports.Presentation = Presentation;
