/* global rClient,lPub,config,nodeId,exports,require,users */

/**
 *
 * @param {int} id
 * @returns {User}
 */
function User (id) {
    this.id = parseInt( id );
	this.rKey = 'user:' + this.id;
	this.uid = null;
	this.name = null;
	this.email = null;
	this.clientid = null;
	this.clientTypeID = null;
	this.guest = false;
	this.userType = 'M';
	this.blockExhibits = false;
	this.active = false;
	this.depositionID = null;
	this.sKey = null;
	this.isTempWitness = false;
	this.connectionCount = 0;
}

User.prototype.assign = function( entity ) {
//	ConsoleLog( 'User::assign()', this.id );
//	ConsoleLog( 'User::assign():', entity );
	for( var property in entity ) {
		if( entity.hasOwnProperty( property ) ) {
			if( this.hasOwnProperty( property ) ) {
//				ConsoleLog( 'User::assign();', property, '=>', entity[property] );
				this[property] = entity[property];
			} else {
				ConsoleLog( 'User::assign(); error: unknown property:', property );
			}
		}
	}
};

User.prototype.save = function() {
	ConsoleLog( 'User::save()', this.id );
	var userID = this.id;
	// expire in 1 day
	rClient.setex( this.rKey, config.redis.expire, JSON.stringify( this.bundle() ), function( err ) {
		if( err ) ConsoleLog( err );
		lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'User.publish', args:userID}} ) );
	} );
};


User.prototype.set = function( entity ) {
//	console.log( 'User::set():', entity.id );
	for( var property in entity ) {
		if( entity.hasOwnProperty( property ) ) {
			if( property === 'depositionID' ) continue;
			this[property] = entity[property];
		}
	}
};

User.prototype.bundle = function() {
//	ConsoleLog( 'User::bundle()', this.id );
	var _this = {};
	for( var property in this ) {
		if( this.hasOwnProperty( property ) ) {
			if( ['rKey'].indexOf( property ) !== -1 ) continue;
			_this[property] = JSON.parse( JSON.stringify( this[property] ) );	//clone
		}
	}
	return _this;
};

User.prototype.setProperty = function( property, value, isSubscribeEvent ) {
	this[property] = value;
	if( typeof users[this.id] !== 'undefined' && users[this.id].uid === this.uid ) {
		users[this.id][property] = value;
	}
	if( typeof isSubscribeEvent === 'undefined' || !isSubscribeEvent ) {
//JKL - sync
//		lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'User.setProperty', args:{deposition:(this.deposition)?this.deposition.id:null, user:this.id, property:property, value:value}}} ) );
	}
};

User.prototype.addDeposition = function( deposition, isSubscribeEvent ) {
	var Deposition = require( './Deposition' ).Deposition;
	if( deposition && deposition instanceof Deposition ) {
		this.deposition = deposition;
	}
	if( typeof isSubscribeEvent === 'undefined' || !isSubscribeEvent ) {
//JKL - sync
//		lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'User.addDeposition', args:{deposition:(this.deposition)?this.deposition.id:null, user:this.id}}} ) );
	}
};

User.prototype.removeDeposition = function( isSubscribeEvent ) {
	var depo = this.deposition;
	this.deposition = null;
	if( typeof isSubscribeEvent === 'undefined' || !isSubscribeEvent ) {
//JKL - sync
//		lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'User.removeDeposition', args:{deposition:(depo)?depo.id:null, user:this.id}}} ) );
	}
};

User.prototype.info = function() {

	var properties = {
        id: this.id,
        clientid: this.clientid,
        clientTypeID: this.clientTypeID,
        name: this.name,
        email: this.email,
        uid: this.uid,
        guest: this.guest,
		active: this.active,
		userType: this.userType,
		blockExhibits: this.blockExhibits,
		"connectionCount": this.connectionCount
    };

	if ( this.depositionID ) {
		properties.depositionID = this.depositionID;
//		properties.depoParameters = {
//			id: this.deposition.id
//		};

//		if ( this.deposition.usersParameters[this.id] )
//		{
//			var userParameters = this.deposition.usersParameters[this.id];
//			if ( userParameters.hasOwnProperty( 'blockExhibits' ) ) {
//				properties.depoParameters.blockExhibits = userParameters.blockExhibits;
//			}
//
//			if ( userParameters.hasOwnProperty( 'userRole' ) ) {
//				properties.depoParameters.userRole = userParameters.userRole;
//			}
//		}
 	}

    return properties;
};

exports.User = User;