/**
 * Created with JetBrains WebStorm.
 * User: trofimov
 * Date: 10/8/12
 * Time: 4:00 PM
 */
//
// ### function randomString (bits)
// #### @bits {integer} The number of bits for the random base64 string returned to contain
// randomString returns a pseude-random ASCII string which contains at least the specified number of bits of entropy
// the return value is a string of length ⌈bits/6⌉ of characters from the base64 alphabet
//
function random(bits) {
    var chars, rand, i, ret;

    chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-';
    ret = '';

    // in v8, Math.random() yields 32 pseudo-random bits (in spidermonkey it gives 53)
    while (bits > 0) {
        // 32-bit integer
        rand = Math.floor(Math.random() * 0x100000000);
        // base 64 means 6 bits per character, so we use the top 30 bits from rand to give 30/6=5 characters.
        for (i = 26; i > 0 && bits > 0; i -= 6, bits -= 6) {
            ret += chars[0x3F & rand >>> i];
        }
    }

    return ret;
};

if( !Array.isArray )
{
	Array.isArray = function( vArg )
	{
		return Object.prototype.toString.call( vArg ) === "[object Array]";
	};
}

Array.prototype.remove = function(from, to) {
    var rest = this.slice((to || from) + 1 || this.length);
    this.length = from < 0 ? this.length + from : from;
    return this.push.apply(this, rest);
}

Array.prototype.indexOfObject = function (fnc) {
    if (!fnc || typeof (fnc) != 'function') {
        return -1;
    }

    if (!this.length || this.length < 1) return -1;

    for (var i = 0; i < this.length; i++) {
        if (fnc(this[i])) return i;
    }
    return -1;
}




Object.prototype.getObject = function(parts) {

    if ( typeof parts === 'string' ) {
        parts = parts.split('.');
    }

    if ( typeof create !== 'boolean' ) {
        obj = this;
        create = undefined;
    }

    var p;

    while ( obj && parts.length ) {
        p = parts.shift();
        if ( obj[p] === undefined && create ) {
            obj[p] = {};
        }
        obj = obj[p];
    }

    return obj;
}

Object.prototype.setObject = function( name, value) {
    var parts = name.split('.'),
        prop = parts.pop(),
        obj = this.getObject( parts );

    // Only return the value if it is set successfully.
    return obj && typeof obj === 'object' && prop ? ( obj[prop] = value ) : undefined;
}

Object.prototype.exists = function( name ) {
    return this.getObject(name) !== undefined;
}

/*! https://mths.be/endswith v0.2.0 by @mathias */
if (!String.prototype.endsWith) {
	(function() {
		'use strict'; // needed to support `apply`/`call` with `undefined`/`null`
		var defineProperty = (function() {
			// IE 8 only supports `Object.defineProperty` on DOM elements
			try {
				var object = {};
				var $defineProperty = Object.defineProperty;
				var result = $defineProperty(object, object, object) && $defineProperty;
			} catch(error) {}
			return result;
		}());
		var toString = {}.toString;
		var endsWith = function(search) {
			if (this == null) {
				throw TypeError();
			}
			var string = String(this);
			if (search && toString.call(search) == '[object RegExp]') {
				throw TypeError();
			}
			var stringLength = string.length;
			var searchString = String(search);
			var searchLength = searchString.length;
			var pos = stringLength;
			if (arguments.length > 1) {
				var position = arguments[1];
				if (position !== undefined) {
					// `ToInteger`
					pos = position ? Number(position) : 0;
					if (pos != pos) { // better `isNaN`
						pos = 0;
					}
				}
			}
			var end = Math.min(Math.max(pos, 0), stringLength);
			var start = end - searchLength;
			if (start < 0) {
				return false;
			}
			var index = -1;
			while (++index < searchLength) {
				if (string.charCodeAt(start + index) != searchString.charCodeAt(index)) {
					return false;
				}
			}
			return true;
		};
		if (defineProperty) {
			defineProperty(String.prototype, 'endsWith', {
				'value': endsWith,
				'configurable': true,
				'writable': true
			});
		} else {
			String.prototype.endsWith = endsWith;
		}
	}());
}