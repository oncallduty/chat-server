/**
 * STL Transcripts
 */
var fs = require('fs');
var dateFormat = require('dateformat');
var url = require('url');
var request = require('request');
var os= require('os');
var localIP = os.networkInterfaces().eth0[0].address;
var launchTime = Date.now();
var transcriptIntervals = {};
var config = (fs.existsSync( "./local.js" )) ? require( "./local.js" ) : require( "./config.js" ) ;
module.exports = config;
var listenerHostPort = localIP + ':' + config.https.port;

ConsoleLog( 'STL Transcripts; listening on:', listenerHostPort );

if( config.handleErrors ) {
	process.on( 'uncaughtException', function( err ) {
		var now = new Date();
		console.error( '========================\n' + now.toISOString() + ' Caught exception: ', err, '\n' );
		console.error( err.stack, '\n========================\n' );
		lPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'uncaughtException', args:{nodeId:nodeId, err:err.toString()}}} ) );
		process.exit( 1 );
	} );
}

process.on( 'SIGTERM', function() {
	var now = new Date();
	console.error( '========================\n' + now.toISOString() + ' SIGTERM\n========================' );
	logToFile( null, 'STL Transcripts: Exiting.' );
	process.exit( 0 );
} );

process.on( 'exit', function( code ) {
	var now = new Date();
	console.error( '========================\n' + now.toISOString() + ' Exit: ', code, '\n========================\n' );
	logToFile( null, 'STL Transcripts: Exiting.' );
} );

var RedisStore = require( 'socket.io/lib/stores/redis' );
var redis = require( 'socket.io/node_modules/redis' );
var rClient = redis.createClient( config.redis.port, config.redis.host, config.redis.opts );
var rPub = redis.createClient( config.redis.port, config.redis.host, config.redis.opts );
var rSub = redis.createClient( config.redis.port, config.redis.host, config.redis.opts );
rClient.on( 'error', function( err ) { console.log( 'Redis Client Error', err ); console.log( err.stack ); } );
rPub.on( 'error', function( err ) { console.log( 'Redis Pub Error', err ); console.log( err.stack ); } );
rSub.on( 'error', function( err ) { console.log( 'Redis Sub Error', err ); console.log( err.stack ); } );
var redisStore = new RedisStore( {redisPub:rPub, redisSub:rSub, redisClient:rClient} );
var nodeId = redisStore.nodeId;

var lPub = redis.createClient( config.redis.port, config.redis.host, config.redis.opts );
var lSub = redis.createClient( config.redis.port, config.redis.host, config.redis.opts );
lPub.on( 'error', function( err ) { console.log( 'Local Pub Error', err ); console.log( err.stack ); } );
lSub.on( 'error', function( err ) { console.log( 'Local Sub Error', err ); console.log( err.stack ); } );

var httpsOptions = {
	key: fs.readFileSync( '../SSL/edepoze-v2.key' ),
	cert: fs.readFileSync( '../SSL/edepoze-v2.crt' ),
	ca: fs.readFileSync( '../SSL/gd_bundle-g2-g1.crt' ),
	requestCert: false,
	rejectUnauthorized: false,
	honorCipherOrder: true,
	ciphers: 'ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:AES256-GCM-SHA384:AES256-SHA256:AES256-SHA'
};

var httpsServer = require( 'https' ).createServer( httpsOptions, function( req, res ) {
	var remoteAddress = req.headers['x-forwarded-for'] || req.headers['x-real-ip'] || req.connection.remoteAddress || req.socket.remoteAddress || req.connection.socket.remoteAddress;
	if( !verifyIP( remoteAddress ) ) {
		res.writeHead( 501, "Authorization failed", {"Content-Type":"application/json"} );
        res.end( JSON.stringify( {success:false, error:"Authorization failed", code:501} ) );
		return;
	}
	if( req.method.toLowerCase() === 'post' ) {
		switch( req.url ) {
			case '/':
				notImplemented( req, res );
				break;
			default:
				notFound( req, res );
				break;
		}
	} else {
		var urlInfo = url.parse( req.url, true );
		switch( urlInfo.pathname ) {
			case '/ping':
				sendOk( res );
				break;
            case '/status':
                status( req, res );
                break;
			case '/startTranscript':
				var urlInfo = url.parse( req.url, true );
				var transcriptID = parseInt( urlInfo.query.transcriptID );
				startTranscriptListener( transcriptID );
//				startStreamTextListener( transcriptID );
				sendOk( res );
				break;
			case '/stopTranscript':
				var urlInfo = url.parse( req.url, true );
				var transcriptID = parseInt( urlInfo.query.transcriptID );
				stopTranscriptListener( transcriptID );
//				stopStreamTextListener( transcriptID );
				sendOk( res );
				break;
			default:
				notImplemented( req, res );
				break;
		}
    }
} ).listen( config.https.port );

var httpsSocketIO = require( 'socket.io' ).listen( httpsServer );
httpsSocketIO.configure( function() {
	httpsSocketIO.set( 'log level', 2 );
	httpsSocketIO.set( 'store', redisStore );
} );


// error handlers

function notImplemented( request, response )
{
	console.log( "\n[501]", request.method, "to", request.url );
	if( config.https.responseMode === 'HTML' ) {
		response.writeHead( 501, "Not implemented", {"Content-Type": "text/html"} );
		response.end( "<html><head><title>501 - Not implemented</title></head><body><h1>Not implemented!</h1></body></html>" );
	} else {
		response.writeHead( 501, "Not implemented", {"Content-Type": "application/json"} );
		response.end( JSON.stringify( {success:false, error:'Not implemented', code:501} ) );
	}
}

function notFound( request, response )
{
	console.log( "\n[404]", request.method, "to", request.url );
	if( config.https.responseMode === 'HTML' ) {
		response.writeHead( 404, "Not found", {"Content-Type": "text/html"} );
		response.end( '<html><head><title>404 - Not found</title></head><body><h1>Not found.</h1></body></html>' );
	} else {
		response.writeHead( 404, 'Not found', {"Content-Type": "application/json"} );
		response.end( JSON.stringify( {success:false, error:'Not found', code:404} ) );
	}
}

// ok
function sendOk( response )
{
    if( config.https.responseMode === 'HTML' ) {
        response.writeHead( 200, "OK", {"Content-Type": "text/html"} );
        response.end();
    } else {
        response.writeHead( 200, "OK", {"Content-Type": "application/json"} );
        response.end( JSON.stringify( {success:true} ) );
    }
}


function status( request, response )
{
	ConsoleLog( "[200]", request.method, "to", request.url );
	response.writeHead( 200, "OK", {"Content-Type":'application/json'} );
	var status = {
	};
	response.end( JSON.stringify( status ) );
}



/**
 *
 * @param {String} v "1.4" -- current version
 * @param {String} r "1.5" -- version required
 * @returns {Boolean}
 */
function compareVersions( v, r )
{
	if( typeof v !== 'string' || v.indexOf( '.' ) === -1 || typeof r !== 'string' || r.indexOf( '.' ) === -1 ) return false;
	//current version
	var _v = v.split( '.' );
	_v[0] = parseInt( _v[0] );	//major
	_v[1] = parseInt( _v[1] );	//minor
	// required version
	var _r = r.split( '.' );
	_r[0] = parseInt( _r[0] );	//major
	_r[1] = parseInt( _r[1] );	//minor
	if( !isNaN( _v[0] ) && !isNaN( _r[0] ) && _v[0] >= _r[0] )
	{
		if( !isNaN( _v[1] ) && !isNaN( _r[1] ) && _v[1] >= _r[1] )
			return true;
	}
	return false;
}

/**
 *
 * @param {String} ra remote ip address
 * @returns {Boolean}
 */
function verifyIP( ra )
{
	var wl = config.whitelistIPs;
	var r = ra.split( '.' );
	for( var i=0; i<wl.length; ++i ) {
		var a = wl[i].split( '.' );
		if( (parseInt( a[0] ) === parseInt( r[0] ) || a[0] === '*')
				&& (parseInt( a[1] ) === parseInt( r[1] ) || a[1] === '*')
				&& (parseInt( a[2] ) === parseInt( r[2] ) || a[2] === '*')
				&& (parseInt( a[3] ) === parseInt( r[3] ) || a[3] === '*') ) {
			return true;
		}
	}
	return false;
}

function ConsoleLog()
{
	var now = new Date();
	var logStr = now.toISOString();
	for( var n in arguments ) {
		if( !arguments.hasOwnProperty( n ) ) continue;
		//console.log( 'arguments', n, arguments[n] );
		if( arguments[n] && typeof arguments[n] === 'object' ) {
			logStr += ' ' + JSON.stringify( arguments[n] );
		} else if( arguments[n] && typeof arguments[n].toString === 'function' ) {
			logStr += ' ' + arguments[n].toString();
		} else {
			logStr += ' ' + arguments[n];
		}
	}
	console.log( logStr );
}

lSub.subscribe( 'transcripts' );
lSub.on( 'message', function( channel, jsonData ) {
	ConsoleLog( 'lSub::onMessage:', channel, jsonData );
	var data = JSON.parse( jsonData );
	if( !data || typeof data.nodeId === 'undefined' || typeof data.args === 'undefined' ) {
		ConsoleLog( 'lSub -- unsupported message', channel, jsonData );
		return;
	}

	//ignore messages from ourselves
	var performOnSelf = ['blah'];
	if( data.nodeId === nodeId && performOnSelf.indexOf( data.args.event ) === -1 ) {
		ConsoleLog( 'skipping message from same node:', data.args.event );
		return;
	}

	var args = data.args;
	if( !args || typeof args.event === 'undefined' || typeof args.args === 'undefined' ) {
		ConsoleLog( 'lSub -- missing event or args!', channel, jsonData );
		return;
	}

	switch( args.event ) {
		case 'joinTranscript':
//			startTranscriptListener( args.args );
			startStreamTextListener( args.args );
			break;
		case 'uncaughtException':
			onSubscribeUncaughtException( args.args );
			break;
		default:
			ConsoleLog( channel, args.event, args.args );
			break;
	}
} );


function onSubscribeUncaughtException( args )
{
	ConsoleLog( 'onSubscribeUncaughtException:', args );
}


function startTranscriptListener( transcriptID )
{
	ConsoleLog( 'startTranscriptListener', transcriptID );
	var intervalInfo;
	if( typeof transcriptIntervals[transcriptID] === 'undefined' ) {
		intervalInfo = {interval:null, started:null, transcriptLastPosition:0};
	} else {
		intervalInfo = transcriptIntervals[transcriptID];
	}
	ConsoleLog( transcriptID, intervalInfo.started );
	if( intervalInfo ) {
		clearInterval( intervalInfo.interval );
		intervalInfo.interval = setInterval( function() { transcriptListener( transcriptID ); }, 1500 );
		intervalInfo.started = new Date();
		transcriptIntervals[transcriptID] = intervalInfo;
	}
}


function stopTranscriptListener( transcriptID, restart )
{
	ConsoleLog( 'stopTranscriptListener', transcriptID );
	if( typeof transcriptIntervals[transcriptID] !== 'undefined' ) {
		var intervalInfo = transcriptIntervals[transcriptID];
		ConsoleLog( 'clearInterval', transcriptID, intervalInfo.started );
		clearInterval( intervalInfo.interval );
		delete transcriptIntervals[transcriptID];
	}
	if( restart === true ) {
		startTranscriptListener( transcriptID );
	}
}


function randomInt( min, max )
{
	return Math.floor( Math.random() * (max - min + 1)) + min;
}

function transcriptListener( transcriptID )
{
	var now = new Date();
	if( typeof transcriptIntervals[transcriptID] === 'undefined' ) {
		stopTranscriptListener( transcriptID );
		return;
	}
	var intervalInfo = transcriptIntervals[transcriptID];
	if( ((now - intervalInfo.started) / 1000) > (1*30) ) {
		stopTranscriptListener( transcriptID );
		return;
	}
//	ConsoleLog( 'transcript interval for transcript ID:', transcriptID );
	var WORDS = [
        "lorem", "ipsum", "dolor", "sit", "amet,", "consectetur", "adipiscing", "elit", "ut", "aliquam,", "purus", "sit", "amet", "luctus", "venenatis,",
		"lectus", "magna", "fringilla", "urna,", "porttitor", "rhoncus", "dolor", "purus", "non", "enim", "praesent", "elementum", "facilisis", "leo,",
		"vel", "fringilla", "est", "ullamcorper", "eget", "nulla", "facilisi", "etiam", "dignissim", "diam", "quis", "enim", "lobortis", "scelerisque",
		"fermentum", "dui", "faucibus", "in", "ornare", "quam", "viverra", "orci", "sagittis", "eu", "volutpat", "odio", "facilisis", "mauris", "sit",
		"amet", "massa", "vitae", "tortor", "condimentum", "lacinia", "quis", "vel", "eros", "donec", "ac", "odio", "tempor", "orci", "dapibus", "ultrices",
		"in", "iaculis", "nunc", "sed", "augue", "lacus,", "viverra", "vitae", "congue", "eu,", "consequat", "ac", "felis", "donec", "et", "odio",
		"pellentesque", "diam", "volutpat", "commodo", "sed", "egestas", "egestas", "fringilla", "phasellus", "faucibus", "scelerisque", "eleifend",
		"donec", "pretium", "vulputate", "sapien", "nec", "sagittis", "aliquam", "malesuada", "bibendum", "arcu", "vitae", "elementum", "curabitur",
		"vitae", "nunc", "sed", "velit", "dignissim", "sodales", "ut", "eu", "sem", "integer", "vitae", "justo", "eget", "magna", "fermentum", "iaculis",
		"eu", "non", "diam", "phasellus", "vestibulum", "lorem", "sed", "risus", "ultricies", "tristique", "nulla", "aliquet", "enim", "tortor,", "at",
		"auctor", "urna", "nunc", "id", "cursus", "metus", "aliquam", "eleifend", "mi", "in", "nulla", "posuere", "sollicitudin", "aliquam", "ultrices",
		"sagittis", "orci,", "a", "scelerisque", "purus", "semper", "eget", "duis", "at", "tellus", "at", "urna", "condimentum", "mattis", "pellentesque",
		"id", "nibh", "tortor,", "id", "aliquet", "lectus", "proin", "nibh", "nisl,", "condimentum", "id", "venenatis", "a,", "condimentum", "vitae",
		"sapien", "pellentesque", "habitant", "morbi", "tristique", "senectus", "et", "netus", "et", "malesuada", "fames", "ac", "turpis", "egestas", "sed",
		"tempus,", "urna", "et", "pharetra", "pharetra,", "massa", "massa", "ultricies", "mi,", "quis", "hendrerit", "dolor", "magna", "eget", "est",
		"lorem", "ipsum", "dolor", "sit", "amet,", "consectetur", "adipiscing", "elit", "pellentesque", "habitant", "morbi", "tristique", "senectus", "et",
		"netus", "et", "malesuada", "fames", "ac", "turpis", "egestas", "integer", "eget", "aliquet", "nibh", "praesent", "tristique", "magna", "sit",
		"amet", "purus", "gravida", "quis", "blandit", "turpis", "cursus", "in", "hac", "habitasse", "platea", "dictumst", "quisque", "sagittis,", "purus",
		"sit", "amet", "volutpat", "consequat,", "mauris", "nunc", "congue", "nisi,", "vitae", "suscipit", "tellus", "mauris", "a", "diam", "maecenas",
		"sed", "enim", "ut", "sem", "viverra", "aliquet", "eget", "sit", "amet", "tellus", "cras", "adipiscing", "enim", "eu", "turpis", "egestas", "pretium",
		"aenean", "pharetra,", "magna", "ac", "placerat", "vestibulum,", "lectus", "mauris", "ultrices", "eros,", "in", "cursus", "turpis", "massa",
		"tincidunt", "dui", "ut", "ornare", "lectus", "sit", "amet", "est", "placerat", "in", "egestas", "erat", "imperdiet", "sed", "euismod", "nisi",
		"porta", "lorem", "mollis", "aliquam", "ut", "porttitor", "leo", "a", "diam", "sollicitudin", "tempor", "id", "eu", "nisl", "nunc", "mi", "ipsum,",
		"faucibus", "vitae", "aliquet", "nec,", "ullamcorper", "sit", "amet", "risus", "nullam", "eget", "felis", "eget", "nunc", "lobortis", "mattis",
		"aliquam", "faucibus", "purus", "in", "massa", "tempor", "nec", "feugiat", "nisl", "pretium", "fusce", "id", "velit", "ut", "tortor", "pretium",
		"viverra", "suspendisse", "potenti", "nullam", "ac", "tortor", "vitae", "purus", "faucibus", "ornare", "suspendisse", "sed", "nisi", "lacus,", "sed",
		"viverra", "tellus", "in", "hac", "habitasse", "platea", "dictumst", "vestibulum", "rhoncus", "est", "pellentesque", "elit", "ullamcorper",
		"dignissim", "cras", "tincidunt", "lobortis", "feugiat", "vivamus", "at", "augue", "eget", "arcu", "dictum", "varius", "duis", "at", "consectetur"
    ];
	var texts = [];
	var count = randomInt( 3, 15 );
	var wordIndex = randomInt( 0, WORDS.length - count - 1 );
    var text = WORDS.slice( wordIndex, wordIndex + count ).join( ' ' ).replace( /\.|\,/g, '' );
	if( count >= 7 ) {
		var first = text.split( ' ' ).slice( 0, 6 ).join( ' ' ) + "\n";
		var second = text.split( ' ' ).slice( 6, count ).join( ' ' );
		texts.push( first );
		texts.push( second );
	} else {
		texts.push( text );
	}
	ConsoleLog( transcriptID, texts );
	var roomID = transcriptID + '.transcript';
	httpsSocketIO.sockets.in( roomID ).emit( 'transcriptMessage', texts );
}


function startStreamTextListener( transcriptID )
{
	ConsoleLog( 'startStreamTextListener', transcriptID );
	var intervalInfo;
	if( typeof transcriptIntervals[transcriptID] === 'undefined' ) {
		intervalInfo = {interval:null, started:null, lastPosition:0, rKey:'transcript:' + transcriptID, roomID:transcriptID+'.transcript'};
	} else {
		intervalInfo = transcriptIntervals[transcriptID];
	}
	if( intervalInfo ) {
		clearInterval( intervalInfo.interval );
		intervalInfo.interval = setInterval( function() { streamTextRequest( transcriptID ); }, 1500 );
		intervalInfo.started = new Date();
		transcriptIntervals[transcriptID] = intervalInfo;
	}
}


function stopStreamTextListener( transcriptID, restart )
{
	ConsoleLog( 'stopStreamTextListener', transcriptID );
	if( typeof transcriptIntervals[transcriptID] !== 'undefined' ) {
		var intervalInfo = transcriptIntervals[transcriptID];
		ConsoleLog( 'clearInterval', transcriptID, intervalInfo.started );
		clearInterval( intervalInfo.interval );
	}
	if( restart === true ) {
		startStreamTextListener( transcriptID );
	}
}


function streamTextRequest( transcriptID )
{
	ConsoleLog( 'streamTextRequest', transcriptID );
	if( typeof transcriptIntervals[transcriptID] === 'undefined' ) {
		return;
	}
	var intervalInfo = transcriptIntervals[transcriptID];
	var url = 'https://www.streamtext.net/text-data.ashx?event=legaldemo&last=' + intervalInfo.lastPosition;
	console.log( url );
	request( {url:url, json:true}, function( e, r, b ) { streamTextResponse( intervalInfo, e, r, b ); } );
//	stopStreamTextListener( transcriptID );
}

var message = '';

function streamTextResponse( intervalInfo, error, response, body )
{
	if( response.statusCode !== 200 || typeof body !== 'object' || !body.hasOwnProperty( 'lastPosition' ) || !body.hasOwnProperty( 'i' ) ) {
		ConsoleLog( 'streamTextResponse code:', response.statusCode, 'error:', error );
		ConsoleLog( 'Error: invalid response' );
		return;
	}
	var lastPos = intervalInfo.lastPosition;
	intervalInfo.lastPosition = parseInt( body.lastPosition );
	if( intervalInfo.lastPosition < lastPos ) {
		ConsoleLog( '--end' );
		clearInterval( intervalInfo.interval );
		intervalInfo.lastPosition = 0;
		message = '';
		return;
	}
	clearInterval( intervalInfo.interval );	//temporarily disable transcript spamming
	var transcripts = body.i;
	var messages = [];	
	rClient.lrange( intervalInfo.rKey, 0, -1, function( err, response ) {
		if( err ) ConsoleLog( err );
		if( response && response.hasOwnProperty( 'length' ) && response.length > 0 ) {
			//lset
		} else {
			//rpush
		}
		for( var idx in transcripts ) {
			if( transcripts.hasOwnProperty( idx ) ) {
				var t = transcripts[idx];
				message = decodeURIComponent( message + t.d );
				if( t.m !== '' && t.d.indexOf( '%0A' ) !== -1 ) {
					console.log( message );
					messages.push( message );
					message = '';
				}
			}
		}
		if( messages.length > 0 ) {
			httpsSocketIO.sockets.in( intervalInfo.roomID ).emit( 'transcriptMessage', messages );
		} else if( message.length > 0 ) {
			httpsSocketIO.sockets.in( intervalInfo.roomID ).emit( 'transcriptMessage', [message] );
			message = '';
		}
	} );
}

ConsoleLog( 'STL Transcripts: Okay to go.' );
