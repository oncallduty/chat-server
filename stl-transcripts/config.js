/**
 * Configuration
 */

var config = module.exports = {};

config.https = {
	"port":1443
};

config.database = {
	name:"eDepozeDev",
	user:"edepo",
	password:"edepoze",
	host:"10.0.25.48",
	port:13306
};

config.whitelistIPs = [
	"127.0.0.1",
	"10.0.25.*",
	"172.31.*.*",
	"50.198.180.125"
];

config.redis = {
	host:"127.0.0.1",
	port:6379,
	opts:{},
	expire:604800
};

config.debug = true;
config.logFile = "../logs/stl-transcripts.log";
