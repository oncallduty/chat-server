"use strict";
/* global global,require,module */

var url = require( "url" );
var dateFormat = require( "dateformat" );

// net (transcripts https client)
var net = require( "net" );
module.exports = net;
global.net = net;

// promise
var Promise = require( "bluebird" );
module.exports = Promise;
global.Promise = Promise;

// fs
var fs = require( "fs" );
Promise.promisifyAll( fs );
module.exports = fs;
global.fs = fs;

var express = require( "express" );
var chatserver = express(); //app

// config
var config = (fs.existsSync( "./local.js" )) ? require( "./local.js" ) : require( "./config.js" );
module.exports = config;
global.config = config;

// redis
var redis = require( "redis" );
Promise.promisifyAll( redis.RedisClient.prototype );
Promise.promisifyAll( redis.Multi.prototype ); // probably unused

// socket.io publish client
var pubClient = redis.createClient( config.redis.port, config.redis.host, config.redis.opts );
pubClient.on( "error", function( err ) {
	ConsoleLog( "Pub Client; Error:", err.name, err.message );
	console.log( err.stack );
} );
pubClient.on( "connect", function() {
	ConsoleLog( "Pub Client; Connect" );
	loadRedisData();
} );
pubClient.on( "reconnect", function() {
	ConsoleLog( "Pub Client; Reconnect" );
	loadRedisData();
} );

// socket.io subscribe client
var subClient = redis.createClient( config.redis.port, config.redis.host, {"return_buffers":true} );
subClient.on( "error", function( err ) {
	ConsoleLog( "Sub Client; Error", err.name, err.message );
	console.log( err.stack );
} );
subClient.on( "connect", function() {
	ConsoleLog( "Sub Client; Connect" );
	loadRedisData();
} );
subClient.on( "reconnect", function() {
	ConsoleLog( "Sub Client; Reconnect" );
	loadRedisData();
} );

// redis client
var redisClient = redis.createClient( config.redis.port, config.redis.host, config.redis.opts );
module.exports = redisClient;
global.redisClient = redisClient;
redisClient.on( "error", function( err ) {
	ConsoleLog( "redisClient; Error:", err.name, err.message );
	console.log( err.stack );
} );
redisClient.on( "connect", function() {
	ConsoleLog( "redisClient; Connect" );
	loadRedisData();
} );
redisClient.on( "reconnect", function() {
	ConsoleLog( "redisClient; Reconnect" );
	loadRedisData();
} );

// redis publish client
var redisPub = redis.createClient( config.redis.port, config.redis.host, config.redis.opts );
module.exports = redisPub;
global.redisPub = redisPub;
redisPub.on( "error", function( err ) {
	ConsoleLog( "Redis Pub; Error:", err.name, err.message );
	console.log( err.stack );
} );
redisPub.on( "connect", function() {
	ConsoleLog( "Redis Pub; Connect" );
	loadRedisData();
} );
redisPub.on( "reconnect", function() {
	ConsoleLog( "Redis Pub; Reconnect" );
	loadRedisData();
} );

// redis subscribe client
var redisSub = redis.createClient( config.redis.port, config.redis.host, config.redis.opts );
redisSub.on( "error", function( err ) {
	ConsoleLog( "Redis Sub; Error:", err.name, err.message );
	console.log( err.stack );
} );
redisSub.on( "connect", function() {
	ConsoleLog( "Redis Sub; Connect" );
	loadRedisData();
} );
redisSub.on( "reconnect", function() {
	ConsoleLog( "Redis Sub; Reconnect" );
	loadRedisData();
} );

redisSub.subscribe( "metadata" );
redisSub.on( "message", function( channel, jsonData ) {
	// ConsoleLog( "Redis Sub; message:", channel, jsonData );
	var data = JSON.parse( jsonData );
	if( !isset( data ) || typeof data.nodeId === "undefined" || typeof data.args === "undefined" ) {
		ConsoleLog( "redisSub -- unsupported message", channel, jsonData );
		return;
	}

	// ignore messages from ourselves unless listed otherwise
	var performOnSelf = ["User.publish","Room.publish","Deposition.publish","Message.publish","Transcript.publish","Transcript.chatter"];
	if( data.nodeId === nodeId && performOnSelf.indexOf( data.args.event ) === -1 ) {
		// ConsoleLog( "skipping message from same node:", data.args.event );
		return;
	}

	var args = data.args;
	if( !args || typeof args.event === "undefined" || typeof args.args === "undefined" ) {
		ConsoleLog( "redisSub -- missing event or args!", channel, jsonData );
		return;
	}

	switch( args.event ) {
		case "User.publish":
			syncUser( args.args );
			break;
		case "Room.publish":
			syncRoom( args.args );
			break;
		case "Message.publish":
			syncMessage( args.args );
			break;
		case "Presentation.publish":
			syncPresentation( args.args );
			break;
		case "Annotation.publish":
			syncPresentationAnnotations( args.args );
			break;
		case "Callout.publish":
			syncPresentationCallouts( args.args );
			break;
		case "Presentation.end":
			syncPresentationEnd( args.args );
			break;
		case "Deposition.publish":
			syncSession( args.args, false );
			break;
		case "Transcript.publish":
			syncTranscript( args.args );
			break;
		case "closeDeposition":
			syncCloseDeposition( args.args );
			break;
		case "Room.close":
//			syncRoomClose( args.args );
//			break;
		case "Transcript.chatter":
//			transcriptChatter( args.args );
//			break;
		case "Transcript.streaming":
//			transcriptStreaming( args.args );
//			break;
		case "Transcript.reset":
//			transcriptReset( args.args );
//			break;
		case "Transcript.abort":
//			transcriptAbort( args.args );
//			break;
		case "uncaughtException":
//			onSubscribeUncaughtException( args.args );
//			break;
		default:
			ConsoleLog( channel, args.event, args.args );
			break;
	}
} );

// mysql
var mysql = require( "mysql" );
Promise.promisifyAll( mysql );
Promise.promisifyAll( require( "mysql/lib/Connection" ).prototype );
Promise.promisifyAll( require( "mysql/lib/Pool" ).prototype );
var dbPool = mysql.createPool( config.database );

// https
var https = require( "https" );
module.exports = https;
global.https = https;
var httpsServer = https.createServer( config.https.options, chatserver );
httpsServer.listen( config.https.port, '0.0.0.0', function() {
	var os = require( "os" );
	var eth0 = os.networkInterfaces().eth0[0].address;
	ConsoleLog( "listening on:", eth0 + ":" + config.https.port );
} );

// socket.io
var sIO = require( "socket.io" )( httpsServer, config.socketIO );
module.exports = sIO;
global.sIO = sIO;
var sIORedisAdapter = require( "socket.io-redis" );
var sIOAdapter = sIORedisAdapter( {"pubClient":pubClient, "subClient":subClient} );
var nodeId = sIOAdapter.uid;

ConsoleLog( "nodeId:", nodeId );
module.exports = nodeId;
global.nodeId = nodeId;
sIO.adapter( sIOAdapter );

// entities
var depositions = {};
global.depositions = depositions;
var users = {};
global.users = users;
var rooms = {};
global.rooms = rooms;
var messages = {};
global.messages = messages;
var presentations = {};
global.presentations = presentations;
var transcripts = {};
global.transcripts = transcripts;

var Deposition = require( "./entity/Deposition" ).Deposition;
var User = require( "./entity/User" ).User;
var Room = require( "./entity/Room" ).Room;
var Message = require( "./entity/Message" ).Message;
var Presentation = require( "./entity/Presentation" ).Presentation;
var Transcript = require( "./entity/Transcript" ).Transcript;

var sockets = {};
global.sockets = sockets;

// socket.io authorization
sIO.use( function( socket, next ) {

	var handshakeData = socket.handshake;
	//ConsoleLog( "sIO; authorization; handshakeData:", handshakeData );
	var remoteAddress = parseIPv4( handshakeData.headers["x-forwarded-for"] || handshakeData.headers["x-real-ip"] || socket.request.connection.remoteAddress );
	var sKey = (isset( handshakeData ) && isset( handshakeData.query ) && isset( handshakeData.query.sKey )) ? handshakeData.query.sKey.toString() : null;
	if( !isset( sKey ) ) {
		unauthorizedConnection();
		return;
	}

	var userInfo = null;
	Promise.using( DBConnection(), function( dbConn ) {
		return dbConn.queryAsync( config.database.selectUserBySKey, [sKey] );
	} ).then( function( rows ) {
		if( isset( rows ) && rows.length === 1 ) {
			userInfo = rows.shift();
			return;
		}
		ConsoleLog( "[ERROR] invalid sKey:", sKey );
	} ).catch( function( e ) {
		ConsoleLog( "selectUserBySKey; caught:", e.name, e.message );
		console.log( e.stack );
	} ).finally( function() {
		if( isset( userInfo ) && (isset( userInfo.userID) || isset( userInfo.guestID )) ) {
			ConsoleLog( "authorized:", userInfo );
			socket.handshake.user = userInfo;
			return next();
		}
		unauthorizedConnection();
	} );

	function unauthorizedConnection() {
		var errMsg = "Not authorized";
		ConsoleLog( "[ERROR] sIO::authorization; remoteAddress:", remoteAddress, errMsg );
		next( new Error( errMsg ) );
		socket.disconnect();
	}
} );

// socket.io authorized connection
sIO.on( "connection", function( socket ) {

	var handshake = socket.handshake;
	var remoteAddr = parseIPv4( handshake.headers["x-forwarded-for"] || handshake.headers["x-real-ip"] || handshake.address );
	var authUserLog = (isset( handshake ) && isset( handshake.query ) && isset( handshake.query.aul )) ? handshake.query.aul.toString() : null;

	ConsoleLog( authUserLog, "socket connection:", remoteAddr, handshake.user );
	var currentUserID = (isset( handshake.user.guestID ) ? handshake.user.guestID : handshake.user.userID) + 0; // clone, cast
	var currentUser = new User( currentUserID );
	var userSocket = socket;



	sockets[socket.id] = socket;
	currentUser.uid = socket.id;
	currentUser.remoteAddr = remoteAddr;
	currentUser.clientid = handshake.user.clientID;
	currentUser.clientTypeID = handshake.user.clientTypeID;
	currentUser.name = handshake.user.name;
	currentUser.email = handshake.user.email;
	currentUser.userType = handshake.user.role;
	currentUser.sKey = socket.handshake.query["sKey"].substr( 0, 6 ) + "~" + socket.handshake.query["sKey"].substr( -6, 6 );
	currentUser.guest = (isset( handshake.user.guestID ));
	currentUser.isTempWitness = (currentUser.userType === "TW");
	currentUser.active = true;
	currentUser.connectionCount = 1;
	currentUser.authUserLog = authUserLog;

	var lastLogin = null;
	if( typeof users[currentUserID] !== "undefined" ) {
		lastLogin = Object.assign( {}, users[currentUserID] ); // clone
		//console.log( "last login:", lastLogin, currentUser );
		currentUser.blockExhibits = (typeof lastLogin.blockExhibits !== "undefined" ? lastLogin.blockExhibits : false);
		currentUser.connectionCount = (lastLogin.sKey === currentUser.sKey ? ++lastLogin.connectionCount : 1);
	}
	//ED-2207 -- end presentations if leader connects with new skey
	autoEndPresentations()
	.then( autoKickOnLogin )
	.then( resetUserIsActive )
	.then( currentUser.save() )
	.then( function() {
		users[currentUserID] = currentUser;
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "userDidConnect:", currentUser );
	} )
	.catch( function( e ) {
		ConsoleLog( "sIO.on(connection); caught:", e.name, e.message );
		console.log( e.stack );
	} )
	.finally( function() {
		lastLogin = null;
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "socket connection:", remoteAddr, "user:", currentUserID, '"' + currentUser.name + '"', currentUser.sKey, currentUser.connectionCount );
	} );

//	setTimeout( function() {
//		userSocket.disconnect();
//	}, 30000 );

	/**
	 * Socket Disconnect
	 */
	function socketDidDisconnect() {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER' , "socket disconnect:", remoteAddr, "user:", currentUserID, '"' + currentUser.name + '"', currentUser.sKey, currentUser.uid );
		setTimeout( function() {
			cleanupUserSocket( userSocket );
		}, 500 ); // allow connection of possible new user
	};
	socket.on( "disconnect", socketDidDisconnect );

	/**
	 * Select Deposition
	 * @param {Object} args
	 * @param {function} callback
	 */
	function selectDeposition( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(selectDeposition):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = currentUser = users[currentUserID];
		if( typeof cUser === "undefined" || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "selectDeposition; Error:", errMsg );
			return;
		}
		if ( typeof args === "undefined" || !args.hasOwnProperty( "deposition" ) || !args.deposition ) {
			errMsg = "Session not specified";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "selectDeposition; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}

		var depositionID = parseInt( args.deposition );
		var depoRoomID = depositionID + ".deposition";
		var deposition;

		if( currentUser.depositionID && currentUser.depositionID !== depositionID ) {
			var prevDepo = depositions[depositionID];
			if( prevDepo && prevDepo instanceof Deposition ) {
				prevDepo.users[currentUserID] = false;	//inactive
				prevDepo.save();
			}
		}

		// create deposition if not exists
		if( typeof depositions[depositionID] === "undefined" || !(depositions[depositionID] instanceof Deposition) || !depositions[depositionID].statusID || depositions[depositionID].statusID === "N" ) {
			deposition = new Deposition( depositionID, currentUser );
			var sessionInfo = null;
			Promise.using( DBConnection(), function( dbConn ) {
				return dbConn.queryAsync( config.database.selectSessionByID, [depositionID] );
			} ).then( function( rows ) {
				if( isset( rows ) && rows.length === 1 ) {
					sessionInfo = rows.shift();
					return;
				}
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "[ERROR] invalid sessionID:", depositionID );
			} ).catch( function( e ) {
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "selectDeposition; selectSessionByID caught:", e.name, e.message );
			} ).finally( function() {
				if( isset( sessionInfo ) ) {
					ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "selectDeposition; session:", sessionInfo );
					deposition.title = sessionInfo.depositionOf;
					deposition.ownerID = parseInt( sessionInfo.ownerID );
					deposition.speakerID = parseInt( (sessionInfo.speakerID) ? sessionInfo.speakerID : sessionInfo.ownerID );
					deposition.class = sessionInfo.class;
					deposition.statusID = sessionInfo.statusID;
					depositions[depositionID] = deposition;
					deposition.save()
					.then( userSelectDeposition );
				}
			} );
		} else {
			deposition = depositions[depositionID];
			userSelectDeposition();
		}

		function userSelectDeposition() {
			if( sessionIsDemo( deposition.class ) && currentUserID === deposition.ownerID ) {
				redisClient.setex( "demo:" + depositionID, config.redis.expire, "i:" + currentUserID + ";", function( err ) {
					if( err ) {
						ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "userSelectDeposition; Error:", err );
					}
				} );
			}

			// create "All Attendees" room if not exists
			if( typeof rooms[depoRoomID] !== "undefined" && rooms[depoRoomID] instanceof Room ) {
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "joining room:", depoRoomID );
			} else {
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "creating room:", depoRoomID );
				createRoom( depositionID, depoRoomID, currentUserID, {"title":"All Attendees", "private":false, "readOnly":true, "isChatEnabled":false}, userSocket );
			}
			deposition.rooms[depoRoomID] = true;
			deposition.users[currentUserID] = true;	// active
			deposition.save().then( function() {
				rooms[depoRoomID].addUser( currentUser, userSocket )
				.then( function() {
					rooms[depoRoomID].save().then( function() {
						ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "userSelectDeposition;", currentUserID, '"' + currentUser.name + '"', "emit: user_did_connect_to_deposition to:", depoRoomID );
						userSocket.broadcast.to( depoRoomID ).emit( "user_did_connect_to_deposition", {"deposition":{"id":depositionID}, "user":currentUser.info()} );

						currentUser.depositionID = depositionID;
						currentUser.save().then( function() {
							rejoinRooms();

							if( typeof callback === "function" ) {
								var _depo = deposition.info();
								var _users = [];
								var _rooms = [];
								for( var userID in _depo.users ) {
									if( _depo.users.hasOwnProperty( userID ) && typeof users[userID] !== "undefined" ) {
										var _user = users[userID].info();
										_user.active = !!_depo.users[userID];
										_users.push( _user );
									}
								}
								for( var roomID in _depo.rooms ) {
									if( _depo.rooms.hasOwnProperty( roomID ) && typeof rooms[roomID] !== "undefined" ) {
										if( typeof rooms[roomID].users[currentUserID] === "undefined" ) {
											continue;
										}
										var _room = rooms[roomID].info( currentUser );
										_rooms.push( _room );
									}
								}
								_depo.users = _users;
								_depo.rooms = _rooms;
								_depo.conductorID = deposition.ownerID;
								//console.log( "selectDeposition; callback:", _depo );
								callback( _depo );
							}
						} );
					} );
				} );
			} );
		}

		function rejoinRooms() {
			return new Promise( function( resolve ) {
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "rejoinRooms; userID:", currentUserID, "depositionID:", currentUser.depositionID, "uid:", currentUser.uid );
				if( typeof depositions[currentUser.depositionID] === "undefined" ) {
					return;
				}
				var deposition = depositions[currentUser.depositionID];
				var roomList = Object.keys( deposition.rooms );
				if( roomList.length === 0 ) {
					return resolve();
				}
				for( var roomID in deposition.rooms ) {
					if( deposition.rooms.hasOwnProperty( roomID ) && typeof rooms[roomID] !== "undefined" ) {
						var _room = rooms[roomID];
						if( !isset( _room.users[ currentUserID ] ) ) {
							//ConsoleLog( "rejoinRooms;", currentUserID, "not member of room:", _room.id );
							continue;
						}
						if( _room.hasOwnProperty( "readOnly" ) && _room.readOnly ) {
							ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "rejoinRooms;", currentUserID, roomID, "-- Room is read only" );
							userSocket.join( roomID );
							roomList.splice( roomList.indexOf( roomID ), 1 );
							if( roomList.length === 0 ) {
								return resolve();
							}
							continue;
						}
						if( _room.hasOwnProperty( "isChatEnabled" ) && !_room.isChatEnabled ) {
							ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "rejoinRooms;", currentUserID, roomID, "-- Room is not chat enabled" );
							userSocket.join( roomID );
							roomList.splice( roomList.indexOf( roomID ), 1 );
							if( roomList.length === 0 ) {
								return resolve();
							}
							continue;
						}
						//ConsoleLog( "rejoinRooms; members of room:", _room.id, Object.keys( _room.users ) );
						for( var userID in _room.users ) {
							if( _room.users.hasOwnProperty( userID ) && currentUserID === parseInt( userID ) ) {
								if( _room.users.hasOwnProperty( userID ) && typeof users[userID] !== "undefined" ) {
									//var roomMember = users[userID];
									//console.log( roomID, "--- user:", roomMember );
									ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "rejoinRooms; user:", userID, "in room:", _room.id );
									userSocket.join( roomID );
									if( !!_room.users[userID] ) {
										ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "rejoinRooms; userID:", userID, "is active in room" );
									} else {
										ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "rejoinRooms; userID:", userID, "JOINING roomID:", roomID, "title:", _room.title );
										_room.addUser( currentUser, userSocket );
										_room.save();
									}
									roomList.splice( roomList.indexOf( roomID ), 1 );
									if( roomList.length === 0 ) {
										return resolve();
									}
								}
								break;
							}
						}
					}
					if( roomList.length === 0 ) {
						return resolve();
					}
				}
			} );
		}
	};
	socket.on( "selectDeposition", selectDeposition );

	/**
	 * Deselect Deposition
	 * @param {Object} args
	 * @param {function} callback
	 */
	function deselectDeposition( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "socket.on(deselectDeposition):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = currentUser = users[currentUserID];
		if( typeof cUser === "undefined" || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "deselectDeposition; Error:", errMsg );
			return;
		}

		// disconnect user
		if( currentUser.depositionID ) {
			var userDepo = depositions[currentUser.depositionID];
			if( userDepo && userDepo instanceof Deposition ) {
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "deselectDeposition; session ID:", currentUser.depositionID, userDepo.depoRoomID );

				// clean up demo
				if( sessionIsDemo( userDepo.class ) && currentUserID === userDepo.ownerID ) {
					ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "deselectDeposition; removing demo owner key" );
					redisClient.del( "demo:" + currentUser.depositionID, function( err ) {
						if( err ) {
							ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "deselectDeposition; Error:", err );
						}
					} );
				}

				var dUser = currentUser.info();
				dUser.active = false;
				userDepo.users[currentUserID] = false;
				userDepo.save().then( function() {
					ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "deselectDeposition;", currentUserID, '"' + currentUser.name + '"', "emit: user_did_disconnect_from_deposition to:", userDepo.depoRoomID );
					userSocket.broadcast.to( userDepo.depoRoomID ).emit( "user_did_disconnect_from_deposition", {"deposition":{"id":currentUser.depositionID}, "user":dUser} );
					ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "deselectDeposition;", currentUserID, '"' + currentUser.name + '"', "leaving (session) room ID:", userDepo.depoRoomID );
					userSocket.leave( userDepo.depoRoomID ); // leave "All Attendees"

					var userRooms = Object.keys( userSocket.rooms );
					if( Array.isArray( userRooms ) && userRooms.length > 0 ) {
						for( var idx in userRooms ) {
							var roomID = userRooms[idx];
							//ConsoleLog( "deselectDeposition; roomID:", roomID );
							if( roomID !== "" && roomID !== currentUser.uid ) {
								ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "deselectDeposition;", currentUserID, '"' + currentUser.name + '"', "leaving room ID:", roomID );
								userSocket.leave( roomID );
								if( typeof rooms[roomID] !== "undefined" && rooms[roomID] instanceof Room ) {
									rooms[roomID].users[currentUserID] = false;
									rooms[roomID].save()
									.then( function() {
										ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "deselectDeposition;", currentUserID, '"' + currentUser.name + '"', "left room ID:", roomID );
									} );
								}
							}
						}
					}

					currentUser.depositionID = null;
					currentUser.save().then( function() {
						if( typeof callback === "function" ) {
							callback( {"success":true} );
						}
					} );
				} );
			} else {
				if( typeof callback === "function" ) {
					callback( {"success":true} ); // lies
				}
			}
		} else {
			errMsg = "Session not selected";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "deselectDeposition; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
		}
	}
	socket.on( "deselectDeposition", deselectDeposition );

	// chat

	/**
	 * Join To Room
	 * @param {Object} args
	 * @param {function} callback
	 */
	function joinToRoom( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "socket.on(join_to_room):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = currentUser = users[currentUserID];
		if( typeof cUser === "undefined" || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "joinToRoom; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var _room = null;
		var deposition;
		var didCreateRoom = false;
		if( cUser.depositionID && typeof depositions[cUser.depositionID] !== "undefined" ) {
			deposition = depositions[cUser.depositionID];
			var roomID = args.id;
			if( typeof rooms[roomID] !== "undefined" ) {
				_room = rooms[roomID];
			} else {
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "creating room:", roomID, "creatorID:", currentUserID, args );
				didCreateRoom = true;
				createRoom( cUser.depositionID, roomID, currentUserID, {"title":args.title, "private":args.private, "readOnly":false, "isChatEnabled":true}, userSocket );
				_room = rooms[roomID];
			}
		} else {
			errMsg = "Session not selected";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
				return;
			}
		}

		if( !_room ) {
			errMsg = "Room not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "joinToRoom; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}

		if( typeof _room.users[currentUserID] !== "undefined" && !didCreateRoom ) {
			userSocket.join( _room.id ); // join room
			errMsg = "Already in room";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "joinToRoom:", errMsg, _room.id, currentUserID );
			if( cUser.depositionID && typeof depositions[cUser.depositionID] !== "undefined" ) {
				deposition = depositions[cUser.depositionID];
				deposition.rooms[roomID] = true;
				deposition.save();
			}
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}

		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "User join to room:", args.id, currentUserID, '"' + cUser.name + '"', cUser.uid );

		_room.addUser( cUser, userSocket );
		_room.save()
		.then( function() {
			if( cUser.depositionID && typeof depositions[cUser.depositionID] !== "undefined" ) {
				deposition = depositions[cUser.depositionID];
				deposition.rooms[roomID] = true;
				deposition.save();
			}
			if( typeof callback === "function" ) {
				var roomInfo = _room.info( cUser );
				callback( roomInfo );
			}
		} );
	}
	socket.on( "join_to_room", joinToRoom );

	/**
	 * Leave Room
	 * @param {Object} args
	 * @param {function} callback
	 */
	function leaveRoom( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "socket.on(leave_room):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = currentUser = users[currentUserID];
		if( typeof cUser === "undefined" || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "leaveRoom; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		// leave room if user in deposition and room exists
		if( cUser.depositionID && typeof depositions[cUser.depositionID] !== "undefined" && typeof rooms[args.id] !== "undefined" ) {
			rooms[args.id].removeUser( cUser, userSocket );
			rooms[args.id].save()
			.then( function() {
				if( typeof callback === "function" ) {
					callback( {"success":true} );
				}
			} );
		} else {
			errMsg = "Room does not exist in selected session";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "leaveRoom; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
		}
	}
	socket.on( "leave_room", leaveRoom );

	/**
	 * Rename Room
	 * @param {Object} args
	 * @param {function} callback
	 */
	function renameRoom( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "socket.on(rename_room):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = currentUser = users[currentUserID];
		if( typeof cUser === "undefined" || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "renameRoom; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( cUser.depositionID && typeof depositions[cUser.depositionID] !== "undefined" ) {
			//var userDepo = depositions[cUser.depositionID];
			var roomID = args.id;
			// if room exists
			if( typeof rooms[roomID] !== "undefined" ) {
				var _room = rooms[roomID];
				if( args.hasOwnProperty( "title" ) && typeof args.title === "string" ) {
					// if user is creator of this room
					if( currentUserID === _room.creatorID ) {
						_room.title = args.title;
						sIO.to( roomID ).emit( "room_renamed", {"room":{"id":roomID, "title":_room.title, "users":_room.info().users}} );
						if( typeof callback === "function" ) {
							callback( {"success":true} );
						}
						return;
					} else {
						errMsg = "User not creator";
						ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "renameRoom; Error:", errMsg );
						if( typeof callback === "function" ) {
							callback( {"success":false, "error":errMsg} );
						}
						return;
					}
				} else {
					errMsg = "Missing 'title' parameter";
					ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "renameRoom; Error:", errMsg );
					if( typeof callback === "function" ) {
						callback( {"success":false, "error":errMsg} );
					}
					return;
				}
			} else {
				errMsg = "Room does not exist";
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "renameRoom; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
		} else {
			errMsg = "Session not selected";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "renameRoom; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	}
	socket.on( "rename_room", renameRoom );

	/**
	 * Close Room
	 * @param {Object} args
	 * @param {function} callback
	 */
	function closeRoom( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "socket.on(close_room):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = currentUser = users[currentUserID];
		if( typeof cUser === "undefined" || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "closeRoom; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( !cUser.depositionID ) {
			errMsg = "Session not selected";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "closeRoom; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
				return;
			}
		}
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "closeRoom; cUser.depositionID:", cUser.depositionID );
		var _room = rooms[args.id];
		if( !_room || !(_room instanceof Room) ) {
			errMsg = "Room does not exist";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "closeRoom; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
				return;
			}
		}
		var roomID = _room.id;
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "closeRoom; roomID:", roomID );
		if( cUser.id !== _room.creatorID || _room.readOnly ) {
			errMsg = "User not room creator";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "closeRoom; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
				return;
			}
		}
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "closeRoom; Closing roomID:", roomID );
		sIO.to( roomID ).emit( "room_closed", {"room":{"id":roomID}} );
		for( var userID in _room.users ) {
			if( _room.users.hasOwnProperty( userID ) && typeof users[userID] !== "undefined" && typeof sockets[cUser.uid] !== "undefined" ) {
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "closeRoom; UserID:", userID, "leaving room:", roomID, '"' + _room.title + '"' );
				sockets[cUser.uid].leave( roomID );
				_room.removeUser( users[userID], sockets[cUser.uid] );
			}
		}
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "closeRoom; Deleting roomID:", roomID );
		redisClient.del( _room.redisKey, function( err ) {
			if( err ) {
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "closeRoom; Error:", err );
			}
		} );
		delete rooms[roomID];
		delete depositions[cUser.depositionID].rooms[roomID];
		depositions[cUser.depositionID].save()
		.then( function() {
			redisPub.publish( "metadata", JSON.stringify( {"nodeId":nodeId, "args":{"event":"Room.close", "args":roomID}} ) );
		} );
	};
	socket.on( "close_room", closeRoom );

	/**
	 * Invite Users To Room
	 * @param {Object} args
	 * @param {function} callback
	 */
	function inviteUsersToRoom( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "socket.on(inviteUsersToRoom):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = currentUser = users[currentUserID];
		if( typeof cUser === "undefined" || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "inviteUsersToRoom; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( !cUser.depositionID || typeof depositions[cUser.depositionID] === "undefined" ) {
			errMsg = "Session not selected";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "inviteUsersToRoom; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var userDepo = depositions[cUser.depositionID];
		if( typeof rooms[args.roomID] === "undefined" ) {
			errMsg = "Room does not exist";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "inviteUsersToRoom; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var _room = rooms[args.roomID];
		if( typeof _room.users[currentUserID] === "undefined" ) {
			errMsg = "User not in room";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "inviteUsersToRoom; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		// prepare a soft invite for every member of the room
		for( var i=0; i<args.users.length; ++i ) {
			var userID = parseInt( args.users[i] );
			var joinTS = Math.round( (new Date()).getTime() / 1000 );
			if( typeof userDepo.users[userID] !== "undefined" ) {
				_room.users[userID] = userDepo.users[userID];
				_room.joinTimestamp[userID] = (typeof _room.joinTimestamp[userID] === "number" ? _room.joinTimestamp[userID] : joinTS );
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "inviteUsersToRoom; soft invite -- added userID:", userID );
			} else {
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "inviteUsersToRoom; soft invite -- user not found in session:", userDepo.id, "by ID:", userID );
			}
		}
		_room.save()
		.then( function() {
			var room = _room.info();
			var roomInvite = {"room":room, "user":cUser.info()};
			// send invitations to all connected room members
			for( var i=0; i<args.users.length; ++i ) {
				var userID = parseInt( args.users[i] );
				if( typeof users[ userID ] !== "undefined" && typeof sockets[ users[ userID ].uid ] !== "undefined" ) {
					var _user = users[ userID ];
					if( typeof _user !== "undefined" ) {
						ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "inviteUsersToRoom; emit: invite", userID, roomInvite );
						sockets[_user.uid].emit( "invite", roomInvite );
					}
				} else {
					ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "inviteUsersToRoom; user not found by ID:", userID );
				}
			}
			if( typeof callback === "function" ) {
				callback( {"success":true} );
			}
		} );
	};
	socket.on( "inviteUsersToRoom", inviteUsersToRoom );

	/**
	 * Remove Users From Room
	 * @param {Object} args
	 * @param {function} callback
	 */
	function removeUsersFromRoom( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "socket.on(remove_users_from_room):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = currentUser = users[currentUserID];
		if( typeof cUser === "undefined" || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "removeUsersFromRoom; Error", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( cUser.depositionID ) {
			var roomID = args.id;
			if( typeof rooms[roomID] !== "undefined" ) {
				var _room = rooms[roomID];
				if( args.users ) {
					if( currentUserID === _room.creatorID ) {
						for( i=0; i<args.users.length; i++ ) {
							var userID = parseInt( args.users[i] );
							if( typeof users[userID] !== "undefined" ) {
								if( typeof _room.users[userID] !== "undefined" && typeof sockets[users[userID]] !== "undefined" ) {
									_room.removeUser( users[userID], sockets[users[userID].uid] );
								}
							}
							_room.save();
						}
					} else {
						errMsg = "User is not creator";
						ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "removeUsersFromRoom; Error", errMsg );
						if( typeof callback === "function" ) {
							callback( {"success":false, "error":errMsg} );
						}
						return;
					}
				} else {
					errMsg = "Missing 'users' parameter";
					ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "removeUsersFromRoom; Error", errMsg );
					if( typeof callback === "function" ) {
						callback( {"success":false, "error":errMsg} );
					}
					return;
				}
			} else {
				errMsg = "Room does not exist";
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "removeUsersFromRoom; Error", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
		} else {
			errMsg = "Session not selected";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "removeUsersFromRoom; Error", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	}
	socket.on( "remove_users_from_room", removeUsersFromRoom );

	/**
	 * Remove Users From Room
	 * @param {Object} args
	 * @param {function} callback
	 */
	function messageToRoom( args, callback ) {
		var sanitizedArgs = Object.assign( {}, args ); // clone
		delete sanitizedArgs.message;
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "socket.on(message_to_room):", currentUserID, '"' + currentUser.name + '"', sanitizedArgs );
		var errMsg;
		var cUser = currentUser = users[currentUserID];
		if( typeof cUser === "undefined" || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( cUser.depositionID && typeof depositions[cUser.depositionID] !== "undefined" ) {
			//var userDepo = depositions[cUser.depositionID];
			if( typeof rooms[args.roomID] !== "undefined" ) {
				var _room = rooms[args.roomID];
				if( typeof _room.users[cUser.id] !== "undefined" ) {
					var message = new Message( args.roomID, args.message, currentUserID, args.system );
					if( typeof messages[args.roomID] === "undefined" ) {
						ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "creating room message history" );
						messages[args.roomID] = [];
					}
					messages[args.roomID].push( message );
					message.save()
					.then( function() {
						sIO.to( args.roomID ).emit( "message_to_room", message.info() );
						if( typeof callback === "function" ) {
							callback( {"success":true} );
						}
					} );
				} else {
					ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "User Not In ROOM:", args.roomID, cUser.info() );
					if( typeof callback === "function" ) {
						callback( {"success":false, "error":"User not in room"} );
					}
				}
			} else {
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":"Room does not exist"} );
				}
			}
		} else {
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":"Session not selected"} );
			}
		}
	}
	socket.on( "message_to_room", messageToRoom );

	// sharing

	/**
	 * Confirm Sharing
	 * @param {Object} args
	 * @param {function} callback
	 */
	function confirmSharing( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "socket.on(confirm_sharing):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = currentUser = users[currentUserID];
		if( typeof cUser === "undefined" || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "confirmSharing; Error", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( typeof users[args.userID] !== "undefined" ) {
			sIO.to( users[args.userID].uid ).emit( "sharing_confirmed", {"shareID":args.shareID, "user":cUser.info()} );
			if( typeof callback === "function" ) {
				callback( {"success":true} );
			}
		} else if( typeof callback === "function" ) {
			errMsg = "User does not exist or is offline";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "confirmSharing; Error", errMsg );
			callback( {"success":false, "error":errMsg} );
		}
	}
	socket.on( "confirm_sharing", confirmSharing );

	/**
	 * Cancel Sharing
	 * @param {Object} args
	 * @param {function} callback
	 */
	function cancelSharing( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "socket.on(cancel_sharing):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = currentUser = users[currentUserID];
		if( typeof cUser === "undefined" || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "cancelSharing; Error", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( typeof users[args.userID] !== "undefined" ) {
			sIO.sockets.socket( users[args.userID].uid ).emit( "sharing_canceled", {"shareID":args.shareID, "user":cUser.info()} );
			if( typeof callback === "function" ) {
				callback( {"success":true} );
			}
		} else if( typeof callback === "function" ) {
			errMsg = "User does not exist or is offline";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "cancelSharing; Error", errMsg );
			callback( {"success":false, "error":errMsg} );
		}
	}
	socket.on( "cancel_sharing", cancelSharing );

	/**
	 * Complete Sharing
	 * @param {Object} args
	 * @param {function} callback
	 */
	function completeSharing( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "socket.on(complete_sharing):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = currentUser = users[currentUserID];
		if( typeof cUser === "undefined" || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "completeSharing; Error", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( typeof users[args.userID] !== "undefined" ) {
			sIO.sockets.socket( users[args.userID].uid ).emit( "sharing_completed", {"shareID":args.shareID, "user":cUser.info()} );
			if( typeof callback === "function" ) {
				callback( {"success":true} );
			}
		} else if( typeof callback === "function" ) {
			errMsg = "User does not exist or is offline";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "completeSharing; Error", errMsg );
			callback( {"success":false, "error":errMsg} );
		}
	}
	socket.on( "complete_sharing", completeSharing );

	/**
	 * Sharing Progress
	 * @param {Object} args
	 * @param {function} callback
	 */
	function sharingProgress( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "socket.on(progress_sharing):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = currentUser = users[currentUserID];
		if( typeof cUser === "undefined" || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "sharingProgress; Error", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( typeof users[args.userID] !== "undefined" ) {
			sIO.sockets.socket( users[args.userID].uid ).emit( "sharing_updated", {"shareID":args.shareID, "progress":args.progress, "user":cUser.info()} );
			if( typeof callback === "function" ) {
				callback( {"success":true} );
			}
		} else if( typeof callback === "function" ) {
			errMsg = "User does not exist or is offline";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "sharingProgress; Error", errMsg );
			callback( {"success":false, "error":errMsg} );
		}
	}
	socket.on( "progress_sharing", sharingProgress );

	// witness

	/**
	 * Witness Login
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function witnessLogin( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "socket.on(witnessLogin):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		if( !args || typeof args !== "object" || !args.hasOwnProperty( "depositionID" ) || typeof depositions[args.depositionID] === "undefined" || !(depositions[args.depositionID] instanceof Deposition) ) {
			errMsg = "Invalid session ID";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "witnessLogin; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			userSocket.disconnect();
			return;
		}
		if( !args.hasOwnProperty( "witnessID" ) ) {
			errMsg = "Missing argument: witnessID";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "witnessLogin; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			userSocket.disconnect();
			return;
		}
		if( !args.hasOwnProperty( "name" ) ) {
			errMsg = "Missing argument: name";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "witnessLogin; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			userSocket.disconnect();
			return;
		}
		var depositionID = parseInt( args.depositionID );
		var _depo = depositions[depositionID];
		var witnessID = parseInt( args.witnessID );
		var witnessName = args.name;
		var witness = users[witnessID];
		if( !isset( witness ) || !isset( witness.isTempWitness ) || !witness.isTempWitness ) {
			errMsg = "Invalid witness";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "witnessLogin; Error:", errMsg, witness );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			userSocket.disconnect();
			return;
		}
		var conductor;
		if( typeof _depo.users[_depo.ownerID] !== "undefined" && typeof users[_depo.ownerID] !== "undefined" ) {
			conductor = users[_depo.ownerID];
			if( !conductor.depositionID || parseInt( conductor.depositionID ) !== parseInt( _depo.id ) ) {
				conductor = false;
			}
		}
		if( !conductor ) {
			errMsg = "Request is unable to be authorized";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "witnessLogin; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			userSocket.disconnect();
			return;
		}
		witness.depositionID = depositionID;
		witness.save();
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "witness asking for authorization", witness );
		sIO.to( conductor.uid ).emit( "witness_login", {"depositionID":depositionID, "witnessID":witnessID, "name":witnessName} );
		if( typeof callback === "function" ) {
			callback( {"success":true} );
		}
	}
	socket.on( "witnessLogin", witnessLogin );

	/**
	 * Deny Witness Login
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function rejectWitnesses( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(rejectWitnesses):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === "undefined" || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "rejectWitnesses;", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var depositionID = cUser.depositionID;
		if( typeof depositions[depositionID] === "undefined" || depositions[depositionID] === null || depositions[depositionID].ownerID !== cUser.id ) {
			errMsg = "Access denied";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "rejectWitnesses;", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var allUsers = Object.keys( users );
		for( var u=0; u<allUsers.length; ++u ) {
			var _userID = allUsers[u];
			var _user = users[_userID];
			if( typeof _user === "undefined" || _user === null ) {
				continue;
			}
			if( typeof _user.depositionID === "undefined" || _user.depositionID === null || _user.depositionID !== depositionID ) {
				continue;
			}
			if( typeof _user.isTempWitness === "undefined" || !_user.isTempWitness ) {
				continue;
			}
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "rejectWitnesses;", depositionID, " witness:", _user.id );
			sIO.to( _user.uid ).emit( "rejected_witness", {depositionID:depositionID, witnessID:_user.id} );
			if( typeof sockets[_user.uid] !== "undefined" ) {
				sockets[_user.uid].disconnect();
			}
			delete users[_user.id];
		}
	}
	socket.on( "rejectWitnesses", rejectWitnesses );

	/**
	 * Block Exhibits
	 * @param {Object} args
	 * @param {function} callback
	 */
	function exhibitsBlock( args, callback ) {
		// TODO -- needs more security around witness
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(exhibits_block):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var deposition = depositions[args.depositionID];
		var witness = users[args.userID];
		if( typeof deposition !== "undefined" && typeof witness !== "undefined" ) {
			witness.blockExhibits = !!args.block; // cast
			witness.save()
			.then( function() {
				var params = {"depositionID":deposition.id, "userID":witness.id, "block":witness.blockExhibits};
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "exhibitsBlock; emit:", JSON.stringify( params ) );
				sIO.to( witness.uid ).emit( "exhibits_block", params );
				if( typeof callback === "function" ) {
					callback( {"success":true} );
				}
			} );
		} else {
			errMsg = "invalid parameters";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "exhibitsBlock; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
		}
	};
	socket.on( "exhibitsBlock", exhibitsBlock );
	socket.on( "exhibits_block", exhibitsBlock );

	// presentation

	/**
	 * Start Presentation
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function startPresentation( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(startPresentation):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = users[currentUserID];
		if( !isset( cUser ) || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "startPresentation; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( !args || typeof args !== "object" || !args.hasOwnProperty( "depositionID" ) ) {
			errMsg = "Invalid session ID";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "startPresentation; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"error":errMsg} );
			}
			return;
		}
		var depositionID = args.depositionID;
		if( typeof depositions[ depositionID ] !== "undefined" ) {
			var _depo = depositions[ depositionID ];
			if( typeof presentations[ depositionID ] !== "undefined" ) {
				errMsg = "Presentation already started for this session";
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "startPresentation; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
			if( parseInt( currentUserID ) !== parseInt( _depo.speakerID ) ) {
				errMsg = "Only the leader may start a presentation";
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "startPresentation; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
			if( _depo.users[currentUserID] === "undefined" || !cUser.depositionID || parseInt( cUser.depositionID ) !== parseInt( _depo.id ) ) {
				errMsg = "Cannot start a presentation for a session you are not attending";
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "startPresentation; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
			if( !isset( args.roomID ) || !args.roomID || !isset( _depo.rooms[ args.roomID ] ) || !isset( rooms[ args.roomID ] ) || !(rooms[ args.roomID ] instanceof Room) ) {
				errMsg = "Invalid roomID";
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "startPresentation; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
			var roomID = args.roomID;
			if( typeof rooms[roomID].users[ currentUserID ] === "undefined" ) {
				errMsg = "Invalid roomID";
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "startPresentation; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
			if( !args.hasOwnProperty( "fileID" ) || !(args.fileID > 0) ) {
				errMsg = "Invalid fileID";
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "startPresentation; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
			if( typeof args.pageIndex === "undefined" || !(args.pageIndex >= 0) ) {
				errMsg = "Invalid pageIndex";
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "startPresentation; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
			if( typeof args.orientation === "undefined" || typeof args.orientation !== "string" || (args.orientation !== "landscape" && args.orientation !== "portrait") ) {
				errMsg = "Invalid orientation";
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "startPresentation; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
			var presentation = new Presentation( depositionID );
			presentation.annotatorID = _depo.speakerID;
			presentation.roomID = roomID;
			presentation.subscribers.push( currentUserID );
			presentation.orientation = args.orientation;
			presentation.fileID = args.fileID;
			presentation.pageIndex = args.pageIndex;
			var eventData = presentation.sanitizeProperties( args );
			for( var _prop in eventData ) {
				if( eventData.hasOwnProperty( _prop ) && presentation.hasOwnProperty( _prop ) ) {
					ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER', "startPresentation;", _prop, "=>", eventData[ _prop ] );
					presentation[ _prop ] = eventData[ _prop ];
				}
			}
			presentations[depositionID] = presentation;
			presentation.save( function() {
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "startPresentation;", presentation.info() );
				userSocket.broadcast.to( roomID ).emit( "startPresentation", presentation.info() );
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "startPresentation; started!" );
				if( typeof callback === "function" ) {
					callback( {"success":true} );
				}
			} );
			return;
		} else {
			errMsg = "Invalid session ID";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "startPresentation; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	}
	socket.on( "startPresentation", startPresentation );

	/**
	 * End Presentation
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function endPresentation( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(endPresentation):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = users[currentUserID];
		if( !isset( cUser ) || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "endPresentation; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( !args || typeof args !== "object" || !args.hasOwnProperty( "depositionID" ) ) {
			errMsg = "Invalid session ID";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "endPresentation; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var depositionID = args.depositionID;
		if( isset( depositions[ depositionID ] ) ) {
			var _depo = depositions[ depositionID ];
			if( isset( presentations[ depositionID ] ) ) {
				if( parseInt( currentUserID ) !== parseInt( _depo.speakerID ) ) {
					errMsg = "Only the leader may end the presentation";
					ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "endPresentation; Error:", errMsg );
					if( typeof callback === "function" ) {
						callback( {"success":false, "error":errMsg} );
					}
					return;
				}
				if( _depo.users[currentUserID] === "undefined" || typeof cUser.depositionID === "undefined" || parseInt( cUser.depositionID ) !== parseInt( _depo.id ) ) {
					errMsg = "Cannot end a presentation for a session you are not attending";
					ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "endPresentation; Error:", errMsg );
					if( typeof callback === "function" ) {
						callback( {"success":false, "error":errMsg} );
					}
					return;
				}
				var presentation = presentations[depositionID];
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "endPresentation", _depo.id, presentation.roomID );
				presentation.deleteAllCallouts( function() {
					presentation.deleteAllAnnotations( function() {
						redisClient.del( presentation.redisKey, function( err ) {
							if( err ) {
								ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "endPresentation; Error:", err );
							}
						} );
						ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "endPresentation; emit: endPresentation" );
						userSocket.broadcast.to( presentation.roomID ).emit( "endPresentation" );
						delete presentations[ depositionID ];
						redisPub.publish( "metadata", JSON.stringify( {"nodeId":nodeId, "args":{"event":"Presentation.end", "args":depositionID}} ) );
						if( typeof callback === "function" ) {
							callback( {"success":true} );
						}
					} );
				} );
				return;
			} else {
				errMsg = "Cannot end an unknown presentation";
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "endPresentation; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
		} else {
			errMsg = "Invalid session ID";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "endPresentation; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	}
	socket.on( "endPresentation", endPresentation );

	/**
	 * Join Presentation
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function joinPresentation( args, callback )	{
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(joinPresentation):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = users[ currentUserID ];
		if( !isset( cUser ) || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "joinPresentation;", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( !args || typeof args !== "object" || !args.hasOwnProperty( "depositionID" ) || typeof depositions[args.depositionID] === "undefined" || !(depositions[args.depositionID] instanceof Deposition) ) {
			errMsg = "Invalid session ID";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "joinPresentation;", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var depositionID = parseInt( args.depositionID );
		var _depo = depositions[depositionID];
		if( typeof _depo.users[currentUserID] === "undefined" || !cUser.depositionID || parseInt( cUser.depositionID ) !== parseInt( _depo.id ) ) {
			errMsg = "Cannot join presentation for a session you are not attending";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "joinPresentation;", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( typeof presentations[depositionID] === "undefined" ) {
			errMsg = "No presentation to join for this session";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "joinPresentation;", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var presentation = presentations[depositionID];
		if( typeof rooms[presentation.roomID].users[currentUserID] === "undefined" ) {
			errMsg = "Access denied";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "joinPresentation;", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var userID = parseInt( currentUserID );
		if( presentation.subscribers.indexOf( userID ) === -1 ) {
			presentation.subscribers.push( userID );
			presentation.save( function() {
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "joinPresentation; emit: userDidJoinPresentation", currentUserID, '"' + currentUser.name + '"' );
				userSocket.broadcast.to( presentation.roomID ).emit( "userDidJoinPresentation", {user:cUser.info()} );
			} );
		}
		if( typeof callback === "function" ) {
			callback( {"success":true, "presentationInfo":presentation.info()} );
		}
	}
	socket.on( "joinPresentation", joinPresentation );

	/**
	 * Leave Presentation
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function leavePresentation( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(leavePresentation):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = users[currentUserID];
		if( !isset( cUser ) || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "leavePresentation; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( !args || typeof args !== "object" || !args.hasOwnProperty( "depositionID" ) || typeof depositions[args.depositionID] === "undefined" || !(depositions[args.depositionID] instanceof Deposition) ) {
			errMsg = "Invalid session ID";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "leavePresentation; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var depositionID = parseInt( args.depositionID );
		var _depo = depositions[ depositionID ];
		if( typeof _depo.users[ currentUserID ] === "undefined" || !cUser.depositionID || parseInt( cUser.depositionID ) !== parseInt( _depo.id ) ) {
			errMsg = "Cannot leave presentation for a session you are not attending";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "leavePresentation; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( typeof presentations[depositionID] === "undefined" ) {
			errMsg = "No presentation to leave for this session";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "leavePresentation; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var presentation = presentations[depositionID];
		if( typeof rooms[presentation.roomID].users[currentUserID] === "undefined" ) {
			errMsg = "Access denied";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "leavePresentation; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var userID = parseInt( currentUserID );
		if( userID === parseInt( _depo.speakerID ) ) {
			errMsg = "Cannot leave your own presentation";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "leavePresentation; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var userIndex = presentation.subscribers.indexOf( userID );
		if( userIndex !== -1 ) {
			presentation.subscribers.splice( userIndex, 1 );
			presentation.save( function() {
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "leavePresentation; emit: userDidLeavePresentation", currentUserID, '"' + currentUser.name + '"' );
				userSocket.broadcast.to( presentation.roomID ).emit( "userDidLeavePresentation", {user:cUser.info()} );
			} );
		}
		if( typeof callback === "function" ) {
			callback( {"success":true} );
		}
	}
	socket.on( "leavePresentation", leavePresentation );

	/**
	 * Presentation Status
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function presentationStatus( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(presentationStatus):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = users[currentUserID];
		if( !isset( cUser ) || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "presentationStatus; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( !args || typeof args !== "object" || !args.hasOwnProperty( "depositionID" ) || typeof depositions[args.depositionID] === "undefined" || !(depositions[args.depositionID] instanceof Deposition) ) {
			errMsg = "Invalid session ID";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "presentationStatus; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var depositionID = parseInt( args.depositionID );
		var _depo = depositions[depositionID];
		if( typeof _depo.users[currentUserID] === "undefined" || !cUser.depositionID || parseInt( cUser.depositionID ) !== parseInt( _depo.id ) ) {
			errMsg = "Cannot get presentation status for a session you are not attending";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "presentationStatus; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( typeof presentations[depositionID] === "undefined" ) {
			errMsg = "Unknown presentation";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "presentationStatus; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var presentation = presentations[ depositionID ];
		if( typeof rooms[ presentation.roomID ].users[ currentUserID ] === "undefined" ) {
			errMsg = "Access denied";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "presentationStatus; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( typeof callback === "function" ) {
			callback( {"success":true, "presentationInfo":presentation.info()} );
		}
	}
	socket.on( "presentationStatus", presentationStatus );

	/**
	 * Set Page Properties
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function setPageProperties( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(setPageProperties):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		if( !args || typeof args !== "object" || !args.hasOwnProperty( "depositionID" ) ) {
			errMsg = "Invalid session ID";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "setPageProperties; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( !args || typeof args !== "object" || !args.hasOwnProperty( "eventData" ) || typeof args.eventData !== "object" ) {
			errMsg = "Missing required event data";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "setPageProperties; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var depositionID = parseInt( args.depositionID );
		var isValidResult = validatePresentationAnnotator( currentUserID, depositionID );
		if( isValidResult === true ) {
			if( typeof presentations[depositionID] !== "undefined" ) {
				var presentation = presentations[depositionID];
				var eventData = presentation.sanitizeProperties( args.eventData );
				presentation.assign( eventData );
				presentation.save( function() {
					var subscribers = presentation.subscribers.slice( 0 ); // clone
					var indexOfSelf = subscribers.indexOf( currentUserID );
					if( indexOfSelf >= 0 ) {
						subscribers.splice( indexOfSelf, 1 );
					}
					notifySubscribers( subscribers, "setPageProperties", eventData );
					if( typeof callback === "function" ) {
						callback( {"success":true} );
					}
				} );
				return;
			} else {
				errMsg = "Unknown presentation";
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "setPageProperties; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
		} else {
			errMsg = isValidResult;
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "setPageProperties; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	}
	socket.on( "setPageProperties", setPageProperties );

	/**
	 * Set Presentation File
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function setPresentationFile( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(setPresentationFile):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var depositionID = parseInt( args.depositionID );
		var fileID = parseInt( args.fileID );
		var isValidResult = validatePresentationLeader( currentUserID, depositionID );
		if( isValidResult === true && !isNaN( fileID ) && fileID > 0 ) {
			var presentation = presentations[depositionID];
			var eventData = presentation.sanitizeProperties( args );
			presentation.assign( eventData );
			presentation.fileID = fileID;
			presentation.deleteAllCallouts( function() {
				presentation.deleteAllAnnotations( function() {
					presentation.save( function() {
						var subscribers = presentation.subscribers.slice( 0 ); // clone
						var indexOfSelf = subscribers.indexOf( currentUserID );
						if( indexOfSelf >= 0 ) {
							subscribers.splice( indexOfSelf, 1 );
						}
						ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "setPresentationFile; notify subscribers: reloadPresentationSource", subscribers );
						notifySubscribers( subscribers, "reloadPresentationSource", presentation );
						if( typeof callback === "function" ) {
							callback( {"success":true} );
						}
					} );
				} );
			} );
		} else {
			errMsg = isValidResult;
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
		}
	}
	socket.on( "setPresentationFile", setPresentationFile );

	/**
	 * Set Annotator
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function setAnnotator( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(setAnnotator):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		if( typeof args !== "object" || !args.hasOwnProperty( "eventData" ) || typeof args.eventData !== "object" || !args.eventData.hasOwnProperty( "userID" ) ) {
			errMsg = "Inhvalid arguments";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "setAnnotator; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var depositionID = parseInt( args.depositionID );
		var annotatorID = parseInt( args.eventData.userID );
		var isValidResult = validatePresentationLeader( currentUserID, depositionID );
		if( isValidResult === true ) {
			var _depo = depositions[depositionID];
			if( !isNaN( annotatorID ) && typeof _depo.users[annotatorID] !== "undefined" && typeof users[annotatorID] !== "undefined" ) {
				var _user = users[annotatorID];
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "setAnnotator; Annotator:", _user );
				if( (_user.userType === "M" && _user.id === _depo.speakerID) || (_user.userType === "W" || _user.userType === "WM") ) {
					if( isset( presentations[depositionID] ) ) {
						var presentation = presentations[depositionID];
						presentation.annotatorID = annotatorID;
						presentation.save( function() {
							sIO.to( presentation.roomID ).emit( "setAnnotator", {"annotatorID":annotatorID} );
							if( typeof callback === "function" ) {
								callback( {"success":true} );
							}
						} );
					}
				} else {
					errMsg = "Invalid user";
					ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "setAnnotator; Error:", errMsg );
					if( typeof callback === "function" ) {
						callback( {"success":false, "error":errMsg} );
					}
					return;
				}
			} else {
				errMsg = "User not found";
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "setAnnotator; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
		}
	}
	socket.on( "setAnnotator", setAnnotator );

	/**
	 * Add Annotation
	 * @param {Object} args Argument parameters
	 * @param {Function} callback
	 */
	function addAnnotation( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(addAnnotation):", currentUserID, '"' + currentUser.name + '"', args );
		var sessionID = parseInt( args.depositionID );
		var presentation = presentations[sessionID];
		if( isset( presentation ) ) {
			presentation.eventQueue.push( {"sessionID":sessionID, "userID":currentUserID, "op":"addAnnotation", "args":args, "callback":callback} );
			presentationProcessEventQueue( presentation );
		} else {
			errMsg = "Presentation not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "addAnnotation; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	}
	socket.on( "addAnnotation", addAnnotation );

	/**
	 * Modify Annotation
	 * @param {Object} args Argument parameters
	 * @param {Function} callback
	 */
	function modifyAnnotation( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(modifyAnnotation):", currentUserID, '"' + currentUser.name + '"', args );
		var sessionID = parseInt( args.depositionID );
		var presentation = presentations[sessionID];
		if( isset( presentation ) ) {
			presentation.eventQueue.push( {"sessionID":sessionID, "userID":currentUserID, "op":"modifyAnnotation", "args":args, "callback":callback} );
			presentationProcessEventQueue( presentation );
		} else {
			errMsg = "Presentation not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "modifyAnnotation; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	}
	socket.on( "modifyAnnotation", modifyAnnotation );

	/**
	 * Delete Annotation
	 * @param {Object} args Argument parameters
	 * @param {Function} callback
	 */
	function deleteAnnotation( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(deleteAnnotation):", currentUserID, '"' + currentUser.name + '"', args );
		var sessionID = parseInt( args.depositionID );
		var presentation = presentations[sessionID];
		if( isset( presentation ) ) {
			presentation.eventQueue.push( {"sessionID":sessionID, "userID":currentUserID, "op":"deleteAnnotation", "args":args, "callback":callback} );
			presentationProcessEventQueue( presentation );
		} else {
			errMsg = "Presentation not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "deleteAnnotation; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	}
	socket.on( "deleteAnnotation", deleteAnnotation );

	/**
	 * Initial Annotation
	 * @param {Object} args Argument parameters
	 * @param {Function} callback
	 */
	function initialAnnotation( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(initialAnnotation):", currentUserID, '"' + currentUser.name + '"', args );
		var sessionID = parseInt( args.depositionID );
		var presentation = presentations[sessionID];
		if( isset( presentation ) ) {
			presentation.eventQueue.push( {"sessionID":sessionID,"userID":currentUserID,"op":"initialAnnotation","args":args,"callback":callback} );
			presentationProcessEventQueue( presentation );
		} else {
			errMsg = "Presentation not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "initialAnnotation; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	}
	socket.on( "initialAnnotation", initialAnnotation );

	/**
	 * Clear Annotations
	 * @param {Object} args Argument parameters
	 * @param {Function} callback
	 */
	function clearAnnotations( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(clearAnnotations):", currentUserID, '"' + currentUser.name + '"', args );
		var sessionID = parseInt( args.depositionID );
		var presentation = presentations[sessionID];
		if( isset( presentation ) ) {
			presentation.eventQueue.push( {"sessionID":sessionID,"userID":currentUserID,"op":"clearAnnotations","args":args,"callback":callback} );
			presentationProcessEventQueue( presentation );
		} else {
			errMsg = "Presentation not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "clearAnnotations; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	}
	socket.on( "clearAnnotations", clearAnnotations );

	function annotationUndo( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(annotationUndo):", currentUserID, '"' + currentUser.name + '"', args );
		var sessionID = parseInt( args.depositionID );
		var presentation = presentations[sessionID];
		if( isset( presentation ) ) {
			presentation.eventQueue.push( {"sessionID":sessionID,"userID":currentUserID,"op":"annotationUndo","args":args,"callback":callback} );
			presentationProcessEventQueue( presentation );
		} else {
			errMsg = "Presentation not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "annotationUndo; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	}
	socket.on( "annotationUndo", annotationUndo );

	function getAnnotations( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(getAnnotations):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === "undefined" || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "getAnnotations; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( !args || typeof args !== "object" || !args.hasOwnProperty( "depositionID" ) || typeof depositions[args.depositionID] === "undefined" || !(depositions[args.depositionID] instanceof Deposition) ) {
			errMsg = "Invalid session ID";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "getAnnotations; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var depositionID = parseInt( args.depositionID );
		var _depo = depositions[depositionID];
		if( typeof _depo.users[currentUserID] === "undefined" || !cUser.depositionID || parseInt( cUser.depositionID ) !== parseInt( _depo.id ) ) {
			errMsg = "Cannot get annotations for a presentation in a session you are not attending";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "getAnnotations; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( typeof presentations[depositionID] === "undefined" ) {
			errMsg = "Unknown presentation";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "getAnnotations; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var presentation = presentations[depositionID];
		if( typeof rooms[presentation.roomID].users[currentUserID] === "undefined" ) {
			errMsg = "Access denied";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "getAnnotations; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		//ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "Socket.getAnnotations;", presentation.pageAnnotations );
		if( typeof callback === "function" ) {
			callback( {"success":true, "annotations":presentation.pageAnnotations} );
		}
	}
	socket.on( "getAnnotations", getAnnotations );


	/**
	 * Add Callout
	 * @param {Object} args Argument parameters
	 * @param {Function} callback
	 */
	function addCallout( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(addCallout):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var sessionID = parseInt( args.depositionID );
		var presentation = presentations[sessionID];
		if( isset( presentation ) ) {
			presentation.eventQueue.push( {"sessionID":sessionID,"userID":currentUserID,"op":"addCallout","args":args,"callback":callback} );
			presentationProcessEventQueue( presentation );
		} else {
			errMsg = "Presentation not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "addCallout; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	}
	socket.on( "addCallout", addCallout );

	/**
	 * Modify Callout
	 * @param {Object} args Argument parameters
	 * @param {Function} callback
	 */
	function modifyCallout( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(modifyCallout):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var sessionID = parseInt( args.depositionID );
		var presentation = presentations[sessionID];
		if( isset( presentation ) ) {
			presentation.eventQueue.push( {"sessionID":sessionID,"userID":currentUserID,"op":"modifyCallout","args":args,"callback":callback} );
			presentationProcessEventQueue( presentation );
		} else {
			errMsg = "Presentation not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "modifyCallout; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	}
	socket.on( "modifyCallout", modifyCallout );

	/**
	 * Delete Callout
	 * @param {Object} args Argument parameters
	 * @param {Function} callback
	 */
	function deleteCallout( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(deleteCallout):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var sessionID = parseInt( args.depositionID );
		var presentation = presentations[sessionID];
		if( isset( presentation ) ) {
			presentation.eventQueue.push( {"sessionID":sessionID,"userID":currentUserID,"op":"deleteCallout","args":args,"callback":callback} );
			presentationProcessEventQueue( presentation );
		} else {
			errMsg = "Presentation not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "deleteCallout; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	}
	socket.on( "deleteCallout", deleteCallout );

	/**
	 * Set Callout(s) Z-Index
	 * @param {Object} args Argument parameters
	 * @param {Function} callback
	 */
	function setCalloutZIndex( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(setCalloutZIndex):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var sessionID = parseInt( args.depositionID );
		var presentation = presentations[sessionID];
		if( isset( presentation ) ) {
			presentation.eventQueue.push( {"sessionID":sessionID,"userID":currentUserID,"op":"setCalloutZIndex","args":args,"callback":callback} );
			presentationProcessEventQueue( presentation );
		} else {
			errMsg = "Presentation not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "setCalloutZIndex; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	}
	socket.on( "setCalloutZIndex", setCalloutZIndex );

	/**
	 * Get Callouts
	 * @param {Object} args Argument parameters
	 * @param {Function} callback
	 */
	function getCallouts( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(getCallouts):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = users[currentUserID];
		if( typeof cUser === "undefined" || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "Socket.getCallouts; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( !args || typeof args !== "object" || !args.hasOwnProperty( "depositionID" ) || typeof depositions[args.depositionID] === "undefined" || !(depositions[args.depositionID] instanceof Deposition) ) {
			errMsg = "Invalid session ID";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "getCallouts; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var depositionID = parseInt( args.depositionID );
		var _depo = depositions[depositionID];
		if( typeof _depo.users[currentUserID] === "undefined" || !cUser.depositionID || parseInt( cUser.depositionID ) !== parseInt( _depo.id ) ) {
			errMsg = "Cannot get callouts for a presentation in a session you are not attending";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "getCallouts; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( typeof presentations[depositionID] === "undefined" ) {
			errMsg = "Unknown presentation";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "getCallouts; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var presentation = presentations[depositionID];
		if( typeof rooms[presentation.roomID].users[currentUserID] === "undefined" ) {
			errMsg = "Access denied";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "getCallouts; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		//ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "Socket.getCallouts;", presentation.pageCallouts );
		if( typeof callback === "function" ) {
			callback( {"success":true, "callouts":presentation.pageCallouts} );
		}
	}
	socket.on( "getCallouts", getCallouts );


	// transcripts

	/**
	 * Join Transcript
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function joinTranscript( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(joinTranscript):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = users[ currentUserID ];
		if( typeof cUser === "undefined" || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "joinTranscript; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var eventID = parseInt( args.eventID );
		if( isNaN( eventID ) || eventID <= 0 ) {
			errMsg = "Invalid Event ID";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "joinTranscript: Error:", errMsg, args.eventID );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var accessKey = args.accessKey;
		if( typeof accessKey !== "string" || accessKey === null || accessKey.trim().length < 4 ) {
			errMsg = "Invalid Access Key";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "joinTranscript: Error:", errMsg, args.accessKey );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		accessKey = accessKey.trim();
		var depositionID = cUser.depositionID;
		if( !isset( depositionID ) || !isset( depositions[ depositionID ] ) ) {
			errMsg = "Session not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "joinTranscript: Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var transcript;
		if( !isset( transcripts[ depositionID ] ) || !isset( transcripts[ depositionID ].guid ) ) {
			transcript = new Transcript( depositionID );
			transcripts[ depositionID ] = transcript;
		} else {
			transcript = transcripts[ depositionID ];
		}
		transcript.authenticate( cUser, eventID, accessKey, callback );
	}
	socket.on( "joinTranscript", joinTranscript );

	/**
	 * Reset Transcript
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function resetTranscript( args, callback )
	{
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(resetTranscript):", currentUserID, '"' + currentUser.name + '"', args );
		var errMsg;
		var cUser = users[ currentUserID ];
		if( typeof cUser === "undefined" || !cUser ) {
			errMsg = "User not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "resetTranscript; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		var depositionID = cUser.depositionID;
		if( !isset( depositionID ) || !isset( depositions[ depositionID ] ) ) {
			errMsg = "Session not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "resetTranscript; Error:", errMsg );
			if( typeof callback === 'function' ) {
				callback( {success:false, error:errMsg} );
			}
			return;
		}
		var deposition = depositions[depositionID];
		if( deposition.ownerID !== cUser.id ) {
			errMsg = "Access denied";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "resetTranscript; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
		if( !isset( transcripts[ depositionID ] ) || !isset( transcripts[ depositionID ].guid ) ) {
			errMsg = "Transcript not found";
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "resetTranscript; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		} else {
			var transcript = transcripts[depositionID];
			transcript.abort();
			if( typeof callback === "function" ) {
				callback( {"success":true} );
			}
		}
	}
	socket.on( "resetTranscript", resetTranscript );

	/**
	 * Log to file, device Console Log entry
	 * @param {Object} args
	 * @param {Function} callback
	 */
	function logToConsole( args, callback ) {
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "socket.on(consoleLog):", currentUserID, '"' + currentUser.name + '"', args );
		logToFile( users[ currentUserID ], args.message );
		if( typeof callback === "function" ) {
			callback( {"success":true} );
		}
	};
	socket.on( "consoleLog", logToConsole );


	// utility functions

	function cleanupUserSocket( userSocket ) {
		if( !isset( users[ currentUserID ] ) ) {
			if( isset( sockets[ currentUser.uid ] ) ) {
				delete sockets[ currentUser.uid ];
			}
			return;
		}
		var cUser = users[ currentUserID ];
		ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "cleanupUserSocket:", currentUserID, '"' + currentUser.name + '"', "uid:", currentUser.uid, "sessionID:", currentUser.depositionID );

		// disconnect user -- make sure its the same socket
		if( currentUser.uid === cUser.uid ) {
			ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "cleanupUserSocket; disconnect uid:", currentUser.uid );
			cUser.active = false;
			var _rooms = isset( depositions[ cUser.depositionID ] ) ? depositions[ cUser.depositionID ].rooms : [];
			for( var roomID in _rooms ) {
				if( isset( rooms[ roomID ] ) && isset( rooms[ roomID ].users[ cUser.id ] ) ) {
					ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "cleanupUserSocket; leaving roomID:", roomID );
					rooms[ roomID ].users[ cUser.id ] = false;
					rooms[ roomID ].save();
					if( isset( userSocket ) ) {
						userSocket.leave( roomID );
					}
				}
			}
			if( cUser.depositionID && isset( depositions[cUser.depositionID] ) ) {
				var userDepo = depositions[ cUser.depositionID ];
				if( isset( userDepo ) && userDepo instanceof Deposition ) {
					if( sessionIsDemo( userDepo.class ) && cUser.id === userDepo.ownerID ) {
						ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "cleanupUserSocket; removing demo owner key" );
						redisClient.del( "demo:" + cUser.depositionID, function( err ) {
							if( err ) {
								ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "cleanupUserSocket; Error:", err );
							}
						} );
					}
					var dUser = cUser.info();
					ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "cleanupUserSocket; disconnect -- user_did_disconnect_from_deposition:", userDepo.depoRoomID, JSON.stringify( dUser ) );
					userDepo.users[ currentUserID ] = false;
					userDepo.save()
					.then( function() {
						sIO.to( userDepo.depoRoomID ).emit( "user_did_disconnect_from_deposition", {"deposition":{"id":cUser.depositionID}, "user":dUser} );
					} );
				}
				cUser.depositionID = null;
			}

			if( isset( sockets[ cUser.uid ] ) ) {
				delete sockets[ cUser.uid ];
			}
			cUser.uid = null;
			cUser.save();
		}
		if( isset( sockets[ currentUser.uid ] ) ) {
			delete sockets[ currentUser.uid ];
		}
	};

	function autoEndPresentations() {
		return new Promise( function( resolve ) {
			if( isset( lastLogin ) && lastLogin.sKey !== currentUser.sKey ) {
				//console.log( "ED-2207 -- Looking for presentations ..." );
				var presentationIDs = Object.keys( presentations );
				for( var i=0; i<presentationIDs.length; ++i ) {
					var pID = presentationIDs[i];
					var pDepo = depositions[pID];
					if( typeof pDepo !== "undefined" && pDepo !== null ) {
						if( pDepo.speakerID !== currentUserID ) {
							continue;
						}
						var presentation = presentations[pID];
						if( typeof presentation !== "undefined" && presentation !== null ) {
							ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "autoEndPresentations; endPresentation", pID, presentation.roomID );
							presentation.deleteAllCallouts( function() {
								presentation.deleteAllAnnotations( function() {
									redisClient.del( presentation.redisKey, function( err ) {
										if( err ) {
											ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "autoEndPresentations; Error:", err );
										}
									} );
									sIO.in( presentation.roomID ).emit( "endPresentation" );
									delete presentations[pID];
									redisPub.publish( "metadata", JSON.stringify( {"nodeId":nodeId, "args":{"event":"Presentation.end", "args":pID}} ) );
								} );
							} );
						}
					}
				}
			}
			return resolve();
		} );
	}

	function autoKickOnLogin() {
		return new Promise( function( resolve ) {
			if( isset( lastLogin ) && typeof lastLogin.uid !== "undefined" ) {
				var lastUID = lastLogin.uid;
				lastLogin.active = false;
				if( isset( lastLogin.depositionID ) ) {
					ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "autoKickOnLogin; current user uid:", currentUser.uid );
					ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "autoKickOnLogin; last login; uid:", lastUID, "sessionID:", lastLogin.depositionID, "-- kicking user:", lastLogin.id, '"' + lastLogin.name + '"' );
					sIO.to( lastLogin.uid ).emit( "attendee_kick", {"depositionID":lastLogin.depositionID, "userID":currentUser.id, "permanently":false, "anotherUserLoggedIn":true} );
					if( typeof sIO.sockets.connected[lastUID] !== "undefined" ) {
						sIO.sockets.connected[lastUID].disconnect();
					}
				} else if( lastUID && typeof sIO.sockets.connected[lastUID] !== "undefined" ) {
					ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "Kicking user -- already logged in:", lastLogin.id, '"' + lastLogin.name + '"', lastLogin.uid );
					sIO.to( lastUID ).emit( "attendee_kick", {"anotherUserLoggedIn":true} );
					sIO.sockets.connected[lastUID].disconnect();
				}
			}
			return resolve();
		} );
	}

	function resetUserIsActive() {
		var user = currentUser;
		return new Promise( function( resolve ) {
			if( !isset( user ) || !(user instanceof User) ) {
				ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "resetUserIsActive; User not found" );
				throw new Error( "User not found" );
			}
			var depoIDs = Object.keys( depositions );
			for( var d in depoIDs ) {
				var depoID = depoIDs[d];
				if( typeof depoID === "function" ) {
					continue;
				}
				var depo = depositions[depoID];
				if( typeof depo.users[currentUserID] !== "undefined" ) {
					if( depo.users[currentUserID] ) {
						var dUser = user.info();
						dUser.active = false;
						depo.users[currentUserID] = false;
						depo.save( function() {
							ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "resetUserIsActive; user_did_disconnect_from_deposition:", depo.depoRoomID );
							userSocket.broadcast.to( depo.depoRoomID ).emit( "user_did_disconnect_from_deposition", {"deposition":{"id":depo.id}, "user":dUser} );
						} );
					}
				}
			}
			var roomKeys = Object.keys( rooms );
			for( var r in roomKeys ) {
				var roomID = roomKeys[r];
				if( typeof roomID === "function" ) {
					continue;
				}
				var room = rooms[roomID];
				if( typeof room.users[currentUserID] !== "undefined" ) {
					if( room.users[currentUserID] ) {
						ConsoleLog( (typeof currentUser != 'undefined') ? currentUser.authUserLog : 'UNKNOWN_USER',  "resetUserIsActive; flagging user inactive for room:", room.id );
						room.users[currentUserID] = false;
						room.save();
					}
				}
			}
			return resolve();
		} );
	};

} );
// end of sIO.on() handlers


chatserver.get( "/status", function( req, res ) {

ConsoleLog("req statusss");

	var isOK = false;
	var remoteAddress = parseIPv4( req.headers["x-forwarded-for"] || req.headers["x-real-ip"] || req.connection.remoteAddress || req.socket.remoteAddress );
	isOK = verifyIP( remoteAddress );
	if( !isOK ) {
		ConsoleLog( "chatserver; 501", remoteAddress, req.method, req.url );
		res.writeHead( 501, "Authorization failed", {"Content-Type":"application/json", "Strict-Transport-Security": "max-age=15768000"} );
		res.end( JSON.stringify( {"success":false, "error":"Authorization failed", "code":501} ) );
		return;
	}
	ConsoleLog( "chatserver;", remoteAddress, req.method, req.url );
	res.writeHead( 200, "OK", {"Content-Type":"application/json", "Strict-Transport-Security": "max-age=15768000"} );
	var _depos = [];
	var depoKeys = Object.keys( depositions );
	for( var depoIdx in depoKeys ) {
		if( depoKeys.hasOwnProperty( depoIdx ) ) {
			var depoID = parseInt( depoKeys[depoIdx] );
			if( typeof depositions[depoID] !== "undefined" && depositions[depoID] instanceof Deposition ) {
				_depos.push( depositions[depoID].bundle() );
			}
		}
	}
	var status = {
		"Total Depositions": Object.keys( depositions ).length,
		"Total Rooms": Object.keys( rooms ).length,
		"Total Users": Object.keys( users ).length,
		"Total Sockets": Object.keys( sockets ).length,
		"Sockets": Object.keys( sockets ),
		"socket.io rooms": sIO.sockets.adapter.rooms,
		"Depositions": depositions,
		"Rooms": rooms,
		"Users": users,
		"Transcripts":transcripts,
		"Presentations":presentations
	};
	res.end( JSON.stringify( status ) );
} );

chatserver.get( "/reload", function( req, res ) {
	var isOK = false;
	var remoteAddress = parseIPv4( req.headers["x-forwarded-for"] || req.headers["x-real-ip"] || req.connection.remoteAddress || req.socket.remoteAddress );
	isOK = verifyIP( remoteAddress );
	if( !isOK ) {
		ConsoleLog( "chatserver; 501", remoteAddress, req.method, req.url );
		res.writeHead( 501, "Authorization failed", {"Content-Type":"application/json", "Strict-Transport-Security": "max-age=15768000"} );
		res.end( JSON.stringify( {"success":false, "error":"Authorization failed", "code":501} ) );
		return;
	}
	ConsoleLog( "chatserver;", remoteAddress, req.method, req.url );
	res.writeHead( 200, "OK", {"Content-Type":"application/json", "Strict-Transport-Security": "max-age=15768000"} );
	var depoKeys = Object.keys( depositions );
	for( var depoIdx in depoKeys ) {
		if( depoKeys.hasOwnProperty( depoIdx ) ) {
			var depoID = parseInt( depoKeys[ depoIdx ] );
			if( isset( depositions[ depoID ] ) && depositions[ depoID ] instanceof Deposition ) {
				reloadSession( depoID );
			}
		}
	}
	res.end( JSON.stringify( {"success": true} ) );
} );

chatserver.get( "/ping", function( req, res ) {
	var isOK = false;
	var remoteAddress = parseIPv4( req.headers[ "x-forwarded-for" ] || req.headers[ "x-real-ip" ] || req.connection.remoteAddress || req.socket.remoteAddress );
	ConsoleLog( "chatserver;", remoteAddress, req.method, req.url );
	res.writeHead( 200, "OK", {"Content-Type":"application/json", "Strict-Transport-Security": "max-age=15768000"} );
	res.end( JSON.stringify( {"success": true} ) );
} );

function notFound( req, res ) {
	ConsoleLog( "[404]", req.method, req.url );
	res.writeHead( 404, "Not found", {"Content-Type": "application/json", "Strict-Transport-Security": "max-age=15768000"} );
	res.end( JSON.stringify( {"success":false, "error":"Not found", "code":404} ) );
}

function sendOK( res ) {
	res.writeHead( 200, "OK", {"Content-Type": "application/json", "Strict-Transport-Security": "max-age=15768000"} );
	res.end( JSON.stringify( {"success":true} ) );
}

function handleRequest( req, res ) {
	var isOK = false;
	var remoteAddress = parseIPv4( req.headers["x-forwarded-for"] || req.headers["x-real-ip"] || req.connection.remoteAddress || req.socket.remoteAddress );
	isOK = (req.method.toLowerCase() === "post" && verifyIP( remoteAddress ));
	if( !isOK ) {
		ConsoleLog( "chatserver;", remoteAddress, req.method, req.url, 501 );
		res.writeHead( 501, "Authorization failed", {"Content-Type":"application/json", "Strict-Transport-Security": "max-age=15768000"} );
		res.end( JSON.stringify( {"success":false, "error":"Authorization failed", "code":501} ) );
		return;
	}
	ConsoleLog( "chatserver;", remoteAddress, req.method, req.url );
	//ConsoleLog( "TEST1000~~~~~~~~~~~~~~~~;", remoteAddress, req.method, req.query.aul );
	switch( req.url ) {
		case "/attendee_kick":
			attendeeKick( req, res );
			break;
		case "/authorize_witness":
			authorizeWitness( req, res );
			break;
		case "/deposition_start":
			depositionStarted( req, res );
			break;
		case "/deposition_end":
			depositionFinished( req, res );
			break;
		case "/reset_deposition":
			resetDeposition( req, res );
			break;
		case "/set_demo_deposition_owner":
			setDemoDepositionOwner( req, res );
			break;
		case "/deposition_setspeaker":
			depositionSetSpeaker( req, res );
			break;
		case "/deposition_link":
			depositionLink( req, res );
			break;
		case "/deposition_uploaded_files":
			depositionUploadedFiles( req, res );
			break;
		case "/deposition_deleted_file":
			depositionDeletedFile( req, res );
			break;
		case "/deposition_deleted_folder":
			depositionDeletedFolder( req, res );
			break;
		case "/witness_uploaded":
			witnessUploadFiles( req, res );
			break;
		case "/file_modified":
			fileModified( req, res );
			break;
		case "/cases_updated":
			casesUpdated( req, res );
			break;
		case "/introduced_exhibit":
			sharedItem( req, res, "introduced_exhibit" );
			break;
		case "/shared_file":
			sharedItem( req, res, "shared_file" );
			break;
		case "/shared_folder":
			sharedItem( req, res, "shared_folder" );
			break;
		case "/presentation.publish":
			presentationPublish( req, res );
			break;
		case '/requestConsoleLog':
			requestConsoleLog( req, res );
			break;
		case '/enableConsoleLog':
			enableConsoleLog( req, res );
			break;
		case '/clearConsoleLog':
			clearConsoleLog( req, res );
			break;
		case "/didChangeCustomSort":
			didChangeCustomSort( req, res );
			break;
//		case "/":
//			notImplemented( req, res );
//			break;
//		case "/sharefiles":
//			filesShared( req, res );
//			break;
//		case "/abortshare":
//			sharingAborted( req, res );
//			break;
//		case "/select_complete":
//			selectComplete( req, res );
//			break;
//		case "/reject_witness":
//			rejectWitness( req, res );
//			break;
//		case "/getPresentationInfo":
//			getPresentationInfo( req, res );
//			break;
		default:
			notFound( req, res );
			break;
	}
}

chatserver.all( "/*", handleRequest );


function attendeeKick( req, res ) {
	ConsoleLog( "[200]", req.method, req.url );
	var data = "";

	req.on( "data", function( chunk ) {
		data += chunk.toString();
	} );

	req.on( "end", function() {

		var object = JSON.parse( data );

		var logIdSource = '';
		if (typeof object === "object" && isset(object.logId)){logIdSource = object.logId.replace('>','<');}

		ConsoleLog( logIdSource, "attendeeKick; data:", data );


		if( isset( object.args.userID ) || isset( object.args.guestID ) ) {
			var fromDepo = false;
			var user = (isset( object.args.guestID ) ? users[ object.args.guestID ] : users[ object.args.userID ]);
			if( isset( user ) ) {
				if( isset( object.args.depositionID ) && isset( depositions[ object.args.depositionID ] ) ) {
					var deposition = depositions[ object.args.depositionID ];
					fromDepo = deposition.id;
				}
				if( user.depositionID && isset( depositions[ user.depositionID ] ) ) {
					var deposition = depositions[ user.depositionID ];
					fromDepo = deposition.id;
				}
				user.active = false;
				user.save();
				if( fromDepo ) {
					delete deposition.users[ user.id ];
					deposition.save();
				}
				ConsoleLog( logIdSource,"attendeeKick; kicking user:", JSON.stringify( user.info() ) );
				//user.socket.emit( "attendee_kick", {"depositionID":object.args.depositionID, "userID":user.id, "permanently":object.args.ban} );
				if( fromDepo ) {
					sIO.to( user.uid ).emit( "attendee_kick", {"depositionID":fromDepo, "userID":user.id, "permanently":object.args.ban} );
				}
				if( isset( sockets[ user.uid ] ) ) {
					sockets[ user.uid ].disconnect();
				}
				if( fromDepo ) {
					//remove shadow users
					for( var roomID in deposition.rooms ) {
						if( deposition.rooms.hasOwnProperty( roomID ) && typeof rooms[roomID] !== "undefined" ) {
							ConsoleLog( logIdSource,"kicking", user.id, "from", roomID );
							var room = rooms[roomID];
							var _user = users[user.id];
							if( _user instanceof User ) {
								room.removeUser( user );
								room.save();
							}
						}
					}
					var userInfo = user.info();
					ConsoleLog( logIdSource,"attendeeKick; sIO.to(", deposition.depoRoomID, ').emit( "user_kicked",', JSON.stringify( userInfo ), ")" );
					sIO.to( deposition.depoRoomID ).emit( "user_kicked", {"user":userInfo} );
				}
			}
		}
		sendOK( res );
	} );
}

/**
 * Session Started
 * @param {Object} req
 * @param {Object} res
 */
function depositionStarted( req, res ) {
	ConsoleLog( "[200]", req.method, req.url );
	var data = "";

	req.on( "data", function( chunk ) {
		data += chunk.toString();
	} );

	req.on( "end", function() {

		var object = JSON.parse( data );

		var logIdSource = '';
		if (typeof object === "object" && isset(object.logId)){logIdSource = object.logId.replace('>','<');}

		ConsoleLog( logIdSource, "depositionStarted; data:", data );

		var depoID = parseInt( object.args.depositionID );
		if( !isset( depoID ) || isNaN( depoID ) || !(depoID > 0) ) {
			ConsoleLog( logIdSource, "depositionStarted; Error: Invalid arguments, missing/invalid depositionID:", data );
			return;
		}
		if( isset( depositions[ depoID ] ) ) {
			var _depo = depositions[ depoID ];
			sIO.to( _depo.depoRoomID ).emit( "deposition_start", {"deposition":_depo.info()} );
		} else {
			ConsoleLog( logIdSource, "depositionStarted; Session does not exist" );
		}
		sendOK( res );
	} );
}

/**
 * Session Finished
 * @param {Object} req
 * @param {Object} res
 */
function depositionFinished( req, res ) {
	ConsoleLog( "[200]", req.method, req.url );
	var data = "";

	req.on( "data", function( chunk ) {
		data += chunk.toString();
	} );

	req.on( "end", function() {

		var object = JSON.parse( data );

		var logIdSource = '';
		if (typeof object === "object" && isset(object.logId)){logIdSource = object.logId.replace('>','<');}

		ConsoleLog( logIdSource, "depositionFinished; data:", data );

		var depoID = parseInt( object.args.depositionID );
		if( !isset( depoID ) || isNaN( depoID ) || !(depoID > 0) ) {
			ConsoleLog( logIdSource, "depositionFinished; Error: Invalid arguments, missing/invalid depositionID:", data );
			return;
		}
		if( isset( depositions[ depoID ] ) ) {
			var _depo = depositions[ depoID ];
			sIO.to( _depo.depoRoomID ).emit( "deposition_end", {"deposition":_depo.info()} );
			if( sessionIsDemo( _depo.class ) ) {
				ConsoleLog( logIdSource, "depositionFinished; removing demo owner key" );
				redisClient.del( "demo:" + depoID, function( err ) {
					if( err ) {
						ConsoleLog( logIdSource, "depositionFinished; Error:", err );
					}
				} );
			}
			closeDeposition( depoID );
			redisPub.publish( "metadata", JSON.stringify( {"nodeId":nodeId, "args":{"event":"closeDeposition", "args":depoID}} ) );
		}
		sendOK( res );
	} );
}

/**
 * Reset Demo Sessions
 * @param {Object} req
 * @param {Object} res
 */
function resetDeposition( req, res ) {
	ConsoleLog( "[200]", req.method, req.url );
	var data = "";

	req.on( "data", function( chunk ) {
		data += chunk.toString();
	} );

	req.on( "end", function() {

		var object = JSON.parse( data );

		var logIdSource = '';
		if (typeof object === "object" && isset(object.logId)){logIdSource = object.logId.replace('>','<');}

		ConsoleLog( logIdSource, "resetDeposition; data:", data );

		var depositionID = parseInt( object.args.depositionID );
		var depoRoomID = depositionID + ".deposition";
		if( object.args.hasOwnProperty( "notifyUsers" ) ) {
			var notifyUsers = object.args.notifyUsers;
			ConsoleLog( logIdSource, "resetDeposition; -- notify users:", notifyUsers );
			if( typeof depositions[ depositionID ] !== "undefined" && sessionIsDemo( depositions[ depositionID ].class ) ) {
				var _depo = depositions[ depositionID ];
				for( var idx in notifyUsers ) {
					if( notifyUsers.hasOwnProperty( idx ) ) {
						var userID = parseInt( notifyUsers[ idx ] );
						if( typeof users[ userID ] !== "undefined" && users[ userID ].depositionID === depositionID ) {
							var _user = users[ userID ];
							ConsoleLog( logIdSource, "resetDeposition; notify user:", _user.id, '"' + _user.name + '", to vacate session' );
							_user.depositionID = null;
							_user.save();
							sIO.to( _user.uid ).emit( "vacate_deposition", {"depositionID":depositionID} );
							if( isset( sockets[ _user.uid ] ) ) {
								sockets[ _user.uid ].leave( depoRoomID );
							}
						} else {
							ConsoleLog( logIdSource, "resetDeposition; userID:", userID, "not found or not attending session" );
						}
					}
				}
			} else {
				ConsoleLog( logIdSource, "resetDeposition; depositionID:", depositionID, "not found or not a demo session" );
			}
		} else {
			if( typeof depositions[depositionID] !== "undefined" && sessionIsDemo( depositions[depositionID].class ) ) {
				//reset redis key
				ConsoleLog( logIdSource, "resetDeposition; removing demo owner key" );
				redisClient.del( "demo:" + depositionID, function( err ) {
					if( err ) {
						ConsoleLog( logIdSource, "resetDeposition; Error:", err );
					}
				} );

				var _depo = depositions[ depositionID ];

				//force per user deselectDeposition
				for( var u in _depo.users ) {
					if( _depo.users.hasOwnProperty( u ) && isset( users[ u ] ) ) {
						var _user = users[ u ];
						_user.depositionID = null;
						_user.save();
					}
				}
				ConsoleLog( logIdSource, "resetDeposition; notify room:", depoRoomID, "to vacate session" );
				sIO.to( depoRoomID ).emit( "vacate_deposition", {"depositionID":depositionID} );
				closeDeposition( depositionID );
				redisPub.publish( "metadata", JSON.stringify( {"nodeId":nodeId, "args":{"event":"closeDeposition", "args":depositionID}} ) );
			}
		}
		sendOK( res );
	} );
}

/**
 * Set Demo Session Owner
 * @param {Object} req
 * @param {Object} res
 */
function setDemoDepositionOwner( req, res ) {
	ConsoleLog( "[200]", req.method, req.url );
	var data = "";

	req.on( "data", function( chunk ) {
		data += chunk.toString();
	} );

	req.on( "end", function() {

		var object = JSON.parse( data );

		var logIdSource = '';
		if (typeof object === "object" && isset(object.logId)){logIdSource = object.logId.replace('>','<');}

		ConsoleLog( logIdSource, "setDemoDepositionOwner; data:", data );

		var depoID = parseInt( object.args.depositionID );
		if( !isset( depoID ) || isNaN( depoID ) || !(depoID > 0) ) {
			ConsoleLog( logIdSource, "setDemoDepositionOwner; Error: Invalid arguments, missing/invalid depositionID:", data );
			return;
		}
		if( typeof depositions[ depoID ] !== "undefined" && sessionIsDemo( depositions[ depoID ].class ) ) {
			if( typeof object === "object" && object.hasOwnProperty( "args" ) && typeof object.args === "object" && object.args.hasOwnProperty( "ownerID" ) ) {
				var deposition = depositions[ depoID ];
				if( typeof users[deposition.ownerID] !== "undefined" ) {
					var owner = users[ deposition.ownerID ];
					if( owner.depositionID && owner.depositionID === depoID ) {
						ConsoleLog( logIdSource, "Demo deposition owner changed, previous owner needs to vacate.\ndepositionID:", depoID, "previous owner:", owner.id, "new owner:", object.args.ownerID );
						sIO.to( owner.uid ).emit( "vacate_deposition", {"depositionID":depoID} );
					}
				}
				ConsoleLog( logIdSource, "setDemoDepositionOwner:", deposition.class, deposition.ownerID, "to", object.args.ownerID );
				deposition.ownerID = parseInt( object.args.ownerID );
				deposition.speakerID = deposition.ownerID;
				deposition.save();
			}
		}
		sendOK( res );
	} );
}

/**
 * Set Speaker (Leader)
 * @param {Object} req
 * @param {Object} res
 */
function depositionSetSpeaker( req, res ) {
	ConsoleLog( "[200]", req.method, req.url );

	var data = "";

	req.on( "data", function( chunk ) {
		data += chunk.toString();
	} );

	req.on( "end", function() {

		var object = JSON.parse( data );

		var logIdSource = '';
		if (typeof object === "object" && isset(object.logId)){logIdSource = object.logId.replace('>','<');}

		ConsoleLog( logIdSource, "depositionSetSpeaker; data:", data );

		if ( object.args.depositionID ) {
			var mDeposition = depositions[object.args.depositionID];
			if( mDeposition instanceof Deposition ) {
				ConsoleLog( logIdSource, "depositionSetSpeaker; sessionID:", mDeposition.id );
				mDeposition.speakerID = parseInt( object.args.newSpeakerID );
				mDeposition.save()
				.then( function() {
					//check for presentation
					if( typeof presentations[mDeposition.id] !== "undefined" ) {
						var presentation = presentations[mDeposition.id];
						ConsoleLog( logIdSource, "depositionSetSpeaker; speaker changed -- end presentation:", mDeposition.id, presentation.roomID );
						presentation.deleteAllCallouts( function() {
							presentation.deleteAllAnnotations( function() {
								redisClient.delAsync( presentation.redisKey )
								.catch( function( e ) {
									ConsoleLog( logIdSource, "depositionSetSpeaker; delete caught:", e.name, e.message );
								} )
								.finally( function() {
									sIO.to( presentation.roomID ).emit( "endPresentation" );
									delete presentations[mDeposition.id];
									redisPub.publish( "metadata", JSON.stringify( {"nodeId":nodeId, "args":{"event":"Presentation.end", "args":mDeposition.id}} ) );
								} );
							} );
						} );
					}

					var witnesses = [];
					for( var userID in mDeposition.users ) {
						if( mDeposition.users.hasOwnProperty( userID ) && typeof users[userID] !== "undefined" ) {
							if( typeof users[userID].userType !== "undefined" && ["W","WM"].indexOf( users[userID].userType ) >= 0 ) {
								witnesses.push( users[userID].info() );
							}
						}
					}

					var info = {
						"depositionID":object.args.depositionID,
						"newSpeakerID":object.args.newSpeakerID,
						"oldSpeakerID":object.args.oldSpeakerID,
						"exhibitTitle":object.args.exhibitTitle,
						"exhibitSubTitle":object.args.exhibitSubTitle,
						"exhibitDate":object.args.exhibitDate,
						"exhibitXOrigin":object.args.exhibitXOrigin,
						"exhibitYOrigin":object.args.exhibitYOrigin,
						"witnesses":witnesses
					};
					sIO.to( mDeposition.depoRoomID ).emit( "deposition_setspeaker", info );
				} );
			}
		}
		sendOK( res );
	} );
}

/**
 * Link Session (to parent)
 * @param {Object} req
 * @param {Object} res
 */
function depositionLink( req, res ) {
    ConsoleLog( "[200]", req.method, req.url );

	var data = "";

	req.on( "data", function( chunk ) {
		data += chunk.toString();
	} );

	req.on( "end", function() {

		var object = JSON.parse( data );

		var logIdSource = '';
		if (typeof object === "object" && isset(object.logId)){logIdSource = object.logId.replace('>','<');}

		ConsoleLog( logIdSource, "depositionLink; data:", data );

		for( var u in users ) {
 			if ( users[ u ] && object.args.clientID === users[ u ].clientid ) {
  				ConsoleLog( logIdSource, "depositionLink; client:", object.args.clientID, "userID:", users[ u ].id, '"' + users[ u ].name + '"' );
                //users[u].socket.emit('deposition_link', {"targetDepositionID":object.args.targetDepositionID} );
                sIO.to( users[ u ].uid ).emit( "deposition_link", {"targetDepositionID":object.args.targetDepositionID} );
 			}
 		}
		sendOK( res );
    } );
}

/**
 * Session Uploaded Files
 * @param {Object} req
 * @param {Object} res
 */
function depositionUploadedFiles( req, res ) {
	ConsoleLog( "[200]", req.method, req.url );

	var data = "";

	req.on( "data", function( chunk ) {
		data += chunk.toString();
	} );

	req.on( "end", function() {

		var object = JSON.parse( data );

		var logIdSource = '';
		if (typeof object === "object" && isset(object.logId)){logIdSource = object.logId.replace('>','<');}

		ConsoleLog( logIdSource, "depositionUploadedFiles; data:", data );


		var folderID = object.args.folderID;
		var folderName = object.args.folderName;
		var notifyUsers = object.args.notifyUsers;
		var depositionID = isset( object.args.depositionParentID ) ? object.args.depositionParentID : object.args.depositionID;
		var byUserID = object.args.byUserID;
		var mDeposition = depositions[ depositionID ];
		var attendee;

		if( mDeposition instanceof Deposition ) {
			ConsoleLog( logIdSource, "depositionUploadedFiles; depositionID:", mDeposition.id, "number of users:", Object.keys( mDeposition.users ).length, "folderID:", folderID, "name:", folderName );

			for( var n = 0; n < notifyUsers.length; ++n ) {
				var notifyUserID = notifyUsers[ n ];
				ConsoleLog( logIdSource, "depositionUploadedFiles; looking for attendee:", notifyUserID );
				attendee = users[ notifyUserID ];
				if( typeof attendee !== "undefined" ) {
					ConsoleLog( logIdSource, "depositionUploadedFiles; found attendee -- userID:", attendee.id, '"' + attendee.name + '"' );
					//attendee.socket.emit( "deposition_uploaded_files", {"depositionID": object.args.depositionID, "folderID": folderID, "folderName": folderName} );
					var uploadInfo = {"depositionID":object.args.depositionID, "folderID":folderID, "folderName":folderName, "byUserID":byUserID};
					ConsoleLog( logIdSource, "depositionUploadedFiles; emit: deposition_uploaded_files", uploadInfo );
					sIO.to( attendee.uid ).emit( "deposition_uploaded_files", uploadInfo );
				}
			}
		} else {
			ConsoleLog( logIdSource, "depositionUploadedFiles; session not found by ID:", depositionID );
		}

		sendOK( res );
	} );
}

/**
 * Session Deleted File(s)
 * @param {Object} req
 * @param {Object} res
 */
function depositionDeletedFile( req, res ) {
	ConsoleLog( "[200]", req.method, req.url );

	var data = "";

	req.on( "data", function( chunk ) {
		data += chunk.toString();
	} );

	req.on( "end", function() {

		var object = JSON.parse( data );

		var logIdSource = '';
		if (typeof object === "object" && isset(object.logId)){logIdSource = object.logId.replace('>','<');}

		ConsoleLog( logIdSource, "depositionDeletedFile; data:", data );

		if( typeof object === "object" && object.hasOwnProperty( "args" ) && typeof object.args === "object" && object.args.hasOwnProperty( "users" ) ) {
			var notifyUsers = object.args.users;
			for( var userID in notifyUsers ) {
				if( notifyUsers.hasOwnProperty( userID ) ) {
					ConsoleLog( logIdSource, "depositionDeletedFile; looking for userID:", userID );
					if( typeof users[ userID ] !== "undefined" && users[ userID ] instanceof User ) {
						var _user = users[ userID ];
						var userArgs = notifyUsers[ userID ];
						ConsoleLog( logIdSource, "depositionDeletedFile; emit: deposition_deleted_file", _user.id, '"' + _user.name + '"', userArgs );
						sIO.to( _user.uid ).emit( "deposition_deleted_file", userArgs );
					} else {
						ConsoleLog( logIdSource, "depositionDeletedFile; invalid or unknown userID:", userID );
					}
				}
			}
		} else {
			ConsoleLog( logIdSource, "depositionDeletedFile; invalid arguments:", object );
		}
		sendOK( res );
	} );
}

/**
 * Session Deleted Folder
 * @param {Object} req
 * @param {Object} res
 */
function depositionDeletedFolder( req, res ) {
	ConsoleLog( "[200]", req.method, req.url );

	var data = "";

	req.on( "data", function( chunk ) {
		data += chunk.toString();
	} );

	req.on( "end", function() {

		var object = JSON.parse( data );

		var logIdSource = '';
		if (typeof object === "object" && isset(object.logId)){logIdSource = object.logId.replace('>','<');}

		ConsoleLog( logIdSource, "depositionDeletedFolder; data:", data );

		if( typeof object === "object" && object.hasOwnProperty( "args" ) && typeof object.args === "object" && object.args.hasOwnProperty( "users" ) ) {
			var notifyUsers = object.args.users;
			for( var userID in notifyUsers ) {
				if( notifyUsers.hasOwnProperty( userID ) ) {
					ConsoleLog( logIdSource, "depositionDeletedFolder; Looking for UserID:", userID );
					if( typeof users[ userID ] !== "undefined" && users[userID] instanceof User ) {
						var _user = users[ userID ];
						var userArgs = notifyUsers[ userID ];
						ConsoleLog( logIdSource, "depositionDeletedFolder; emit: deposition_deleted_folder", _user.id, '"' + _user.name + '"', userArgs );
						sIO.to( _user.uid ).emit( "deposition_deleted_folder", userArgs );
					} else {
						ConsoleLog( logIdSource, "depositionDeletedFolder; invalid or unknown userID:", userID );
					}
				}
			}
		} else {
			ConsoleLog( logIdSource, "depositionDeletedFolder; invalid arguments:", object );
		}
		sendOK( res );
	} );
}

/**
 * Witness Uploaded Files
 * @param {Object} req
 * @param {Object} res
 */
function witnessUploadFiles( req, res ) {
	ConsoleLog( "[200]", req.method, req.url );

	var data = "";

	req.on( "data", function( chunk ) {
		data += chunk.toString();
	} );

	req.on( "end", function() {

		var object = JSON.parse( data );

		var logIdSource = '';
		if (typeof object === "object" && isset(object.logId)){logIdSource = object.logId.replace('>','<');}

		ConsoleLog( logIdSource, "witnessUploadFiles; data:", data );

		var folderID = object.args.folderID;
		var folderName = object.args.folderName;
		var notifyUsers = object.args.notifyUsers;
		var mDeposition = depositions[ object.args.depositionID ];

		if( mDeposition instanceof Deposition ) {
			ConsoleLog( logIdSource, "witnessUploadFiles; depositionID:", mDeposition.id, "folderID:", folderID, "name:", folderName );

			for( var n = 0; n < notifyUsers.length; ++n ) {
				var notifyUserID = notifyUsers[ n ];
				ConsoleLog( logIdSource, "witnessUploadFiles; looking for attendee:", notifyUserID );
				var attendee = users[ notifyUserID ];
				if( isset( attendee ) ) {
					ConsoleLog( logIdSource, "witnessUploadFiles; found attendee:", attendee.id, '"' + attendee.name + '"' );
					//attendee.socket.emit( 'witness_uploaded', {"depositionID": mDeposition.id, "folderID": folderID, "folderName": folderName} );
					sIO.to( attendee.uid ).emit( "witness_uploaded", {"depositionID":object.args.targetDepositionID , "folderID":folderID, "folderName":folderName} );
				}
			}
		} else {
			ConsoleLog( logIdSource, "witnessUploadFiles; session not found by ID:", object.args.depositionID );
		}

		sendOK( res );
	} );
}

/**
 * Cases Updated
 * @param {Object} req
 * @param {Object} res
 */
function casesUpdated( req, res ) {
	ConsoleLog( "[200]", req.method, req.url );

	var data = "";

	req.on( "data", function( chunk ) {
		data += chunk.toString();
	} );

	req.on( "end", function() {

		var object = JSON.parse( data );

		var logIdSource = '';
		if (typeof object === "object" && isset(object.logId)){logIdSource = object.logId.replace('>','<');}

		ConsoleLog( logIdSource, "casesUpdated; data:", data );


		var notifyUsers = object.args.notifyUsers;
		for( var n in notifyUsers ) {
			if( notifyUsers.hasOwnProperty( n ) ) {
				var notifyUserID = parseInt( notifyUsers[n] );
				var _user = users[ notifyUserID ];
				if( isset( _user ) ) {
					ConsoleLog( logIdSource, "casesUpdated; emit: cases_updated user:", _user.id, '"' + _user.name + '"' );
					sIO.to( _user.uid ).emit( 'cases_updated' );
				} else {
					ConsoleLog( logIdSource, "casesUpdated; user not found by ID:", notifyUserID );
				}
			}
		}
		sendOK( res );
	} );
}

/**
 * File Modified
 * @param {Object} req
 * @param {Object} res
 */
function fileModified( req, res ) {
	ConsoleLog( "[200]", req.method, req.url );

	var data = "";

	req.on( "data", function( chunk ) {
		data += chunk.toString();
	} );

	req.on( "end", function() {

		var object = JSON.parse( data );

		var logIdSource = '';
		if (typeof object === "object" && isset(object.logId)){logIdSource = object.logId.replace('>','<');}

		ConsoleLog( logIdSource, "fileModified; data:", data );


		if( typeof object === "object" && object.hasOwnProperty( "args" ) && typeof object.args === "object" && object.args.hasOwnProperty( "users" ) ) {
			var notifyUsers = object.args.users;
			var byUser = (typeof object.args.byUser === "string" ? object.args.byUser : "");
			var byUserID = (typeof object.args.byUserID === "number" ? object.args.byUserID : 0);
			for( var userID in notifyUsers ) {
				if( notifyUsers.hasOwnProperty( userID ) ) {
					ConsoleLog( logIdSource, "fileModified; looking for userID:", userID );
					if( typeof users[ userID ] !== "undefined" && users[ userID ] instanceof User ) {
						var _user = users[ userID ];
						var eventData = {"fileID":notifyUsers[ userID ], "byUserID":byUserID, "byUser":byUser};
						ConsoleLog( logIdSource, "fileModified; emit: file_modified", _user.id, '"' + _user.name + '"', eventData );
						sIO.to( _user.uid ).emit( "file_modified", eventData );
					} else {
						ConsoleLog( logIdSource, "fileModified; invalid or unknown userID:", userID );
					}
				}
			}
		} else {
			ConsoleLog( logIdSource, "fileModified; invalid arguments:", object );
		}
		sendOK( res );
	} );
}

/**
 * Shared Item (Exhibit, File, Folder)
 * @param {Object} req
 * @param {Object} res
 * @param {String} eventName ["introduced_exhibit" | "shared_file" | "shared_folder"]
 */
function sharedItem( req, res, eventName ) {
	ConsoleLog( "[200]", req.method, req.url );
	var data = "";

	req.on( "data", function( chunk ) {
		data += chunk.toString();
	} );

	req.on( "end", function() {

		var object = JSON.parse( data );
		var params = object.args;

		var logIdSource = '';
		if (typeof object === "object" && isset(object.logId)){logIdSource = object.logId.replace('>','<');}

		ConsoleLog( logIdSource, "sharedItem; eventName:", eventName, "data:", data );

		//console.log( "--- receivers: ", params.receivers );
		//console.log( "--- deposition: ", params.depositionID );
		if ( typeof params.receivers !== "undefined" && typeof params.depositionID !== "undefined" ) {
			var depositionID = parseInt( params.depositionID );
			if( typeof depositions[depositionID] !== "undefined" ) {
				var _depo = depositions[depositionID];
				for( var i=0; i<params.receivers.length; ++i ) {
					var receiver = params.receivers[i];
					var receiverUserID = parseInt( receiver.userID ? receiver.userID : receiver.guestID );
					//console.log( "--- receivers[" + i + "]: " + receiverUserID );
					if( typeof users[receiverUserID] !== "undefined" && typeof _depo.users[receiverUserID] !== "undefined" && _depo.users[receiverUserID] ) {
						ConsoleLog( logIdSource + "sharedItem; userID:" + receiverUserID + " emit:" + eventName );
						//_depo.users[receiverUserID].socket.emit( "files_shared", {deposition: {id: _depo.id}, files: receiver.files, folder: receiver.folder, sharedBy: _depo.users[params.sharedBy].info()} );
						var _sharedBy = users[params.sharedBy].info();
						sIO.to( users[receiverUserID].uid ).emit( eventName, {"deposition":{"id":depositionID}, "files":receiver.files, "folder":receiver.folder, "sharedBy":_sharedBy} );
					}
				}
			}
		}
		sendOK( res );
	} );
}

/**
 * Authorize Witness
 * @param {Object} req
 * @param {Object} res
 */
function authorizeWitness( req, res ) {
	ConsoleLog( "[200]", req.method, req.url );
	var data = "";

	req.on( "data", function( chunk ) {
		data += chunk.toString();
	} );

	req.on( "end", function() {

		var object = JSON.parse( data );

		var logIdSource = '';
		if (typeof object === "object" && isset(object.logId)){logIdSource = object.logId.replace('>','<');}

		ConsoleLog( logIdSource, "authorizeWitness; data:", data );

		var depositionID = parseInt( object.args.depositionID );
		var witnessID = parseInt( object.args.witnessID );
		var errMsg;
		if( typeof users[ witnessID ] === "undefined" ) {
			errMsg = "Witness not found";
			ConsoleLog(  logIdSource,"authorizeWitness;", errMsg, witnessID );
			res.end( JSON.stringify( {"success":false, "error":errMsg} ) );
			return;
		}
		var witness = users[ witnessID ];
		witness.userType = "W";
		witness.isTempWitness = false;
		witness.save()
		.then( function() {
			var allUsers = Object.keys( users );
			for( var u = 0; u < allUsers.length; ++u ) {
				var _userID = allUsers[ u ];
				var _user = users[ _userID ];
				if( !isset( _user ) ) {
					continue;
				}
				if( !isset( _user.depositionID ) || _user.depositionID !== depositionID ) {
					continue;
				}
				if( typeof _user.isTempWitness === "undefined" || !_user.isTempWitness ) {
					continue;
				}
				ConsoleLog(  logIdSource,"authorizeWitness; emit: rejected_witness", depositionID, "witness:", _user.id, _user.uid );
				sIO.to( _user.uid ).emit( "rejected_witness", {"depositionID":depositionID, "witnessID":_user.id} );
				if( typeof sockets[ _user.uid ] !== "undefined" ) {
					sockets[ _user.uid ].disconnect();
				}
				delete users[ _user.id ];
			}
		} )
		.then( function() {
			ConsoleLog(  logIdSource,"authorizeWitness; emit: authorized_witness;", depositionID, "witness:", witnessID, witness.uid );
			sIO.to( witness.uid ).emit( "authorized_witness", {"depositionID":depositionID, "witnessID":witnessID} );
		} );

		sendOK( res );
	} );
}

/**
 * Presentation Publish
 * @param {Object} req
 * @param {Object} res
 */
function presentationPublish( req, res ) {
	ConsoleLog( "[200]", req.method, req.url );

	var data = "";

	req.on( "data", function( chunk ) {
		data += chunk.toString();
	} );

	req.on( "end", function() {

		var object = JSON.parse( data );

		var logIdSource = '';
		if (typeof object === "object" && isset(object.logId)){logIdSource = object.logId.replace('>','<');}

		ConsoleLog( logIdSource, "presentationPublish; data:", data );


		if( typeof object === "undefined" || object === null || typeof object.args === "undefined" || object.args === null ) {
			res.end( JSON.stringify( {"success":false, "error":"Invalid arguments"} ) );
		}
		var args = object.args;
		var _depo = depositions[ args.sessionID ];
		var presentation = presentations[ args.sessionID ];
		if( _depo && _depo instanceof Deposition ) {
			if( presentation && presentation instanceof Presentation ) {
				redisPub.publish( "metadata", JSON.stringify( {"nodeId":nodeId, "args":{"event":"Presentation.publish", "args":_depo.id}} ) );
				sendOK( res );
			} else {
				ConsoleLog( logIdSource, "presentationPublish; missing presentation in:", data );
				res.end( JSON.stringify( {"success":false, "error":"Unknown presentation: " + args.sessionID} ) );
			}
		} else {
			ConsoleLog( logIdSource, "presentationPublish; missing sessionID in:", data );
			res.end( JSON.stringify( {"success":false, "error":"Unknown session ID: " + args.sessionID} ) );
		}
	} );
}

/**
 * Request Console Log
 * @param {Object} req
 * @param {Object} res
 */
function requestConsoleLog( req, res ) {
	ConsoleLog( "[200]", req.method, res.url );
	var urlInfo = url.parse( req.url, true );
	var userID = parseInt( urlInfo.query.userID );
	if( isset( users[ userID ] ) ) {
		var user = users[ userID ];
		logToFile( null, "requestConsoleLog; userID: " + userID );
		//user.socket.emit( 'request_console_log' );
		sIO.to( user.uid ).emit( "request_console_log" );
		sendOK( res );
		return;
	}
	ConsoleLog( "requestConsoleLog; User not found by ID:", userID );
	res.end( JSON.stringify( {"success":false} ) );
}

/**
 * Enable Console Log
 * @param {Object} req
 * @param {Object} res
 */
function enableConsoleLog( req, res ) {
	ConsoleLog( "[200]", req.method, req.url );
	var urlInfo = url.parse( req.url, true );
	var userID = parseInt( urlInfo.query.userID );
	if( isset( users[ userID ] ) ) {
		var user = users[ userID ];
		logToFile( null, "enableConsoleLog; userID: " + userID + " enabled: " + urlInfo.query.enabled );
		//user.socket.emit( 'enable_console_log', {"enabled":urlInfo.query.enabled} );
		sIO.to( user.uid ).emit( "enable_console_log", {"enabled":urlInfo.query.enabled} );
		sendOK( res );
		return;
	}
	ConsoleLog( "enableConsoleLog; User not found by ID:", userID );
	res.end( JSON.stringify( {"success":false} ) );
}

/**
 * Clear Console Log
 * @param {Object} req
 * @param {Object} res
 */
function clearConsoleLog( req, res ) {
	ConsoleLog( "[200]", req.method, req.url );
	var urlInfo = url.parse( req.url, true );
	var userID = parseInt( urlInfo.query.userID );
	if( isset( users[ userID ] ) ) {
		var user = users[ userID ];
		logToFile( null, "clearConsoleLog; userID: " + userID );
		//user.socket.emit( 'clear_console_log' );
		sIO.to( user.uid ).emit( "clear_console_log" );
		sendOK( res );
		return;
	}
	ConsoleLog( "clearConsoleLog; User not found by ID:", userID );
	res.end( JSON.stringify( {"success":false} ) );
}

function didChangeCustomSort( req, res ) {
	ConsoleLog( "[200]", req.method, req.url );
	var data = "";

	req.on( "data", function( chunk ) {
		data += chunk.toString();
	} );

	req.on( "end", function() {

		var object = JSON.parse( data );

		var logIdSource = '';
		if (typeof object === "object" && isset(object.logId)){logIdSource = object.logId.replace('>','<');}

		ConsoleLog( logIdSource, "didChangeCustomSort; data:", data );


		var sessionID = (typeof object.args.sessionID !== "undefined") ? object.args.sessionID : false;
		var sessionParentID = (typeof object.args.sessionParentID !== "undefined") ? object.args.sessionParentID : sessionID;
		var sortObject = (typeof object.args.sortObject === "string") ? object.args.sortObject : false;
		var folderID = (typeof object.args.folderID !== "undefined") ? object.args.folderID : false;
		var notifyUsers = (typeof object.args.notifyUsers !== "undefined") ? object.args.notifyUsers : [];
		if( !sessionID || (sortObject !== "Folders" && sortObject !== "Files") || notifyUsers.length === 0 || (sortObject === "Files" && !folderID) ) {
			ConsoleLog( logIdSource, "didChangeCustomSort; invalid or missing parameters:", object.args );
		}
		var mSession = depositions[ sessionParentID ];
		var attendee;
		if( mSession && mSession instanceof Deposition ) {
			ConsoleLog( logIdSource, "didChangeCustomSort; Session:", mSession.id, "users:", Object.keys( mSession.users ).length );
			for( var n=0; n<notifyUsers.length; ++n ) {
				var notifyUserID = notifyUsers[ n ];
				console.log( "didChangeCustomSort; looking for:", notifyUserID, "as session attendee" );
				attendee = users[ notifyUserID ];
				if( typeof attendee !== "undefined" ) {
					//sIO.to( attendee.uid ).emit( "deposition_uploaded_files", {"depositionID": object.args.depositionID, "folderID": folderID, "folderName": folderName} );
					var userInfo = {
						"sessionID": sessionID,
						"sortObject": sortObject,
						"folderID": folderID
					};
					ConsoleLog( logIdSource, "socket.emit: session_changed_custom_sort", attendee.id, attendee.name, userInfo );
					sIO.to( attendee.uid ).emit( "session_changed_custom_sort", userInfo );
				}
			}
		} else {
			console.log( "didChangeCustomSort; Session not found by ID:", sessionParentID );
		}
		res.end( JSON.stringify( {"success": false} ) );
	} );
}


/**
 * Close Deposition -- purge chat services
 * @param {Integer} depoID
 */
function closeDeposition( depoID ) {
	depoID = parseInt( depoID );
	if( typeof depositions[ depoID ] !== "undefined" && depositions[ depoID ] instanceof Deposition ) {
		ConsoleLog( "closeDeposition; Closing Session:", depoID );
		var _depo = depositions[ depoID ];
		for( var roomID in _depo.rooms ) {
			if( _depo.rooms.hasOwnProperty( roomID ) ) {
				if( typeof rooms[ roomID ] !== "undefined" && rooms[ roomID ] instanceof Room ) {
					ConsoleLog( "closeDeposition; Closing Room:", roomID );
					for( var userID in rooms[ roomID ].users ) {
						if( rooms[ roomID ].users.hasOwnProperty( userID ) ) {
							var _user = users[ userID ];
							if( !isset( _user ) ) {
								ConsoleLog( "closeDeposition; Closing Room:", roomID, "for user:", userID, "-- not found!" );
								continue;
							}
							ConsoleLog( "closeDeposition; Closing Room:", roomID, "for user:", userID );
							if( isset( sockets[ _user.uid ] ) ) {
								sockets[ _user.uid ].leave( roomID );
							}
						}
					}
					redisClient.del( rooms[ roomID ].redisKey, function( err ) {
						if( err ) {
							ConsoleLog( "closeDeposition; Error:", err );
						}
					} );
					delete rooms[ roomID ];
					delete _depo.rooms[ roomID ];
				}
			}
		}
		var transcript = transcripts[ depoID ];
		if( isset( transcript ) ) {
			transcript.abort( true );
			setTimeout( function() {
				redisClient.del( transcript.redisKey, function( err ) {
					if( err ) {
						ConsoleLog( "closeDeposition; Error:", err );
					}
				} );
				delete transcripts[ depoID ];
			}, 300 );
		}

		redisClient.del( depositions[ depoID ].redisKey, function( err ) {
			if( err ) {
				ConsoleLog( "closeDeposition; Error:", err );
			}
		} );
		delete depositions[ depoID ];
	}
}

function logToFile( user, message ) {
	var now = new Date();
	var now_ISO2822 = dateFormat( now, "ddd, dd mmm yyyy HH:MM:ss +0000" );
	var msg = {
		"now": now_ISO2822,
		"userID": (user instanceof User && typeof user.id !== "undefined") ? user.id : 0,
		"sKey": (user instanceof User && typeof user.sKey !== "undefined") ? user.sKey : "-",
		"message": (typeof message !== "string") ? JSON.stringify( message ) : message
	};
	fs.appendFile( config.logFile, JSON.stringify( msg ) + '\n', function( err ) {
		if( err ) {
			ConsoleLog( "logToFile; Error:", err );
		}
	} );
}

function ConsoleLog() {
	var now = new Date();
	var logStr = now.toISOString();
	/*
	if (typeof currentUser != 'undefined'){
		logStr = currentUser+' ';
	}
	*/

	for( var n in arguments ) {
		if( !arguments.hasOwnProperty( n ) ) {
			continue;
		}
		if( arguments[n] && typeof arguments[n] === "object" ) {
			logStr += " " + JSON.stringify( arguments[n] );
		} else if( arguments[n] && typeof arguments[n].toString === "function" ) {
			logStr += " " + arguments[n].toString();
		} else {
			logStr += " " + arguments[n];
		}
	}
	console.log( logStr );
}
module.exports = ConsoleLog;
global.ConsoleLog = ConsoleLog;

function isset( n ) {
	return (typeof n !== "undefined" && n !== null);
}
module.exports = isset;
global.isset = isset;

/**
 * Create Room
 * @param {Integer} depositionID
 * @param {String} roomID
 * @param {Integer} creatorID
 * @param {Object} args
 * @param {Object} userSocket
 */
function createRoom( depositionID, roomID, creatorID, args, userSocket ) {
	ConsoleLog( "createRoom", depositionID, roomID, creatorID, args );
	var _room = new Room( roomID, args.title, creatorID, args.private );
	_room.creatorID = creatorID;
	_room.readOnly = ((typeof args.readOnly !== "undefined") ? args.readOnly : false);
	_room.isChatEnabled = ((typeof args.isChatEnabled !== "undefined") ? args.isChatEnabled : true);
	_room.users[creatorID] = true;
	rooms[roomID] = _room;
	_room.save()
	.then( function() {
		userSocket.join( roomID );
		depositions[depositionID].rooms[roomID] = true;
		depositions[depositionID].save();
	} )
	.catch( function( e ) {
		ConsoleLog( "createRoom; caught:", e.name, e.message );
		console.log( e.stack );
	} );
}

/**
 * Validate Presention Leader (Owner)
 * @param {Integer} currentUserID
 * @param {Integer} depositionID
 * @returns {String|Boolean}
 */
function validatePresentationLeader( currentUserID, depositionID ) {
	var cUser = users[ currentUserID ];
	if( !isset( cUser ) ) {
		return "User not found";
	}
	var userID = parseInt( cUser.id );
	depositionID = parseInt( depositionID );
	if( !(depositionID > 0) ) {
		return "Invalid session ID: " + depositionID;
	}
	if( isset( depositions[ depositionID ] ) ) {
		var _depo = depositions[ depositionID ];
		if( isset( presentations[ depositionID ] ) ) {
			if( userID !== parseInt( _depo.speakerID ) ) {
				return "Unauthorized: you are not the leader";
			}
			if( !isset( _depo.users[userID] ) || !cUser.depositionID || parseInt( cUser.depositionID ) !== parseInt( _depo.id ) ) {
				return "Cannot perform actions for a session you are not attending";
			}
			return true;
		} else {
			return "Unknown presentation";
		}
	}
	return "Invalid session ID: " + depositionID;
}

/**
 * Validate Presention Annotator
 * @param {Integer} currentUserID
 * @param {Integer} depositionID
 * @returns {String|Boolean}
 */
function validatePresentationAnnotator( currentUserID, depositionID ) {
	var cUser = users[ currentUserID ];
	if( !isset( cUser ) ) {
		return "User not found";
	}
	var userID = parseInt( cUser.id );
	depositionID = parseInt( depositionID );
	if( !(depositionID > 0) ) {
		return "Invalid session ID: " + depositionID;
	}
	if( isset( depositions[ depositionID ] ) ) {
		var _depo = depositions[ depositionID ];
		if( isset( presentations[ depositionID ] ) ) {
			if( userID !== presentations[ depositionID ].annotatorID ) {
				return "Unauthorized: you are not the annotator";
			}

			var oID = _depo.ownerID;
			if( !isset( _depo.users[userID]) && !isset(_depo.users[oID]) ){
				ConsoleLog("--- Fixing ownerID not in depo attendance");
				depositions[depositionID].users[oID] = "true";
			}

			if( !isset( _depo.users[userID] ) || !cUser.depositionID || parseInt( cUser.depositionID ) !== parseInt( _depo.id ) ) {
				var errorType = 'userID: '+userID+' / cUser.depoId: '+cUser.depositionID+' / _depoID: '+_depo.id+' -> ';
				if( !isset( _depo.users[userID])){errorType = errorType + "1";}
				if( !cUser.depositionID){errorType = errorType + "2";}
				if( parseInt(cUser.depositionID) !== parseInt(_depo.id)){errorType = errorType + "3";}
				//ConsoleLog(users);
				//ConsoleLog(_depo);
				//var oID = _depo.ownerID;
				//depositions[depositionID].users[oID] = "true";
				return "Cannot perform actions for a session you are not attending! (details) "+errorType;
			}
			//ConsoleLog(users);
			//ConsoleLog(_depo);
			return true;
		} else {
			return "Unknown presentation";
		}
	}
	return "Invalid session ID: " + depositionID;
}

/**
 * Notify Subscribers
 * @param {Array} subscribers
 * @param {String} event
 * @param {Object} eventData
 */
function notifySubscribers( subscribers, event, eventData ) {
	if( !isset( subscribers ) || !Array.isArray( subscribers ) ) {
		return;
	}
	for( var u in subscribers ) {
		if( subscribers.hasOwnProperty( u ) ) {
			var userID = parseInt( subscribers[u] );
			if( isset( users[userID] ) && users[userID] instanceof User ) {
				var _user = users[userID];
				ConsoleLog( "notifySubscribers; user:", userID, "for event:", event );
				sIO.to( _user.uid ).emit( event, eventData );
			} else {
				ConsoleLog( "notifySubscribers; user:", userID, "-- user not found" );
			}
		}
	}
}

/**
 * Initial Annotation
 * @param {Integer} currentUserID
 * @param {Object} args Argument parameters
 * @param {Function} callback
 */
function PresentationInitialAnnotation( currentUserID, args, callback ) {
	ConsoleLog( "PresentationInitialAnnotation:", args );
	var errMsg;
	var _user = users[ currentUserID ];
	if( !isset( _user ) || !_user ) {
		errMsg = "User not found";
		ConsoleLog( "PresentationInitialAnnotation; Error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
	var depositionID = parseInt( args.depositionID );
	var isValidResult = validatePresentationAnnotator( currentUserID, depositionID );
	if( isValidResult === true ) {
		if( args && args.hasOwnProperty( "eventData" ) && args.eventData && args.eventData.hasOwnProperty( "Index" ) && args.eventData.hasOwnProperty( "Page" ) ) {
			if( isset(  presentations[ depositionID ] ) ) {
				var presentation = presentations[ depositionID ];
				var pageIndex = parseInt( args.eventData.Page );
				presentation.initialAnnotation( pageIndex, args.eventData.Index, args.eventData, true, function() {
					ConsoleLog( "presentation.initialAnnotation; onComplete..." );
					syncPresentation( depositionID, function() {
						if( typeof callback === "function" ) {
							callback( {"success":true} );
						}
					} );
				} );
				return;
			} else {
				errMsg = "Unknown presentation";
				ConsoleLog( "PresentationInitialAnnotation; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
		} else {
			errMsg = "Missing event data or index";
			ConsoleLog( "PresentationInitialAnnotation; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	} else {
		errMsg = isValidResult;
		ConsoleLog( "PresentationInitialAnnotation; Error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
}

/**
 * Add Annotation
 * @param {Integer} currentUserID
 * @param {Object} args Argument parameters
 * @param {Function} callback
 */
function PresentationAddAnnotation( currentUserID, args, callback ) {
	ConsoleLog( "PresentationAddAnnotation:", args );
	var errMsg;
	var _user = users[currentUserID];
	if( !isset( _user ) || !_user ) {
		errMsg = "User not found";
		ConsoleLog( "PresentationAddAnnotation; Error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
	var depositionID = parseInt( args.depositionID );
	var isValidResult = validatePresentationAnnotator( currentUserID, depositionID );
	if( isValidResult === true ) {
		if( args && args.hasOwnProperty( "eventData" ) && args.eventData && args.eventData.hasOwnProperty( "Index" ) && args.eventData.hasOwnProperty( "Page" ) ) {
			if( isset( presentations[ depositionID ] ) ) {
				var presentation = presentations[ depositionID ];
				var pageIndex = parseInt( args.eventData.Page );
				presentation.addAnnotation( pageIndex, args.eventData.Index, args.eventData, true, function() {
					ConsoleLog( "presentation.addAnnotation; onComplete..." );
					syncPresentation( depositionID, function() {
						var subscribers = presentation.subscribers.slice( 0 ); // clone
						var indexOfSelf = subscribers.indexOf( currentUserID );
						if( indexOfSelf >= 0 ) {
							subscribers.splice( indexOfSelf, 1 );
						}
						notifySubscribers( subscribers, "addAnnotation", args.eventData );
						if( typeof callback === "function" ) {
							callback( {"success":true, "undoStackDepth":presentation.undoStack.length} );
						}
					} );
				} );
				return;
			} else {
				errMsg = "Unknown presentation";
				ConsoleLog( "PresentationAddAnnotation; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
		} else {
			errMsg = "Missing event data or index";
			ConsoleLog( "PresentationAddAnnotation; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	} else {
		errMsg = isValidResult;
		ConsoleLog( "PresentationAddAnnotation; Error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
}

/**
 * Modify Annotation
 * @param {Integer} currentUserID
 * @param {Object} args Argument parameters
 * @param {Function} callback
 */
function PresentationModifyAnnotation( currentUserID, args, callback ) {
	ConsoleLog( "PresentationModifyAnnotation:", args );
	var errMsg;
	var _user = users[ currentUserID ];
	if( !isset( _user ) || !_user ) {
		errMsg = "User not found";
		ConsoleLog( "PresentationModifyAnnotation; Error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
	var depositionID = parseInt( args.depositionID );
	var isValidResult = validatePresentationAnnotator( currentUserID, depositionID );
	if( isValidResult === true ) {
		if( args && args.hasOwnProperty( "eventData" ) && args.eventData && args.eventData.hasOwnProperty( "Index" ) && args.eventData.hasOwnProperty( "Page" ) ) {
			if( isset( presentations[ depositionID ] ) ) {
				var presentation = presentations[ depositionID ];
				var pageIndex = parseInt( args.eventData.Page );
				var annotationIndex = args.eventData.Index;
				if( typeof presentation.pageAnnotations[ pageIndex ] === "object" && typeof presentation.pageAnnotations[ pageIndex ][ annotationIndex ] === "object" ) {
					presentation.modifyAnnotation( pageIndex, annotationIndex, args.eventData, true, function() {
						ConsoleLog( "presentation.modifyAnnotation; onComplete..." );
						syncPresentation( depositionID, function() {
							var subscribers = presentation.subscribers.slice( 0 ); // clone
							var indexOfSelf = subscribers.indexOf( currentUserID );
							if( indexOfSelf >= 0 ) {
								subscribers.splice( indexOfSelf, 1 );
							}
							notifySubscribers( subscribers, "modifyAnnotation", args.eventData );
							if( typeof callback === "function" ) {
								callback( {"success":true, "undoStackDepth":presentation.undoStack.length} );
							}
						} );
					} );
					return;
				} else {
					errMsg = "Unknown annotation";
					ConsoleLog( "PresentationModifyAnnotation; Error:", errMsg );
					if( typeof callback === "function" ) {
						callback( {"success":false, "error":errMsg} );
					}
					return;
				}
			} else {
				errMsg = "Unknown presentation";
				ConsoleLog( "PresentationModifyAnnotation; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
		} else {
			errMsg = "Missing event data or index";
			ConsoleLog( "PresentationModifyAnnotation; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	} else {
		errMsg = isValidResult;
		ConsoleLog( "PresentationModifyAnnotation; Error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
}

/**
 * Delete Annotation
 * @param {Integer} currentUserID
 * @param {Object} args Argument parameters
 * @param {Function} callback
 */
function PresentationDeleteAnnotation( currentUserID, args, callback ) {
	ConsoleLog( "PresentationDeleteAnnotation:", args );
	var errMsg;
	var _user = users[ currentUserID ];
	if( !isset( _user ) || !_user ) {
		errMsg = "User not found";
		ConsoleLog( "PresentationDeleteAnnotation; Error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
	var depositionID = parseInt( args.depositionID );
	var isValidResult = validatePresentationAnnotator( currentUserID, depositionID );
	if( isValidResult === true ) {
		if( args && args.hasOwnProperty( "eventData" ) && args.eventData && args.eventData.hasOwnProperty( "Index" ) && args.eventData.hasOwnProperty( "Page" ) ) {
			var pageIndex = parseInt( args.eventData.Page );
			var annotationIndex = args.eventData.Index;
			if( isset( presentations[ depositionID ] ) ) {
				var presentation = presentations[ depositionID ];
				if( isset( presentation.pageAnnotations[ pageIndex ] ) && isset( presentation.pageAnnotations[ pageIndex ][ annotationIndex ] ) ) {
					presentation.deleteAnnotation( pageIndex, annotationIndex, args.eventData, true, function() {
						ConsoleLog( "presentation.deleteAnnotation; onComplete..." );
						syncPresentation( depositionID, function() {
							var subscribers = presentation.subscribers.slice( 0 ); // clone
							var indexOfSelf = subscribers.indexOf( currentUserID );
							if( indexOfSelf >= 0 ) {
								subscribers.splice( indexOfSelf, 1 );
							}
							notifySubscribers( subscribers, "deleteAnnotation", args.eventData );
							if( typeof callback === "function" ) {
								callback( {"success":true, "undoStackDepth":presentation.undoStack.length} );
							}
						} );
					} );
					return;
				} else {
					errMsg = "Unknown annotation";
					ConsoleLog( "PresentationDeleteAnnotation; Error:", errMsg );
					if( typeof callback === "function" ) {
						callback( {"success":false, "error":errMsg} );
					}
					return;
				}
			} else {
				errMsg = "Unknown presentation";
				ConsoleLog( "PresentationDeleteAnnotation; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
		} else {
			errMsg = "Missing event data, page or index";
			ConsoleLog( "PresentationDeleteAnnotation; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	} else {
		errMsg = isValidResult;
		ConsoleLog( "PresentationDeleteAnnotation; Error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
}

/**
 * Undo Annotation
 * @param {Integer} currentUserID
 * @param {Object} args Argument parameters
 * @param {Function} callback
 */
function PresentationAnnotationUndo( currentUserID, args, callback ) {
	ConsoleLog( "PresentationAnnotationUndo:", args );
	var errMsg;
	var _user = users[ currentUserID ];
	if( !isset( _user ) || !_user ) {
		errMsg = "User not found";
		ConsoleLog( "PresentationAnnotationUndo; Error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
	var emitAction = null;
	var eventData = {};
	var depositionID = parseInt( args.depositionID );
	var isValidResult = validatePresentationLeader( currentUserID, depositionID );
	if( isValidResult === true ) {
		if( typeof presentations[depositionID] !== "undefined" ) {
			var presentation = presentations[depositionID];
			var lastAction = presentation.undoStack.pop();
			if( typeof lastAction !== "undefined" && lastAction.hasOwnProperty( "event" ) && lastAction.hasOwnProperty( "Page" ) && lastAction.hasOwnProperty( "Index" ) ) {
				eventData = {"Page":lastAction.Page, "Index":lastAction.Index};
				ConsoleLog( "PresentationAnnotationUndo; Last Action:", lastAction );
				var historyRedisKey = presentation.redisKey + ":annotationHistory:" + lastAction.Page + ":" + lastAction.Index;
				var annotationHistory = presentation.annotationHistory[lastAction.Page][lastAction.Index];
				if( lastAction.event === "addAnnotation" ) {
					emitAction = "deleteAnnotation";
					ConsoleLog( "PresentationAnnotationUndo; Undo add = delete" );
					presentation.deleteAnnotation( lastAction.Page, lastAction.Index, {}, false, function() {
						annotationHistory.pop();
						updateAnnotationHistory( savePresentionAndEmit );
					} );
				} else if( lastAction.event === "modifyAnnotation" ) {
					emitAction = "modifyAnnotation";
					presentation.annotationHistory[lastAction.Page][lastAction.Index].pop();
					var history = presentation.annotationHistory[lastAction.Page][lastAction.Index];
					eventData = history[(history.length - 1)];
					ConsoleLog( "PresentationAnnotationUndo; Undo modify = modify:", eventData );
					presentation.modifyAnnotation( lastAction.Page, lastAction.Index, eventData, false, function() {
						updateAnnotationHistory( savePresentionAndEmit );
					} );
				} else if( lastAction.event === "deleteAnnotation" ) {
					emitAction = "addAnnotation";
					var history = presentation.annotationHistory[lastAction.Page][lastAction.Index];
					eventData = history[(history.length - 1)];
					ConsoleLog( "PresentationAnnotationUndo; Undo delete = add:", eventData );
					presentation.addAnnotation( lastAction.Page, lastAction.Index, eventData, false, function() {
						savePresentionAndEmit();
					} );
				}
			} else {
				errMsg = "Unable to undo";
				ConsoleLog( "PresentationAnnotationUndo; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg, "undoStackDepth":presentation.undoStack.length} );
				}
			}
		}
	}

	function savePresentionAndEmit() {
		presentation.save( function() {
			syncPresentation( depositionID, function() {
				if( emitAction !== null ) {
					// send to everyone
					sIO.to( presentation.roomID ).emit( emitAction, eventData );
				}
				if( typeof callback === "function" ) {
					callback( {"success":true, "undoStackDepth":presentation.undoStack.length} );
				}
			} );
		} );
	}

	function updateAnnotationHistory( onComplete ) {
		ConsoleLog( "PresentationAnnotationUndo; updateHistory" );
		redisClient.setex( historyRedisKey, config.redis.expire, JSON.stringify( annotationHistory ), function( err ) {
			if( err ) {
				ConsoleLog( "updateAnnotationHistory; Error:", err );
			}
			if( typeof onComplete === "function" ) {
				onComplete();
			}
		} );
	}
}

/**
 * Clear Annotations
 * @param {Integer} currentUserID
 * @param {Object} args Argument parameters
 * @param {Function} callback
 */
function PresentationClearAnnotations( currentUserID, args, callback ) {
	ConsoleLog( "PresentationClearAnnotations:", args );
	var errMsg;
	var _user = users[ currentUserID ];
	if( !isset( _user ) || !_user ) {
		errMsg = "User not found";
		ConsoleLog( "PresentationClearAnnotations; Error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
	var depositionID = parseInt( args.depositionID );
	var isValidResult = validatePresentationLeader( currentUserID, depositionID );
	if( isValidResult === true ) {
		if( isset( presentations[ depositionID ] ) ) {
			var presentation = presentations[ depositionID ];
			var pageAnnotationIndexes = {};
			for( var page in presentation.pageAnnotations ) {
				if( presentation.pageAnnotations.hasOwnProperty( page ) ) {
					pageAnnotationIndexes[ page ] = Object.keys( presentation.pageAnnotations[ page ] );
				}
			}
			presentation.deleteAllAnnotations();
			var subscribers = presentation.subscribers.slice( 0 ); // clone
			var indexOfSelf = subscribers.indexOf( currentUserID );
			if( indexOfSelf >= 0 ) {
				subscribers.splice( indexOfSelf, 1 );
			}
			notifySubscribers( subscribers, "clearAnnotations", pageAnnotationIndexes );
			if( typeof callback === "function" ) {
				callback( {"success":true, "annotationIndexes":pageAnnotationIndexes, "undoStackDepth":presentation.undoStack.length} );
			}
			return;
		} else {
			errMsg = "Unknown presentation";
			ConsoleLog( "PresentationClearAnnotations; Error:", errMsg );
			if (typeof callback === "function") {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	} else {
		errMsg = isValidResult;
		ConsoleLog( "PresentationClearAnnotations; Error:", errMsg );
		if (typeof callback === "function") {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
}

/**
 * Add Callout
 * @param {Integer} currentUserID
 * @param {Object} args Argument parameters
 * @param {Function} callback
 */
function PresentationAddCallout( currentUserID, args, callback ) {
	ConsoleLog( "PresentationAddCallout:", args );
	var errMsg;
	var _user = users[currentUserID];
	if( typeof _user === "undefined" || !_user ) {
		errMsg = "User not found";
		ConsoleLog( "PresentationAddCallout; Error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
	var depositionID = parseInt( args.depositionID );
	var isValidResult = validatePresentationAnnotator( currentUserID, depositionID );
	if( isValidResult === true ) {
		if( args && args.hasOwnProperty( "eventData" ) && args.eventData && args.eventData.hasOwnProperty( "Index" ) && args.eventData.hasOwnProperty( "Page" ) ) {
			if( typeof presentations[depositionID] !== "undefined" ) {
				var presentation = presentations[depositionID];
				var pageIndex = parseInt( args.eventData.Page );
				errMsg = presentation.addCallout( pageIndex, args.eventData.Index, args.eventData, function() {
					ConsoleLog( "presentation.addCallout; onComplete..." );
					syncPresentation( depositionID, function() {
						var subscribers = presentation.subscribers.slice( 0 ); // clone
						var indexOfSelf = subscribers.indexOf( currentUserID );
						if( indexOfSelf >= 0 ) {
							subscribers.splice( indexOfSelf, 1 );
						}
						notifySubscribers( subscribers, "addCallout", args.eventData );
						if( typeof callback === "function" ) {
							callback( {"success":true} );
						}
					} );
				} );
				if( errMsg ) {
					ConsoleLog( "PresentationAddCallout; Error:", errMsg );
					callback( {"success":false, "error":errMsg} );
				}
				return;
			} else {
				errMsg = "Unknown presentation";
				ConsoleLog( "PresentationAddCallout; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
		} else {
			errMsg = "Missing event data or index";
			ConsoleLog( "PresentationAddCallout; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	} else {
		errMsg = isValidResult;
		ConsoleLog( "PresentationAddCallout; Error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
}

/**
 * Modify Callout
 * @param {Integer} currentUserID
 * @param {Object} args Argument parameters
 * @param {Function} callback
 */
function PresentationModifyCallout( currentUserID, args, callback ) {
	ConsoleLog( "PresentationModifyCallout:", args );
	var errMsg;
	var _user = users[ currentUserID ];
	if( !isset( _user ) || !_user ) {
		errMsg = "User not found";
		ConsoleLog( "PresentationModifyCallout; Error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
	var depositionID = parseInt( args.depositionID );
	var isValidResult = validatePresentationAnnotator( currentUserID, depositionID );
	if( isValidResult === true ) {
		if( args && args.hasOwnProperty( "eventData" ) && args.eventData && args.eventData.hasOwnProperty( "Index" ) && args.eventData.hasOwnProperty( "Page" ) ) {
			if( typeof presentations[depositionID] !== "undefined" ) {
				var presentation = presentations[depositionID];
				var pageIndex = parseInt( args.eventData.Page );
				var calloutIndex = args.eventData.Index;
				if( typeof presentation.pageCallouts[ pageIndex ] === "object" && typeof presentation.pageCallouts[ pageIndex ][ calloutIndex ] === "object" ) {
					errMsg = presentation.modifyCallout( pageIndex, calloutIndex, args.eventData, function() {
						ConsoleLog( "presentation.modifyAnnotation; onComplete" );
						syncPresentation( depositionID, function() {
							var subscribers = presentation.subscribers.slice( 0 ); // clone
							var indexOfSelf = subscribers.indexOf( currentUserID );
							if( indexOfSelf >= 0 ) {
								subscribers.splice( indexOfSelf, 1 );
							}
							notifySubscribers( subscribers, "modifyCallout", args.eventData );
							if( typeof callback === "function" ) {
								callback( {"success":true} );
							}
						} );
					} );
					if( errMsg ) {
						ConsoleLog( "PresentationModifyCallout; Error:", errMsg );
						callback( {"success":false, "error":errMsg} );
					}
					return;
				} else {
					errMsg = "Unknown callout";
					ConsoleLog( "PresentationModifyCallout; Error:", errMsg );
					if( typeof callback === "function" ) {
						callback( {"success":false, "error":errMsg} );
					}
					return;
				}
			} else {
				errMsg = "Unknown presentation";
				ConsoleLog( "PresentationModifyCallout; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
		} else {
			errMsg = "Missing event data or index";
			ConsoleLog( "PresentationModifyCallout; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	} else {
		errMsg = isValidResult;
		ConsoleLog( "PresentationModifyCallout; Error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
}

/**
 * Delete Callout
 * @param {Integer} currentUserID
 * @param {Object} args Argument parameters
 * @param {Function} callback
 */
function PresentationDeleteCallout( currentUserID, args, callback ) {
	ConsoleLog( "PresentationDeleteCallout:", args );
	var errMsg;
	var _user = users[currentUserID];
	if( !isset( _user ) || !_user ) {
		errMsg = "User not found";
		ConsoleLog( "PresentationDeleteCallout; Error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
	var depositionID = parseInt( args.depositionID );
	var isValidResult = validatePresentationAnnotator( currentUserID, depositionID );
	if( isValidResult === true ) {
		if( args && args.hasOwnProperty( "eventData" ) && args.eventData && args.eventData.hasOwnProperty( "Index" ) && args.eventData.hasOwnProperty( "Page" ) ) {
			var pageIndex = parseInt( args.eventData.Page );
			var calloutIndex = args.eventData.Index;
			if( isset( presentations[ depositionID ] ) ) {
				var presentation = presentations[ depositionID ];
				if( isset( presentation.pageCallouts[ pageIndex ] ) && isset( presentation.pageCallouts[ pageIndex ][ calloutIndex ] ) ) {
					errMsg = presentation.deleteCallout( pageIndex, calloutIndex, args.eventData, function() {
						ConsoleLog( "presentation.deleteCallout; onComplete..." );
						syncPresentation( depositionID, function() {
							var subscribers = presentation.subscribers.slice( 0 ); // clone
							var indexOfSelf = subscribers.indexOf( currentUserID );
							if( indexOfSelf >= 0 ) {
								subscribers.splice( indexOfSelf, 1 );
							}
							notifySubscribers( subscribers, "deleteCallout", args.eventData );
							if( typeof callback === "function" ) {
								callback( {"success":true} );
							}
						} );
					} );
					if( errMsg ) {
						ConsoleLog( "PresentationDeleteCallout; Error:", errMsg );
						callback( {"success":false, "error":errMsg} );
					}
					return;
				} else {
					errMsg = "Unknown callout";
					ConsoleLog( "PresentationDeleteCallout; Error:", errMsg );
					if( typeof callback === "function" ) {
						callback( {"success":false, "error":errMsg} );
					}
					return;
				}
			} else {
				errMsg = "Unknown presentation";
				ConsoleLog( "PresentationDeleteCallout; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
		} else {
			errMsg = "Missing event data, page or index";
			ConsoleLog( "PresentationDeleteCallout; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	} else {
		errMsg = isValidResult;
		ConsoleLog( "PresentationDeleteCallout; Error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
}

/**
 * Set Callout(s) Z-Index
 * @param {Integer} currentUserID
 * @param {Object} args Argument parameters
 * @param {Function} callback
 */
function PresentationSetCalloutZIndex( currentUserID, args, callback ) {
	ConsoleLog( "PresentationSetCalloutZIndex:", args );
	var errMsg;
	var _user = users[currentUserID];
	if( !isset( _user ) || !_user ) {
		errMsg = "User not found";
		ConsoleLog( "PresentationSetCalloutZIndex; Error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
	var depositionID = parseInt( args.depositionID );
	var isValidResult = validatePresentationAnnotator( currentUserID, depositionID );
	if( isValidResult === true ) {
		if( args && args.hasOwnProperty( "eventData" ) && args.eventData && args.eventData.hasOwnProperty( "Indices" ) && args.eventData.hasOwnProperty( "Page" ) ) {
			var pageIndex = parseInt( args.eventData.Page );
			if( typeof presentations[depositionID] !== "undefined" ) {
				var presentation = presentations[ depositionID ];
				errMsg = presentation.setCalloutZIndex( pageIndex, args.eventData.Indices, args.eventData, function() {
					ConsoleLog( "presentation.setCalloutZIndex; onComplete" );
					syncPresentation( depositionID, function() {
						var subscribers = presentation.subscribers.slice( 0 ); // clone
						var indexOfSelf = subscribers.indexOf( currentUserID );
						if( indexOfSelf >= 0 ) {
							subscribers.splice( indexOfSelf, 1 );
						}
						notifySubscribers( subscribers, "setCalloutZIndex", args.eventData );
						if( typeof callback === "function" ) {
							callback( {"success":true} );
						}
					} );
				} );
				if( errMsg ) {
					ConsoleLog( "PresentationSetCalloutZIndex; Error:", errMsg );
					callback( {"success":false, "error":errMsg} );
				}
				return;
			} else {
				errMsg = "Unknown presentation";
				ConsoleLog( "PresentationSetCalloutZIndex; Error:", errMsg );
				if( typeof callback === "function" ) {
					callback( {"success":false, "error":errMsg} );
				}
				return;
			}
		} else {
			errMsg = "Missing event data, page or indices";
			ConsoleLog( "PresentationSetCalloutZIndex; Error:", errMsg );
			if( typeof callback === "function" ) {
				callback( {"success":false, "error":errMsg} );
			}
			return;
		}
	} else {
		errMsg = isValidResult;
		ConsoleLog( "PresentationSetCalloutZIndex; Error:", errMsg );
		if( typeof callback === "function" ) {
			callback( {"success":false, "error":errMsg} );
		}
		return;
	}
}

/**
 * Process Presentation Event Queue
 * @param {Presentation} presentation
 */
function presentationProcessEventQueue( presentation ) {
	ConsoleLog( "presentationProcessEventQueue; eventQueue.length:", presentation.eventQueue.length, " lock:", presentation.lock );
	if( presentation.lock ) {
		ConsoleLog( "presentationProcessEventQueue; locked!" );
		return;
	}
	if( presentation.eventQueue.length > 0 ) {
		presentation.lock = true;
		var qItem = presentation.eventQueue.shift();
		ConsoleLog( "presentationProcessEventQueue;", qItem.op );
		switch( qItem.op ) {
			case "addAnnotation":
				PresentationAddAnnotation( qItem.userID, qItem.args, callbackComplete );
				break;
			case "modifyAnnotation":
				PresentationModifyAnnotation( qItem.userID, qItem.args, callbackComplete );
				break;
			case "deleteAnnotation":
				PresentationDeleteAnnotation( qItem.userID, qItem.args, callbackComplete );
				break;
			case "initialAnnotation":
				PresentationInitialAnnotation( qItem.userID, qItem.args, callbackComplete );
				break;
			case "clearAnnotations":
				PresentationClearAnnotations( qItem.userID, qItem.args, callbackComplete );
				break;
			case "annotationUndo":
				PresentationAnnotationUndo( qItem.userID, qItem.args, callbackComplete );
				break;
			case "addCallout":
				PresentationAddCallout( qItem.userID, qItem.args, callbackComplete );
				break;
			case "modifyCallout":
				PresentationModifyCallout( qItem.userID, qItem.args, callbackComplete );
				break;
			case "deleteCallout":
				PresentationDeleteCallout( qItem.userID, qItem.args, callbackComplete );
				break;
			case "setCalloutZIndex":
				PresentationSetCalloutZIndex( qItem.userID, qItem.args, callbackComplete );
				break;
		}
	} else {
		ConsoleLog( "presentationProcessEventQueue; empty!" );
	}

	function callbackComplete( retval ) {
		if( typeof qItem.callback === "function" ) {
			qItem.callback( retval );
		}
		onComplete();
	}

	function onComplete() {
		presentation.lock = false;
		ConsoleLog( "presentationProcessEventQueue; done(", qItem.op, ") -- eventQueue.length:", presentation.eventQueue.length );
		if( presentation.eventQueue.length > 0 ) {
			presentationProcessEventQueue( presentation );
		}
	}
}


/**
 * Parse IPv4
 * @param {String} addr
 * @returns {String}
 */
function parseIPv4( addr ) {
	if( addr.indexOf( ":" ) >= 0 && addr.indexOf( "." ) > 0 ) {
		var segments = addr.split( ":" );
		var ipv4 = segments.pop();
		if( ipv4 ) {
			//ConsoleLog( "parseIPv4:", ipv4 );
			return ipv4;
		}
	}
	return addr;
}

/**
 * verify IP Address
 * @param {String} ra remote ip address
 * @returns {Boolean}
 */
function verifyIP( ra ) {
	//ConsoleLog( "verifyIP:", ra );
	var wl = config.whitelistIPs;
	if( ra.indexOf( ":" ) >= 0 ) {
		var addrs = ra.split( ":" );
		var ipv4 = addrs[addrs.length - 1];
		if( ipv4 ) {
			//ConsoleLog( "verifyIP (IPv4):", ipv4 );
			ra = ipv4;
		}
	}
	var r = ra.split( "." );
	for( var i=0; i<wl.length; ++i ) {
		var a = wl[i].split( "." );
		if( (parseInt( a[0] ) === parseInt( r[0] ) || a[0] === "*")
				&& (parseInt( a[1] ) === parseInt( r[1] ) || a[1] === "*")
				&& (parseInt( a[2] ) === parseInt( r[2] ) || a[2] === "*")
				&& (parseInt( a[3] ) === parseInt( r[3] ) || a[3] === "*") ) {
			return true;
		}
	}
	return false;
}

function sessionIsDemo( sClass ) {
	return (["Demo","WPDemo","Demo Trial"].indexOf( sClass ) >= 0);
}

function sessionIsWitnessPrep( sClass ) {
	return (["WitnessPrep","WPDemo"].indexOf( sClass ) >= 0);
}

function sessionIsTrial( sClass ) {
	return (["Trial","Mediation","Arbitration","Hearing","Demo Trial"].indexOf( sClass ) >= 0);
}

function DBConnection() {
	return dbPool.getConnectionAsync().disposer( function( connection ) {
		try {
			connection.release();
		} catch( e ) {
			ConsoleLog( "[ERROR] releasing db connection:", e.name, e.message );
			console.log( e.stack );
		}
	} );
}

function reloadSession( sessionID ) {
	if( !isset( sessionID ) || !isset( depositions[ sessionID ] ) ) {
		ConsoleLog( "reloadSession; [ERROR] invalid sessionID:", sessionID );
		return;
	}
	if( isset( depositions[ sessionID ] ) && depositions[ sessionID ] instanceof Deposition ) {
		var deposition = depositions[ sessionID ];
		var sessionInfo = null;
		Promise.using( DBConnection(), function( dbConn ) {
			return dbConn.queryAsync( config.database.selectSessionByID, [ sessionID ] );
		} ).then( function( rows ) {
			if( isset( rows ) && rows.length === 1 ) {
				sessionInfo = rows.shift();
				return;
			}
			ConsoleLog( "reloadSession; [ERROR] invalid sessionID:", sessionID );
		} ).catch( function( e ) {
			ConsoleLog( "reloadSession; selectSessionByID caught:", e.name, e.message );
		} ).finally( function() {
			if( isset( sessionInfo ) ) {
				ConsoleLog( "reloadSession; session:", sessionInfo );
				deposition.title = sessionInfo.depositionOf;
				deposition.ownerID = parseInt( sessionInfo.ownerID );
				deposition.speakerID = parseInt( (sessionInfo.speakerID) ? sessionInfo.speakerID : sessionInfo.ownerID );
				deposition.class = sessionInfo.class;
				deposition.statusID = sessionInfo.statusID;
				depositions[ sessionID ] = deposition;
				deposition.save();
			}
		} );
	}
}

var loadingRedisData = false;
function loadRedisData() {
	return new Promise( function( resolve ) {
		if( loadingRedisData === true ) {
			ConsoleLog( "loadRedisData; locked." );
			return resolve();
		}
		ConsoleLog( "loadRedisData; ... loading" );
		loadingRedisData = true;
		redisClient.keysAsync( "deposition:*" ).then( function( sessionKeys ) {
			if( Array.isArray( sessionKeys ) ) {
				var sessionList = sessionKeys;
				for( var keyIndex in sessionKeys ) {
					if( sessionKeys.hasOwnProperty( keyIndex ) ) {
						var redisKey = sessionKeys[keyIndex];
						var sessionID = redisKey.split( ":" )[1];
						syncSession( sessionID, true )
						.then( function( sessionID ) {
							var sessionKey = "deposition:" + sessionID;
							sessionList.splice( sessionList.indexOf( sessionKey ), 1 );
							if( sessionList.length === 0 ) {
								loadingRedisData = false;
								ConsoleLog( "loadRedisData; ... done" );
								return resolve();
							}
						} );
					}
				}
				if( sessionList.length === 0 ) {
					loadingRedisData = false;
					ConsoleLog( "loadRedisData; ... done" );
					return resolve();
				}
			} else {
				loadingRedisData = false;
				ConsoleLog( "loadRedisData; ... done" );
				return resolve();
			}
		} ).catch( function( e ) {
			ConsoleLog( "loadRedisData; caught:", e.name, e.message );
			console.log( e.stack );
		} );
	} );
}

function syncSession( sessionID, recursive ) {
	return new Promise( function( resolve ) {
		ConsoleLog( "syncSession:", sessionID );
		var deposition = isset( depositions[sessionID] ) ? depositions[sessionID] : new Deposition( sessionID );
		redisClient.getAsync( deposition.redisKey ).then( function( response ) {
			if( isset( response ) && typeof response === "string" ) {
				var _deposition = JSON.parse( response );
				if( isset( _deposition ) && typeof _deposition.id !== "undefined" ) {
					deposition.assign( _deposition );
					depositions[sessionID] = deposition;
					if( !!recursive ) {
						syncSessionUsers()
						.then( syncSessionRooms )
						.then( function() { syncPresentation( sessionID ); } )
						.then( function() { resolve( sessionID ); } );
					}
					var hasActiveUsers = false;
					var sessionUsers = Object.keys( deposition.users );
					for( var u=0; u<sessionUsers.length; ++u ) {
						var _userID = sessionUsers[u];
						var isActive = deposition.users[_userID];
						var _user = users[_userID];
						if( !isset( _user ) ) {
							continue;
						}
						//witnesses do not qualify as active
						if( isActive && ["M","G"].indexOf( _user.userType ) >= 0 ) {
							hasActiveUsers = true;
							break;
						}
					}
					if( !hasActiveUsers ) {
						if( isset( transcripts[sessionID] ) ) {
							var transcript = transcripts[sessionID];
							if( transcript.isStreamHost ) {
								ConsoleLog( "syncSession; ending live transcript -- no active attendees in Session ID:", sessionID );
								transcript.reset();
							}
						}
					}
				}
			}
			if( !recursive ) {
				return resolve( sessionID );
			}

			function syncSessionUsers() {
				return new Promise( function( resolve ) {
					var userList = deposition.users;
					for( var userID in deposition.users ) {
						//ConsoleLog( "updateSessionUsers;", userID );
						if( deposition.users.hasOwnProperty( userID ) ) {
							syncUser( userID ).then( function( userID ) {
								delete userList[userID];
								if( Object.keys( userList ).length === 0 ) {
									return resolve();
								}
							} );
						}
					}
					if( Object.keys( userList ).length === 0 ) {
						return resolve();
					}
				} )
				.catch( function( e ) {
					ConsoleLog( "updateSessionUsers; caught:", e.name, e.message );
					console.log( e.stack );
				} );
			}

			function syncSessionRooms() {
				return new Promise( function( resolve ) {
					var roomList = Object.assign( {}, deposition.rooms ); // clone
					for( var roomID in deposition.rooms ) {
						if( deposition.rooms.hasOwnProperty( roomID ) ) {
							syncRoom( roomID )
							.then( syncMessage )
							.then( function( roomID ) {
								delete roomList[roomID];
								if( Object.keys( roomList ).length === 0 ) {
									return resolve();
								}
							} );
						}
					}
					if( Object.keys( roomList ).length === 0 ) {
						return resolve();
					}
				} );
			}
		} )
		.catch( function( e ) {
			ConsoleLog( "syncSession; caught:", e.name, e.message );
			console.log( e.stack );
		} );
	} );
}

function syncUser( userID ) {
	return new Promise( function( resolve ) {
		ConsoleLog( "syncUser:", userID );
		userID = parseInt( userID );
		var user = isset( users[userID] ) ? users[userID] : new User( userID );
		redisClient.getAsync( user.redisKey ).then( function( response ) {
			if( isset( response ) && typeof response === "string" ) {
				var _user = JSON.parse( response );
				if( isset( _user ) && typeof _user.id !== "undefined" ) {
					user.assign( _user );
					users[user.id] = user;
				}
			}
			return resolve( userID );
		} )
		.catch( function( e ) {
			ConsoleLog( "syncUser; caught:", e.name, e.message );
			console.log( e.stack );
		} );
	} );
}

function syncRoom( roomID ) {
	return new Promise( function( resolve ) {
		ConsoleLog( "syncRoom:", roomID );
		var room = isset( rooms[roomID] ) ? rooms[roomID] : new Room( roomID );
		redisClient.getAsync( room.redisKey ).then( function( response ) {
			if( isset( response ) && typeof response === "string" ) {
				var _room = JSON.parse( response );
				if( isset( _room ) && typeof _room.id !== "undefined" ) {
					room.assign( _room );
					rooms[roomID] = room;
				}
			}
			return resolve( roomID );
		} )
		.catch( function( e ) {
			ConsoleLog( "syncRoom; caught:", e.name, e.message );
			console.log( e.stack );
		} );
	} );
}


function syncMessage( roomID ) {
	return new Promise( function( resolve ) {
		ConsoleLog( "syncMessage:", roomID );
		var redisKey = "room:" + roomID + ":messages";
		if( !isset( messages[roomID] ) ) {
			messages[roomID] = [];
		}
		var messageIndex = messages[roomID].length;
		var _roomID = roomID;
		redisClient.lrangeAsync( redisKey, messageIndex, -1 ).then( function( response ) {
			if( isset( response ) ) {
				ConsoleLog( "new messages for:", _roomID, "count:", response.length );
				for( var msgIndex in response ) {
					if( response.hasOwnProperty( msgIndex ) ) {
						var _msg = JSON.parse( response[msgIndex] );
						if( _msg.hasOwnProperty( "roomID" ) ) {
							var message = new Message( _msg.roomID );
							message.assign( _msg );
							messages[_msg.roomID].push( message );
						} else {
							ConsoleLog( "missing roomID for message:", _msg );
						}
					}
				}
				ConsoleLog( "Messages:", _roomID, "count:", messages[_roomID].length );
			}
			return resolve( roomID );
		} )
		.catch( function( e ) {
			ConsoleLog( "syncMessage; caught:", e.name, e.message );
			console.log( e.stack );
		} );
	} );
}

function syncPresentation( sessionID, onComplete ) {
	return new Promise( function( resolve ) {
		ConsoleLog( "syncPresentation:", sessionID );
		var presentation =	(typeof presentations[sessionID] !== "undefined" ) ? presentations[sessionID] : new Presentation( sessionID );
		redisClient.getAsync( presentation.redisKey ).then( function( response ) {
			if( isset( response ) && typeof response === "string" ) {
				var _presentation = JSON.parse( response );
				if( isset(_presentation ) && typeof _presentation.id !== "undefined" ) {
					presentation.assign( _presentation );
					presentations[sessionID] = presentation;
					presentation.reloadCallouts( null, function() {
						presentation.reloadAnnotations( null, onComplete );
					} );
				}
			}
			return resolve();
		} )
		.catch( function( e ) {
			ConsoleLog( "syncPresentation; caught:", e.name, e.message );
			console.log( e.stack );
		} );
	} );
}

function syncPresentationAnnotations( args ) {
	ConsoleLog( 'syncPresentationAnnotations:', args.sessionID, args.pageIndex );
	var sessionID = parseInt( args.sessionID );
	var pageIndex = parseInt( args.pageIndex );
	if( typeof presentations[sessionID] !== 'undefined' && presentations[sessionID] instanceof Presentation ) {
		presentations[sessionID].reloadAnnotations( pageIndex );
	}
}

function syncPresentationCallouts( args ) {
	ConsoleLog( "syncPresentationCallouts:", args.sessionID, args.pageIndex );
	var sessionID = parseInt( args.sessionID );
	var pageIndex = parseInt( args.pageIndex );
	if( typeof presentations[sessionID] !== "undefined" && presentations[sessionID] instanceof Presentation ) {
		presentations[sessionID].reloadCallouts( pageIndex );
	}
}

function syncPresentationEnd( sessionID ) {
	return new Promise( function( resolve, reject ) {
		ConsoleLog( 'syncPresentationEnd:', sessionID );
		if( typeof presentations[sessionID] !== 'undefined' && presentations[sessionID] instanceof Presentation ) {
			delete presentations[sessionID];
			return resolve();
		} else {
			ConsoleLog( 'Presentation not instance of Presentation:', sessionID, presentations[sessionID] );
		}
		return reject();
	} )
	.catch( function( e ) {
		ConsoleLog( "syncPresentationEnd; caught:", e.name, e.message );
		console.log( e.stack );
	} );
}

function syncTranscript( sessionID ) {
	return new Promise( function( resolve ) {
		ConsoleLog( "syncTranscript:", sessionID );
		var transcript = isset( transcripts[sessionID] ) ? transcripts[sessionID] : new Transcript( sessionID );
		redisClient.getAsync( transcript.redisKey )
		.then( function( response ) {
			if( isset( response ) && typeof response === "string" ) {
				var _transcript = JSON.parse( response );
				if( isset( _transcript ) && typeof _transcript.id !== "undefined" ) {
					transcript.assign( _transcript );
					transcripts[sessionID] = transcript;
					transcript.processQueue();
				}
			}
			return resolve();
		} )
		.catch( function( e ) {
			ConsoleLog( "syncTranscript; caught:", e.name, e.message );
			console.log( e.stack );
		} );
	} );
}

function syncCloseDeposition( sessionID ) {
	return new Promise( function( resolve ) {
		ConsoleLog( "syncCloseDeposition:", sessionID );
		if( isset( sessionID ) && isset( depositions[ sessionID ] ) && depositions[ sessionID ] instanceof Deposition ) {
			closeDeposition( sessionID );
		}
		return resolve();
	} );
}

