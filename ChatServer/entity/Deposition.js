/* global redisClient,redisPub,config,nodeId,exports,require */

/**
 * Deposition
 * @param {Integer} id
 * @returns {Deposition}
 */
function Deposition( id ) {
	this.id = parseInt( id );
	this.redisKey = "deposition:" + this.id;
	this.title = "";
	this.rooms = {};
	this.users = {};
	//this.usersParameters = {};
	//this.notAuthorizedUsers = {};
	//this.count = 0;
	this.depoRoomID = id + ".deposition";	//All Attendees
	this.ownerID = null;
	this.speakerID = null;
	this.class = "Deposition";
	this.statusID = "";
	//this.presentationInfo = null;
};

Deposition.prototype.assign = function( entity ) {
//	ConsoleLog( 'Deposition::assign()', this.id );
//	ConsoleLog( 'Deposition::assign():', entity );
	for( var property in entity ) {
		if( entity.hasOwnProperty( property ) ) {
			if( ["transcriptInterval","transcriptStarted"].indexOf( property ) !== -1 ) {
				continue;
			}
			if( this.hasOwnProperty( property ) ) {
//				ConsoleLog( 'Deposition::assign();', property, '=>', entity[property] );
				this[property] = entity[property];
			} else {
				ConsoleLog( "Deposition::assign(); error: unknown property:", property );
			}
		}
	}
};

Deposition.prototype.save = function( onComplete ) {
	var self = this;
	return new Promise( function( resolve, reject ) {
		ConsoleLog( "Deposition::save()", self.id );
		var depositionID = self.id;
		var entity = JSON.stringify( self.bundle() );
		redisClient.setexAsync( self.redisKey, config.redis.expire, entity )
		.then( function() {
			redisPub.publish( "metadata", JSON.stringify( {"nodeId":nodeId, "args":{"event":"Deposition.publish", "args":depositionID}} ) );
			if( typeof onComplete === "function" ) {
				onComplete();
			}
			return resolve();
		} )
		.catch( function( e ) {
			ConsoleLog( "Deposition.save; caught:", e.name, e.message );
			console.log( e.stack );
			return reject();
		} );
	} );
};

Deposition.prototype.set = function( entity )
{
//	console.log( 'Deposition::set():', entity.id );
	for( var property in entity ) {
		if( entity.hasOwnProperty( property ) ) {
			this[property] = entity[property];
		}
	}
	this.count = Object.keys( this.rooms ).length;
};

Deposition.prototype.bundle = function() {
//	ConsoleLog( 'Deposition::bundle()', this.id );
	var exclude = ['redisKey','transcriptInterval','transcriptStarted'];
	var _this = Object.assign( {}, this );
	for( var i in exclude ) {
		if( typeof _this[exclude[i]] !== "undefined" ) {
			delete _this[exclude[i]];
		}
	}
	return _this;
};

Deposition.prototype.createRoom = function( creator, user, args, isSubscribeEvent )
{
	console.log( '====== [Deposition.createRoom] ======' );
	console.log( '-- deprecated --' );
	return;
	console.log( '====== [ROOM CREATOR] ======' );
	console.log( creator.info() );
	console.log( '=======\n\n' );
	console.log( '====== [CURRENT USER] ======' );
	console.log( user.id );
	console.log( '=======\n\n' );
	console.log( '====== [ARGS] ======' );
	console.log( args );
	console.log( '=======\n\n' );

	if( typeof isSubscribeEvent === 'undefined' || !isSubscribeEvent ) {
//JKL - sync
//		redisPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Deposition.createRoom', args:{deposition:this.id, creator:creator.id, user:user.id, args:args}}} ) );
	}

	var Room = require( './Room' ).Room;

	console.log( '=== addRoom' );
	this.addRoom( new Room( args.id, args.title, creator, args.private ) );
	this.rooms[args.id].readOnly = ((typeof args.readOnly !== 'undefined') ? args.readOnly : false);
	this.rooms[args.id].isChatEnabled = ((typeof args.isChatEnabled !== 'undefined') ? args.isChatEnabled : true);
	this.count = Object.keys( this.rooms ).length;
	console.log( '=== getRoom' );
	this.getRoom( args.id ).addUser( user );

	return this.getRoom( args.id ).info( user );
};

Deposition.prototype.addRoom = function( room ) {
	console.log( 'Deposition::addRoom:', room.id );
    this.rooms[room.id] = room;
	this.count = Object.keys( this.rooms ).length;
};

Deposition.prototype.getRoom = function( room_id )
{
    return this.rooms[room_id];
};

Deposition.prototype.closeRoom = function( room_id, isSubscribeEvent )
{
	console.log( 'closeRoom: ' + room_id );
	this.rooms[room_id].close();
	if( typeof isSubscribeEvent === 'undefined' || !isSubscribeEvent ) {
//JKL - sync
//		redisPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Deposition.closeRoom', args:{deposition:this.id, room:room_id}}} ) );
	}
	if( typeof user !== 'undefined' && user.socket ) {
		user.socket.broadcast.to( room_id ).emit( 'room_closed', {room:{id:this.id}} );
	}
	delete this.rooms[room_id];
};

Deposition.prototype.addUser = function( user, userRole, isSubscribeEvent )
{
	console.log( "\n=====\nDeposition.addUser()\n", user.info(), '\nuserRole: ' + userRole );
	if( !userRole || typeof userRole === 'undefined' ) userRole = 0;
    this.users[user.id] = user;

//	if ( this.usersParameters[user.id] )
//		this.usersParameters[user.id].userRole = userRole;
//	else
//		this.usersParameters[user.id] = {userRole:userRole};

	if( typeof isSubscribeEvent === 'undefined' || !isSubscribeEvent ) {
//JKL - sync
//		redisPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Deposition.addUser', args:{deposition:this.id, user:user.info(), userRole:userRole}}} ) );
	}

    user.addDeposition( this );
	if( user.socket ) {
		user.socket.join( this.depoRoomID );
		user.socket.broadcast.to( this.depoRoomID ).emit( 'user_did_connect_to_deposition', {deposition:{id:this.id}, user:user.info()} );
	}
	console.log( "=====\n" );
};

Deposition.prototype.removeUser = function(user, isSubscribeEvent )
{
	console.log( "\n=====\nDeposition.removeUser()\n", user.info() );
    delete this.users[user.id];
   
	if( typeof isSubscribeEvent === 'undefined' || !isSubscribeEvent ) {
//JKL - sync
//		redisPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Deposition.removeUser', args:{deposition:this.id, user:user.id}}} ) );
	}
	if( typeof user.socket !== 'undefined' ) {
		user.socket.leave( this.depoRoomID );
		var dUser = user.info();
		dUser.active = false;
		console.log( "broadcast.to( '" + this.depoRoomID + "' ).emit( 'user_did_disconnect_from_deposition' )" );
		user.socket.broadcast.to( this.depoRoomID ).emit( 'user_did_disconnect_from_deposition', {deposition:{id:this.id}, user:dUser} );
	}
    user.deposition = null;
	console.log( "=====\n" );
};

Deposition.prototype.containsUser = function( user )
{
	console.log( '\n--- containsUser: ' + user.uid );
	//console.log( 'this.users[]: ' + ((typeof this.users[user.id]) ? 'exists' : 'undefined' ) );
	//console.log( 'deposition: ' + ((typeof this.users[user.id] !== 'undefined' && this.users[user.id].hasOwnProperty( 'deposition' ) ) ? this.users[user.id].deposition : 'undefined' ) );
	//console.log( 'active: ' + ((typeof this.users[user.id] !== 'undefined' && this.users[user.id].hasOwnProperty( 'active' ) ) ? this.users[user.id].active : 'false' ) );
	if( typeof this.users[user.id] === 'undefined' || ( !this.users[user.id].deposition && !this.users[user.id].active ) ) {
		console.log( '\n--- containsUser: false' );
		return false;
	}
	console.log( '\n--- containsUser: true' );
    return true;
};

Deposition.prototype.addUnauthorizedUser = function( user, isSubscribeEvent )
{
	this.notAuthorizedUsers[user.id] = user;
	user.deposition = this;
	if( typeof isSubscribeEvent === 'undefined' || !isSubscribeEvent ) {
//JKL - sync
//		redisPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Deposition.addUnauthorizedUser', args:{deposition:this.id, user:user.info()}}} ) );
	}
};

Deposition.prototype.setPresentationInfo = function( info, isSubscribeEvent )
{
	this.presentationInfo = info;
	if( typeof isSubscribeEvent === 'undefined' || !isSubscribeEvent ) {
//JKL - sync
//		redisPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Deposition.setPresentationInfo', args:{deposition:this.id, info:info}}} ) );
	}
};

Deposition.prototype.updatePresentationSubscribers = function( subscribers, isSubscribeEvent )
{
	this.presentationInfo.subscribers = subscribers;
	if( typeof isSubscribeEvent === 'undefined' || !isSubscribeEvent ) {
//JKL - sync
//		redisPub.publish( 'metadata', JSON.stringify( {nodeId:nodeId, args:{event:'Deposition.updatePresentationSubscribers', args:{deposition:this.id, subscribers:subscribers}}} ) );
	}
};


Deposition.prototype.info = function( forUser )
{
//    var users = [];
//    var _users = this.users;
//    for ( var user in _users )
//	{
//        if( _users.hasOwnProperty( user ) )
//		{
//            users.push( _users[user].info() );
//        }
//    }

//	var rooms = [];
//	var _rooms = this.rooms;
//	for( var room in _rooms )
//	{
//        if( _rooms.hasOwnProperty( room ) )
//		{
//			if( typeof forUser !== 'undefined' )
//			{
//				//is attendee?
////				if( typeof _rooms[room].users[forUser.id] === 'undefined' )
//				if( typeof users[forUser.id] === 'undefined' )
//				{
//					//console.log( 'user: ' + forUser.id + ' is not an attendee of: (' + _rooms[room].id + ') ' + _rooms[room].title );
//					continue;
//				} else {
//					if( _rooms[room].readOnly && !_rooms[room].isChatEnabled && forUser.id !== _rooms[room].creator.id )
//					{
//						//console.log( 'user: ' + forUser.id + ' is not creator of: (' + _rooms[room].id + ') ' + _rooms[room].title );
//						continue;
//					}
//				}
//				rooms.push( _rooms[room].info( forUser ) );
//				continue;
//			}
//            rooms.push( _rooms[room].info() );
//        }
//    }

    return ({
        id:this.id,
        title:this.title,
		ownerID:this.ownerID,
		speakerID:this.speakerID,
		statusID:this.statusID,
		users:this.users,
		rooms:this.rooms,
		userCount:Object.keys( this.users ).length,
//		users:users,
//		usersParameters:this.usersParameters,
		roomCount:Object.keys( this.rooms ).length
//		rooms:rooms,
//		presentationInfo:this.presentationInfo
	});
};

exports.Deposition = Deposition;
