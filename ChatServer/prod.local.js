/**
 *  * Configuration
 *   */

var config = require( "./config.js" );

config.https.port = 9443;

config.database.database = "edepo_prod";
config.database.user = "edepo";
config.database.password = "Gr4nt3d!";
config.database.host = "ed-mysql-1.cd2jroqwwfdt.us-east-1.rds.amazonaws.com";
config.database.port = 3306;

config.whitelistIPs = [
	"127.0.0.1",
	"10.0.25.*",
	"172.31.*.*",
	"172.31.0.*",
	"172.31.16.*",
	"54.86.54.70",
	"54.86.76.143",
	"190.*.*.*",
	"69.169.138.228"
];

config.redis.host = "edcache.o6zcmy.ng.0001.use1.cache.amazonaws.com";

config.courtroomConnect.remoteCounselAPI.token = "66699e8d-6b76-42c0-a15d-9c382f4f6c86";

module.exports = config;

