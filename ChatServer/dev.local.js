/**
 * Configuration
 */

var config = require( "./config.js" );

config.https.port = 8443;

config.database.database = "eDepozeDevJL";
config.database.user = "edepo";
config.database.password = "edepoze";
config.database.host = "10.0.25.48";
config.database.port = 13306;
config.database.connectionLimit = 100;

config.socketIO.transports = ["polling","websocket"];

config.redis.expire = 31536000;	// 1 year-ish in seconds (86400*365)

config.courtroomConnect.remoteCounselAPI.options.host = "staging.remotecounsel.com";
config.courtroomConnect.liveStreamAPI.options.host = "staging.text.speche.com";
config.courtroomConnect.liveTranscriptsAPI.options.host = "staging.speche.com";
config.courtroomConnect.remoteCounselAPI.token = "8u5eyp8c51ePA5zd351U0Y7st325o4DI";
config.courtroomConnect.client = "dev.edepoze.com@69.169.138.209";

module.exports = config;
